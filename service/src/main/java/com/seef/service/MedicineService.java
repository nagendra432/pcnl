package com.seef.service;

import java.util.List;
import java.util.Map;

import com.seef.models.ApplicationUser;
import com.seef.models.OutPatient;
import com.seef.models.pharmacy.Medicine;


public interface MedicineService {
	
	public void saveMedicine(Medicine medicine,ApplicationUser applicationUser);
	public void saveOrUpdateMedicine(Medicine medicine,ApplicationUser applicationUser);
	public Boolean validateUnique(Medicine medicine, Map<String, Object> map);
	public List<Medicine> medicine_list();
	public Medicine getById(Medicine medicine);
	public Medicine getById(Long id);
}
