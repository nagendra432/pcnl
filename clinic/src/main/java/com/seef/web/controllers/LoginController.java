package com.seef.web.controllers;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.AuthorizingRealm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.seef.dao.GenericDAO;
import com.seef.exception.BlockedException;
import com.seef.exception.InactiveException;
import com.seef.exception.LockedException;
import com.seef.models.ApplicationUser;
import com.seef.models.Preferences;
import com.seef.models.SecurityQuestion;
import com.seef.models.base.Status;
import com.seef.models.dto.UserSession;
import com.seef.models.enums.UserType;
import com.seef.models.menu.dto.MenuList;
import com.seef.service.AuthenticationService;
import com.seef.service.SystemUserService;
import com.seef.service.menu.MenuBuilder;

@Controller
public class LoginController {

	private static final Logger _log = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	AuthenticationService  authenticationService;
	
	
	@Autowired
	private MenuBuilder menuBuilder;

	@Autowired
	MessageSource messageSource;

	
	@Autowired
	@Qualifier("systemUserAuthRealm")
	private AuthorizingRealm authorizingRealm;

	@Autowired
	SystemUserService systemUserService;
	
	@Autowired
	private GenericDAO genericDAO;
	
	@RequestMapping(value = "/login.htm", method = RequestMethod.GET)
	public String login(HttpServletRequest request,ModelMap modelMap) {
		   Locale locale = request.getLocale();
		   String localeCode=locale.getLanguage();
		   Locale locale1=new Locale(localeCode);
			modelMap.addAttribute("username",messageSource.getMessage("label.admin.username",null, locale1));
			modelMap.addAttribute("password",messageSource.getMessage("label.password",null, locale1));
			modelMap.addAttribute("logintoyouraccount",messageSource.getMessage("label.Logintoyouraccount",null, locale1));
			modelMap.addAttribute("login",messageSource.getMessage("label.login",null, locale1));
			modelMap.addAttribute("forgotyourpassword",messageSource.getMessage("label.Forgotyourpassword",null, locale1));
			modelMap.addAttribute("noworriesclick",messageSource.getMessage("label.noworriesclick",null, locale1));
			modelMap.addAttribute("here",messageSource.getMessage("label.here",null, locale1));
			modelMap.addAttribute("toresetyourpassword",messageSource.getMessage("label.toresetyourpassword",null, locale1));
			modelMap.addAttribute("ForgetPassword",messageSource.getMessage("label.ForgetPassword",null, locale1));
			modelMap.addAttribute("Back",messageSource.getMessage("label.Back",null, locale1));
			modelMap.addAttribute("MbankingAdminProtal",messageSource.getMessage("label.MbankingAdminProtal",null, locale1));
			modelMap.addAttribute("EnteryourUserNamebelowtoresetyourpassword",messageSource.getMessage("label.EnteryourUserNamebelowtoresetyourpassword",null, locale1));
			modelMap.addAttribute("submit",messageSource.getMessage("button.submit",null, locale1));
			modelMap.addAttribute("question1",messageSource.getMessage("label.admin.question1",null, locale1));
			modelMap.addAttribute("question2",messageSource.getMessage("label.admin.question2",null, locale1));
			modelMap.addAttribute("secutityform",messageSource.getMessage("label.SECURITYFROM",null, locale1));
			modelMap.addAttribute("error.msg.authexcp",messageSource.getMessage("error.msg.authexcp",null, locale1));
			modelMap.addAttribute("error.msg.locked",messageSource.getMessage("error.msg.blocked",null, locale1));
			modelMap.addAttribute("error.msg.blocked",messageSource.getMessage("error.msg.inactive",null, locale1));
			modelMap.addAttribute("error.msg.inactive",messageSource.getMessage("error.msg.invalidaccess",null, locale1));
			modelMap.addAttribute("error.msg.invalidaccess",messageSource.getMessage("error.msg.invalidcreds",null, locale1));
			modelMap.addAttribute("error.msg.locked",messageSource.getMessage("error.msg.locked",null, locale1));

			

			
		    return "login";
	}

	
	@RequestMapping(value = "/loginhomepage.htm", method = RequestMethod.GET)
	public String loginhomepage(ModelMap modelMap,HttpServletRequest request) {
		request.getSession().removeAttribute("nav");
		request.getSession().removeAttribute("innerNav");
		return "tileDefinition.home";
	}

	@RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	public String processRequest(@RequestParam("userName") String username, @RequestParam("password") String password, HttpServletRequest request, HttpServletResponse response,
			Locale locale, Model model,ModelMap modelMap) {
		_log.info(">>> {"+username+"}");
		
		   Locale locale1 = request.getLocale();
		   String localeCode=locale1.getLanguage();
			_log.info(">>>localeCode<<<<< {"+localeCode+"}");

		request.getSession().removeAttribute("nav");
		request.getSession().removeAttribute("innerNav");
		boolean flag = false;
		locale = new Locale(localeCode);
		modelMap.addAttribute("logintoyouraccount",messageSource.getMessage("label.Logintoyouraccount",null, locale));
		modelMap.addAttribute("username",messageSource.getMessage("label.admin.username",null, locale));
		modelMap.addAttribute("password",messageSource.getMessage("label.password",null, locale));
		modelMap.addAttribute("logintoyouraccount",messageSource.getMessage("label.Logintoyouraccount",null, locale));
		modelMap.addAttribute("login",messageSource.getMessage("label.login",null, locale));
		modelMap.addAttribute("forgotyourpassword",messageSource.getMessage("label.Forgotyourpassword",null, locale));
		modelMap.addAttribute("noworriesclick",messageSource.getMessage("label.noworriesclick",null, locale));
		modelMap.addAttribute("here",messageSource.getMessage("label.here",null, locale));
		modelMap.addAttribute("toresetyourpassword",messageSource.getMessage("label.toresetyourpassword",null, locale));
		modelMap.addAttribute("ForgetPassword",messageSource.getMessage("label.ForgetPassword",null, locale));
		modelMap.addAttribute("Back",messageSource.getMessage("label.Back",null, locale));
		modelMap.addAttribute("MbankingAdminProtal",messageSource.getMessage("label.MbankingAdminProtal",null, locale));
		modelMap.addAttribute("EnteryourUserNamebelowtoresetyourpassword",messageSource.getMessage("label.EnteryourUserNamebelowtoresetyourpassword",null, locale));
		modelMap.addAttribute("submit",messageSource.getMessage("button.submit",null, locale));
		modelMap.addAttribute("question1",messageSource.getMessage("label.admin.question1",null, locale));
		modelMap.addAttribute("question2",messageSource.getMessage("label.admin.question2",null, locale));
		modelMap.addAttribute("secutityform",messageSource.getMessage("label.SECURITYFROM",null, locale));
		modelMap.addAttribute("error.msg.authexcp",messageSource.getMessage("error.msg.authexcp",null, locale));
		modelMap.addAttribute("error.msg.locked",messageSource.getMessage("error.msg.blocked",null, locale));
		modelMap.addAttribute("error.msg.blocked",messageSource.getMessage("error.msg.inactive",null, locale));
		modelMap.addAttribute("error.msg.inactive",messageSource.getMessage("error.msg.invalidaccess",null, locale));
		modelMap.addAttribute("error.msg.invalidaccess",messageSource.getMessage("error.msg.invalidcreds",null, locale));
		modelMap.addAttribute("error.msg.locked",messageSource.getMessage("error.msg.locked",null, locale));
		modelMap.addAttribute("logintoyouraccount",messageSource.getMessage("label.Logintoyouraccount",null, locale));
		
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("MyRecentOnlineActivities",messageSource.getMessage("label.MyRecentOnlineActivities",null, locale));
		map.put("PreviousSiteVisit",messageSource.getMessage("label.PreviousSiteVisit",null, locale));
		map.put("LastFailedLoginAttempt",messageSource.getMessage("label.LastFailedLoginAttempt",null, locale));
		map.put("LastLoginPasswordChange",messageSource.getMessage("label.LastLoginPasswordChange",null, locale));
		
		_log.info("modelMap  ======>"+map);
        request.getSession().setAttribute("popuplabels", map);
		try {
			
			SecurityUtils.getSubject().login(new UsernamePasswordToken(username.toUpperCase(), password));
			setSessionLocale(request);
			flag = true;
		} catch (LockedException lockedException) {
			_log.error(">>> message:{"+lockedException.getMessage()+"}");
			request.setAttribute("infoMessage", messageSource.getMessage(lockedException.getMessage(), null, locale));
			lockedException.printStackTrace();			
			loginfailed(username);
			return "tileDefinition.login";
		} catch (BlockedException blockedException) {
			_log.error(">>> message:{"+blockedException.getMessage()+"}");
			request.setAttribute("infoMessage", messageSource.getMessage(blockedException.getMessage(), null, locale));
			blockedException.printStackTrace();
			return "tileDefinition.login";
		} catch (InactiveException inactiveException) {
			_log.error(">>> message:{"+inactiveException.getMessage()+"}");
			request.setAttribute("infoMessage", messageSource.getMessage(inactiveException.getMessage(), null, locale));
			inactiveException.printStackTrace();
			loginfailed(username);
			return "tileDefinition.login";
		} catch (IncorrectCredentialsException incorrectCredentialsException) {
			_log.error(">>> message:{"+incorrectCredentialsException.getMessage()+"}");
			incorrectCredentialsException.printStackTrace();
			loginfailed(username);
			@SuppressWarnings("unused")
			ApplicationUser systemUser = systemUserService.findByUserName(username.toUpperCase());
	
			return "tileDefinition.login";

		} catch (AuthenticationException authenticationException) {			
			_log.error(">>> message:{"+authenticationException.getMessage()+"}");
			request.setAttribute("infoMessage", messageSource.getMessage("error.msg.authexcp", new Object[]{username}, locale));
			authenticationException.printStackTrace();
			loginfailed(username);
			return "tileDefinition.login";
		}		

		if (flag == true) {
			ApplicationUser applicationUser = ((UserSession) SecurityUtils.getSubject().getPrincipal()).getApplicationUser();
			UserType userType = applicationUser.getUserType();
			locale = new Locale(applicationUser.getPreferences().getLanguage().getCode().split("_")[0]);
			
			/*if (!(userType.equals(UserType.ADMIN)||!(userType.equals(UserType.SCRUBNURSE)) || userType.equals(UserType.SUPER_ADMIN)|| userType.equals(UserType.BRANCH_TELLER))){
				request.setAttribute("infoMessage", messageSource.getMessage("error.msg.invalidaccess", null, locale));
				return "tileDefinition.login";
			}*/
			if(null!=applicationUser)
			if(systemUserService.findUserStatus(applicationUser.getId()).equals(Status.NEW.toString())){
				modelMap.addAttribute("model",applicationUser);
				modelMap.addAttribute("questions",genericDAO.findAll(SecurityQuestion.class));
				return "tileDefinition.securityquestions";
			}
	
			request.getSession().setAttribute("logedinuserimagename", applicationUser.getUserImageName());
		    _log.info(">>>>loged in user pref lang code <<<<<<<"+applicationUser.getPreferences().getLanguage().getCode());
			request.getSession().setAttribute("applicationUser", applicationUser);
			Timestamp currTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
			authenticationService.updateUserLastLoginTime(applicationUser, currTime);

			
			
					buildMenu(request, "adminMenuConfig.xml");
					@SuppressWarnings("unused")
					String logoName = null;
					applicationUser.setLoginAttempts(0);
					if (applicationUser.getUserType().equals(UserType.SUPER_ADMIN)){
						logoName = "setupadmin.png";
					}
					
					Preferences preferences=applicationUser.getPreferences();
					if(preferences!=null){
						request.getSession().setAttribute("themeName", preferences.getTheme().getName());
					}else{
						request.getSession().setAttribute("themeName","default");
					}
					
					modelMap.remove("dateexpired");
					 if(userType.equals(UserType.SUPER_ADMIN)){
						
						return "redirect:/outpatient/outptsearch.htm";
					}
					else
					{
						return "redirect:/outpatient/outptsearch.htm";
					}
			

		}
		return "tileDefinition.login";
	}

	public void loginfailed(String username){
		Timestamp currTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
		ApplicationUser systemUser = systemUserService.findByUserName(username.toUpperCase());
		authenticationService.updateUserLastFailedLogin(systemUser, currTime);
	}

	private void setSessionLocale(HttpServletRequest request) {
		UserSession userSession = (UserSession) SecurityUtils.getSubject().getPrincipal();
		if(null !=userSession.getPreferences()){
			String localeCode=userSession.getPreferences().getLanguage().getCode();
			request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale(getLanguage(localeCode),getCountry(localeCode)));
		}
	}

	private String getCountry(String localeCode) {
		return localeCode.split("_")[1];
	}

	private String getLanguage(String localeCode) {
		return localeCode.split("_")[0];
	}

	private void buildMenu(HttpServletRequest httpServletRequest, String configFile) {
		initializeAuthorizationCache();
		Collection<String> permissions = authorizingRealm.getAuthorizationCache().get(SecurityUtils.getSubject().getPrincipals()).getStringPermissions();
		menuBuilder.setMenuConfigFile(configFile);
		MenuList menuList = menuBuilder.buildAdminMenu(permissions);
		SecurityUtils.getSubject().getSession().setAttribute("MENULIST", menuList);
		httpServletRequest.setAttribute("MENULIST", SecurityUtils.getSubject().getSession().getAttribute("MENULIST"));
	}

	
	private void initializeAuthorizationCache(){
		try{
			authorizingRealm.checkPermission(SecurityUtils.getSubject().getPrincipals(), "cust:view");
		}catch(Exception exception){
		}
	}

	public AuthorizingRealm getAuthorizingRealm() {
		return authorizingRealm;

	}
	public void setAuthorizingRealm(AuthorizingRealm authorizingRealm) {
		this.authorizingRealm = authorizingRealm;
	}
	
	
}
