<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="datatables"
	uri="http://github.com/dandelion/datatables"%>


<spring:message code="label.view" var="lblView"/>
<spring:message code="label.edit" var="lblEdit"/>
<spring:message code="label.action" var="lblaction"/>
<spring:message code="label.takeappointment" var="lbltakeappointment"/>
<spring:message code="label.completed" var="lblCompleted"/>
<spring:message code="label.rejected" var="lblRejected"/>
<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.name" var="lblname"/>
<spring:message code="label.phneno" var="lblphneno"/>
<spring:message code="label.appointmentdate" var="lblappointmentdate"/>
<spring:message code="label.admin.status" var="lblstatus"/>


<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<spring:message code="header.appointment.manage" />
			
		</h3>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
	<div class="col-md-12">

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-edit"></i>
					<spring:message code="label.resultsfor" />
					
					<spring:message code="header.appointment.manage" />
					
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					 <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="col-lg-2 pull-right">
					<div class="table-toolbar pull-right">
						<div class="btn-group">
							<a href="create.htm?id=${sessionScope.outptid}"><button id="sample_editable_1_new"
									class="btn green">
									<spring:message code="button.addnew" />
									<i class="icon-plus"></i>
								</button></a>
						</div>

					</div>
				</div>
				<datatables:table id="sample_editable_1" data="${modelList}" cdn="false" row="appointment" cssClass="table table-striped table-bordered table-hover">
					<datatables:column title="${lblmrno}" property="outPatient.mrno" />
					<datatables:column title="${lblname}" property="outPatient.firstName"
						cssClass="hidden-480" cssCellClass="hidden-480" />	
				   <datatables:column title="${lblphneno}" property="outPatient.phoneNo" />
				    <datatables:column title="${lblappointmentdate}" property="appointmentDate" />
				    <datatables:column title="${lblstatus}" property="status"  />
				   <%--  <datatables:column title="${lblaction}" sortable="false">
						<div class='hidden-phone visible-desktop btn-group'>
							<a href="close.htm?id=${appointment.id }" data-rel="tooltip"
								title="${lblCompleted}"><i
								class="btn btn-default btn-sm my-table_btn icon-ok-sign"></i></a>
								<a href="rejected.htm?id=${appointment.id }" data-rel="tooltip"
								title="${lblRejected}"><i
								class="btn btn-default btn-sm my-table_btn icon-remove-sign"></i></a>
								</div>
								</datatables:column> --%>
				    
				</datatables:table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->