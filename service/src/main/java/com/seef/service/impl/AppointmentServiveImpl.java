package com.seef.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.Appointment;
import com.seef.models.base.Status;
import com.seef.models.doctor.DoctorSchedule;
import com.seef.service.AppointmentService;

@Service
public class AppointmentServiveImpl implements AppointmentService {
	
private static final Logger _log = LoggerFactory.getLogger(AppointmentServiveImpl.class);
	
	@Autowired
	GenericDAO genericDAO;

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveOrUpdateAppointment(Appointment appointment,
			ApplicationUser applicationUser) {
		// TODO Auto-generated method stub
		appointment.setModifiedBy(applicationUser);
		appointment.setModifiedOn(Calendar.getInstance().getTime());
		genericDAO.saveOrUpdate(appointment);
		_log.info(">>>>>>> Updated Successfully");
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveAppointment(Appointment appointment,
			ApplicationUser applicationUser) {
		// TODO Auto-generated method stub
		appointment.setStatus(Status.NEW);
		appointment.setCreatedBy(applicationUser);
		appointment.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(appointment);
		_log.info(">>>>>>> Save Successfully");
		
	}

	@Override
	public List<Appointment> Appointment_list(Map<String,Object> criteria) {
	return	genericDAO.findByNamedQuery("listbystatus", criteria);
		
	
	}

	@Override
	public Appointment getById(Long id) {
		// TODO Auto-generated method stub
		return genericDAO.getById(Appointment.class, id);
		
	}

	@Override
	public List<Appointment> Appointment_byDate(Map<String, Object> criteria) {
		// TODO Auto-generated method stub
		return genericDAO.findByNamedQuery("listbydate", criteria);
	}

	@Override
	public int sameDayAppointmentCheck(Map<String, Object> criteria) {
		// TODO Auto-generated method stub
		return genericDAO.count("samedayappointment", criteria);
	}

	@Override
	@Transactional(readOnly=true)
	public List<DoctorSchedule> Doctors_listbyDate(Map<String, Object> criteria) {
		// TODO Auto-generated method stub
		return genericDAO.findByNamedQuery("doctorslistbydate", criteria);
	}

	@Override
	public List<Appointment> DoctorsSchedule_listbyDate(
			Map<String, Object> criteria) {
		// TODO Auto-generated method stub
		return genericDAO.findByNamedQuery("doctorsschedulelistbydate", criteria);
	}

	
	
	

}
