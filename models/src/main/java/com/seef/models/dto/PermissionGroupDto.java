package com.seef.models.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.seef.models.master.Permission;


public class PermissionGroupDto implements Serializable{
		
	private static final long serialVersionUID = -1369643807897654809L;
	
	String roleId;	
	List<Permission> permisions;
	Map<Long, String> institutionPermissionMap;
	
	public PermissionGroupDto(){
		
	}

	public Map<Long, String> getInstitutionPermissionMap() {
		return institutionPermissionMap;
	}

	public void setInstitutionPermissionMap(
			Map<Long, String> institutionPermissionMap) {
		this.institutionPermissionMap = institutionPermissionMap;
	}

	public List<Permission> getPermisions() {
		return permisions;
	}

	public void setPermisions(List<Permission> permisions) {
		this.permisions = permisions;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
}
