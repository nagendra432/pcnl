package com.seef.models.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name = "CURRENCY")
public class Currency extends AbstractDomainObject{

	private static final long serialVersionUID = 4331318659389549080L;
	
	private Long id;	
	private String name;	
	private String isoCode;	
	private String symbol;	
	private int isoCodeNumeric;	
	private Integer scale;
	private Integer precision;	
	private String pattern;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "CURRENCY_SEQ")
	@SequenceGenerator(name = "CURRENCY_SEQ", sequenceName = "CURRENCY_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "PATTERN", length = 40, nullable = false)
	public String getPattern() {
		return pattern;
	}

	@Column(name = "NAME", length = 40, nullable = false)
	public String getName() {
		return name;
	}
	
	@Column(name = "ISO_CODE", length = 20, nullable = false)
	public String getIsoCode() {
		return isoCode;
	}
	
	@Column(name = "SYMBOL", length = 20, nullable = false)
	public String getSymbol() {
		return symbol;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	
	@Column(name = "SCALE")
	public Integer getScale() {
		return scale;
	}
	
	public void setScale(Integer scale) {
		this.scale = scale;
	}
	
	@Column(name = "ISO_CODE_NUMERIC", length = 20, nullable = false)
	public int getIsoCodeNumeric() {
		return isoCodeNumeric;
	}
	
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	
	public void setIsoCodeNumeric(int isoCodeNumeric) {
		this.isoCodeNumeric = isoCodeNumeric;
	}

	@Column(name = "PRECISION1")
	public Integer getPrecision() {
		return precision;
	}

	public void setPrecision(Integer precision) {
		this.precision = precision;
	}
	public Currency()
	{
		
	}
	
}
