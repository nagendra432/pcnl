package com.seef.web.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.models.pharmacy.Supplier;
import com.seef.service.BaseService;
import com.seef.service.SupplierService;

@Controller
@RequestMapping("/supplier")

public class SupplierController extends CRUDController<Supplier>{
	
	private static final Logger _log = LoggerFactory.getLogger(SupplierController.class);
	
	
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	MessageSource messageSource;
	
	
	
	@Override
	@RequestMapping(value="/create.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request) {
		setupCreate(modelMap,request);
		return "tileDefinition.create-"+getNameSpace();
	}
	
	@Override
	@RequestMapping(value="/view.htm",method=RequestMethod.GET)
	public String view(ModelMap modelMap,HttpServletRequest request){
		Long id = Long.valueOf(request.getParameter("id"));
		_log.info(">>>>> id is:"+id);
		Supplier supplier=supplierService.getById(id);
		modelMap.addAttribute("model", supplier);
		setupCreate(modelMap, request);
		modelMap.addAttribute("disabledStatus", "true");
		return "tileDefinition.view-"+getNameSpace();
		
	}
	
	
	@Override
	@RequestMapping(value="edit.htm",method=RequestMethod.GET)
	public String edit(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Long id = Long.valueOf(request.getParameter("id"));
		Supplier supplier=supplierService.getById(id);
		setupCreate(modelMap, request);
		modelMap.addAttribute("model", supplier);
		return "tileDefinition.edit-"+getNameSpace();
	}
	
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
	
		modelMap.addAttribute("modelList", supplierService.supplier_list());
		/*Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("medicine.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.medicine.menu.name", null, userLocale));*/
	
		return "tileDefinition.manage-"+getNameSpace();
	}
	

	@RequestMapping(value="/save.htm",method=RequestMethod.POST)
	public String save(@ModelAttribute("model") Supplier model, HttpServletRequest request, HttpServletResponse response, BindingResult bindingResult, ModelMap modelMap)
	{
		supplierService.save(model, getLoggedInUser());
		 String referer = request.getHeader("Referer");
		 return "redirect:"+ referer;	
	}
	
	@RequestMapping(value="/createsupplier.htm",method=RequestMethod.GET)
	public String createsupplier(ModelMap modelMap, HttpServletRequest request) {
		setupCreate(modelMap,request);
		return "tileDefinition.createsupplier-"+getNameSpace();
	}	
	
	
	@RequestMapping()
	public void createresponsiveSupplier(@RequestBody Supplier supplier, ModelMap modelMap,HttpServletRequest request){
		
		
	}

	@Override
	public boolean validateUpdate(Supplier model, BindingResult result) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateCreate(Supplier entity, BindingResult bindingResult) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BaseService<Supplier, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		return "supplier";
	}

}
