package com.seef.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.base.Status;
import com.seef.models.doctor.Doctor;
import com.seef.service.DoctorService;

@Service
public class DoctorServiceImpl implements DoctorService {
	
	private static final Logger _log = LoggerFactory.getLogger(DoctorServiceImpl.class);

	@Autowired
	GenericDAO genericDAO;

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveDoctor(Doctor doctor, ApplicationUser applicationUser) {
		if(doctor.getId()==null){
			doctor.setStatus(Status.ACTIVE);
			doctor.setCreatedBy(applicationUser);
			doctor.setCreatedOn(Calendar.getInstance().getTime());
			genericDAO.save(doctor);
			_log.info(">>>>>Save Successfully");
		}
		else{
			doctor.setModifiedBy(applicationUser);
			doctor.setModifiedOn(Calendar.getInstance().getTime());
			genericDAO.saveOrUpdate(doctor);
		}
		
	}

	@Override
	public List<Doctor> doctor_list() {
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("status", Status.ACTIVE);
		return genericDAO.findAllByCriteria(Doctor.class, params);

		
	}


	@Override
	public Doctor editDoctor(Long id) {
		
		return genericDAO.getById(Doctor.class, id);
	}

	@Override
	public Doctor getById(Doctor entity) {
		return genericDAO.getById(Doctor.class, entity);
	}

	@Override
	public Boolean validateUnique(Doctor doctor, Map<String, Object> map) {
		 return genericDAO.isUnique(Doctor.class, map);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveOrUpdateDoctor(Doctor doctor, ApplicationUser applicationUser) {
		doctor.setModifiedBy(applicationUser);
		doctor.setModifiedOn(Calendar.getInstance().getTime());
		genericDAO.saveOrUpdate(doctor);
		
	}

	@Override
	public Doctor getById(Long id) {
		return genericDAO.getById(Doctor.class, id);
	}



}
