package com.seef.models.pharmacy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.MedicineType;

@Entity
@Table(name="MEDICINE")
public class Medicine extends AbstractDomainObject {

	private static final long serialVersionUID = 1695879057788840457L;

	private Long id;
	
	private String medicineName;
	
	private String company;
	
	private MedicineType medicineType;
	
	private String description;
	

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "MEDICINE_SEQ")
	@SequenceGenerator(name = "MEDICINE_SEQ", sequenceName = "MEDICINE_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "MEDICINE_NAME")
	public String getMedicineName() {
		return medicineName;
	}
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	
	@Column(name="MEDICINE_TYPE")
	@Enumerated(EnumType.STRING)
	public MedicineType getMedicineType() {
		return medicineType;
	}
	public void setMedicineType(MedicineType medicineType) {
		this.medicineType = medicineType;
	}
	
	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="COMPANY_NAME")
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}

}
