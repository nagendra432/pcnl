package com.seef.service;

import com.seef.models.ApplicationUser;
import com.seef.models.pharmacy.Billing;

public interface BillingService  {
	public void saveBill(Billing billing,ApplicationUser applicationUser);
    public long getMaxBillno(); 
    public Billing getBill(Long billno);
}
