package com.seef.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.PasswordHistory;
import com.seef.models.enums.PasswordType;
import com.seef.service.GetPasswordsService;
import com.seef.service.PasswordHistoryService;
import com.seef.service.PasswordManager;



@Component("passwordHistoryService")
public class PasswordHistoryServiceImpl extends BaseServiceImpl<PasswordHistory, Long> implements PasswordHistoryService{

	private static final Logger _log = LoggerFactory.getLogger(PasswordHistoryServiceImpl.class);

	@Autowired
	PasswordManager passwordManager;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	GetPasswordsService getPasswordsService;

	@Autowired
	GenericDAO genericDAO;



	public boolean isInLastThreeUsedPasswords(Long sysUserId, String newPass,PasswordType type) {
		boolean flag=false;
		List<PasswordHistory> passwords = getPasswordsService.getMobileUserLastPasswords(sysUserId,type);
		_log.info(">>> Size of PasswordHistory: {" + passwords.size() + "}");
		String newpassword=passwordManager.getEncryptedPassword(sysUserId, newPass);
		for(PasswordHistory ph : passwords){
			if(ph.getPassword().equals(newpassword)){
				flag=true;
				break;
			}
		}
		return flag;
	}


	/*@Transactional(propagation=Propagation.REQUIRED)
	public void savePasswordHistory(ApplicationUser applicationUser, String password,PasswordType type) {
		InstSecurityPolicy instSecurityPolicy=genericDAO.getById(InstSecurityPolicy.class,  institution.getSecurityPolicy().getId());
		Long restrictedNoOfPwds=instSecurityPolicy.getNumOldPwd();
		PasswordHistory passwordHistoryNew = new PasswordHistory();

		List<PasswordHistory> existingPasswords = getPasswordsService.getApplicationUserLastPasswords(applicationUser.getId(), type);		
		passwordHistoryNew.setApplicationUser(applicationUser);
		passwordHistoryNew.setChangeDate(Calendar.getInstance().getTime());
		passwordHistoryNew.setPassword(passwordManager.getEncryptedPassword(applicationUser.getId(), password));
		passwordHistoryNew.setType(type);

		if(existingPasswords.size() != restrictedNoOfPwds)
		{
			getPasswordsService.savePassword(passwordHistoryNew);
		}
		else
		{			
			PasswordHistory passwordHistoryLast = existingPasswords.get(0);
			getPasswordsService.deleteOldPassword(passwordHistoryLast);
			getPasswordsService.savePassword(passwordHistoryNew);

		}

	}

	

*/
	

	
	@Override
	public Date getLastChangedPasswordByUserId(Long sysUserId) {
		PasswordHistory passwordHistoryNew;
		List<PasswordHistory> existingPasswords = getPasswordsService.getApplicationUserLastPasswords(sysUserId,PasswordType.APP);		
		if(existingPasswords.size()!=0)
		{
		int index=existingPasswords.size()-1;
		passwordHistoryNew=existingPasswords.get(index);
	    return passwordHistoryNew.getChangeDate();
		}
		return null;
	}


	

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void savePasswordHistory(ApplicationUser userFromDB, String password,PasswordType app) {
		//SecurityPolicy securityPolicy=genericDAO.getById(SecurityPolicy.class,userFromDB.getId());
		//Long restrictedNoOfPwds=securityPolicy.getNumOldPwd();
		PasswordHistory passwordHistoryNew = new PasswordHistory();
        _log.info(" >>> User id at existing password " + userFromDB.getId());
		List<PasswordHistory> existingPasswords = getPasswordsService.getApplicationUserLastPasswords(userFromDB.getId(), app);		
		passwordHistoryNew.setApplicationUser(userFromDB);
		passwordHistoryNew.setChangeDate(Calendar.getInstance().getTime());
		passwordHistoryNew.setPassword(passwordManager.getEncryptedPassword(userFromDB.getId(), password));
		passwordHistoryNew.setType(app);

		 if(existingPasswords.size() > 0){
			PasswordHistory passwordHistoryLast = existingPasswords.get(0);
			getPasswordsService.deleteOldPassword(passwordHistoryLast);
		 }
			getPasswordsService.savePassword(passwordHistoryNew);
	
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public boolean isValidatePasswordInPwdHistory(String password,ApplicationUser user) {
		boolean result=true;		
		String newPwd=passwordManager.getEncryptedPassword(user.getId(),password);		
		List<PasswordHistory> oldPasswords = getPasswordsService.getApplicationUserLastPasswords(user.getId(), PasswordType.APP);

		if(oldPasswords.size() > 0){

			for(int i=0;i<oldPasswords.size();i++){	
				if(oldPasswords.get(i).getPassword().equals(newPwd)){
					result=false;
					break;
				}

			}
		}

		return result;
	}

}
