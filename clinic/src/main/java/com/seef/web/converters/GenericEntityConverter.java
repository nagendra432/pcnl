package com.seef.web.converters;

import java.util.HashSet;
import java.util.Set;

import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;

import com.seef.service.UserService;




public class GenericEntityConverter implements GenericConverter{
	
	private UserService userService;

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		Long primaryKey=getPrimaryKeyFromSource(source);
		Object entity=null;
		
		if(null!=primaryKey){
			entity = userService.find(targetType.getType(),new Long(source.toString()));
		}
		
		if(null == entity){
			entity=createNewInstance(targetType.getType());
		}
		return entity; 
	}

	private Object createNewInstance(Class class1) {
		Object entity=null;
		try{
			entity=class1.newInstance();
		}catch(Throwable exce){
			// ingnoring
		}
		return entity;
	}

	private Long getPrimaryKeyFromSource(Object source) {
		Long primaryKey=null;
		try{
			primaryKey=Long.parseLong(source.toString());
		}catch(Throwable th){
			// skipping
		}
		return primaryKey;
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		Set<ConvertiblePair> supportedList=new HashSet<GenericConverter.ConvertiblePair>();
		return supportedList;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
