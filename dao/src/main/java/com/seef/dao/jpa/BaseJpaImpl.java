package com.seef.dao.jpa;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seef.dao.BaseDao;
import com.seef.dao.util.CrudUtil;
import com.seef.models.ApplicationUser;
import com.seef.models.audit.Audit;
import com.seef.models.audit.AuditLog;
import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.UserStatus;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class BaseJpaImpl<T extends AbstractDomainObject,ID extends Serializable> implements BaseDao<T, ID>{
	private static final Logger logger = LoggerFactory.getLogger(BaseJpaImpl.class);

	private Class<T> persistentClass;
	@PersistenceContext
	private EntityManager entityManager;

	private static Logger log=LoggerFactory.getLogger(BaseJpaImpl.class);


	public BaseJpaImpl(){
		Class clazz=getClass();
		while(true){
			if(clazz.getGenericSuperclass() instanceof ParameterizedType){
				this.persistentClass = (Class<T>) ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];		
				break;
			}else{
				clazz=getClass().getSuperclass();
				log.debug("Trying to find generic super class {}",clazz.getSimpleName());

			}
		}

	}
	public T save(T entity) {
		entityManager.persist(entity);
		writeEntityLog("label.audit.ADD", entity,null);

		return entity;
	}

	public T update(T entity) {
	    	T oldEntity= null;
		oldEntity = getOldEntity(entity, oldEntity);
		writeEntityLog("label.audit.UPDATE", entity, oldEntity);
		return entityManager.merge(entity);
	}
	private T getOldEntity(T entity, T oldEntity) {
		
		if(entity instanceof ApplicationUser){
			oldEntity = (T) entityManager.find(ApplicationUser.class, entity.getId());
		}
		
		
		return oldEntity;
	}
	public T find(ID id) {
		return entityManager.find(persistentClass, id);
	}


	public T delete(ID id){
		T entity =find(id);
		entityManager.remove(entity);
		writeEntityLog("label.audit.DELETE", entity,null);

		return entity;
	}

	public void delete(T t){
		entityManager.remove(t);

	}

	public int count(String queryName,Map<String,Object> params){
		Query query= entityManager.createNamedQuery(queryName);
		setQueryParameters(query, params);
		Long count=(Long) query.getSingleResult();
		return  (int)(count==null?0:count);
	}

	public int count(String queryName,Object...values){
		Query query= entityManager.createNamedQuery(queryName);
		setQueryParameters(query, values);
		Long count=(Long) query.getSingleResult();
		return  (int)(count==null?0:count);
	}

	public boolean isExist(String queryName,Map<String,Object> params){
		return count(queryName,params)>0;
	}

	public boolean isExist(String queryName,Object...values ){
		return count(queryName,values)>0;
	}

	public <K extends AbstractDomainObject> List<K> executeNamedQuery(String queryName,Class<K> clazz){
		return entityManager.createNamedQuery(queryName, clazz).getResultList();
	}

	public <K extends AbstractDomainObject> List<K> executeNamedQuery(String queryName,Class<K> clazz,Map<String, Object> params){
		Query query= entityManager.createNamedQuery(queryName, clazz);
		setQueryParameters(query, params);
		return query.getResultList();
	}


	private void setQueryParameters(Query typedQuery, Object[] values) {
		if(null!=values){
			for(int pos=0;pos<values.length;pos++){
				typedQuery.setParameter(pos+1, values[pos]);
			}
		}

	}
	public List<T> executeNamedQuery(String queryName,Map<String,Object> params){
		TypedQuery<T> typedQuery= entityManager.createNamedQuery(queryName, this.persistentClass);
		setQueryParameters(typedQuery,params);
		return typedQuery.getResultList();
	}

	public T findOne(String queryName, Map<String, Object> params) {
		TypedQuery<T> typedQuery= entityManager.createNamedQuery(queryName, this.persistentClass);
		setQueryParameters(typedQuery,params);
		return findOne(typedQuery);
	}


	private T findOne(TypedQuery<T> query){
		try{
			return query.getSingleResult();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	private void setQueryParameters(Query typedQuery,
			Map<String, Object> params) {
		if(null!=params){
			for(String paramName : params.keySet()){
				typedQuery.setParameter(paramName, params.get(paramName));
			}
		}

	}
	public EntityManager getEntityManager() {
		return entityManager;
	}
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public <K extends AbstractDomainObject> K find( Class<K> clazz, ID id){
		return entityManager.find(clazz, id);
	}

	public <K extends AbstractDomainObject> K findOneByQuery(String queryName, Class<K> clazz, Map<String, Object> params)	{
		TypedQuery<K> typedQuery= entityManager.createNamedQuery(queryName, clazz);
		setQueryParameters(typedQuery,params);
		K k=null;
		try{
			k=typedQuery.getSingleResult();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return k;
	}

	public <K extends AbstractDomainObject> List<K> findAll(String queryName, Class<K> clazz, Map<String, Object> params){
		TypedQuery<K> typedQuery= entityManager.createNamedQuery(queryName, clazz);
		setQueryParameters(typedQuery,params);
		return typedQuery.getResultList();
	}



	public T findOneNativeQuery(String query,Map<String,Object> params){
		T object=null;
		Query q= entityManager.createNativeQuery(query, this.persistentClass);
		setQueryParameters(q, params);
		try{
			object=(T) q.getSingleResult();
		}catch(Exception ex){
			//log exception
		}
		return object; 
	} 

	public <K extends AbstractDomainObject> K findOneNativeQuery(String query,Map<String,Object> params,Class<K> clazz){
		K object=null;
		Query q= entityManager.createNativeQuery(query, clazz);
		setQueryParameters(q, params);
		try{
			object=(K) q.getSingleResult();
		}catch(Exception ex){
			//log exception
		}
		return object; 
	}


	public <K extends AbstractDomainObject> List<K> findAll(Class<K> clazz)	{
		return entityManager.createQuery("SELECT clazz FROM "+clazz.getSimpleName()+" clazz where status='ACTIVE' ").getResultList();
	}

	public <K extends AbstractDomainObject> K findOne(ID identifier,Class<K> clazz)	{		
		return entityManager.find(clazz, identifier);
	}

	public int executeNamedUpdate(String queryName,Map<String,Object> params){
		Query query=entityManager.createNamedQuery(queryName);
		setQueryParameters(query, params);
		return query.executeUpdate();
	}

	public int executeNativeUpdate(String query, Map<String, Object> params) {
		Query qry=entityManager.createNativeQuery(query);
		setQueryParameters(qry, params);
		return qry.executeUpdate();
	}

	public T findTop(String queryName, Map<String, Object> params) {
		T object=null;
		TypedQuery<T> query=entityManager.createNamedQuery(queryName,this.persistentClass);
		setQueryParameters(query, params);
		query.setMaxResults(1);
		List<T> result=query.getResultList();
		if(null != result && result.size()>0){
			object=result.get(0); 
		}
		return object;
	}

	public void detach(T t) {
		getEntityManager().detach(t);

	}

	public void flush() {
		getEntityManager().flush();
	}

	public T getReference(ID id) {
		return getEntityManager().getReference(this.persistentClass, id);
	}
	@Override
	public Class<T> getPersistentClass() {
		return this.persistentClass;
	}


	/*************************************
	/*** Added by SRK, 17th June, 2013 ***
	 **************************************/	
	@Override
	public <K extends AbstractDomainObject> K  getOneByCriteria(Class<K> clazz, Map<String, Object> criterias) {
		List<K> items =  findAllByCriteria(clazz, criterias, null, false);
		if (items!=null && items.size() > 0)
			return items.get(0);
		return null;
	}

	@Override
	public <K extends AbstractDomainObject> List<K> findAllByCriteria( Class<K> clazz, Map<String, Object> criterias){
		return findAllByCriteria(clazz, criterias, null, false);
	}

	@Override
	public <K extends AbstractDomainObject> List<K> findAllByCriteria( Class<K> clazz, Map<String, Object> criterias, String sortField, boolean desc) {

		StringBuilder query = new StringBuilder("Select obj from ").append(
				clazz.getSimpleName()).append(" obj");
		if (criterias != null && criterias.size()>0 ) {
			Object[] keyArray = criterias.keySet().toArray();
			for (int i = 0; i < keyArray.length; i++) {

				if (i == 0) {
					query.append(" where");
				}
				query.append(" obj.").append(keyArray[i]).append("=:").append("p"+i);
				if (i != (keyArray.length - 1)) {
					query.append(" and");

				}
			}
		}
		if(sortField !=null){
			query.append("  ORDER BY obj.").append(sortField);
			if(!desc)
				query.append(" asc");
			else
				query.append(" desc");

		}
		//Build the query Object
	
		Query jpaQuery = entityManager.createQuery(
				query.toString());
		//Set the search Parameters for the jpaQuery
		if (criterias != null && criterias.size()>0 ) {
			Object[] keyArray = criterias.keySet().toArray();
			for (int i = 0; i < keyArray.length; i++){ 

				jpaQuery.setParameter("p"+i, criterias.get(keyArray[i]));
			}
		}

		//Execute the query and return List of objects
		return (List<K>) jpaQuery.getResultList();
	}
	@Override
	public T getByQuery(String jpaQuery) {
		Query query=entityManager.createQuery(jpaQuery);
		return (T) query.getResultList().get(0);
	}

	@Override
	public ID getEntityID(T t) {
		return (ID) entityManager.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(t);
	}

	@Override
	public <K extends AbstractDomainObject> boolean isUnique(Class<K> clazz, K entity, Map<String, Object> properties) {
		StringBuffer query = new StringBuffer("select obj from "+clazz.getSimpleName()+" obj where 1=1");
		Iterator<String> it = properties.keySet().iterator();
		while(it.hasNext()) {
			String key = it.next();
			query.append(" and "); 
			if (properties.get(key)==null) {
				query.append("obj."+key+" is null");
			}
			else {
				query.append("obj."+key+"='"+properties.get(key)+"'");
			}
		}
		long id = -1l;
		if (entity.getId()!=null)
			id=entity.getId();
		query.append(" and obj.id != "+id);
		Query q = entityManager.createQuery(query.toString());
		List<T> resultList = q.getResultList();
		logger.info("##### ResultList Size:" + resultList.size());
		if (resultList !=null && resultList.size()>0) {
			return false;
		}
		return true;
	}

	public <K extends AbstractDomainObject> boolean isUnique(Class<K> clazz, Map<String, Object> properties) {
		StringBuffer stringBuffer = new StringBuffer("select count(obj) from "+ clazz.getSimpleName() +" obj where 1=1");
		Iterator<String> iterator = properties.keySet().iterator();
		logger.info("EntrySet: " + properties.entrySet());
		
		
		while(iterator.hasNext()) {
			String key = iterator.next();
			stringBuffer.append(" and "); 
			stringBuffer.append("obj."+key+"='"+properties.get(key)+"'");
		}

		Query query = entityManager.createQuery(stringBuffer.toString());
		Long resultCount = (Long) query.getSingleResult();
		logger.info("### Result Count for isUnique: " + resultCount);
		return  ((int)(resultCount == null?0:resultCount)) <= 0;

	}

	public String executeQuerySingleString(String queryName, Map<String, Object> params){
		log.info(">>>"+String.class);
		Query query = entityManager.createNamedQuery(queryName, String.class);
		if(params != null) setQueryParameters(query, params);		
		return (String)query.getSingleResult();
	}
	
	public UserStatus executeQueryStatusString(String queryName, Map<String, Object> params){
		log.info(">>>"+String.class);
		Query query = entityManager.createNamedQuery(queryName, UserStatus.class);
		if(params != null) setQueryParameters(query, params);		
		return (UserStatus) query.getSingleResult();
	}
	
	@Override
	public <K extends AbstractDomainObject> List<K> findByNamedQuery(String namedQuery, Map<String,Object> map) {
		Query q = entityManager.createNamedQuery(namedQuery);
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			q.setParameter(key, map.get(key));
		}
		return (List<K>)q.getResultList();
	}

	@Override
	public <K extends AbstractDomainObject> K getById(Class<K> clazz, AbstractDomainObject entity)
	{
		entityManager.detach(entity);
		return (K) entityManager.find(clazz, entity.getId());
	}

	@Override
	public <K extends AbstractDomainObject> List<K> searchByQuery(Class<K> clazz, int noOfRecord, String query, String countQuery) {
		StringBuffer queryStmt = new StringBuffer(query);
		return (List<K>)entityManager.createQuery(queryStmt.toString()).setMaxResults(noOfRecord).getResultList();
	}

	@Override
	public <T extends AbstractDomainObject> List<T> executeSimpleQuery(String query) {
		Query q = entityManager.createQuery(query);
		return (List<T>)q.getResultList();
	}

	private void writeEntityLog(String activityType, T entity, T oldEntity) {
		if (entity instanceof Audit) {
			AuditLog auditLog = new AuditLog();
			auditLog.setCreatedAt(Calendar.getInstance().getTime());
			auditLog.setReference(activityType);
			auditLog.setEntityClass(entity.getClass().getSimpleName());
			auditLog.setEntityId(entity.getId());

			if ("label.audit.ADD".equalsIgnoreCase(activityType))
			{
				if(null!=entity.getCreatedBy())
				{
					log.info("<<<Sysuser CReate <<<<"+entity.getCreatedBy().getId());
					log.info("<<<Sysuser CReate <<<<"+entity.getId());
					ApplicationUser applicationUser= entityManager.find(ApplicationUser.class, entity.getCreatedBy().getId());
					
					auditLog.setUserId(entity.getCreatedBy().getName());
					auditLog.setMessage(CrudUtil.buildCreateMessage((Audit)entity));
					entityManager.merge(auditLog);

				}
			}
			if ("label.audit.UPDATE".equalsIgnoreCase(activityType))
			{
				if(null!=entity.getModifiedBy())
				{
					ApplicationUser applicationUser= entityManager.find(ApplicationUser.class, entity.getModifiedBy().getId());
					log.info("<<<Sysuser UPDATE<<<<"+entity.getModifiedBy().getId());
					
					auditLog.setUserId(entity.getModifiedBy().getName());
					auditLog.setMessage(CrudUtil.buildUpdateMessage((Audit)oldEntity,(Audit)entity));
					entityManager.merge(auditLog);

				}
			}
			if ("label.audit.DELETE".equalsIgnoreCase(activityType))
			{
				if(null!=entity.getModifiedBy())
				{
					ApplicationUser applicationUser= entityManager.find(ApplicationUser.class, entity.getModifiedBy().getId());
					
					auditLog.setMessage(CrudUtil.buildDeleteMessage((Audit)entity));
					auditLog.setUserId(entity.getModifiedBy().getName());
					entityManager.merge(auditLog);

				}
			}
		}
	}
	
	// Added by akilesh
		public Long executeQuerySinglequery(String queryName, Map<String, Object> params){
		log.info(">>>"+String.class);
		Query query = entityManager.createNamedQuery(queryName, Long.class);
		if(params != null) setQueryParameters(query, params);		
		return (Long)query.getSingleResult();
	}
		
	@Override
	public <K extends AbstractDomainObject> List<K> executeNativeQuery(String query, String resultSetMapping) {
		Query nativeQuery = entityManager.createNativeQuery(query, resultSetMapping);
		return nativeQuery.getResultList();		
	}

}
