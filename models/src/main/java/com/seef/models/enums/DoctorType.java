package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;

public enum DoctorType {
	
	PERMANENT("PERMANENT"),CONSULTANT("CONSULTANT"),EMERGENCY("EMERGENCY");

	private String type;
	
	private DoctorType(String type) {
		this.type = type;
	}

	public static List<DoctorType> list() 
	{
		return Arrays.asList(values());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
