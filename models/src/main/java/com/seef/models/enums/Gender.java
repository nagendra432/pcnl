package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;


public enum Gender
{
	Male("Male"), Female("Female"),Others("Others");

	private String type;
	
	private Gender(String type) {
		this.type = type;
	}

	public static List<Gender> list() 
	{
		return Arrays.asList(values());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	
}
