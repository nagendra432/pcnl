package com.seef.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.master.Language;
import com.seef.models.master.NotificationType;
import com.seef.models.master.Theme;

@Entity
@Table(name = "PREFERENCE")
public class Preferences extends AbstractDomainObject {

	private static final long serialVersionUID = 8675285358017897801L;
	
	private Long id;
	private Language language;
	private NotificationType notificationType;	
	private Theme theme;

	public Preferences(){

	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "PREFERENCE_SEQ")
	@SequenceGenerator(name = "PREFERENCE_SEQ", sequenceName = "PREFERENCE_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="THEME_ID")
	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="LANG_ID")
	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="NOTIFICATION_TYPE")
	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

}
