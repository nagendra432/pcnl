package com.seef.service;


import java.util.List;
import java.util.Map;

import com.seef.models.ADNLProcedure;
import com.seef.models.ApplicationUser;

public interface ADNLProcedureService {
	
	public void saveADNLProcedure(ADNLProcedure procedure,ApplicationUser applicationUser);
	public Boolean validateUnique(ADNLProcedure procedure,Map<String, Object> map);
	public List<ADNLProcedure> getAllADNLProcedures();

}
