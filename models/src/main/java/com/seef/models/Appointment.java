package com.seef.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.doctor.Doctor;
import com.seef.models.enums.CounsultationType;

@Entity
@Table(name="APPOINTMENT")
@NamedQueries({
@NamedQuery(name="listbystatus",query="select  appointment from Appointment appointment where appointment.status IN ('COMPLETED','NEW') and appointment.outPatient.id=:outpatient"),
@NamedQuery(name="listbydate",query="select  appointment from Appointment appointment where appointment.status IN ('COMPLETED','NEW') and DATE(appointment.appointmentDate)=DATE(:date)"),
@NamedQuery(name="samedayappointment",query="select count(*) from Appointment appointment where appointment.status IN ('NEW') and DATE(appointment.appointmentDate)=DATE(:date) and appointment.outPatient.id=:outpatient"),
@NamedQuery(name="doctorslistbydate",query="select doctor from DoctorSchedule doctor where DATE(doctor.scheduledDate)=DATE(:scheduledDate)"),
@NamedQuery(name="lasttworecords", query="select appointment from Appointment appointment where appointment.outPatient=:outpatient and appointment.status IN ('COMPLETED','NEW') ORDER BY appointment.appointmentDate desc 2"),
@NamedQuery(name="doctorsschedulelistbydate",query="select appointment from Appointment appointment where DATE(appointment.appointmentDate)=DATE(:appointmentDate) and appointment.doctor.id=:doctorid")})

public class Appointment extends AbstractDomainObject {

	
	private static final long serialVersionUID = 861188021389769951L;
	private Long id;
	private OutPatient outPatient;
	private Date appointmentDate;
	private CounsultationType counsultationType;
	private Double fee;
	public int age;
	private String time;
	private Doctor doctor;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "APPOINTMENT_SEQ")
	@SequenceGenerator(name = "APPOINTMENT_SEQ", sequenceName = "APPOINTMENT_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "OUTPATIENT_ID")
	public OutPatient getOutPatient() {
		return outPatient;
	}
	public void setOutPatient(OutPatient outPatient) {
		this.outPatient = outPatient;
	}
	
	@Column(name="APPOINTMENT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	
	@Column(name="FEE")
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	
	@Column(name="CONSULTATION_TYPE")
	@Enumerated(EnumType.STRING)
	public CounsultationType getCounsultationType() {
		return counsultationType;
	}
	public void setCounsultationType(CounsultationType counsultationType) {
		this.counsultationType = counsultationType;
	}
	
	@Transient
	public int getAge() {
		
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}


	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "DOCTOR_ID")
	public Doctor getDoctor() {
		return doctor;
	}


	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	@Column(name = "TIME")
	public String getTime() {
		return time;
	}


	public void setTime(String time) {
		this.time = time;
	}

}
