<%@ taglib prefix="form"  uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row">
	<div class="col-md-12" id="body">
<form:form action="outptsearch.htm" modelAttribute="model">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-cogs"></i>Search Surgery 
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Age</label>
						<div class="col-md-6">
							<form:input path="age" class="form-control" type="number"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Gender</label>
						<div class="col-md-6">
							 <form:select path="gender" class="form-control">
							   <form:option value="">----   please select   ----</form:option>
							   <form:options items="${gender}" itemLabel="type" itemValue="type"/>
							 </form:select>
						</div>
					</div>
				</div>
				</div><br/>
				
				<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">surgery</label>
						<div class="col-md-6">
						  <form:select path="surgery" class="form-control">
						   <form:option value="0">----   please select   ----</form:option>
						   <c:forEach items="${surgerys}" var="surgery">
						   	<form:option value="${surgery.id}">${surgery.name}</form:option>
						   </c:forEach>
						  </form:select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Surgery Type</label>
						<div class="col-md-6">
						   <form:select path="surgeryType" class="form-control">
						      <form:option value="0">----   please select   ----</form:option>
						      <c:forEach items="${surgeryTypes}" var="surgeryType">
							   	<form:option value="${surgeryType.id}">${surgeryType.name}</form:option>
							  </c:forEach>
						    </form:select>
						</div>
					</div>
				</div>
				</div><br/>
				
				<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Remark</label>
						<div class="col-md-6">
						   <form:input path="remarks" class="form-control" />
						</div>
					</div>
				</div>
					<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Surgery Type</label>
						<div class="col-md-6">
						   <form:select path="surgeon" class="form-control">
						      <form:option value="0">----   please select   ----</form:option>
						      <c:forEach items="${surgeons}" var="surgeon">
							   	<form:option value="${surgeon.id}">${surgeon.name}</form:option>
							  </c:forEach>
						    </form:select>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	  <div class="form-actions fluid">
               <div class="col-md-offset-5 col-md-7">
                  <button type="submit" class="btn green">Search</button>
                  <button type="button" onclick="javascript:history.go(-1);" class="btn default">cancel</button>                              
               </div>
         </div> 
      </form:form>
	</div>
</div>

