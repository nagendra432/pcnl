package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;

public enum SmsStatus {

	SUBMITTED, SEND_FAIL, SEND_SUCCESS, DELIVERY_SUCCESS, DELIVERY_FAIL, ERROR, PENDING;

	public static List<SmsStatus> list() 
	{
		return Arrays.asList(values());
	}
}
