package com.seef.web.controllers;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seef.dao.GenericDAO;
import com.seef.models.ADNLProcedure;
import com.seef.models.Appointment;
import com.seef.models.OutPatient;
import com.seef.models.Surgery;
import com.seef.models.SurgeryType;
import com.seef.models.SystemUser;
import com.seef.models.doctor.Doctor;
import com.seef.models.doctor.DoctorSchedule;
import com.seef.models.enums.Anes;
import com.seef.models.enums.CounsultationType;
import com.seef.models.enums.Gender;
import com.seef.models.enums.UserType;
import com.seef.models.master.Country;
import com.seef.models.master.State;
import com.seef.service.ADNLProcedureService;
import com.seef.service.AppointmentService;
import com.seef.service.BaseService;
import com.seef.service.OutPatientService;
import com.seef.service.SurgeryService;
import com.seef.service.SurgeryTypeService;
import com.seef.service.SystemUserService;
import com.seef.web.dto.SearchPatientDto;
import com.seef.web.editors.AbstractModelEditor;

@Controller
@RequestMapping("/outpatient")
public class OutPatientRegistrationController extends CRUDController<OutPatient> {
	private static final Logger _log = LoggerFactory.getLogger(OutPatientRegistrationController.class);
	
	@Autowired
	OutPatientService outPatientService;
	
	@Autowired
	GenericDAO genericDAO;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	AppointmentService appointmentService;
	
	@Autowired
	private SurgeryService surgeryService;
	
	@Autowired
	ADNLProcedureService adnlProcedureService;
	
	@Autowired
	private SystemUserService userService;
	
	@Autowired
	private SurgeryTypeService surgeryTypeService;
	
	@Override
	public void setupCreate(ModelMap modelMap,HttpServletRequest request)
	{
		//modelMap.addAttribute("mrno",outPatientService.getMaxMRNO());
		modelMap.addAttribute("countries",genericDAO.findAll(Country.class));
	    modelMap.addAttribute("states", genericDAO.findAll(State.class));
	    modelMap.addAttribute("counsultationType", CounsultationType.list());
	    modelMap.addAttribute("doctors", genericDAO.findAll(Doctor.class));
	
	}
	
	@RequestMapping(value = "/surgery.htm", method = RequestMethod.GET)
	public String info(ModelMap modelMap, HttpServletRequest request) {
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.surgeries.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.outpatient.info.menu.name", null, userLocale));
		modelMap.addAttribute("surgeryType", genericDAO.findAll(SurgeryType.class));
		return "tileDefinition.user-info";
	}
	
	@RequestMapping(value = "/surgery.htm", method = RequestMethod.POST)
	public String find(ModelMap modelMap, HttpServletRequest request,@RequestParam("searchMrno") String searchMrno) {
		modelMap.addAttribute("model",outPatientService.findByMrNo(searchMrno));
		modelMap.addAttribute("surgeryType", genericDAO.findAll(SurgeryType.class));
		modelMap.addAttribute("mrNum", searchMrno);
		modelMap.addAttribute("surgeries", surgeryService.getAllsurgeries());
		modelMap.addAttribute("anes", Anes.values());
		modelMap.addAttribute("adnlProcedures", adnlProcedureService.getAllADNLProcedures());
		modelMap.addAttribute("surgeons", userService.findByUserType(UserType.SURGEON));
		modelMap.addAttribute("asstSurgeons", userService.findByUserType(UserType.AsstSURGEON));
		
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.surgeries.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.outpatient.info.menu.name", null, userLocale));
		return "tileDefinition.user-info";
	}
	
	@RequestMapping(value = "/updatePatient.htm", method = RequestMethod.POST)
	public String updatePatient(ModelMap modelMap, HttpServletRequest request,@ModelAttribute("model") OutPatient model) {
		outPatientService.saveOrUpdateOutPatient(model, getLoggedInUser());
		return "redirect:/outpatient/manage.htm";
	}
	
	@RequestMapping(value = "/outptsearch.htm", method = RequestMethod.GET)
	public String outptSearch(ModelMap modelMap, HttpServletRequest request) {
		modelMap.addAttribute("model",new SearchPatientDto());
		modelMap.addAttribute("gender",Gender.values());
		modelMap.addAttribute("surgerys",surgeryService.getAllsurgeries());
		modelMap.addAttribute("surgeryTypes",surgeryTypeService.getAllSurgeryTypes());
		modelMap.addAttribute("surgeons", userService.findByUserType(UserType.SURGEON));
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.surgeries.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.outpatient.search.menu.name", null, userLocale));
		return "tileDefinition.user-search";
	}
	
	@RequestMapping(value = "/outptsearch.htm", method = RequestMethod.POST)
	public String outptInfo(ModelMap modelMap, HttpServletRequest request ) {
		int age=Integer.parseInt(request.getParameter("age")) ;
		String gender=request.getParameter("gender");
		String remarks=request.getParameter("remarks");
		String surgeon=request.getParameter("surgeon");
		Long surgeryType=null;
		Long surgery=null;
		String surgeryName = null;
		String surgeryTypeName = null;
		if((Long.parseLong(request.getParameter("surgery")) > 0)){
			surgery=Long.parseLong(request.getParameter("surgery"));
			surgeryName = surgeryService.findById(surgery).getName();
		}
		if(Long.parseLong(request.getParameter("surgeryType")) > 0){
			surgeryType=Long.parseLong(request.getParameter("surgeryType"));
			surgeryTypeName = surgeryTypeService.findById(surgeryType).getName();
		}
		if(null!=surgeon)
			modelMap.addAttribute("surgeon", userService.getSystemUserById(Long.parseLong(surgeon)).getFirstName());
		modelMap.addAttribute("modelList", outPatientService.outPatiensByParams(age, gender, surgery, surgeryType, remarks,surgeon));
		modelMap.addAttribute("age", age);
		modelMap.addAttribute("gender", gender);
		modelMap.addAttribute("remarks", remarks);
		modelMap.addAttribute("surgery", surgeryName);
		modelMap.addAttribute("surgeryType", surgeryTypeName);
		modelMap.addAttribute("page", "search");
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.outpatient.menu.name", null, userLocale));
	    return "tileDefinition.manage-"+getNameSpace();
	}
	
	
	@RequestMapping(value="/surType.htm",method=RequestMethod.GET)
	@ResponseBody
	public String surgeryFinder(ModelMap modelMap, HttpServletRequest request,@RequestParam("surgeryTypeID") Long surgeryTypeID) {
		
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		    String json = null;
			try {
				json = ow.writeValueAsString(surgeryService.findBySurgeryType(surgeryTypeID));
			} catch (Exception e) {
				e.printStackTrace();
			}
		    _log.info(">>>>>>>. jsonObject :"+json);
			return json;
		
		
	}
	
	@Override
	@RequestMapping(value="/create.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap,HttpServletRequest request) 
	{
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("create.outpatient.menu.name", null, userLocale));
		setupCreate(modelMap,request);
		modelMap.addAttribute("model",new OutPatient());
		return "tileDefinition.create-"+getNameSpace();
	
	}
	
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
	
		modelMap.addAttribute("modelList", outPatientService.outPatient_list());
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.outpatient.menu.name", null, userLocale));
	    modelMap.addAttribute("page", "manage");
		return "tileDefinition.manage-"+getNameSpace();
	}
	
	
	@Override
	@RequestMapping(value = "/edit.htm", method = RequestMethod.GET)
	public String edit(ModelMap modelMap, HttpServletRequest request) {
				
		Long id = Long.valueOf(request.getParameter("id"));
		OutPatient outpatient=outPatientService.getById(id);
		setupCreate(modelMap, request);
		modelMap.addAttribute("model", outpatient);
		return "tileDefinition.edit-"+getNameSpace();
	}
	
	@Override
	public String view(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Long id = Long.valueOf(request.getParameter("id"));
		_log.info(">>>>> id is:"+id);
		OutPatient outpatient=outPatientService.getById(id);
		modelMap.addAttribute("model", outpatient);
		setupCreate(modelMap, request);
		modelMap.addAttribute("surgeryType", genericDAO.findAll(SurgeryType.class));
		modelMap.addAttribute("surgeries", surgeryService.getAllsurgeries());
		modelMap.addAttribute("anes", Anes.values());
		modelMap.addAttribute("adnlProcedures", adnlProcedureService.getAllADNLProcedures());
		modelMap.addAttribute("surgeons", userService.findByUserType(UserType.SURGEON));
		modelMap.addAttribute("asstSurgeons", userService.findByUserType(UserType.AsstSURGEON));
		return "tileDefinition.view-"+getNameSpace();
	}
	
	
	@Override
	@RequestMapping(value="/save.htm",method=RequestMethod.POST)
	public String save(@Valid @ModelAttribute("model") OutPatient model,HttpServletRequest request,HttpServletResponse response,BindingResult bindingResult,ModelMap modelMap)
	{
		
		 if(model.getId() == null){
			 
			 if(!validateCreate(model, bindingResult))
			 {
			     setupCreate(modelMap, request);
				 return "tileDefinition.create-" + getNameSpace();
			 }
			 else
			 {
				 outPatientService.saveOutPatient(model, getLoggedInUser());
				 return getRedirectURL("/manage.htm");
			 }
			
			
	     }else{
	    	 if(!validateUpdate(model, bindingResult))
	    	 {
			     setupCreate(modelMap, request);
				 return "tileDefinition.create-" + getNameSpace();
			 }
			 else
			 {
				 outPatientService.saveOrUpdateOutPatient(model, getLoggedInUser());
				 return getRedirectURL("/manage.htm");
			 }
	    	 
	     }
		
	}
	
	@Override
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	 binder.registerCustomEditor(Country.class, new AbstractModelEditor(Country.class));
	 binder.registerCustomEditor(SearchPatientDto.class, new AbstractModelEditor(SearchPatientDto.class));
	 binder.registerCustomEditor(Gender.class, new AbstractModelEditor(Gender.class));
	 binder.registerCustomEditor(ADNLProcedure.class, new AbstractModelEditor(ADNLProcedure.class));
	 binder.registerCustomEditor(SystemUser.class, new AbstractModelEditor(SystemUser.class));
	 binder.registerCustomEditor(State.class, new AbstractModelEditor(State.class));
	 binder.registerCustomEditor(Doctor.class, new AbstractModelEditor(Doctor.class));
	 binder.registerCustomEditor(Surgery.class, new AbstractModelEditor(Surgery.class));
	 binder.registerCustomEditor(SurgeryType.class, new AbstractModelEditor(SurgeryType.class));
	 binder.registerCustomEditor(Date.class, "appointmentDate", new PropertyEditorSupport(){
		     public void setAsText(String value) {
		           try {
		        	   
		               setValue(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(value));
		               
		           } catch(ParseException e) {
		               setValue(null);
		           }
		       }

		     public String getAsText() {
		         String s = "";
		         if (getValue() != null) {
		            s = new SimpleDateFormat("yyyy-MM-dd HH:mm").format((Date) getValue());
		            System.out.println("Date formate" + s);
		         }
		         return s;
		       }     
		  });
		super.initBinder(binder);
	}
	@Override
	public boolean validateUpdate(OutPatient model, BindingResult result) {
		// TODO Auto-generated method stub
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		OutPatient oldoutPatient=outPatientService.getById(model);
		criteria.clear();
		criteria.put("phoneNo",model.getPhoneNo());
			
		if(!(oldoutPatient.getPhoneNo().equalsIgnoreCase(model.getPhoneNo())))
		{
			if(!outPatientService.validateUnique(model,criteria)){
				FieldError error=new FieldError("modelObject","studentAdmission.admissionNo",model.getPhoneNo(), false, null, null, messageSource.getMessage("label.patient.error.codeMessage",null,userLocale));
				result.addError(error);
				validationResult = true;
			}
		}
		
		return validationResult;
	}

	@Override
	public boolean validateCreate(OutPatient entity, BindingResult bindingResult) {
		// TODO Auto-generated method stub
		
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		criteria.put("phoneNo", entity.getPhoneNo());
		if(!outPatientService.validateUnique(entity, criteria))
		{
			 FieldError error=new FieldError("modelObject","phoneNo",entity.getPhoneNo(), false, null, null, messageSource.getMessage("label.patient.error.codeMessage",null,userLocale));
			   bindingResult.addError(error);
			   validationResult = true;
			
		}
		return validationResult;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/getdoctors", method=RequestMethod.GET)
	public void getChapters(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		PrintWriter out=response.getWriter();
		String date= request.getParameter("date");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	    Map<String,Object> criteria=new HashMap<String,Object>();
		
		try {
			_log.info(">>Date:"+dateFormatter.parse(date));
			criteria.put("scheduledDate",dateFormatter.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<DoctorSchedule> doctorslist = appointmentService.Doctors_listbyDate(criteria);
		_log.info(">>doctor size:"+doctorslist.size());
		criteria.clear();
		List doctors = new ArrayList();
		 Map medicineMap = null;
		for(DoctorSchedule section : doctorslist){
			criteria.put("doctor",section.getDoctor());
			try {
				criteria.put("appointmentDate",dateFormatter.parse(date));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<Appointment> appointments=genericDAO.findByCriteria(Appointment.class, criteria);
			
			if((Integer.parseInt(section.getNoOfAppointments())-appointments.size())!=0){
				medicineMap = new HashMap(); 
				String name=section.getDoctor().getName()+"( "+section.getInTime()+" - "+section.getOutTime()+" )";
				medicineMap.put("id", section.getDoctor().getId());
				medicineMap.put("name", name);
				doctors.add(medicineMap);
			}
		}
		      JSONArray jsonArray = new JSONArray(doctors);
		      
		      out.print(jsonArray);
	}
	
	

	@Override
	public BaseService<OutPatient, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "outpatient";
	}
	
}
