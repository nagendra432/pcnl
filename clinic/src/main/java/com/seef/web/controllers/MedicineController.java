package com.seef.web.controllers;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.seef.models.enums.MedicineType;
import com.seef.models.pharmacy.Medicine;
import com.seef.service.BaseService;
import com.seef.service.MedicineService;

@Controller
@RequestMapping("/medicine")
public class MedicineController extends CRUDController<Medicine> {
	
	private static final Logger _log = LoggerFactory.getLogger(MedicineController.class);
	
	@Autowired
	MedicineService medicineService;
	
	@Autowired
	MessageSource messageSource;
	

	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
	
		modelMap.addAttribute("modelList", medicineService.medicine_list());
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("medicine.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.medicine.menu.name", null, userLocale));
	
		return "tileDefinition.manage-"+getNameSpace();
	}
	
	@Override
	@RequestMapping(value="edit.htm",method=RequestMethod.GET)
	public String edit(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Long id = Long.valueOf(request.getParameter("id"));
		Medicine medicine=medicineService.getById(id);
		setupCreate(modelMap, request);
		modelMap.addAttribute("model", medicine);
		return "tileDefinition.edit-"+getNameSpace();
	}
	
	@Override
	@RequestMapping(value="/view.htm",method=RequestMethod.GET)
	public String view(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Long id = Long.valueOf(request.getParameter("id"));
		_log.info(">>>>> id is:"+id);
		Medicine medicine=medicineService.getById(id);
		modelMap.addAttribute("model", medicine);
		setupCreate(modelMap, request);
		modelMap.addAttribute("disabledStatus", "true");
		return "tileDefinition.view-"+getNameSpace();
	}
	
	
	@Override
	@RequestMapping(value="/create.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("medicine.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("create.medicine.menu.name", null, userLocale));
		
		
		setupCreate(modelMap,request);
		/*modelMap.addAttribute("model",Medicine)*/
		
		return "tileDefinition.create-"+getNameSpace();
	}
	
	
	
	@Override
	@RequestMapping(value="/save.htm",method=RequestMethod.POST)
	public String save(@Valid @ModelAttribute("model") Medicine model, HttpServletRequest request,HttpServletResponse response, BindingResult bindingResult,
			ModelMap modelMap) {
		if(model.getId()==null)
		{
			 if(!validateCreate(model, bindingResult))
			 {
			     setupCreate(modelMap, request);
				 return "tileDefinition.create-" + getNameSpace();
			 }
			 else
			 {
				 medicineService.saveMedicine(model, getLoggedInUser());
				 return getRedirectURL("/manage.htm");
			 }
		
		}
		else
		{
			 if(!validateUpdate(model, bindingResult))
	    	 {
			     setupCreate(modelMap, request);
				 return "tileDefinition.create-" + getNameSpace();
			 }
			 else
			 {
				 medicineService.saveOrUpdateMedicine(model, getLoggedInUser());
				 return getRedirectURL("/manage.htm");
			 }
		}
		
	}
	
	
	@Override
	public void setupCreate(ModelMap modelMap,HttpServletRequest request)
	{
		
	    modelMap.addAttribute("medicineType",MedicineType.list());
	    _log.info(">>>>>.............. size is:"+MedicineType.list().size());
	    
	}
	@Override
	public boolean validateUpdate(Medicine model, BindingResult result) {
		// TODO Auto-generated method stub
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		Medicine oldmedicine=medicineService.getById(model);
		criteria.clear();
		criteria.put("medicineName", model.getMedicineName());
			
		if(!(oldmedicine.getMedicineName().equalsIgnoreCase(model.getMedicineName())))
		{
			if(!medicineService.validateUnique(model,criteria)){
				 FieldError error=new FieldError("modelObject","medicineName",model.getMedicineName(), false, null, null, messageSource.getMessage("label.medicinename.error.codeMessage",null,userLocale));
				result.addError(error);
				validationResult = false;
			}
		}
		
		return validationResult;
	}

	@Override
	public boolean validateCreate(Medicine entity, BindingResult bindingResult) {
		// TODO Auto-generated method stub
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		criteria.put("medicineName", entity.getMedicineName());
		if(!medicineService.validateUnique(entity, criteria))
		{
			 FieldError error=new FieldError("modelObject","medicineName",entity.getMedicineName(), false, null, null, messageSource.getMessage("label.medicinename.error.codeMessage",null,userLocale));
			   bindingResult.addError(error);
			   validationResult = false;
			
		}
		return validationResult;
	}
	
	
	@Override
	public BaseService<Medicine, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "medicine";
	}

}
