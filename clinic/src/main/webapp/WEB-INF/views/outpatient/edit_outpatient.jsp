<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.outpatient.firstname" var="lblfirstname"/>
<spring:message code="label.outpatient.middlename" var="lblmiddlename"/>
<spring:message code="label.outpatient.lastname" var="lbllastname"/>
<spring:message code="label.outpatient.phno" var="lblphno"/>
<spring:message code="label.address.line1" var="lbladdress1"/>
<spring:message code="label.address.line2" var="lbladdress2"/>
<spring:message code="label.address.zipcode" var="lblzipcode"/>
<spring:message code="label.address.city" var="lblcity"/>
<spring:message code="label.address.country" var="lblcountry"/>
<spring:message code="label.address.state" var="lblstate"/>
<spring:message code="label.address.fulladdress" var="lblfulladdress"/>
<spring:message code="label.outpatient.dob" var="lbldob"/>
<spring:message code="label.outpatient" var="lbloutpatient"/>
<spring:message code="label.save" var="lblsave"/>
<spring:message code="label.cancel" var="lblcancel"/>


<script type="text/javascript">
 $(document).ready(function(){
  $(".form_datetime").datetimepicker({         
   isRTL: App.isRTL(),
            format: "yyyy-mm-dd",
            weekStart: 1,
            todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
       });
 });
</script>



  <div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>${lbloutpatient}</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model" >
	                       <form:hidden path="id"/>
	                        <form:hidden path="appointment.id"/>
	                        <form:hidden path="stone"/>
	                        <form:hidden path="dJStent"/>
	                         <form:hidden path="surgery"/>
	                       <div style="color:red">                      		
		                    <form:errors path="*" cssClass="errorcollection"/>
		                  </div>
                        <div class="form-body">
                           <div class="alert alert-danger display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formerrormsg"/>
                           </div>
                           <div class="alert alert-success display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formsuccessmsg"/>
                           </div>
                   </div> 
                           
                            <div class="row">
                                  <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblmrno}</label>
                                       <div class="col-md-7">
                                          <form:input path="mrno"  class="form-control"  readonly="true"/>
                                       </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                          			 <div class="form-group">
                                       <label class="control-label col-md-3">${lblfirstname}<span class="required">*</span></label>
                                       <div class="col-md-7">
                                        <form:input path="firstName"  class="form-control"   
					                                required="required" />
                                       </div>
                                       </div>
                                       </div>
                                    </div>
                           
                           
                           
                            <div class="row">
                             <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblmiddlename}</label>
                                       <div class="col-md-7">
                                          <form:input path="middleName" class="form-control"  data-validate-length-range="3,3"/>
                                       </div>
                                       </div>
                                    </div>
                                    
                                     <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbllastname}</label>
                                       <div class="col-md-7">
                                           <form:input path="lastName" class="form-control"  data-validate-length-range="3,3"/>
                                       </div>
                                       </div>
                                    </div>
                            
                            </div>
                           
                           <div class="row">
                             <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbldob}<span class="required">*</span></label>
                                       <div class="col-md-7">
                                          <div class="input-group date form_datetime">
									   <form:input class="form-control"  path="dob"  required="required" /> <span class="input-group-btn">
										<button class="btn default date-set" type="button">
											<i class="icon-calendar"></i>
										</button>
									</span>
								        </div>
								  </div>
                                       </div>
                                    </div>
                                <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblphno}<span class="required">*</span></label>
                                       <div class="col-md-7">
                                          <form:input path="phoneNo" class="form-control"  required="required"/>
                                       </div>
                                       </div>
                                    </div>    
                                    </div>
                                    
                                     <div class="row">
                             <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbladdress1}</label>
                                       <div class="col-md-7">
                                          <form:input path="address.line1" class="form-control"  data-validate-length-range="3,3"/>
								  </div>
                                       </div>
                                    </div>
                                <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbladdress2}</label>
                                       <div class="col-md-7">
                                         <form:input path="address.line2" class="form-control"  data-validate-length-range="3,3"/>
                                       </div>
                                       </div>
                                    </div>    
                                    </div>
                           
                           
                           
                           
                           <div class="row">
                            <%--  <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblcountry}<span class="required">*</span></label>
                                       <div class="col-md-7">
                                          <span class="">
									 			 <form:select  path="address.country" required="required" class="form-control" id="select2_sample4">
					 								<form:option value="">--select--</form:option>
		                                       			 <c:forEach items="${countries}" var="country" >		                                         
		                                         			 <c:choose>
			                                          			<c:when test="${model.address.country eq country.name}">
			                                        <form:option value="${country.name}" selected="selected"  data-country-code="${country.name }">${lblcountry} </form:option>
			                                      </c:when>
		                                         <c:otherwise>
		                                            <form:option value="${country.name}"  data-country-code="${country.name }"><spring:message code="${country.name}"/></form:option>
		                                        </c:otherwise>
		                                         </c:choose> 
		                                         </c:forEach>
											</form:select>
     							  	</span>
                             
                             
								  </div>
                                       </div>
                                    </div> --%>
                                    
                                    <div class="col-md-6">
                                       <div class="form-group">
                                    	<label class="control-label col-md-3">${lblcountry}</label>
 									      <div class="col-md-7">
 									        <form:select path="address.country" class="form-control" id="country">
 									          <form:option value="">--select--</form:option>
 									           <c:forEach items="${countries}" var="country">
 									            <form:option value="${country.name}"><spring:message code="${country.name}"/></form:option>
 									           </c:forEach>
 									         </form:select>
 									      </div>
 									   </div>
 									</div>                                   
                                <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblstate}</label>
                                       <div class="col-md-7">
                                         <form:select path="address.state" class="form-control" id="state">
	                        				 <form:option value="">--select--</form:option>
     								       	<c:forEach items="${states}" var="state">
				                    	        <form:option value="${state.name}"><spring:message code="${state.name}"/></form:option>
				                    	    </c:forEach>
										  </form:select>
                             
                                       </div>
                                       </div>
                                    </div>    
                                    </div>
                            <div class="row">
                            <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblcity}</label>
                                       <div class="col-md-7">
                                          <form:input path="address.city" class="form-control"  data-validate-length-range="3,3"/>
								  </div>
                                       </div>
                                    </div>
                                <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblzipcode}</label>
                                       <div class="col-md-7">
                                          <form:input path="address.zipCode" class="form-control"  data-validate-length-range="3,3"/>
                                       </div>
                                       </div>
                                    </div>    
                                    </div>
                           
                         
                        <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green">${lblsave}</button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default">${lblcancel}</button>                              
                           </div>
                        </div>
                     </form:form>
                     <!-- END FORM-->
                  </div>
               </div>
               <!-- END VALIDATION STATES-->
            </div>
         </div>
  
   


