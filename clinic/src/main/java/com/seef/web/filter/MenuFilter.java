package com.seef.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.SecurityUtils;

import com.seef.models.dto.UserSession;
import com.seef.models.menu.dto.Menu;
import com.seef.models.menu.dto.MenuList;

public class MenuFilter implements Filter{

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain filterChain) throws IOException, ServletException {
		if(SecurityUtils.getSubject().isAuthenticated()){
			request.setAttribute("MENULIST", SecurityUtils.getSubject().getSession().getAttribute("MENULIST"));
			String menuId = request.getParameter("menuId");
			if(null != menuId){
				try{
				initializeSubMenu(menuId,request);
				setSubMenuInRequest(menuId,request);
				}catch(Exception exception){
					// ignoring exception
				}
			}else{
				String removeMenu=request.getParameter("removesubmenu");
				if(null !=removeMenu){
					removeSubmenu(request);
				}else{
					setSubMenuInRequest(menuId,request);
				}
			}
		}
		
		filterChain.doFilter(request, response);
	}

	private void setSubMenuInRequest(String menuId, ServletRequest request) {
		UserSession userSession = (UserSession) SecurityUtils.getSubject().getPrincipal();
		if(null != userSession.getSubMenu()){
			request.setAttribute("NAVIGATION_MENU",userSession.getSubMenu());
		}
	}

	private void removeSubmenu(ServletRequest request) {
		UserSession userSession = (UserSession) SecurityUtils.getSubject().getPrincipal();
		userSession.setSubMenu(null); 
		request.removeAttribute("NAVIGATION_MENU");
	}

	private void initializeSubMenu(String menuId,ServletRequest request) {
		MenuList menuList=(MenuList) SecurityUtils.getSubject().getSession().getAttribute("MENULIST");
		Menu subMenu = menuList.getByMenuId(Integer.parseInt(menuId));
		request.setAttribute("NAVIGATION_MENU",subMenu);
		request.setAttribute("menuCode",menuId);
		UserSession userSession = (UserSession) SecurityUtils.getSubject().getPrincipal();
		userSession.setSubMenu(subMenu);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
