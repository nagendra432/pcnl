<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="datatables" uri="http://github.com/dandelion/datatables"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<spring:message code="label.view" var="lblView"/>
<spring:message code="label.edit" var="lblEdit"/>
<spring:message code="label.action" var="lblaction"/>
<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.name" var="lblname"/>
<spring:message code="label.phneno" var="lblphneno"/>
<spring:message code="label.view.pcnl.info" var="lblviewappointment"/>


<!-- BEGIN PAGE CONTENT-->

<div class="row">
	<div class="col-md-12">
		<c:if test="${page eq 'search'}">
		<div class="portlet box">
		<div class="portlet-body">
		<form id="form_sample_1" class="form-horizontal">
		<div class="row">
			<c:if test="${age ne 0}">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label label-align">Age : </label>
					<div class="col-md-6">
					   <input name="age" class="form-control" readonly="readonly" value="${age} Years"/>
					</div>
				</div>
			</div>
			</c:if>
			<c:if test="${gender ne ''}">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label label-align">Gender : </label>
					<div class="col-md-6">
					    <input name="gender" class="form-control" readonly="readonly" value="${gender}"/>
					</div>
				</div>
			</div>
			</c:if>
		</div>
		
		<div class="row">
			<c:if test="${surgeryType ne null}">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label label-align">Surgery Type : </label>
					<div class="col-md-6">
					   <input name="surgeryType" class="form-control" readonly="readonly" value="${surgeryType}"/>
					</div>
				</div>
			</div>
			</c:if>
			<c:if test="${surgery ne null}">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label label-align">Surgery : </label>
					<div class="col-md-6">
					    <input name="surgery" class="form-control" readonly="readonly" value="${surgery}"/>
					</div>
				</div>
			</div>
			</c:if>
		</div>
		
		<c:if test="${remarks  ne ''}">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5  control-label label-align">Remarks : </label>
					<div class="col-md-6">
					   <textarea name="remarks" class="form-control" readonly="readonly" value="${remarks}"></textarea>
					</div>
				</div>
			</div>
		</div>
		</c:if>
		<c:if test="${surgeon ne null}">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label label-align">Surgeon : </label>
					<div class="col-md-6">
					    <input name="surgeon" class="form-control" readonly="readonly" value="${surgeon}"/>
					</div>
				</div>
			</div>
			</c:if>
		<c:if test="${page eq 'search' && (age ne 0 || gender ne '' || surgeryType ne null || surgery ne null || remarks  ne '')}">
		<div class="form-actions fluid">
		    <div class="col-md-offset-5 col-md-7">
		       <button type="button" onclick="location.href='${pageContext.request.contextPath}/outpatient/outptsearch.htm';" class="btn green">Modify Search</button>                              
		    </div>
    	</div>
    	</c:if>
		</form>
		</div>
		</div>
		</c:if>

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-edit"></i>
					<spring:message code="label.resultsfor" />
					
					<spring:message code="header.outpatient.manage" />
					
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					 <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="col-lg-2 pull-right">
					<div class="table-toolbar pull-right">
						

					</div>
				</div>
				<datatables:table id="sample_editable_1" data="${modelList}" cdn="false" row="outpatient" cssClass="table table-striped table-bordered table-hover">
					<datatables:column title="${lblmrno}" property="mrno" sortInit="desc" />
					<datatables:column title="${lblname}" property="fullName"
						cssClass="hidden-480" cssCellClass="hidden-480" />	
				   <datatables:column title="${lblphneno}" property="phoneNo" />
					<datatables:column title="${lblaction}" sortable="false">
						<div class='hidden-phone visible-desktop btn-group'>
						 <form action="${pageContext.request.contextPath}/outpatient/surgery.htm"  class="form-horizontal" method="post">
							<a href="view.htm?id=${outpatient.id }" data-rel="tooltip"
								title="${lblView}"><i
								class="btn btn-default btn-sm my-table_btn icon-eye-open"></i></a> <a
								href="edit.htm?id=${outpatient.id}" data-rel="tooltip"
								title="${lblEdit}"><i
								class="btn btn-default btn-sm my-table_btn icon-edit"></i></a>
								   <input type="hidden" name="searchMrno" value="${outpatient.mrno}" id="searchMrno"  class="form-control mrnumber">
								   <a class="form-submit-button font-green-sharp bold" data-rel="tooltip"  style="color:red" title="${lblviewappointment}">
								   <i class="btn btn-default btn-sm my-table_btn icon-book"></i></a> 
							    </form>
								
						</div>

					</datatables:column>
				</datatables:table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->
<script type="text/javascript">
$(document).on("click", ".form-submit-button", function(){
    var $form = $(this).closest("form");
    $form.submit();
   });
</script>