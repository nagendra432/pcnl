package com.seef.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name = "LOGIN_HISTORY")
public class LoginHistory extends AbstractDomainObject {

	private static final long serialVersionUID = -5520868396779229625L;

	private Long id;
	
	private ApplicationUser applicationUser;
	
	private Date loginTime;
	
	private String ipAddress;
	
	private Date logoutTime;
	
	private boolean success;
	
	private String latituted;
	
	private String langitude;

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "LOGIN_HISTORY_SEQ")
	@SequenceGenerator(name = "LOGIN_HISTORY_SEQ", sequenceName = "LOGIN_HISTORY_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "LOGIN_TIME", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	@Column(name = "IP_ADDRESS", length = 100)
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name = "LOGOUT_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLogoutTime() {
		return logoutTime;
	}

	@Column(name="LATITUTED",length=20)
	public String getLatituted() {
		return latituted;
	}

	@Column(name="LANGITUTDE",length=20)
	public String getLangitude() {
		return langitude;
	}

	public void setLatituted(String latituted) {
		this.latituted = latituted;
	}

	public void setLangitude(String langitude) {
		this.langitude = langitude;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	@Column(name = "IS_SUCCESS")
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	public ApplicationUser getApplicationUser() {
		return applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

}
