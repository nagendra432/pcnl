package com.seef.dao.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.seef.dao.LookupDataDao;
import com.seef.models.base.AbstractDomainObject;
import com.seef.models.master.Language;
import com.seef.models.master.NotificationType;
import com.seef.models.master.PermissionGroup;

@Component("lookupDataDao")
public class LookupDataDaoImpl extends BaseJpaImpl<AbstractDomainObject, Long> implements LookupDataDao {



	@Override
	public Language findLangaugeByCode(String localeCode) {
		Map<String, Object> params=new HashMap<String, Object>(1);
		params.put("code", localeCode);
		return findOneByQuery("Language.findByCode", Language.class, params);
	}

	public List<PermissionGroup> findPermissionGroup(){
		return executeNamedQuery("permissiongroup.findAll", PermissionGroup.class);
	}
	
	public PermissionGroup findPermissionGroupById(Long permissionGId){
		return find(PermissionGroup.class,permissionGId);
	}
	
	public List<NotificationType> findNotificationTypes(){
		return findAll(NotificationType.class);
	}
	
	public NotificationType findNotificationTypeById(Long notificationTypeId){
		return findOne(notificationTypeId,NotificationType.class);
	}

	@Override
	public <K extends AbstractDomainObject> K findOneByQuery(String queryName, Class<K> clazz,Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K extends AbstractDomainObject> List<K> findAll(String queryName,Class<K> clazz, Map<String, Object> params) {
		return null;
	}

	
	public boolean isExist(Object value,String queryName){
		Map<String, Object> params=new HashMap<String, Object>(1);
		params.put("value", value);
		return isExist(queryName, params);
	}

	public <K extends AbstractDomainObject> K getById(Class<K> clazz, Long id){
		return find(clazz, id);
	}
	
	public <K extends AbstractDomainObject> List<K> getAll(Class<K> clazz) {
		return findAll(clazz);
	}
	
	public <K extends AbstractDomainObject> List<K> findByCriteria( Class<K> clazz, Map<String, Object> criterias){
		return findByCriteria(clazz, criterias);
	}
	
	@Override
	public AbstractDomainObject save(AbstractDomainObject entity) {
		return super.save(entity);
	}



	
}
