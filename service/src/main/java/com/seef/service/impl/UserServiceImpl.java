package com.seef.service.impl;

import org.springframework.stereotype.Component;

import com.seef.models.SystemUser;
import com.seef.service.UserService;

@Component("userService")
public class UserServiceImpl extends BaseServiceImpl<SystemUser, Long> implements UserService{
	
}
