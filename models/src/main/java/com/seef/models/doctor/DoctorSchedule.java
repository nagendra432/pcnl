package com.seef.models.doctor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name="DOCTOR_SCHEDULE")
@NamedQuery(name="listbyscheduledate",query="select  schedule from DoctorSchedule schedule where schedule.status IN ('ACTIVE','NEW') and DATE(schedule.scheduledDate)=DATE(:date)")
public class DoctorSchedule extends AbstractDomainObject{

	private static final long serialVersionUID = 7395438232287104597L;
	
	private Long id;
	private Doctor doctor;
	private Date scheduledDate;
	private String inTime;
	private String outTime;
	private String noOfAppointments;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "DOCTOR_SCHEDULE_SEQ")
	@SequenceGenerator(name = "DOCTOR_SCHEDULE_SEQ", sequenceName = "DOCTOR_SCHEDULE_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "DOCTOR_ID")
	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	@Column(name = "SCHEDULED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}
	

	@Column(name = "IN_TIME")
	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	@Column(name = "OUT_TIME")
	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}

	@Column(name = "NO_OF_APPOINTMENTS")
	public String getNoOfAppointments() {
		return noOfAppointments;
	}

	public void setNoOfAppointments(String noOfAppointments) {
		this.noOfAppointments = noOfAppointments;
	}
}
