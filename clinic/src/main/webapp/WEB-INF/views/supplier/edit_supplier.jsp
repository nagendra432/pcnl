<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">
               <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i><spring:message code="header.supplier"/></div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>
                  </div>
                  
              <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model">
	                       <form:hidden path="id"/>
	                       <div style="color:red">                      		
		                    <form:errors path="*" cssClass="errorcollection"/>
		                  </div> 
                  <div class="form-body">
                           <div class="alert alert-danger display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formerrormsg"/>
                           </div>
                           <div class="alert alert-success display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formsuccessmsg"/>
                           </div>
                   </div> 
                   
                   <div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.name"/><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="name"  class="form-control"  />
                                       </div>
                                       </div>
                                       
                    <div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.company"/><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="company"  class="form-control"  />
                                       </div>
                                       </div> 
                   
                      <div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.phoneno"/><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="phoneNo"  class="form-control"  />
                                       </div>
                                       </div> 
                   
                      <div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.mailId"/><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="mailId"  class="form-control"  />
                                       </div>
                                       </div> 
                   <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green"><spring:message code="button.save"/></button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default"><spring:message code="button.cancel"/></button>                              
                           </div>
                        </div>
                   
                   </form:form>
                   </div>
                   </div>
                   </div>
                   </div>
                   
