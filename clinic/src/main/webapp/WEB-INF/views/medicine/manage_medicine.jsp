<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="datatables"
	uri="http://github.com/dandelion/datatables"%>


<spring:message code="label.view" var="lblView"/>
<spring:message code="label.edit" var="lblEdit"/>
<spring:message code="label.action" var="lblaction"/>
	<spring:message code="label.medicinename" var="lblmedicinename"/>
	<spring:message code="label.company" var="lblcompany"/>
	<spring:message code="label.medicinetype" var="lblmedicinetype"/>
	<spring:message code="label.medicinepackaging" var="lblmedicinepackaging"/>
<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<spring:message code="header.medicine.manage" />
			
		</h3>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
	<div class="col-md-12">

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					
					<spring:message code="header.medicine.manage" />
					
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					 <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="col-lg-2 pull-right">
					
				<div class="table-toolbar pull-right">
	              <div class="btn-group">
	                <a href="create.htm"><button id="sample_editable_1_new" class="btn green"><spring:message code="button.addnew"/><i class="icon-plus"></i> </button></a>
	              </div>
	           	</div>
				</div>
				<datatables:table id="sample_editable_1" data="${modelList}" cdn="false" row="medicine" cssClass="table table-striped table-bordered table-hover">
					<datatables:column title="${lblmedicinename}" property="medicineName" />
					<datatables:column title="${lblcompany}" property="company"
						cssClass="hidden-480" cssCellClass="hidden-480" />	
				   <datatables:column title="${lblmedicinetype}" property="medicineType.type" />
					<datatables:column title="${lblaction}" sortable="false">
						<div class='hidden-phone visible-desktop btn-group'>
							<a href="view.htm?id=${medicine.id }" data-rel="tooltip"
								title="${lblView}"><i
								class="btn btn-default btn-sm my-table_btn icon-eye-open"></i></a> <a
								href="edit.htm?id=${medicine.id}" data-rel="tooltip"
								title="${lblEdit}"><i
								class="btn btn-default btn-sm my-table_btn icon-edit"></i></a> 
						</div>

					</datatables:column>
					<datatables:column title="${lblpackaging}" sortable="false">
      <a href="${pageContext.request.contextPath}/medicinepackaging/manage.htm?id=${medicine.id}" data-rel="tooltip" title="${lblmedicinepackaging}">
      <i class="btn btn-default btn-sm my-table_btn icon-briefcase"></i>
      </a>
          
      
     </datatables:column>
				</datatables:table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->