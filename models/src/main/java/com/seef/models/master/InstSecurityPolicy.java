package com.seef.models.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;
@Entity
@Table(name="INST_SECURITYPOLICY")
public class InstSecurityPolicy extends AbstractDomainObject {

	private static final long serialVersionUID = 6034525354766697989L;

	private Long id;
	
	private String policyName;
	
	private String description;
	
	private Long pwdMinLength;
	
	private Long pwdMaxLength;
	
	private Long pwdMinSpecialChar;
	
	private Long pwdMinUpperChar;
	
	private Long pwdMinNumChar;

	private Long numOldPwd;
	
	private Long pwdExpDate;
	
	private Long custMinTpin;
	
	private Long custMaxTpin;
	
	private Long custMinMpin;

	private Long custMaxMpin;
	
	private Long allowedDeviceRegCust;
	
	private Long allowedDeviceRegAgt;
	
	private Integer pwdAttempts;
	
	private Long tpinAttempts;
	
	private Long mpinAttempts;
	
	private boolean sameMpinTpin;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "INST_SECURITYPOLICY_SEQ")
	@SequenceGenerator(name = "INST_SECURITYPOLICY_SEQ", sequenceName = "INST_SECURITYPOLICY_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="SAMEMPINTPIN")
	public boolean isSameMpinTpin() {
		return sameMpinTpin;
	}
	public void setSameMpinTpin(boolean sameMpinTpin) {
		this.sameMpinTpin = sameMpinTpin;
	}
	@Column(name="PWD_ATTEMPTS")
	public Integer getPwdAttempts() {
		return pwdAttempts;
	}
	public void setPwdAttempts(Integer pwdAttempts) {
		this.pwdAttempts = pwdAttempts;
	}
	@Column(name="TPIN_ATTEMPTS")
	public Long getTpinAttempts() {
		return tpinAttempts;
	}
	public void setTpinAttempts(Long tpinAttempts) {
		this.tpinAttempts = tpinAttempts;
	}
	@Column(name="MPIN_ATTEMPTS")
	public Long getMpinAttempts() {
		return mpinAttempts;
	}
	public void setMpinAttempts(Long mpinAttempts) {
		this.mpinAttempts = mpinAttempts;
	}
	
	@Column(name="POLICYNAME")
	public String getPolicyName() {
		return policyName;
	}
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="PWD_MIN_LENGTH")
	public Long getPwdMinLength() {
		return pwdMinLength;
	}
	public void setPwdMinLength(Long pwdMinLength) {
		this.pwdMinLength = pwdMinLength;
	}
	@Column(name="PWD_MAX_LENGTH")
	public Long getPwdMaxLength() {
		return pwdMaxLength;
	}
	public void setPwdMaxLength(Long pwdMaxLength) {
		this.pwdMaxLength = pwdMaxLength;
	}
	@Column(name="PWD_MIN_SPECIALCHAR")
	public Long getPwdMinSpecialChar() {
		return pwdMinSpecialChar;
	}
	public void setPwdMinSpecialChar(Long pwdMinSpecialChar) {
		this.pwdMinSpecialChar = pwdMinSpecialChar;
	}
	@Column(name="PWD_MIN_UPPERCHAR")
	public Long getPwdMinUpperChar() {
		return pwdMinUpperChar;
	}
	public void setPwdMinUpperChar(Long pwdMinUpperChar) {
		this.pwdMinUpperChar = pwdMinUpperChar;
	}
	@Column(name="PWD_MIN_NUMCHAR")
	public Long getPwdMinNumChar() {
		return pwdMinNumChar;
	}
	public void setPwdMinNumChar(Long pwdMinNumChar) {
		this.pwdMinNumChar = pwdMinNumChar;
	}
	@Column(name="NUM_OLDPWD")
	public Long getNumOldPwd() {
		return numOldPwd;
	}
	public void setNumOldPwd(Long numOldPwd) {
		this.numOldPwd = numOldPwd;
	}
	@Column(name="PWD_EXPDATE")
	public Long getPwdExpDate() {
		return pwdExpDate;
	}
	public void setPwdExpDate(Long pwdExpDate) {
		this.pwdExpDate = pwdExpDate;
	}
	@Column(name="CUST_MINTPIN")
	public Long getCustMinTpin() {
		return custMinTpin;
	}
	public void setCustMinTpin(Long custMinTpin) {
		this.custMinTpin = custMinTpin;
	}
	@Column(name="CUST_MAXTPIN")
	public Long getCustMaxTpin() {
		return custMaxTpin;
	}
	public void setCustMaxTpin(Long custMaxTpin) {
		this.custMaxTpin = custMaxTpin;
	}
	@Column(name="CUST_MINMPIN")
	public Long getCustMinMpin() {
		return custMinMpin;
	}
	public void setCustMinMpin(Long custMinMpin) {
		this.custMinMpin = custMinMpin;
	}
	@Column(name="CUST_MAXMPIN")
	public Long getCustMaxMpin() {
		return custMaxMpin;
	}
	public void setCustMaxMpin(Long custMaxMpin) {
		this.custMaxMpin = custMaxMpin;
	}
	@Column(name="ALLOW_DEVICE_REGCUST")
	public Long getAllowedDeviceRegCust() {
		return allowedDeviceRegCust;
	}
	public void setAllowedDeviceRegCust(Long allowedDeviceRegCust) {
		this.allowedDeviceRegCust = allowedDeviceRegCust;
	}
	@Column(name="ALLOW_DEVICE_REGAGT")
	public Long getAllowedDeviceRegAgt() {
		return allowedDeviceRegAgt;
	}
	public void setAllowedDeviceRegAgt(Long allowedDeviceRegAgt) {
		this.allowedDeviceRegAgt = allowedDeviceRegAgt;
	}
	
}
