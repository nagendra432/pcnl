package com.seef.models.menu.dto;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="serviceMenu")
public class ServiceMenu extends BaseMenu implements Serializable {

	private static final long serialVersionUID = 5038106608412330880L;
	
	private String permissionCode;
	private boolean removed;


	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	public void filter(Collection<String> permissions) {
		this.removed=true;
		String[] permissionList=this.permissionCode.split(",");
		for(String pCode : permissionList){
			if(permissions.contains(pCode)){
				this.removed=false;
				break;
			}
		}
	}


}
