package com.seef.service;

import java.util.List;

import com.seef.models.pharmacy.MedicinePackagingDetails;

public interface StockDetailsService {
	public List<MedicinePackagingDetails> stock_list();
	public List<MedicinePackagingDetails> expiry_list();
	public MedicinePackagingDetails getById(Long id);
}
