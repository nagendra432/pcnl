package com.seef.service;

import java.util.List;
import java.util.Map;

import com.seef.models.ApplicationUser;
import com.seef.models.Appointment;
import com.seef.models.doctor.DoctorSchedule;

public interface AppointmentService 
{
	
	public void saveOrUpdateAppointment(Appointment appointment,ApplicationUser applicationUser);
	public void saveAppointment(Appointment appointment,ApplicationUser applicationUser);
	public List<Appointment> Appointment_list(Map<String,Object> criteria);
	public List<Appointment> Appointment_byDate(Map<String,Object> criteria);
	public Appointment getById(Long id);
	public  int sameDayAppointmentCheck(Map<String,Object> criteria);
	public List<DoctorSchedule> Doctors_listbyDate(Map<String,Object> criteria);
	public List<Appointment> DoctorsSchedule_listbyDate(Map<String,Object> criteria);  
}
