package com.seef.service.impl;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.qos.logback.core.boolex.Matcher;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.SystemUser;
import com.seef.models.enums.UserStatus;
import com.seef.service.ForcePasswordService;
import com.seef.service.PasswordManager;

@Service
public class ForcePasswordServiceImpl implements ForcePasswordService{
	
	private static final Logger _log = LoggerFactory.getLogger(ForcePasswordServiceImpl.class);
	
	Pattern pattern=null;
	Matcher matcher=null;
	boolean result=true;
	
	@Autowired
	private GenericDAO genericDAO;

	@Autowired
	private PasswordManager passwordManager;

	public void setGenericDAO(GenericDAO genericDAO) {
		this.genericDAO = genericDAO;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public void update(ApplicationUser sysUser, String newPassword){
		sysUser.setPassword(newPassword);
		passwordManager.encryptPassword(sysUser);
		sysUser.setUserStatus(UserStatus.ACTIVE);
		sysUser.setResetPassword(false);
		sysUser.setLoginAttempts(0);
		genericDAO.update(sysUser);	
		_log.info(">>>message:Password SuccessFully Updated ");
	}

	@Override
	public boolean validateOldPassword(ApplicationUser sysUser, String oldPassword) {
		String hashedPassword = passwordManager.getEncryptedPassword(sysUser.getId(), oldPassword);
		if(sysUser.getPassword().equals(hashedPassword)) return true;
		else return false;
	}

	@Override
	public String getEncryptedPwd(SystemUser sysUser, String cnfpwd) {
		sysUser.setPassword(cnfpwd);
		passwordManager.encryptPassword(sysUser);
		return sysUser.getPassword();
		
	}
	
	@Override
	public String validateConformPassword(String conformPassword){
		
		String error=new String();
		
		if(!(conformPassword.length()>8L)){
			error="error.label.min.length";
		}
		
		
		 
		return error;
	}

}
