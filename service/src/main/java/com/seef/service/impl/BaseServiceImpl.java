package com.seef.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.BaseDao;
import com.seef.models.base.AbstractDomainObject;
import com.seef.service.BaseService;

public abstract class BaseServiceImpl<T extends AbstractDomainObject, ID extends Serializable> implements BaseService<T, ID> {


	 
	@Transactional(propagation = Propagation.REQUIRED) 
	public T save(T entity) {
		return getDao().save(entity);
	}
	
	

	@Transactional(propagation = Propagation.REQUIRED)
	public T findOne(ID id) {
		return getDao().find(id);
	}
	

	@Transactional(propagation = Propagation.REQUIRED)
	public T update(T entity) {
		return getDao().update(entity);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(ID id) {
		getDao().delete(id);
	}
	
	 
	public List<T> findAll() {
		Class<T> clazz = getDao().getPersistentClass();
		return getDao().findAll(clazz);
	}
	
	public void flush(){
		getDao().flush();
	}
	
	public BaseDao<T, ID> getDao(){
		return null;
	}


	@Override
	public T find(ID id) {
		return getDao().find(id);
	}


	@Override
	public T getReference(ID id) {
		return getDao().getReference(id);
	}

	
	/*************************************
	/*** Added by SRK, 17th June, 2013 ***
	**************************************/	

	public T getByCriteria(Class<T> clazz, Map<String, Object> criterias){
		return getDao().getOneByCriteria(clazz, criterias);
	}
	 
	 public List<T> findByCriteria(Class<T> clazz, Map<String, Object> criterias, String sortField, boolean desc){
		return getDao().findAllByCriteria(clazz, criterias, sortField, desc);
	 }
	 
	 public List<T> findByCriteria(Class<T> clazz, Map<String, Object> criterias){
		return getDao().findAllByCriteria(clazz, criterias); 
	 }
	 
	 public T getByQuery(String jpaQuery){
		 return getDao().getByQuery(jpaQuery);
	 }
	 
	 public <K extends AbstractDomainObject> List<K> findAll(String queryName, Class<K> clazz, Map<String, Object> params){
		 return getDao().findAll(queryName, clazz, params);
	 }



	@SuppressWarnings("unchecked")
	@Override
	public Object find(@SuppressWarnings("rawtypes") Class clazz, ID id) {
		return getDao().find(clazz, id);
	}




	 

}
