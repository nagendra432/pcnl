package com.seef.models.doctor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.DoctorType;

@Entity
@Table(name="DOCTOR")
public class Doctor extends AbstractDomainObject {

	private static final long serialVersionUID = 5889677940234175406L;
	
	private Long id;
	private String name;
	private String qualification;
	private String designation;
	private String specilization;
	private DoctorType doctorType;

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "DOCTOR_SEQ")
	@SequenceGenerator(name = "DOCTOR_SEQ", sequenceName = "DOCTOR_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "QUALIFICATION")
	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	@Column(name = "DESIGNATION")
	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Column(name = "DOCTOR_TYPE")
	@Enumerated(EnumType.STRING)
	public DoctorType getDoctorType() {
		return doctorType;
	}

	public void setDoctorType(DoctorType doctorType) {
		this.doctorType = doctorType;
	}

	@Column(name = "SPECILIZATION")
	public String getSpecilization() {
		return specilization;
	}

	public void setSpecilization(String specilization) {
		this.specilization = specilization;
	}

}
