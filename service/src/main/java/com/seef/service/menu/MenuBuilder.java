package com.seef.service.menu;

import java.util.Collection;

import com.seef.models.menu.dto.MenuList;

public interface MenuBuilder {

	public void setMenuConfigFile(String menuConfigFile);
	public MenuList buildAdminMenu(Collection<String> permission);	
	public MenuList buildAgencyMenu(Collection<String> permission);
	
}
