package com.seef.dao.jpa;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.seef.dao.GenericDAO;
import com.seef.models.base.AbstractDomainObject;
import com.seef.models.base.Status;
import com.seef.models.enums.UserStatus;

@Repository
public class GenericDAOImpl extends BaseJpaImpl<AbstractDomainObject, Long> implements GenericDAO {
	
	@Override
	public EntityManager getEntityManager() {
		return super.getEntityManager();
	}

	@Override
	public <K extends AbstractDomainObject> K getById(Class<K> clazz, Long id){
		return super.find(clazz, id);
	}
	
	@Override
	public <K extends AbstractDomainObject> K  getByCriteria(Class<K> clazz, Map<String, Object> criterias){
		return getOneByCriteria(clazz, criterias);
	}		
	
	@Override
	public <K extends AbstractDomainObject> List<K> findByCriteria( Class<K> clazz, Map<String, Object> criterias){
		return findAllByCriteria(clazz, criterias);
	}
	
	@Override
	public <K extends AbstractDomainObject> List<K> findAll(Class<K> clazz) {
		return super.findAll(clazz);
	}
	
	@Override
	public AbstractDomainObject save(AbstractDomainObject entity) {
		return super.save(entity);
	}

	@Override
	public AbstractDomainObject saveOrUpdate(AbstractDomainObject entity) {
		if(entity.getId() == null)
			return super.save(entity);
		else
			return super.update(entity);
	}
	
	@Override
	public <T extends AbstractDomainObject> boolean isUnique(Class<T> clazz, T entity, Map<String, Object> properties) {
		return super.isUnique(clazz, entity, properties);
	}

	@Override
	public String executeQuerySingleString(String queryName, Map<String, Object> params){
		return super.executeQuerySingleString(queryName, params);
	}
	
	public <K extends AbstractDomainObject> List<K> findByNamedQuery(String namedQuery, Map<String,Object> map){
		return super.findByNamedQuery(namedQuery, map);
	}
	
	public <T extends AbstractDomainObject> T getById(Class<T> clazz, AbstractDomainObject entity){
		return super.getById(clazz, entity);
	}
	
	public <K extends AbstractDomainObject> List<K> executeNamedQuery(String queryName,Class<K> clazz,Map<String, Object> params){
		return super.executeNamedQuery(queryName, clazz, params);
	}
	
	public <K extends AbstractDomainObject> List<K> searchByQuery(Class<K> clazz, int noOfRecord, String query, String countQuery) {
		return super.searchByQuery(clazz, noOfRecord, query, countQuery);
	}
	
	public <T extends AbstractDomainObject> List<T> executeSimpleQuery(String query){
		return super.executeSimpleQuery(query);
	}

	@Override
	public <K extends AbstractDomainObject> List<K> findByCriteria(Class<K> clazz, Map<String, Object> criterias, String sortField, boolean desc) {
		return super.findAllByCriteria(clazz, criterias, sortField, desc);
	}
	
	public Long executeQuerySinglequery(String queryName, Map<String, Object> params){
		return super.executeQuerySinglequery(queryName, params);
	}
	
	public UserStatus executeQueryStatusString(String queryName, Map<String, Object> params){
		return super.executeQueryStatusString(queryName, params);
	}
	
	@Override
	public <K extends AbstractDomainObject> K findOneNativeQuery(String query,Map<String,Object> params,Class<K> clazz){
		return super.findOneNativeQuery(query,params,clazz);
	}
	/*public  String  findStatus(Class<T> clazz, Long id){
		return super.findStatus(clazz, id);
	}*/

}
