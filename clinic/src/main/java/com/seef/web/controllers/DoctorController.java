package com.seef.web.controllers;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.models.doctor.Doctor;
import com.seef.models.enums.DoctorType;
import com.seef.models.enums.UserType;
import com.seef.service.BaseService;
import com.seef.service.DoctorService;


@Controller
@RequestMapping("/doctor")
public class DoctorController extends CRUDController<Doctor> {
	
	private static final Logger _log = LoggerFactory.getLogger(DoctorController.class);
	
	
	@Autowired
	DoctorService doctorService;
	
	@Autowired
	MessageSource messageSource;
	
	
	@Override
	 public void setupCreate(ModelMap modelMap,HttpServletRequest request)  
	{
		modelMap.addAttribute("doctorType", DoctorType.list());		
	}

	@Override
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request) {
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("doctor.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("create.doctor.menu.name", null, userLocale));
		modelMap.addAttribute("model", new Doctor());
		modelMap.addAttribute("designation", UserType.getDesgnation());
		setupCreate(modelMap, request);
		return "tileDefinition.create-"+getNameSpace();
	} 
	
	
	

	@Override
	@RequestMapping(value="save.htm",method = RequestMethod.POST)
	public String save(@ModelAttribute("model") Doctor model,HttpServletRequest request, HttpServletResponse response,BindingResult bindingResult, ModelMap modelMap) {

		 if(model.getId() == null){
			 
			 if(!validateCreate(model, bindingResult))
			 {
			     setupCreate(modelMap, request);
				 return "tileDefinition.create-" + getNameSpace();
			 }
			 else
			 {
				doctorService.saveDoctor(model, getLoggedInUser());
				 return getRedirectURL("/manage.htm");
			 }
			
			
	     }else{
	    	 if(!validateUpdate(model, bindingResult))
	    	 {
			     setupCreate(modelMap, request);
				 return "tileDefinition.create-" + getNameSpace();
			 }
			 else
			 {
				 doctorService.saveOrUpdateDoctor(model, getLoggedInUser());
				 return getRedirectURL("/manage.htm");
			 }
	    	 
	     }

	}
	
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("doctor.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.doctor.menu.name", null, userLocale));
		modelMap.addAttribute("modelList", doctorService.doctor_list());
		return "tileDefinition.manage-"+getNameSpace();
	}
		

	

		
	@Override
	public boolean validateUpdate(Doctor model, BindingResult result) {
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		Doctor olddoctor=doctorService.getById(model);
		criteria.clear();
		criteria.put("name",model.getName());
			
		if(!(olddoctor.getName().equalsIgnoreCase(model.getName())))
		{
			if(!doctorService.validateUnique(model,criteria)){
				FieldError error=new FieldError("modelObject","name",model.getName(), false, null, null, messageSource.getMessage("label.doctor.error.codeMessage",null,userLocale));
				result.addError(error);
				validationResult = false;
			}
		}
		
		return validationResult;
	}


	@Override
	public boolean validateCreate(Doctor entity, BindingResult bindingResult) {
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		criteria.put("name", entity.getName());
		if(!doctorService.validateUnique(entity, criteria))
		{
			 FieldError error=new FieldError("modelObject","name",entity.getName(), false, null, null, messageSource.getMessage("label.doctor.error.codeMessage",null,userLocale));
			   bindingResult.addError(error);
			   validationResult = false;
			
		}
		return validationResult;
	}
	@Override
	@RequestMapping(value = "/edit.htm", method = RequestMethod.GET)
	public String edit(ModelMap modelMap, HttpServletRequest request) {
		modelMap.addAttribute("designation", UserType.getDesgnation());
		Long id = Long.valueOf(request.getParameter("id"));
		Doctor doctor=doctorService.getById(id);
		setupCreate(modelMap, request);
		modelMap.addAttribute("model", doctor);
		return "tileDefinition.create-"+getNameSpace();
	}

	@Override
	public String view(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Long id = Long.valueOf(request.getParameter("id"));
		_log.info(">>>>> id is:"+id);
		Doctor doctor=doctorService.getById(id);
		modelMap.addAttribute("model", doctor);
		setupCreate(modelMap, request);
		modelMap.addAttribute("disabledStatus", "true");
		return "tileDefinition.view-"+getNameSpace();
	}
	
	@Override
	public BaseService<Doctor, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "doctor";
	}
	@Override
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	 
	 super.initBinder(binder);
	}
	
	

}
