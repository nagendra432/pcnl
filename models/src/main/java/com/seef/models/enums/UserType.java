package com.seef.models.enums;

import java.util.ArrayList;
import java.util.List;

public enum UserType
{
	SUPER_ADMIN, 
	SYSTEM_USER,
	ADMIN, 
	DOCTOR,
	NURSE,
	SURGEON,
	AsstSURGEON,
	ANESTHESIOLOGIST,
	SCRUBNURSE,
	
	CUSTOMER, 
	CUSTOMER_ADMIN,
	AGENT, 
	AGENT_ADMIN, 
	IAGENT,
	IAGENT_ADMIN, 
	
	AGENCY_ADMIN, 
	CORPORATE_ADMIN,
	MERCHANT_ADMIN,
	CASHPOINT,
	CASHPOINT_ADMIN,
	AGENCY,
	AGENCY_USER,
	BRANCH_TELLER,
	BILLER,
	CORPORATE,
	MERCHANT;

	
	public static List<UserType> getDesgnation(){
		List<UserType> userTypes = new ArrayList<UserType>();
		userTypes.add(UserType.SURGEON);
		userTypes.add(UserType.AsstSURGEON);
		userTypes.add(UserType.ANESTHESIOLOGIST);
		userTypes.add(UserType.NURSE);
		userTypes.add(UserType.SCRUBNURSE);
		return userTypes;
	}
	
	public static List<UserType> getUserTypeForFormula(){
		List<UserType> userTypes = new ArrayList<UserType>();
		userTypes.add(UserType.CUSTOMER);
		userTypes.add(UserType.AGENT);
		userTypes.add(UserType.AGENCY);
		userTypes.add(UserType.IAGENT);
		userTypes.add(UserType.BILLER);
		userTypes.add(UserType.MERCHANT);
		userTypes.add(UserType.CORPORATE);
		return userTypes;
	}
	
}
