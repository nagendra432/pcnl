package com.seef.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.base.Status;
import com.seef.models.pharmacy.Medicine;
import com.seef.service.MedicineService;

@Service
public class MedicineServiceImpl  implements MedicineService{

private static final Logger _log = LoggerFactory.getLogger(OutPatientServiceImpl.class);
	
	@Autowired
	GenericDAO genericDAO;

	@Override
	public Boolean validateUnique(Medicine medicine, Map<String, Object> map) {
		// TODO Auto-generated method stub
		return genericDAO.isUnique(Medicine.class,medicine,map);
	}

	@Override
	public List<Medicine> medicine_list() {
		// TODO Auto-generated method stub
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("status", Status.ACTIVE);
		return genericDAO.findAllByCriteria(Medicine.class, params);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveMedicine(Medicine medicine, ApplicationUser applicationUser) {
		// TODO Auto-generated method stub
		medicine.setStatus(Status.ACTIVE);
		medicine.setCreatedBy(applicationUser);
		medicine.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(medicine);
		_log.info(">>>>>>> Save Successfully");
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveOrUpdateMedicine(Medicine medicine,
			ApplicationUser applicationUser) {
		// TODO Auto-generated method stub
		medicine.setModifiedBy(applicationUser);
		medicine.setModifiedOn(Calendar.getInstance().getTime());
		genericDAO.saveOrUpdate(medicine);
		_log.info(">>>>>>> Updated Successfully");
		
	}

	@Override
	public Medicine getById(Medicine medicine) {
		// TODO Auto-generated method stub
		return genericDAO.getById(Medicine.class, medicine);
	}

	@Override
	public Medicine getById(Long id) {
		// TODO Auto-generated method stub
		return genericDAO.getById(Medicine.class,id);
	}

}
