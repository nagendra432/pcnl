<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<%@ page import="java.io.*,java.util.Locale" %>
<%@ page import="javax.servlet.*,javax.servlet.http.* "%>
<%
Locale locale = request.getLocale();
String localeCode=locale.getLanguage();
   pageContext.setAttribute("lcCode", localeCode);
%>
 
<head>
   <meta charset="utf-8" />
   <title>PCNL-LoginPortal</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <meta name="MobileOptimized" content="320">
   	   <link rel="shortcut icon"  href="${pageContext.request.contextPath}/assets/img/57X57.ico">    
   	      
       <c:if test="${lcCode eq 'ar' }">
       
      <link href="${pageContext.request.contextPath}/assets/rtl/css/login-soft-rtl.css" rel="stylesheet" type="text/css"/>
      <link href="${pageContext.request.contextPath}/assets/rtl/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
     <link href="${pageContext.request.contextPath}/assets/rtl/css/style-rtl.css" rel="stylesheet" type="text/css"/>
     <link href="${pageContext.request.contextPath}/assets/rtl/css/style-metronic-rtl.css" rel="stylesheet" type="text/css"/>
     <link href="${pageContext.request.contextPath}/assets/rtl/css/style-responsive-rtl.css" rel="stylesheet" type="text/css"/>
     <link href="${pageContext.request.contextPath}/assets/rtl/css/plugins-rtl.css" rel="stylesheet" type="text/css"/> 
     <link href="${pageContext.request.contextPath}/assets/rtl/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
      <link href="${pageContext.request.contextPath}/assets/css/themes/default-rtl.css" rel="stylesheet" type="text/css" id="style_color"/> 
    
   	   </c:if>
   	   <c:if test="${lcCode ne 'ar'}">
   	   
        <link href="${pageContext.request.contextPath}/assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet" type="text/css"/>
      <link href="${pageContext.request.contextPath}/assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
      <link href="${pageContext.request.contextPath}/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
      <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
      <link href="${pageContext.request.contextPath}/assets/css/custom.css" rel="stylesheet" type="text/css"/>
      <link href="${pageContext.request.contextPath}/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
   
   	   </c:if>
   
   <!-- BEGIN GLOBAL MANDATORY STYLES -->          
   <link href="${pageContext.request.contextPath}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <link href="${pageContext.request.contextPath}/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL STYLES --> 
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/select2/select2_metro.css" />
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- BEGIN THEME STYLES --> 

   
      
    
   <!-- END THEME STYLES -->
   <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
   <!-- BEGIN LOGO -->
   <div class="logo">
   <div class="content top">
      <img src="${pageContext.request.contextPath}/assets/img/setupadmin.png" alt="" width="250" height="120"/> 
      </div>
   </div>
   <!-- END LOGO -->
   <!-- BEGIN LOGIN -->
   <div class="content" style="background-color: #696969">
      <!-- BEGIN LOGIN FORM -->
     
     <form class="login-form" id="LOGINFORM" action="login.htm" method="post">
         <h3 class="form-title">${logintoyouraccount}</h3>
         <%
           String infoMessage = (String)request.getAttribute("infoMessage");
           if(infoMessage != null){
         %>
         <div class="alert alert-danger show">
          <span for="password" class=""><%= infoMessage %></span>
           <!--  <button class="close" data-dismiss="alert"></button> -->
         </div>
         <%
           }
         %>
         <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9"> <spring:message code="label.admin.username"/></label>
            <div class="input-icon">
               <i class="icon-user"></i>
               <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="<spring:message code="label.admin.username"/>" name="userName"/>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9"><spring:message code="label.password"/></label>
            <div class="input-icon">
               <i class="icon-lock"></i>
               <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="<spring:message code="label.password"/>" name="password"/>
            </div>
         </div>
         <div class="form-actions action">
           <%-- <a href="${pageContext.request.contextPath}/signup.htm" class="btn blue">
            signUp
            </a> --%> 
           <button type="submit" class="btn blue pull-right">
            <spring:message code="label.login"/> <i class="m-icon-swapright m-icon-white"></i>
            </button>
                       
         </div>
        
         <div class="forget-password">
            <h4><spring:message code="label.ForgetPassword"/></h4>
            <p>
             <a href="javascript:;"  id="forget-password"><spring:message code="label.toresetyourpassword"/></a>
            </p>
         </div>
         <div class="create-account">
           
         </div>
      </form>
      <!-- END LOGIN FORM -->        
       <!-- BEGIN FORGOT PASSWORD FORM -->
      <form class="forget-form" action="index.html" method="post">
         <h3 >${ForgetPassword}</h3>
         <p>${EnteryourUserNamebelowtoresetyourpassword}</p>
         <div class="form-group">
            <div class="input-icon">
               <i class="icon-user"></i>
               <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="${username}" name="userName" />
            </div>
         </div>
         <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
            <i class="m-icon-swapleft"></i>${back}
            </button>
            <button type="submit" class="btn blue pull-right">
            ${submit} <i class="m-icon-swapright m-icon-white"></i>
            </button>            
         </div>
      </form>
      <!-- END FORGOT PASSWORD FORM -->
      
      <!-- BEGIN Security Qustions FORM -->
      <form class="security-form" action="index.html" method="post" style="display: none;">
         <h3 >${secutityform}</h3>
         <div class="form-group">
            <label class="control-label">${question1}</label>
            <div class="input-icon">
               
               <input class="form-control placeholder-no-fix" type="text" autocomplete="off"  name="securityQuestions0" />
            </div>
         </div>
         <div class="form-group">
            <label class="control-label">${question2}</label>
            <div class="input-icon">
              
               <input class="form-control placeholder-no-fix" type="text" autocomplete="off"  name="securityQuestions1" />
            </div>
         </div>
         <div class="form-actions">
            <button type="button" id="back-btn1" class="btn">
            <i class="m-icon-swapleft"></i>${back}
            </button>
            <button type="submit" class="btn blue pull-right">
           ${submit}<i class="m-icon-swapright m-icon-white"></i>
            </button>            
         </div>
      </form>
      <!-- END Security Qustions FORM -->
   </div>
   <!-- END LOGIN -->
   <!-- BEGIN COPYRIGHT -->
   <div class="copyright">
   <div class="content top">
     &copy;  <spring:message code="label.allrightsreserved"/>
      </div>
   </div>
   <!-- END COPYRIGHT -->
   <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
   <!-- BEGIN CORE PLUGINS -->   
   <!--[if lt IE 9]>
   <script src="${pageContext.request.contextPath}/assets/plugins/respond.min.js"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/excanvas.min.js"></script> 
   <![endif]-->   
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/select2/select2.min.js"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="${pageContext.request.contextPath}/assets/scripts/app.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/scripts/login-soft.js" type="text/javascript"></script>      
   <!-- END PAGE LEVEL SCRIPTS --> 
   <script>
      jQuery(document).ready(function() {     
        App.init();
        Login.init();
        
        
        
      });
   </script>
   <script type="text/javascript">
    $(function (){
    	//$.cookie('intro_show', null, { path: '/' });
    	//$.removeCookie("intro_show"); 
    });
   
   </script>
   <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>