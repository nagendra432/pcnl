package com.seef.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.seef.models.audit.Audit;
import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.UserStatus;
import com.seef.models.enums.UserType;

@Entity
@Table(name="APPLICATION_USER")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="USER_TYPE",discriminatorType=DiscriminatorType.STRING,length=255)
@NamedQueries({
@NamedQuery(name="userStstus",query="select user.userStatus from ApplicationUser user where user.id=:userId")})
public abstract class ApplicationUser extends AbstractDomainObject implements Audit {

	private static final long serialVersionUID = -1630451619113050708L;
	
	protected Long id;
	protected String firstName;
	protected String lastName;
	protected String username;
	protected String password;
	protected String mobileNumber;
	protected String email;
	protected Address address;
	protected UserStatus userStatus;
	protected Date lastLoginTime;
	protected Date lastLogoutTime;
	protected Date lastFailedLoginTime;
	protected Preferences	preferences;
	protected String menuVersion="0";
	protected Integer loginAttempts = 0;
	protected boolean resetPassword;

	
	protected Role role;
	protected List<SecurityQuestion> securityQuestions = new ArrayList<SecurityQuestion>(0);
	protected List<SecurityQuestion> userSecurityQuestion;
	
	protected String uid;
	protected UserType userType;
	protected String customerId;
    private String userImageName;

	public ApplicationUser(){

	}

	public ApplicationUser(Long id){
		this.id=id;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "APPLICATION_USER_SEQ")
	@SequenceGenerator(name = "APPLICATION_USER_SEQ", sequenceName = "APPLICATION_USER_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUserImageName(String userImageName) {
		this.userImageName = userImageName;
	}
	
	@Column(name = "USER_IMAGE_NAME")
	public String getUserImageName() {
		return userImageName;
	}

	@Column(name = "FIRST_NAME")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "LAST_NAME")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "PASSWORD")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "MOBILE_NUMBER")
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(name = "EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name = "ADDRESS_ID")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Column(name="USER_STATUS",length=20,nullable=false)
	@Enumerated(EnumType.STRING)
	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	@Column(name = "LAST_LOGIN_TIME")
	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	@Column(name = "LAST_LOGOUT_TIME")
	public Date getLastLogoutTime() {
		return lastLogoutTime;
	}

	public void setLastLogoutTime(Date lastLogoutTime) {
		this.lastLogoutTime = lastLogoutTime;
	}

	@Column(name = "LAST_FAILED_LOGIN")
	public Date getLastFailedLoginTime() {
		return lastFailedLoginTime;
	}

	public void setLastFailedLoginTime(Date lastFailedLoginTime) {
		this.lastFailedLoginTime = lastFailedLoginTime;
	}

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="PREFERENCE_ID")
	public Preferences getPreferences() {
		return preferences;
	}

	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}

	@Column(name="MENU_VERSION")
	public String getMenuVersion() {
		return menuVersion;
	}

	public void setMenuVersion(String menuVersion) {
		this.menuVersion = menuVersion;
	}

	@Column(name="LOGIN_ATTEMPTS")
	public Integer getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(Integer loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	@ManyToOne
	@JoinColumn(name = "ROLE")
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Transient
	public List<SecurityQuestion> getSecurityQuestions() {
		return securityQuestions;
	}

	public void setSecurityQuestions(List<SecurityQuestion> securityQuestions) {
		this.securityQuestions = securityQuestions;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinTable(name="MUSER_SECURITY_QUESTION",inverseJoinColumns={@JoinColumn(name="SECURITY_QUESTION_ID")}, joinColumns={@JoinColumn(name="MOBILE_USER_ID")}) 
	public List<SecurityQuestion> getUserSecurityQuestion() {
		return userSecurityQuestion;
	}

	public void setUserSecurityQuestion(List<SecurityQuestion> userSecurityQuestion) {
		this.userSecurityQuestion = userSecurityQuestion;
	}

	@Transient
	public String getName(){
		return this.firstName + " " + this.lastName;
	}

	@Column(name="RESET_PASSWORD")
	public boolean isResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(boolean resetPassword) {
		this.resetPassword = resetPassword;
	}


	@Column(name = "USERNAME")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "UUID")
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@Override
	@Transient
	public List<String> getAuditableFields() {
		List<String> props = new ArrayList<String>();
		props.add("firstName");
		props.add("lastName");
		props.add("mobileNumber");
		props.add("email");
		props.add("lastLoginTime");
		props.add("lastLogoutTime");
		props.add("userStatus");

		return props;
	}

	@Override
	@Transient
	public String getAuditMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Enumerated(EnumType.STRING)
	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}
	
	@Column(name = "CUSTOMER_ID")
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
