package com.seef.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.Surgery;
import com.seef.models.SurgeryType;
import com.seef.models.base.Status;
import com.seef.service.SurgeryService;
import com.seef.service.SurgeryTypeService;
@Service
public class SurgeryTypeSeviceImpl implements SurgeryTypeService {
	
	private static final Logger _log = LoggerFactory.getLogger(SurgeryTypeSeviceImpl.class);
	
	@Autowired
	GenericDAO genericDAO;
	
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void saveSurgeryType(SurgeryType surgery, ApplicationUser applicationUser) {
		if(surgery.getId()==null){
			surgery.setStatus(Status.ACTIVE);
			surgery.setCreatedBy(applicationUser);
			surgery.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(surgery);
		_log.info(">>>>>Save Successfully");
		
		}else{
			surgery.setModifiedBy(applicationUser);
			surgery.setModifiedOn(Calendar.getInstance().getTime());
			genericDAO.saveOrUpdate(surgery);
		}
	}

	@Override
	public Boolean validateUnique(SurgeryType surgery, Map<String, Object> map) {
		return genericDAO.isUnique(Surgery.class, map);
	}

	@Override
	public List<SurgeryType> getAllSurgeryTypes() {
		return genericDAO.findAll(SurgeryType.class);
	}

	@Override
	public SurgeryType findById(Long id) {
		return genericDAO.find(SurgeryType.class, id);
	}

}
