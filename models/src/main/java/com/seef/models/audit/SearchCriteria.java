package com.seef.models.audit;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A backing bean for the main hotel search form. Encapsulates the criteria needed to perform a hotel search.
 */
public class SearchCriteria implements Serializable {

	private static final long serialVersionUID = -7198692825105236518L;
	
	/**
     * The user-provided search criteria for finding Hotels.
     */
    private Map searchMap;
    private Map requestParams;
    private String orderBy;
    private boolean desc;
    
    public SearchCriteria() {
    	searchMap = new HashMap();
    }
    /**
     * The maximum page size of the Hotel result list
     */
    private int pageSize = 25;
    
    private int recordCount=0;
    
    private int pageCount=0;

    /**
     * The current page of the Hotel result list.
     */
    private int page;
    

    /**
	 * @return the searchMap
	 */
	public Map getSearchMap() {
		return searchMap;
	}
	
	/**
	 * @return the requestParams
	 */
	public Map getRequestParams() {
		return requestParams;
	}


	/**
	 * @param requestParams the requestParams to set
	 */
	public void setRequestParams(Map requestParams) {
		this.requestParams = requestParams;
	}


	/**
	 * @param searchMap the searchMap to set
	 */
	public void setSearchMap(Map searchMap) {
		this.searchMap = searchMap;
	}

	public int getPageSize() {
	return pageSize;
    }

    public void setPageSize(int pageSize) {
    	this.pageSize = pageSize;
    }

    public int getPage() {
    	return page;
    }

    public void setPage(int page) {
    	this.page = page;
    }

    public void nextPage() {
    	page++;
    }

    public void previousPage() {
    	page--;
    }

    public void resetPage() {
    	page = 0;
    }

    /**
	 * @return the pageCount
	 */
	public int getPageCount() {
		return ((recordCount % pageSize)==0)?(recordCount/pageSize):(recordCount/pageSize)+1;
	}
	
	/**
	 * @return the recordCount
	 */
	public int getRecordCount() {
		return recordCount;
	}

	/**
	 * @param recordCount the recordCount to set
	 */
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	/**
	 * @param pageCount the pageCount to set
	 */
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public String toString() {
		StringBuffer paramString=new StringBuffer("");
		if (getSearchMap()!=null) {
			for(Object param:getSearchMap().keySet()) {
				if (getSearchMap().get(param.toString())!=null)
					paramString.append(param+"="+getSearchMap().get(param.toString())).append("&");
			}
		}
    	return paramString.toString();
    }

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public boolean isDesc() {
		return desc;
	}

	public void setDesc(boolean desc) {
		this.desc = desc;
	}
}