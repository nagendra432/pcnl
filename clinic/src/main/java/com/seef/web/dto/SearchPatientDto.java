package com.seef.web.dto;

import java.io.Serializable;

import com.seef.models.Surgery;
import com.seef.models.SurgeryType;
import com.seef.models.enums.Gender;

public class SearchPatientDto implements Serializable {

	private static final long serialVersionUID = 8200932159888272055L;
	private int age;
	private Gender gender;
	private String remarks;
	private Surgery surgery;
	private SurgeryType surgeryType;
	private Long surgeon;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Surgery getSurgery() {
		return surgery;
	}

	public void setSurgery(Surgery surgery) {
		this.surgery = surgery;
	}

	public SurgeryType getSurgeryType() {
		return surgeryType;
	}

	public void setSurgeryType(SurgeryType surgeryType) {
		this.surgeryType = surgeryType;
	}

	public Long getSurgeon() {
		return surgeon;
	}

	public void setSurgeon(Long surgeon) {
		this.surgeon = surgeon;
	}

}
