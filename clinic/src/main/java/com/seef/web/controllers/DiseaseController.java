package com.seef.web.controllers;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.models.ApplicationUser;
import com.seef.models.Disease;
import com.seef.models.dto.UserSession;
import com.seef.service.DiseaseService;

@Controller
@RequestMapping("/disease")
public class DiseaseController {
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	DiseaseService diseaseService;
	
	
	@RequestMapping("/create.htm")
	public String create(ModelMap modelMap, HttpServletRequest request){
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masterdata.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.disease", null, userLocale));
		modelMap.addAttribute("model", new Disease());	
		return "tileDefinition.create-disease";
		
	}
	
	@RequestMapping(value="save.htm",method = RequestMethod.POST)
	public String save(@ModelAttribute("model") Disease model,HttpServletRequest request, HttpServletResponse response,BindingResult bindingResult, ModelMap modelMap) {

		 if(model.getId() == null){
			 
			 if(!validateCreate(model, bindingResult))
			 {
				 return "tileDefinition.create-disease" ;
			 }
			 else
			 {
				 diseaseService.saveDisease(model, getLoggedInUser());
				 return "redirect:/disease/manage.htm";
			 }
		 }
		return "tileDefinition.create-disease";
	}
	
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
			Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
			request.getSession().setAttribute("nav", messageSource.getMessage("masterdata.menu.name", null, userLocale));
			request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.disease", null, userLocale));
			modelMap.addAttribute("modelList", diseaseService.getAllDiseases());
		    return "tileDefinition.manage-disease";
	}
	
	public ApplicationUser getLoggedInUser() {
		UserSession userSession = (UserSession) SecurityUtils.getSubject() .getPrincipal();
		if(userSession != null)
			return userSession.getApplicationUser();
		else
			return null;
	}
	
	public boolean validateCreate(Disease entity, BindingResult bindingResult) {
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		criteria.put("name", entity.getName());
		if(!diseaseService.validateUnique(entity, criteria))
		{
			 FieldError error=new FieldError("modelObject","name",entity.getName(), false, null, null, messageSource.getMessage("label.doctor.error.codeMessage",null,userLocale));
			   bindingResult.addError(error);
			   validationResult = false;
			
		}
		return validationResult;
	}

}
