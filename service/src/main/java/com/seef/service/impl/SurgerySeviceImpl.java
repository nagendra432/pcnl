package com.seef.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.Surgery;
import com.seef.models.base.Status;
import com.seef.service.SurgeryService;
@Service
public class SurgerySeviceImpl implements SurgeryService {
	
	private static final Logger _log = LoggerFactory.getLogger(SurgerySeviceImpl.class);
	
	@Autowired
	GenericDAO genericDAO;
	
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void saveSurgery(Surgery surgery, ApplicationUser applicationUser) {
		if(surgery.getId()==null){
			surgery.setStatus(Status.ACTIVE);
			surgery.setCreatedBy(applicationUser);
			surgery.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(surgery);
		_log.info(">>>>>Save Successfully");
		
		}else{
			surgery.setModifiedBy(applicationUser);
			surgery.setModifiedOn(Calendar.getInstance().getTime());
			genericDAO.saveOrUpdate(surgery);
		}
	}

	@Override
	public Boolean validateUnique(Surgery surgery, Map<String, Object> map) {
		return genericDAO.isUnique(Surgery.class, map);
	}

	@Override
	public List<Surgery> getAllsurgeries() {
		return genericDAO.findAll(Surgery.class);
	}
	
    @Override
    @Transactional
	public List<Surgery> findBySurgeryType(Long typeID){
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("surgeryTypeId", typeID);
		try{
		    return genericDAO.executeNamedQuery("surgeryByType", Surgery.class, params);
		}catch(java.lang.IndexOutOfBoundsException e){
			return new ArrayList<Surgery>();
		}
	}

	@Override
	public Surgery findById(Long id) {
		return genericDAO.find(Surgery.class, id);
	}
}
