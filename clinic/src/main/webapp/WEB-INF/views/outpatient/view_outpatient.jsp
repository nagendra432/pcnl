<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.outpatient.firstname" var="lblfirstname"/>
<spring:message code="label.outpatient.middlename" var="lblmiddlename"/>
<spring:message code="label.outpatient.lastname" var="lbllastname"/>
<spring:message code="label.outpatient.phno" var="lblphno"/>
<spring:message code="label.address.line1" var="lbladdress1"/>
<spring:message code="label.address.line2" var="lbladdress2"/>
<spring:message code="label.address.zipcode" var="lblzipcode"/>
<spring:message code="label.address.city" var="lblcity"/>
<spring:message code="label.address.country" var="lblcountry"/>
<spring:message code="label.address.state" var="lblstate"/>
<spring:message code="label.address.fulladdress" var="lblfulladdress"/>
<spring:message code="label.outpatient.dob" var="lbldob"/>
<spring:message code="label.outpatient.time" var="lbltime"/>
<spring:message code="label.appointmentdate" var="lblappointmentdate"/>
<spring:message code="label.consultationtype" var="lblconsultationtype"/>
<spring:message code="label.doctors" var="lbldoctors"/>
<spring:message code="label.fee" var="lblfee"/>



<form:form  id="form_sample_1" class="form-horizontal" commandName="model" >
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-cogs"></i>Patient Details</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">FirstName</label>
						<div class="col-md-6">
							<form:input path="firstName" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">LastName</label>
						<div class="col-md-6">
							 <form:input path="lastName" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Birth Date</label>
						<div class="col-md-6">
						   <form:input path="dob" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Gender</label>
						<div class="col-md-6">
						    <form:input path="gender" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Contact Number</label>
						<div class="col-md-6">
						   <form:input path="phoneNo" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">MR No</label>
						<div class="col-md-6">
							<form:input path="mrno" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="portlet box blue">
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label">Surgery Type</label>
						<div class="col-md-6">
						   	<select name="surgeryType" class="form-control select2me" id="surgeryTypeID" disabled="true">
							    <c:forEach items="${surgeryType}" var="surgerytype">
							      <option value="${surgerytype.id}">${surgerytype.name}</option>
							    </c:forEach>
					       </select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
 					<div class="form-group">
					<label class="control-label col-md-5">Surgery</label>
						<div class="col-md-6">
 							<form:select path="surgery" class="form-control select2me" id="surgeryID" disabled="true">
							<form:option value="">-- select --</form:option>
							<c:forEach items="${surgeries}" var="surgery">
								<form:option value="${surgery.id}"><spring:message code="${surgery.name}"/></form:option>
							</c:forEach>
							</form:select>
     					</div>
  					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label">Anes</label>
						<div class="col-md-6">
						   	<select name="anes" class="form-control select2me" disabled="true">
							    <c:forEach items="${anes}" var="ane">
							      <option value="${ane.type}">${ane.type}</option>
							    </c:forEach>
					       </select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
 					<div class="form-group">
					<label class="control-label col-md-5">ADTNL Procedure</label>
						<div class="col-md-6">
 							<select name="adtnlprocedure" class="form-control select2me" disabled="true">
							<option value="">-- select --</option>
							<c:forEach items="${adnlProcedures}" var="adnlProcedure">
								<option value="${adnlProcedure.id}"><spring:message code="${adnlProcedure.name}"/></option>
							</c:forEach>
							</select>
     					</div>
  					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align"></label>
						<div class="col-md-6">
						   <input type="checkbox" id="right" checked="checked" disabled="true"> <label for="right">Stones Left Side </label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align"></label>
						<div class="col-md-6">
							<input type="checkbox" id="left" disabled="true"> <label for="left">Stones Right Side</label>
						</div>
					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Number</label>
						<div class="col-md-6">
							<form:input path="stone[0].number"  class="form-control" disabled="true"/>
						</div>
					</div>
					
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Number</label>
						<div class="col-md-6">
							<form:input path="stone[1].number"  class="form-control" disabled="true"/>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Size(cm) 1</label>
						<div class="col-md-6">
							<!-- <input type="text"  name="size1" class="form-control" > -->
							<form:input path="stone[0].size"  class="form-control"  disabled="true"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label label-align">Size(cm) 2</label>
					<div class="col-md-6">
						<!-- <input type="text"  name="size2" class="form-control" > -->
						<form:input path="stone[1].size"  class="form-control" disabled="true"/>
					</div>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Energy</label>
						<div class="col-md-6">
							<input type="text"  name="energy" class="form-control" value="Energy" disabled="true">
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align"></label>
						<div class="col-md-6">
						   <input type="checkbox" id="right" checked="checked" disabled="true"> <label for="right">DJ Stent Left Side </label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align"></label>
						<div class="col-md-6">
							<input type="checkbox" id="left" disabled="true"> <label for="left">DJ Stent Right Side</label>
						</div>
					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Size</label>
						<div class="col-md-6">
						   <form:input path="dJStent[0].size"  class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Size</label>
						<div class="col-md-6">
						   <form:input path="dJStent[1].size"  class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Dangler</label>
						<div class="col-md-6">
							<input type="text"  name="dangler" class="form-control" value="Dangler" disabled="true">
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label">Surgeon</label>
						<div class="col-md-6">
						   	<select name="surgeon" class="form-control select2me" disabled="true">
							<option value="">-- select --</option>
							<c:forEach items="${surgeons}" var="surgeon">
								<option value="${surgeon.id}"><spring:message code="${surgeon.username}"/></option>
							</c:forEach>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
 					<div class="form-group">
					<label class="control-label col-md-5">Asst Surgeon</label>
						<div class="col-md-6">
 							<select name="asstSurgeon" class="form-control select2me" disabled="true">
							<option value="">-- select --</option>
							<c:forEach items="${asstSurgeons}" var="asstSurgeon">
								<option value="${asstSurgeon.id}"><spring:message code="${asstSurgeon.username}"/></option>
							</c:forEach>
							</select>
     					</div>
  					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-3 control-label label-align">Remarks</label>
						<div class="col-md-8">
							<form:textarea path="remarks" rows="5" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-3 control-label label-align">Complications</label>
						<div class="col-md-8">
							<textarea rows="5" name="Complications" class="form-control" disabled="true"></textarea>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="form-actions fluid">
	    <div class="col-md-offset-5 col-md-7">
	       <button type="button" onclick="javascript:history.go(-1);" class="btn default">Cancel</button>                              
	    </div>
    </div> 
	</form:form>
		
<script>
    $("#myModal").modal({                   
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                   
    });
</script>


   <script type="text/javascript">
      jQuery(document).ready(function() { 
    	  FormComponents.init(); 
         FormValidation.init();
         $("#scheduledate").change(function(){
        	 var str = $(this).val();
             $.get("getdoctors.htm?date="+ str,function(data) {
                $('select#doctors').html('<option value="">-- Select --</option>');
              $.each(data,function(index) {
               $('select#doctors').append('<option value="'+data[index].id+'">'+ data[index].name+ '</option>');
              });
                          
              });
        	  
        	});
         
         
             });
      </script>
      



<script type="text/javascript">  
$(document).ready(function() {  
$('#types').change(function(){  
if($('#types').val() === 'CONSULTATION')  
   {  
   $('#other').show();   
   }  
else 
   {  
   $('#other').hide();     
   }  
});  
});  
</script>
