<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
   
   <style>
   	.invoice .invoice-block {
  text-align:right !important;
  margin-top:2% !important;
}
   </style>
   
   
   
   <!-- BEGIN PAGE CONTENT-->
         <div class="invoice">
            <div class="row invoice-logo">
               <div class="col-xs-6 invoice-logo-space"><img src="${pageContext.request.contextPath}/assets/img/setupadmin.png" alt=""  style="width:130px;"/> </div>
               <div class="col-xs-6">
                  <p>BillNo: ${model.billNo} </p>
                  <p> <span id="time"></span></p>
               </div>
            </div>
            <hr />
            <div class="row">
               <div class="col-xs-12">
                  <table class="table table-striped table-hover">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Item</th>
                           <th class="hidden-480">Batch No</th>
                           <th class="hidden-480">Quantity</th>
                           <th class="hidden-480">Unit Cost</th>
                           <th>Total</th>
                        </tr>
                     </thead>
                     <tbody>
                     <c:set var="count" value="1" scope="page" />
                     
                       <c:forEach items="${model.prescriptions}" var="prescriptions">
				         <tr>
                           <td>${count}</td>
                           <td>${prescriptions.medicine.medicineName}</td>
                           <td class="hidden-480">${prescriptions.medicinePackagingDetails.batchNo}</td>
                           <td class="hidden-480">${prescriptions.units}</td>
                           <td class="hidden-480">${prescriptions.medicinePackagingDetails.cost}</td>
                           <td>${prescriptions.cost}</td>
                           <c:set var="count" value="${count + 1}" scope="page"/>
                        </tr>
				      </c:forEach>
                     </tbody>
                  </table>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-4">
                  <div class="well">
                     <address>
                     <strong>Address:</strong>
                        C.K House,<br /> 3rd Floor, MIG 65, <br />
                        Dharma Reddy Colony Phase-II,<br />
                         Hydernagar, Hyderabad-72.
                     </address>
                  </div>
               </div>
               <div class="col-xs-8 invoice-block">
                  <ul class="list-unstyled amounts">
                     <li><strong>Sub - Total amount:</strong> ${model.totalCost}</li>
                     <li><strong>Discount:</strong> 0%</li>
                     <li><strong>VAT:</strong> -----</li>
                     <li><strong>Grand Total:</strong>Rs. ${model.totalCost}</li>
                  </ul>
                  <br />
                  <a class="btn blue hidden-print" onclick="javascript:window.print();">Print <i class="icon-print"></i></a>
                 
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT-->
         
         <script >
   var today = new Date();
   /* document.getElementById("time").innerHTML =today.toDateString(); */
   document.getElementById("time").innerHTML =today.toUTCString();
   </script>