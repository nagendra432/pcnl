package com.seef.service;

import java.util.List;




import com.seef.models.ApplicationUser;
import com.seef.models.Appointment;
import com.seef.models.pharmacy.MedicinePackagingDetails;

public interface MedicinePackagingService {
	public List<MedicinePackagingDetails> medicinePackaging_list(Long id);
	public List<MedicinePackagingDetails> medicinePackaging_stocklist(Long id);
	public void saveMedicinePackaging(MedicinePackagingDetails medicinepackaging,ApplicationUser applicationUser);
	public void saveOrUpdateMedicinePackaging(MedicinePackagingDetails medicinepackaging,ApplicationUser applicationUser);
	
}
