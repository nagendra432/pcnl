package com.seef.models.core.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Error implements Serializable{

	private static final long serialVersionUID = -8093626155431345363L;
	
	private String code;
	private String message;
	
	public Error(){
		
	}
	
	public Error(String erroCode){
		this.code=erroCode;
	}
	
	public Error(String code,String message){
		this.code=code;
		this.message=message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}


}
