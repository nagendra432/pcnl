<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="datatables"
	uri="http://github.com/dandelion/datatables"%>
	
	<spring:message code="label.medicinename" var="lblmedicinename"/>
	<spring:message code="label.stock.batchno" var="lblbatchno"/>
	<spring:message code="label.stock.receiveddate" var="lblreceiveddate"/>
	<spring:message code="label.stock.expirydate" var="lblexpirydate"/>
	<spring:message code="label.stock.units" var="lblunits"/>
	<spring:message code="label.stock.supplier" var="lblsupplier"/>
	
	
	
	<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<spring:message code="header.stock.adjustment" />
			
		</h3>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->



<div class="row">
	<div class="col-md-12">

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-edit"></i>
					<spring:message code="label.resultsfor" />
					
					<spring:message code="header.stock.adjustment" />
					
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					 <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
			
			
			<datatables:table id="sample_editable_1" data="${modelList}" cdn="false" row="medicinepackaging" cssClass="table table-striped table-bordered table-hover">
					<datatables:column title="${lblmedicinename}" property="medicine.medicineName" />
					<datatables:column title="${lblbatchno}" property="batchNo"></datatables:column>
					<datatables:column title="${lblreceiveddate}" property="received"></datatables:column>
					<datatables:column title="${lblexpirydate}" property="expiryDate"></datatables:column>
					<datatables:column title="${lblunits}" property="units"></datatables:column>
					<datatables:column title="${lblsupplier}" property="supplier.name"></datatables:column>
                    <datatables:column title="${lblaction}" sortable="false">
						<div class='hidden-phone visible-desktop btn-group'>
							<a href="close.htm?id=${medicinepackaging.id }" data-rel="tooltip"
								title="${lblAdjust}">
								<i	class="btn btn-default btn-sm my-table_btn icon-ok-sign"></i></a>
					   </div>
					</datatables:column>
				</datatables:table>
			
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
	<!-- END PAGE CONTENT -->