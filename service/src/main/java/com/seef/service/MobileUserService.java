package com.seef.service;

import com.seef.models.ApplicationUser;

public interface MobileUserService {

	public ApplicationUser getMobileUserByUsername(String username);
	public ApplicationUser getMobileUserByMobile(String mobileNumber, Long institutionId);
	public ApplicationUser getMobileUserByUID(String uid);
	public ApplicationUser getMobileUserById(Long id);
}
