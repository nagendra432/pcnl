package com.seef.web.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.models.ApplicationUser;
import com.seef.models.Surgery;
import com.seef.models.SurgeryType;
import com.seef.models.doctor.Doctor;
import com.seef.models.dto.UserSession;
import com.seef.service.SurgeryService;
import com.seef.service.SurgeryTypeService;
import com.seef.web.editors.AbstractModelEditor;

@Controller
@RequestMapping("/surgery")
public class SurgeryController {
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	SurgeryService surgeryService;
	
	@Autowired
	SurgeryTypeService surgeryTypeService;
	
	
	@RequestMapping("/create.htm")
	public String create(ModelMap modelMap, HttpServletRequest request){
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masterdata.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.surgery", null, userLocale));
		modelMap.addAttribute("model", new Surgery());	
		modelMap.addAttribute("surgeryTypes", surgeryTypeService.getAllSurgeryTypes());
		return "tileDefinition.create-surgery";
		
	}
	
	@RequestMapping(value="save.htm",method = RequestMethod.POST)
	public String save(@ModelAttribute("model") Surgery model,HttpServletRequest request, HttpServletResponse response,BindingResult bindingResult, ModelMap modelMap) {
System.out.println("surgery type"+model.getSurgeryType().getId());
		 if(model.getId() == null){
			 
			 if(!validateCreate(model, bindingResult))
			 {
				 modelMap.addAttribute("surgeryTypes", surgeryTypeService.getAllSurgeryTypes());
				 return "tileDefinition.create-surgery" ;
			 }
			 else
			 {
				 model.setSurgeryType(surgeryTypeService.findById(model.getSurgeryType().getId()));
				 surgeryService.saveSurgery(model, getLoggedInUser());
				 return "redirect:/surgery/manage.htm";
			 }
		 }
		return "tileDefinition.create-surgery";
	}
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
			Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
	    	request.getSession().setAttribute("nav", messageSource.getMessage("masterdata.menu.name", null, userLocale));
		    request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.surgery", null, userLocale));
			modelMap.addAttribute("modelList", surgeryService.getAllsurgeries());
			return "tileDefinition.manage-surgery";
	}
	public ApplicationUser getLoggedInUser() {
		UserSession userSession = (UserSession) SecurityUtils.getSubject() .getPrincipal();
		if(userSession != null)
			return userSession.getApplicationUser();
		else
			return null;
	}
	
	public boolean validateCreate(Surgery entity, BindingResult bindingResult) {
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		criteria.put("name", entity.getName());
		if(!surgeryService.validateUnique(entity, criteria))
		{
			 FieldError error=new FieldError("modelObject","name",entity.getName(), false, null, null, messageSource.getMessage("label.doctor.error.codeMessage",null,userLocale));
			   bindingResult.addError(error);
			   validationResult = false;
			
		}
		return validationResult;
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(SurgeryType.class, new AbstractModelEditor(SurgeryType.class));
	}

}
