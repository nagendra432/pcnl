package com.seef.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name = "SURGERY")
@NamedQueries({@NamedQuery(name="surgeryByType",query="select s from Surgery s where s.surgeryType.id=:surgeryTypeId")})
//@NamedNativeQueries({@NamedNativeQuery(name="surgeryByType",query="select s.ID,s.NAME,s.DISCRIPTION,s.SURGERYTYPE_ID SURGERYTYPEID from SURGERY s where s.SURGERYTYPE_ID=:surgeryTypeId",resultClass=Surgery.class)})
//@NamedNativeQueries({@NamedNativeQuery(name="nativeSurgeryByType",query="select s.id,s.name,s.discription from Surgery s JOIN SurgeryType ss on s.SURGERYTYPE_ID=ss.id where ss.id=:surgeryTypeId",resultClass=Surgery.class)})
public class Surgery extends AbstractDomainObject {

	private static final long serialVersionUID = 3712352796839526791L;
	private Long id;
	private String name;
	private String discription;
	private SurgeryType surgeryType;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "DISCRIPTION")
	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "SURGERYTYPE_ID")
	public SurgeryType getSurgeryType() {
		return surgeryType;
	}

	public void setSurgeryType(SurgeryType surgeryType) {
		this.surgeryType = surgeryType;
	}


}
