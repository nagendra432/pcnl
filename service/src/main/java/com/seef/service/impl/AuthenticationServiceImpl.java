package com.seef.service.impl;


import java.sql.Timestamp;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.core.GenericException;
import com.seef.models.enums.UserStatus;
import com.seef.service.AuthenticationService;
import com.seef.service.MobileUserService;
import com.seef.service.PasswordHistoryService;
import com.seef.service.PasswordManager;
import com.seef.service.SystemUserService;

@Service("authenticationService")
public class AuthenticationServiceImpl implements AuthenticationService{

	Logger logger=LoggerFactory.getLogger(AuthenticationServiceImpl.class);

	@Autowired
	GenericDAO genericDAO;

	@Autowired
	PasswordManager passwordManager;

	@Autowired
	MobileUserService mobileUserService;

	@Autowired
	SystemUserService systemUserService;	


	@Autowired
	PasswordHistoryService passwordHistoryService;


	@Transactional(propagation=Propagation.REQUIRED, noRollbackFor=GenericException.class)
	public void updateUserLastLoginTime(ApplicationUser user, Timestamp currTime) {
		user.setLastLoginTime(currTime);
		user.setLoginAttempts(0);
		genericDAO.save(user);		
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void updateUserLastFailedLogin(ApplicationUser applicationUser, Timestamp currTime) {
		currTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
		if(applicationUser!=null)
		{
			applicationUser.setLastFailedLoginTime(currTime);
			applicationUser.setLoginAttempts(applicationUser.getLoginAttempts()+1);
			if (applicationUser.getLoginAttempts() == 3) { //TODO: To get LoginAttempts from Security Policy
				applicationUser.setUserStatus(UserStatus.LOCKED);
			}
			genericDAO.update(applicationUser);
		}
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void updateUserLastLogoutTime(ApplicationUser applicationUser, Timestamp currTime) {
		applicationUser.setLastLogoutTime(currTime);
		applicationUser.setLoginAttempts(0);
		genericDAO.save(applicationUser);

	}

	
}
