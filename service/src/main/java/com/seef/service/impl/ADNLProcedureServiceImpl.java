package com.seef.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ADNLProcedure;
import com.seef.models.ApplicationUser;
import com.seef.models.Disease;
import com.seef.models.base.Status;
import com.seef.service.ADNLProcedureService;
import com.seef.service.DiseaseService;

@Service
public class ADNLProcedureServiceImpl implements ADNLProcedureService {
	
	private static final Logger _log = LoggerFactory.getLogger(ADNLProcedureServiceImpl.class);

	@Autowired
	GenericDAO genericDAO;
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void saveADNLProcedure(ADNLProcedure procedure, ApplicationUser applicationUser) {
		if(procedure.getId()==null){
			procedure.setStatus(Status.ACTIVE);
			procedure.setCreatedBy(applicationUser);
			procedure.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(procedure);
		_log.info(">>>>>Save Successfully");
		
		}else{
			procedure.setModifiedBy(applicationUser);
			procedure.setModifiedOn(Calendar.getInstance().getTime());
			genericDAO.saveOrUpdate(procedure);
		}

	}

	@Override
	public Boolean validateUnique(ADNLProcedure procedure,Map<String, Object> map) {
		return genericDAO.isUnique(ADNLProcedure.class, map);
	}
	
	@Override
	public List<ADNLProcedure> getAllADNLProcedures(){
		return genericDAO.findAll(ADNLProcedure.class);
	}

}
