package com.seef.service;

import com.seef.models.ApplicationUser;
import com.seef.models.SystemUser;

public interface PasswordManager {
	
	public void encryptPassword(SystemUser applicationUser);
	public void encryptPassword(ApplicationUser user);

	
	public boolean verifyPassword(String password, SystemUser user);	
	public boolean verifyPassword(String password, ApplicationUser user);
	
		
	public boolean verify(String actual,String salt,String provided);	
	public String encrypt(String plain,String salt);
	
	public String prepareSalt(String suffix);
	public String prepareSalt(Long salt);	
	public String getEncryptedPassword(Long id, String plainTextPassword);

}
