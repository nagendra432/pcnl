package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;

public enum UserStatus {
	
	ACTIVE, BLOCKED, LOCKED, INACTIVE,DELETED,REJECTED,NEW;
	
	public static List<UserStatus> list() 
	  {
	    return Arrays.asList(values());
	  }
}
