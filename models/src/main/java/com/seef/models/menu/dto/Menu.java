package com.seef.models.menu.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="menu")
public class Menu extends BaseMenu implements Serializable {
	
	private static final long serialVersionUID = 6213956862421339556L;
	
	private boolean removed;
	private List<ServiceMenu> serviceMenu;
	private List<Menu> menus;
	private boolean parent;


	@XmlElement(name="serviceMenu")
	public List<ServiceMenu> getServiceMenu() {
		return serviceMenu;
	}

	@XmlElement(name="menu")
	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public void setServiceMenu(List<ServiceMenu> serviceMenu) {
		this.serviceMenu = serviceMenu;
	}

	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	public void filter(Collection<String> permissions) {
		this.removed=false;
		this.parent=false;
		boolean hasChildren=false;
		if(null!=this.serviceMenu){
			for(ServiceMenu serviceMenu : this.serviceMenu){
				serviceMenu.filter(permissions);
				if(!serviceMenu.isRemoved()){
					hasChildren=true;
				}
			}
		}
		
		if(null != this.menus){
			for(Menu menu : this.menus){
				menu.filter(permissions);
				if(!menu.isRemoved()){
					hasChildren=true;
				}
			}
		}
		
		if(!hasChildren){
			this.removed=true;
		}else{
			this.parent=true;
		}
		
	}

	public boolean isParent() {
		return parent;
	}

	public void setParent(boolean parent) {
		this.parent = parent;
	}

}
