<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:message code="label.user.oldpassworddata" var="oldpass" />
<spring:message code="label.user.newpassworddata" var="newpass" />
<spring:message code="label.user.confirmpassword" var="confimpass" />
<spring:message code="button.save" var="savelabel" />
<spring:message code='button.cancel' var="cancelLabel" />
<spring:message code="error.header" var="title" />

<script type="text/javascript">
      $(document).ready(function() {
      $("#insert").validate({
    	  errors:{
    		  oldPassword:{
    			  required: true
    		  },
    		  newPassword: {
    			  required: true,
    			  notequal : true
    		  },
    		  confirmPassword:{
    			  required: true
    		  },
    	  },
          messages:{
        	  oldPassword:{
        		  required: 'Old Password Required'
        	  },  
        	  newPassword:{
    	    	  required: 'New Password Required',
    	    	  notequal: 'New and Old Password Should not be same'
    	      },
    	      confirmPassword:{
    	    	  required: 'Confirm Password Required',
    	    	  equalTo:  'Confirm password didnot match with new password'
    	      },
          }
      });
      
      $.validator.addMethod("notequal", function(value, element) {
    	  return $('#oldPassword').val() != $('#newPassword').val();} 
    	  );
      });
</script> 

 <div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">

 <div class="portlet-body form">
                     <!-- BEGIN FORM-->

<form:form action="updatenewpassword.htm" name="passwordForm" class="form-horizontal"  method="post" id="insert">
			<div style="color: #ff0000;">
				<span  class="errorMessage">${errorheader}</span>
				<span  class="errorMessage">${msg}</span>
				<c:forEach items="${error}" var="error"><span class="errorMessage">${error}</span><br/></c:forEach>
			</div>
			  <div class="form-body">
			  </div>
			             <div class="form-group">
                            	<label class="control-label col-md-3">Old password<span class="required">*</span></label>	
                              <div class="col-md-4">
                               <span style="color: #ff0000;"><input type="password" name="oldPassword" Style="width:200px;" id="oldPassword" class="required"/></span>
                              </div>
                           </div>
                           
                            <div class="form-group">
                            	<label class="control-label col-md-3" >New Password<span class="required">*</span></label>	
                              <div class="col-md-4">
                              <span style="color: #ff0000;"><input type="password" maxlength="15" name="newPassword" Style="width:200px;" id="newPassword" class="required" notequal="#oldPassword"/></span>
                              </div>
                           </div>
                            <div class="form-group">
                            	<label class="control-label col-md-3" >Confirm Password<span class="required">*</span></label>	
                              <div class="col-md-4">
                              <span style="color: #ff0000;"><input  type="password" maxlength="15" name="confirmPassword" Style="width:200px;" id="confirmPassword" class="required" equalTo="#newPassword"/></span>
                              </div>
                           </div>
                           
                <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green">${savelabel}</button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default">${cancelLabel}</button>                              
                           </div>
                        </div> 
		</form:form>
		</div>
		</div></div>
		</div>
