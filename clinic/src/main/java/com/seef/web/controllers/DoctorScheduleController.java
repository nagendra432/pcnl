package com.seef.web.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.dao.GenericDAO;
import com.seef.dao.util.CollectionUtil;
import com.seef.models.ApplicationUser;
import com.seef.models.doctor.Doctor;
import com.seef.models.doctor.DoctorSchedule;
import com.seef.service.AppointmentService;
import com.seef.service.BaseService;
import com.seef.service.DoctorScheduleService;
import com.seef.web.editors.AbstractModelEditor;

@Controller
@RequestMapping("/doctorschedule")
public class DoctorScheduleController extends CRUDController<DoctorSchedule> {
	
	private static final Logger _log = LoggerFactory.getLogger(DoctorScheduleController.class);
	
	@Autowired
	DoctorScheduleService doctorScheduleService;
	
	@Autowired
	GenericDAO genericDAO;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	AppointmentService appointmentService;
	
	
	@Override
	public void setupCreate(ModelMap modelMap,HttpServletRequest request)
	{
		modelMap.addAttribute("doctors",genericDAO.findAll(Doctor.class));
	}
	
	@Override
	@RequestMapping(value="/create.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request) {
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("doctor.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("create.doctorschedule.menu.name", null, userLocale));
		setupCreate(modelMap,request);
		return "tileDefinition.create-"+getNameSpace();
	}
	

	@Override
	@RequestMapping(value="/save.htm",method=RequestMethod.POST)
	public String save(@ModelAttribute("model") DoctorSchedule doctorSchedule, HttpServletRequest request,HttpServletResponse response, BindingResult bindingResult,ModelMap modelMap) {
		ApplicationUser user = getLoggedInUser();
		if (doctorSchedule.getId()== null){
			
			doctorScheduleService.save(doctorSchedule, user);
			
			return getRedirectURL("/manage.htm");
			
		
				
		}else{
			
			doctorScheduleService.saveOrUpdate(doctorSchedule, user);
			return getRedirectURL("/manage.htm");
				
			
		}

	}
	
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
	
		//modelMap.addAttribute("modelList", doctorScheduleService.doctorschedule_list());
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("doctor.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.doctorschedule.menu.name", null, userLocale));
	    
	    try
		{
		String searchDate1=request.getParameter("scheduledDate");
		Date searchDate= null;
		
		if(searchDate1==null)
		{	
			
		Calendar cal1 = new GregorianCalendar();
	    searchDate=new Date();
		}
		else
		{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			searchDate =df.parse(searchDate1);
			
		}
		_log.info("searchDate+++++++++++++++="+searchDate);
		modelMap.addAttribute("modelList", doctorScheduleService.schedule_byDate(CollectionUtil.toMap(new String[]{"date"}, new Object[]{searchDate})));
		}
		catch (Exception ex ){
	          System.out.println(ex);
	       }
		return "tileDefinition.manage-"+getNameSpace();
	}
		
	@Override
	@RequestMapping(value = "/edit.htm", method = RequestMethod.GET)
	public String edit(ModelMap modelMap, HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		DoctorSchedule doctorSchedule=doctorScheduleService.getById(id);
		setupCreate(modelMap, request);
		modelMap.addAttribute("model", doctorSchedule);
		return "tileDefinition.create-"+getNameSpace();
	}

	@Override
	public String view(ModelMap modelMap, HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		_log.info(">>>>> id is:"+id);
		DoctorSchedule doctorSchedule=doctorScheduleService.getById(id);
		modelMap.addAttribute("model", doctorSchedule);
		setupCreate(modelMap, request);
		modelMap.addAttribute("disabledStatus", "true");
		return "tileDefinition.view-"+getNameSpace();
	}
	

	@Override
	public boolean validateUpdate(DoctorSchedule model, BindingResult result) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateCreate(DoctorSchedule entity,
			BindingResult bindingResult) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BaseService<DoctorSchedule, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		return "doctorschedule";
	}
	
	@Override
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	 binder.registerCustomEditor(Doctor.class, new AbstractModelEditor(Doctor.class));
	 super.initBinder(binder);
	}
	

}
