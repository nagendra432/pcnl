<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="org.apache.shiro.SecurityUtils,com.seef.models.SystemUser, com.seef.models.dto.UserSession,java.util.Date,com.seef.models.dto.UserSession, com.seef.models.Preferences, java.util.Map
,com.seef.models.master.Theme" %>
<!DOCTYPE html>
<html lang="en">
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->

	<head>
		 <%
            Date lastlogindate=((UserSession) SecurityUtils.getSubject().getPrincipal()).getApplicationUser().getLastLogoutTime();
            Date loginfaileddate=((UserSession)SecurityUtils.getSubject().getPrincipal()).getApplicationUser().getLastFailedLoginTime();
            UserSession userSession = (UserSession)SecurityUtils.getSubject().getPrincipal();
            Preferences preferences = userSession.getPreferences();
            Theme theme = preferences.getTheme();
            String themeName = theme.getName();
            String imgname=((UserSession)SecurityUtils.getSubject().getPrincipal()).getApplicationUser().getUserImageName();
         	String prefLang = preferences.getLanguage().getCode().split("_")[0];
        	String prefLangcode=preferences.getLanguage().getCode();
    		 pageContext.setAttribute("preflang", prefLangcode);
         	 String name=null;
         	 String role=((UserSession) SecurityUtils.getSubject().getPrincipal()).getApplicationUser().getRole().getName();
         		pageContext.setAttribute("role", role);
         	
            Long appId = userSession.getApplicationUser().getId();
         %>
		<meta charset="utf-8" />
		<title>PCNL</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	   <meta content="" name="description" />
	   <meta content="" name="author" />
	   <meta name="MobileOptimized" content="320">
	   
	 
	   <link rel="shortcut icon"  href="${pageContext.request.contextPath}/assets/img/57X57.ico">
	   <!-- BEGIN GLOBAL MANDATORY STYLES -->          
	   <link href="${pageContext.request.contextPath}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	   <link href="${pageContext.request.contextPath}/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	   <!-- END GLOBAL MANDATORY STYLES -->
	   <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	   <link href="${pageContext.request.contextPath}/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
	   <link href="${pageContext.request.contextPath}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	   <link href="${pageContext.request.contextPath}/assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
	   <link href="${pageContext.request.contextPath}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>
	   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/select2/select2_metro.css" />
       <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/data-tables/DT_bootstrap.css" />
       	 
       	  <!-- added for image cropping -->
       	  
       	  
          <!-- added for image cropping -->
       <c:if test="${preflang eq 'ar_AR' }">
       	<c:out value="Inside arabic"></c:out>
     <link href="${pageContext.request.contextPath}/assets/rtl/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/assets/rtl/css/style-rtl.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/assets/rtl/css/jquery.gritter-rtl.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/assets/rtl/css/style-metronic-rtl.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/assets/rtl/css/style-responsive-rtl.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/assets/rtl/css/plugins-rtl.css" rel="stylesheet" type="text/css"/> 
    <link href="${pageContext.request.contextPath}/assets/rtl/css/pages/tasks-rtl.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/assets/css/themes/<%=themeName%>-rtl.css" rel="stylesheet" type="text/css" id="style_color"/> 
    <link href="${pageContext.request.contextPath}/assets/rtl/css/custom-rtl.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL RTL MANDATORY STYLES -->
	</c:if>
	 <c:if test="${preflang != 'ar_AR' }">
	 
		<link href="${pageContext.request.contextPath}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.request.contextPath}/assets/css/themes/<%=themeName%>.css" rel="stylesheet" type="text/css" id="style_color"/>
	    <link href="${pageContext.request.contextPath}/assets/css/custom.css" rel="stylesheet" type="text/css"/>
	   	<link href="${pageContext.request.contextPath}/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet" type="text/css"/>
	    <link href="${pageContext.request.contextPath}/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
    	<link href="${pageContext.request.contextPath}/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	    <link href="${pageContext.request.contextPath}/assets/css/pages/tasks.css" rel="stylesheet" type="text/css"/>
       	</c:if>
       	
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/gritter/css/jquery.gritter.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/select2/select2_metro.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/clockface/css/clockface.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/jquery-multi-select/css/multi-select.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css"/>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
	   <!-- END PAGE LEVEL PLUGIN STYLES -->
	   <!-- BEGIN THEME STYLES --> 
	
	   <link href="${pageContext.request.contextPath}/assets/css/seef-style.css" rel="stylesheet" type="text/css"/>
	    <link href="${pageContext.request.contextPath}/assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
	   <!-- END THEME STYLES -->
	   
	   
	   <!-- Previous CSS Required Files -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/old/tablerowcallback.css" />
	   <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/old/bootstrap-dialog.css" />	
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
   <!-- BEGIN CORE PLUGINS -->   
   <!--[if lt IE 9]>
   <script src="${pageContext.request.contextPath}/assets/plugins/respond.min.js"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/excanvas.min.js"></script> 
   <![endif]-->   
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>   
   <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="${pageContext.request.contextPath}/assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script> 
    <script src="${pageContext.request.contextPath}/assets/plugins/dynamic_list_helper.js" type="text/javascript"></script> 
   <script src="${pageContext.request.contextPath}/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
   <script src="${pageContext.request.contextPath}/assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
   <script src="${pageContext.request.contextPath}/assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
   <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
   <script src="${pageContext.request.contextPath}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
   
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-validation/dist/jquery.validate.min-<%= prefLang %>.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   
   
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/fuelux/js/spinner.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.<%=prefLang%>.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript" ></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript" ></script>
	<script src="${pageContext.request.contextPath}/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript" ></script>
   <!-- END PAGE LEVEL PLUGINS -->
   
   
     <!-- Previous JQuery Required Files -->
	<script src="${pageContext.request.contextPath}/assets/scripts/old/jquery-ui.custom.js"></script>
	<script src="${pageContext.request.contextPath}/assets/scripts/old/bootstrap-dialog.js"></script>
		
		
		
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/select2/select2.min.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/data-tables/jquery.dataTables.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/data-tables/DT_bootstrap_<%= prefLang %>.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/scripts/form-wizard.js"></script> 
   <script type="text/javascript" src="${pageContext.request.contextPath}/assets/scripts/form-components.js"></script> 
   <script src="${pageContext.request.contextPath}/assets/scripts/form-validation.js" type="text/javascript"></script>
   
   <script src="${pageContext.request.contextPath}/assets/scripts/app.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/scripts/index.js" type="text/javascript"></script>
   <script src="${pageContext.request.contextPath}/assets/scripts/tasks.js" type="text/javascript"></script>

	</head>
	
	
<!-- BEGIN BODY -->
<body class="page-header-fixed">


     <!-- BEGIN HEADER -->   
   <div class="header navbar navbar-inverse navbar-fixed-top">
      <!-- BEGIN TOP NAVIGATION BAR -->

      <div class="header-inner">
         <!-- BEGIN LOGO -->  
         <a class="navbar-brand" >
        <img class="img-responsive" alt="logo" width="85" height="120" style="background-color: #f0f8ff;" src="${pageContext.request.contextPath}/assets/img/setupadmin.png">
         </a>
         <!-- END LOGO -->
         <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
         <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
         <img src="${pageContext.request.contextPath}/assets/img/menu-toggler.png" alt="" />
         </a> 
         <!-- END RESPONSIVE MENU TOGGLER -->
         <!-- BEGIN TOP NAVIGATION MENU -->
         <ul class="nav navbar-nav pull-right">
            <li class="dropdown user">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
               <c:if test="${empty sessionScope.logedinuserimagename }">
                  <img alt="" src="${pageContext.request.contextPath }/assets/img/avatar1_small.jpg" width="40" height="29"/>
                </c:if>
                 <c:if test="${not empty sessionScope.logedinuserimagename }">
                    <img src="${pageContext.request.contextPath}/resources/profileImages/${sessionScope.logedinuserimagename}" width="50" height="35" />
                 </c:if>               <span class="username"><%= ((UserSession) SecurityUtils.getSubject().getPrincipal()).getApplicationUser().getUsername()%></span>
               <i class="icon-angle-down"></i>
               </a>
               <ul class="dropdown-menu">
                  <li><a href="${pageContext.request.contextPath }/profile/updateprofile.htm"><i class="icon-user"></i><spring:message code="label.user.profile"/></a>
                  </li>
                  <li><a href="${pageContext.request.contextPath }/changepassword/updatepassword.htm"><i class="icon-user"></i><spring:message code="label.user.changepassword"/></a>
                  </li>

                  <li class="divider"></li>
                  <li><a href="javascript:;" id="trigger_fullscreen"><i class="icon-move"></i><spring:message code="label.user.fullscreen"/></a>
                  </li>
                  
                  <li><a href="${pageContext.request.contextPath }/logout/logout.htm"><i class="icon-key"></i><spring:message code="label.user.logout"/></a>
                  </li>
               </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
         </ul>
         <!-- END TOP NAVIGATION MENU -->
      </div>
      <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->
   
   <div class="clearfix"></div>
   
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
         <!-- BEGIN SIDEBAR -->
      <div class="page-sidebar navbar-collapse collapse">
         <!-- BEGIN SIDEBAR MENU -->        
         <ul class="page-sidebar-menu">
             <li>
               <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
               <div class="sidebar-toggler hidden-phone"></div>
               <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <li>
               
            </li>
            <c:forEach items='${requestScope["MENULIST"].menus}' var="menu" varStatus="countStatus">
              <c:if test="${!menu.removed}">
                <li>
                  <a class="${menu.url}" href="javascript:;">
					<i class="${menu.icon}"></i>
					 <span class="title"><spring:message code="${menu.key}" text="${menu.name}"/></span>
					 <span class="arrow "></span>
				  </a>
				  
				   <c:if test="${menu.parent}">
					  <ul class="sub-menu">
					  <c:forEach items="${menu.serviceMenu}" var="subMenu">
					  	<c:if test="${!subMenu.removed}">
					  		<li>
					  		  <a href="${pageContext.request.contextPath}${subMenu.url}?removesubmenu=true">
					  		   <i class="${subMenu.icon}"></i> 
					  				<spring:message code="${subMenu.key}" text="${subMenu.name}"/>
					  			</a>
					  		</li>
					  	</c:if>
					  </c:forEach>
					  <c:forEach items="${menu.menus}" var="subMenu">
					  	<c:if test="${!subMenu.removed}">
							<li class="dropdown-submenu">
								<a href="${pageContext.request.contextPath}${subMenu.url}?menuId=${subMenu.id}">
									<i class="${subMenu.icon}"></i> 
									<spring:message code="${subMenu.key}" text="${subMenu.name}"/>
									<b class="arrow icon-angle-right"></b>
								</a>
								<ul class="sub-menu">
									<c:forEach items="${subMenu.serviceMenu}" var="subSubMenu">
										<c:if test="${!subSubMenu.removed}">
											<li>																
												<a href="${pageContext.request.contextPath}${subSubMenu.url}?removesubmenu=true">
									  				<i class="${subSubMenu.icon}"></i> 
									  				<spring:message code="${subSubMenu.key}" text="${subSubMenu.name}"/>
									  			</a>																
											</li>
										</c:if>
									</c:forEach>
								</ul>
							</li>
						</c:if>
					  </c:forEach>										
									  
					</ul>
			 </c:if>
          </li>
              </c:if>
            </c:forEach>
            <li>
            
            
         </ul>
         <!-- END SIDEBAR MENU -->
      </div>
     
      <!-- END SIDEBAR -->
      
      <script type="text/javascript">
	  $(function(){
		///  alert('Hai');
		  var navPath = '<%= session.getAttribute("nav") %>',
		      innerNavPath = '<%= session.getAttribute("innerNav") %>',
		      tempspan = '<span id= "test">'+innerNavPath+'</span>',
		      navList = $('ul.page-sidebar-menu > li');
		      $( "body" ).append(tempspan);
		      var innerNavPathVal = $('span#test').text();
		      navList.eq(2).addClass('start');
		      navList.last().addClass('last');
		    // console.log(navPath);
		    // console.log(innerNavPath);
		      $('span#test').empty();
		  $.each(navList,function(){
			  var $this = $(this),
			      val = $this.find('span.title:first').text();
			  if(navPath === val){
				  $this.addClass('active open');
				  $this.find('span.arrow:first').addClass('open');
				  var innerLi = $this.find('ul.sub-menu li');
				  $.each(innerLi,function(){
					  var currentInnerLi = $(this),
					      innerVal = currentInnerLi.find('a:first').text().trim();
					   if(innerNavPathVal === innerVal)
						   currentInnerLi.addClass('active');
				  });
				  
			  }
		  });
		  
		
	  });
	
	</script>
      
       <!-- BEGIN PAGE -->
      <div class="page-content">
      
      
      
      
      
      	<c:set var="sucess"  value='<%= lastlogindate==null?"":lastlogindate%>' />
				<c:set var="fail"   value='<%= loginfaileddate==null?"":loginfaileddate%>' />
				
		<div class="row">
		<div class="col-md-12">		
      <ul class="page-breadcrumb breadcrumb index-top">
          <li ><c:if test="${not empty name}">
					&nbsp <i class="icon-home"></i>&nbsp<small><b><c:out value="${name}"/></b></small>	
                 </c:if></li>
                  <li><c:if test="${not empty role}">
					&nbsp <spring:message code="label.role.name"/>:&nbsp<small><b><c:out value="${role}"/></b></small>	
                 </c:if></li>
         <li class="pull-right"><form class="form-search">
							<span class="input-icon">
									
							<span class="label label-success c1">
							<spring:message code="header.label.lastlogintime"/>:&nbsp;<span> <fmt:formatDate  type="both" dateStyle="short" timeStyle="short" value="${sucess}"/> </span></span>
							<span class="label label-danger c2">
							<spring:message code="header.label.lastfailedtime"/>:&nbsp;<span><fmt:formatDate  type="both" dateStyle="short" timeStyle="short" value="${fail}"/> </span></span>
						</span>
						</form></li> 
        </ul>
        </div>
        
        
        
        </div>	
        
        
          <!-- PAGE CONTENT BEGINS HERE -->
		     <tiles:insertAttribute name="body"/>
		     
	    <!-- PAGE CONTENT ENDS HERE -->
        </div>
      <!-- END PAGE -->
      <div class="footer">
      
  <div class="col-md-offset-5 footer-inner">${MyRecentOnlineActivities} &copy; <spring:message code="label.allrightsreserved"/><a target="_blank" href="http://www.seeftech.com/"> <spring:message code="label.seeftech"/></a></div>
  <div class="footer-tools"> <span class="go-top"> <i class="icon-angle-up"></i> </span> </div>

</div>
    </div>
    
     <spring:message code="label.MyRecentOnlineActivities" var="activities"/>
    <spring:message code="label.PreviousSiteVisit" var="previous"/>
    <spring:message code="label.LastFailedLoginAttempt" var="lastFailed"/>
    <spring:message code="label.LastLoginPasswordChange" var="LastLogin"/>
    
   <!-- END CONTAINER -->
   <%
   HashMap<String,String> map = (HashMap)session.getAttribute("popuplabels");
   String msg1 = map.get("MyRecentOnlineActivities");
   String msg2 = map.get("PreviousSiteVisit");
   String msg3 = map.get("LastFailedLoginAttempt");
   String msg4 = map.get("LastLoginPasswordChange"); 
   %>
  
  
  <!-- for autocomplete off in application level -->
    <script type="text/javascript">
      $(document).ready(function(){
    $( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });
 });
      </script>
   <!-- END PAGE LEVEL SCRIPTS -->  
   <script>
      jQuery(document).ready(function() {    
    	  var data1="<%= lastlogindate %>";
    	  var data2="<%= loginfaileddate %>";
    	  var ctx = "<%=request.getContextPath()%>"
          var img="<%= imgname %>";
          var lastloginpwdchangedate="<%= session.getAttribute("lastloginpwdchangedate") %>";
    	  
    	  var msg1 = '${activities}';
    	  var msg2 = '${previous}';
    	  var msg3 = '${lastFailed}';
    	  var msg4 = '${LastLogin}';
    	  
    	 
    	  var cars=new Array();
    	  cars[0]=data1;
    	  cars[1]=data2;
    	  cars[2]=img;
    	  cars[3]=lastloginpwdchangedate;
    	  cars[4]=ctx;
         App.init(); // initlayout and core plugins
         Index.init();
         Index.initJQVMAP(); // init index page's custom scripts
         Index.initCalendar(); // init index page's custom scripts
         Index.initCharts(); // init index page's custom scripts
         Index.initChat();
         Index.initMiniCharts();
         Index.initDashboardDaterange();
         Index.initIntro(cars,msg1,msg2,msg3,msg4);
        
         Tasks.initDashboardWidget();
         FormComponents.init(); //for dateTime and other components
         FormValidation.init(); //for custom validations

      });
   </script>
  
   <!-- END JAVASCRIPTS -->		
		
	</body><!-- END BODY -->
		
	
</html>















