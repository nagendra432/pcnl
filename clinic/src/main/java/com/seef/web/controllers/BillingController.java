package com.seef.web.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.models.pharmacy.Billing;
import com.seef.models.pharmacy.Medicine;
import com.seef.models.pharmacy.MedicinePackagingDetails;
import com.seef.service.BaseService;
import com.seef.service.BillingService;
import com.seef.service.MedicinePackagingService;
import com.seef.service.MedicineService;

@Controller
@RequestMapping("/billing")
public class BillingController extends CRUDController<Billing> {
	
	private static final Logger _log = LoggerFactory.getLogger(BillingController.class);
	
	@Autowired
	MedicineService medicineService;
	
	@Autowired
	BillingService billingService;

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	MedicinePackagingService medicinePackagingService;
	
	@Override
	@RequestMapping(value="/create.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("billing.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("create.billing.menu.name", null, userLocale));
		List<Medicine> medicineList=medicineService.medicine_list();
		modelMap.addAttribute("medicinelist",medicineList);
		Billing billing=new Billing();
		billing.setBillNo(billingService.getMaxBillno());
		
		modelMap.addAttribute("model",billing);
		return "tileDefinition.create-"+getNameSpace();
	}
	
	@Override
	@RequestMapping(value="/search.htm",method=RequestMethod.GET)
	public String search(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("billing.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("view.billing.menu.name", null, userLocale));
		return "tileDefinition.search-"+getNameSpace();
	}
	
	@Override
	@RequestMapping(value="/view.htm",method=RequestMethod.GET)
	public String view(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Long billno=Long.valueOf(request.getParameter("txtbillno"));
		System.out.println(">>>>>>>>>>>>>>>>>>>>>"+billno);
		Billing bill=billingService.getBill(billno);
		if(bill != null)
		{
		modelMap.addAttribute("model",bill);
		return "tileDefinition.view-"+getNameSpace();
		}
		else
		{
			modelMap.addAttribute("errorheader","Bill no not Exist");
			return "tileDefinition.search-"+getNameSpace();
		}
	}
	
	
	@RequestMapping(value="/print.htm",method=RequestMethod.GET)
	public String print(ModelMap modelMap, HttpServletRequest request)
			{
			  Long billno=Long.valueOf(request.getParameter("tmpBill"));
			  Billing bill=billingService.getBill(billno);
			  modelMap.addAttribute("model",bill);
			  return "tileDefinition.print-"+getNameSpace();
			}
	@Override
	@RequestMapping(value="/save.htm",method=RequestMethod.POST)
	public String save(@ModelAttribute("model") Billing model, HttpServletRequest request,
			HttpServletResponse response, BindingResult bindingResult,
			ModelMap modelMap) {
		// TODO Auto-generated method stub
		
		model.setTotalCost(Double.valueOf( request.getParameter("total")));
		billingService.saveBill(model, getLoggedInUser());
		return getRedirectURL("/print.htm?tmpBill="+model.getBillNo());
		
		
	}	
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/getpackaging", method=RequestMethod.GET)
	public void getChapters(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		PrintWriter out=response.getWriter();
		Long medicineId= Long.valueOf(request.getParameter("medicineid"));
		List<MedicinePackagingDetails> medicinePackagings = medicinePackagingService.medicinePackaging_stocklist(medicineId);
		_log.info(">>getmedicinepkg method():"+medicinePackagings.size());
		
		List batches = new ArrayList();
		 Map medicineMap = null;
		for(MedicinePackagingDetails section : medicinePackagings){
			medicineMap = new HashMap();
			medicineMap.put("id", section.getId());
			medicineMap.put("cost", section.getCost());
			medicineMap.put("units", section.getUnits());
			medicineMap.put("name", section.getBatchNo());
			batches.add(medicineMap);
			
		}
		      JSONArray jsonArray = new JSONArray(batches);
		      
		      out.print(jsonArray);
	}
	
	@Override
	public boolean validateUpdate(Billing model, BindingResult result) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateCreate(Billing entity, BindingResult bindingResult) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BaseService<Billing, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "billing";
	}

}
