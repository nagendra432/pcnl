package com.seef.service;

import java.util.List;
import java.util.Map;

import com.seef.models.ApplicationUser;
import com.seef.models.OutPatient;

public interface OutPatientService {
	
	public void saveOutPatient(OutPatient outPatient,ApplicationUser applicationUser);
	public Long getMaxMRNO();
	public void saveOrUpdateOutPatient(OutPatient outPatient,ApplicationUser applicationUser);
	public List<OutPatient> outPatient_list();
	public OutPatient getById(Long id);
	public OutPatient getById(OutPatient outPatient);
	public Boolean validateUnique(OutPatient outPatient, Map<String, Object> map);
	public OutPatient getOutpatient(Map<String, Object> criteria);
	public OutPatient findByMrNo(String mrNo);
	public List<OutPatient> outPatiensByParams(int age,String gender,Long surgeryId,Long surgeryTypeID,String remarks,String surgeon);
}
