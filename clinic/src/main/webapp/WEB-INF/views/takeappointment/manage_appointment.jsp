<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="datatables"
	uri="http://github.com/dandelion/datatables"%>
	
<spring:message code="label.appointmentdate" var="lblappointmentdate"/>
<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.name" var="lblname"/>
<spring:message code="label.phneno" var="lblphneno"/>
<spring:message code="label.appointmentdate" var="lblappointmentdate"/>
<spring:message code="label.admin.status" var="lblstatus"/>
<spring:message code="label.consult.doctor" var="lbldoctor"/>
<spring:message code="label.action" var="lblaction"/>



	<script type="text/javascript">
 $(document).ready(function(){
  $(".form_datetime").datetimepicker({         
   isRTL: App.isRTL(),
            format: "yyyy-mm-dd",
            weekStart: 1,
            todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
       });
 });
 
 function rowCallback( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	    if (aData[4] =="ACTIVE" ) {
	    	$(nRow).addClass("deactivateRow");
	    }
	}
 
 
</script>
	
	
	
	
	
<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<spring:message code="header.appointment.manage" />
			
		</h3>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
 <form:form action="manage.htm"  id="form_sample_1" class="form-horizontal" method="get" >
 <div class="form-group">
							<label class="control-label col-md-3">${lblappointmentdate}<span class="required">*</span></label>
							<div class="col-md-4">
								<div class="input-group date form_datetime">
								<input id="appointmentDate" name="appointmentDate" required="required" class="form-control">
									    <span class="input-group-btn">
										<button class="btn default date-set" type="button">
											<i class="icon-calendar"></i>
										</button>
									</span>
									 
								</div>
								 <button type="submit" class="btn green">Search</button>
							</div>
						</div>
						
</form:form>
</div>


<div class="row">
	<div class="col-md-12">

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-edit"></i>
					<spring:message code="label.resultsfor" />
					
					<spring:message code="header.appointment.manage" />
					
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					 <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
			
				<datatables:table id="sample_editable_1" data="${modelList}" cdn="false" row="appointment" cssClass="table table-striped table-bordered table-hover">
					<datatables:column title="${lblmrno}" property="outPatient.mrno" />
					<datatables:column title="${lblname}" property="outPatient.firstName"
						cssClass="hidden-480" cssCellClass="hidden-480" />	
				   <datatables:column title="${lblphneno}" property="outPatient.phoneNo" />
				    <datatables:column title="${lblappointmentdate}" property="appointmentDate" />
				    <datatables:column title="${lbldoctor}" property="doctor.name" />
				    <datatables:column title="${lblstatus}">${appointment.status}</datatables:column>
				    <datatables:column title="${lblaction}" sortable="false">
						<div class='hidden-phone visible-desktop btn-group'>
							<a href="close.htm?id=${appointment.id }" data-rel="tooltip"
								title="${lblCompleted}">
								<i
								class="btn btn-default btn-sm my-table_btn icon-ok-sign"></i></a>
								<a href="rejected.htm?id=${appointment.id }" data-rel="tooltip"
								title="${lblRejected}"><i
								class="btn btn-default btn-sm my-table_btn icon-remove-sign"></i></a>
								</div>
								</datatables:column>
				      <datatables:callback function="rowCallback" type="row"/>
				</datatables:table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->

