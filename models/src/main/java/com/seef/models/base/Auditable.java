package com.seef.models.base;

//import java.beans.Transient;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

public abstract class Auditable {
	private Long authQueueId;
	private Long makerId;
	private Long checkerId;
	private String makerComment;
	private String checkerComment;

	@Transient
	public Long getMakerId() {
		return makerId;
	}

	public void setMakerId(Long makerId) {
		this.makerId = makerId;
	}

	@Transient
	public Long getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(Long checkerId) {
		this.checkerId = checkerId;
	}

	@Transient
	@XmlTransient
	public String getMakerComment() {
		return makerComment;
	}

	public void setMakerComment(String makerComment) {
		this.makerComment = makerComment;
	}

	@Transient
	public String getCheckerComment() {
		return checkerComment;
	}

	public void setCheckerComment(String checkerComment) {
		this.checkerComment = checkerComment;
	}

	@Transient
	public Long getAuthQueueId() {
		return authQueueId;
	}

	public void setAuthQueueId(Long authQueueId) {
		this.authQueueId = authQueueId;
	}
	

}
