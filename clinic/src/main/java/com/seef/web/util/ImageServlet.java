package com.seef.web.util;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ImageServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response){
		try{
			String pathinfo = request.getParameter("pathInfo");
			System.out.println("################ Image Retrieving: " + pathinfo);
			File file = new File(pathinfo);
			BufferedInputStream fis = new BufferedInputStream(new FileInputStream(file));
			OutputStream ostream = response.getOutputStream();
			response.setContentType("image/jpeg");
			byte[] imageBytes= new byte[1024];
			while(fis.read(imageBytes)!=-1)
				ostream.write(imageBytes);
			
			fis.close();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

}
