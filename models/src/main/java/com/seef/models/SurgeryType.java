package com.seef.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name="SURGERYTYPE")
public class SurgeryType extends AbstractDomainObject{
	
	
	
	private static final long serialVersionUID = -7344893162105288786L;
	private Long id;
	private String name;
	private String discription;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name = "NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "DISCRTPTION")
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	

}
