package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;

public enum NationalIdType {
	
	NationalIdType1("NationalIdType1"), NationalIdType2("NationalIdType2"), NationalIdType3("NationalIdType3"), NationalIdType4("NationalIdType4"),NationalIdType5("NationalIdType5"),NationalIdType6("NationalIdType6");

	private String val;

	private NationalIdType(String val) {
		this.val = val;
	}

	public static List<NationalIdType> list() 
	{
		return Arrays.asList(values());
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

}
