package com.seef.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.PasswordType;

@Entity
@Table(name = "PASSWORD_HISTORY")
@NamedQueries({ 
	@NamedQuery(name = "PasswordHistory.getApplicationUserLastThree",query="Select ph from PasswordHistory ph where ph.applicationUser.id = :userId and ph.type = :type order by ph.id asc"),
	@NamedQuery(name = "PasswordHistory.LastThree",query="Select ph from PasswordHistory ph where ph.applicationUser = :applicationUser and ph.type = :type order by ph.id asc"),
	@NamedQuery(name = "PasswordHistory.findLastThree",query="Select ph from PasswordHistory ph where ph.applicationUser.id = :userId and ph.type = :type")
})
public class PasswordHistory extends AbstractDomainObject implements Cloneable{

	private static final long serialVersionUID = 4755408037722184240L;
	
	private Long id;	
	private String password;	
	private Date changeDate;	
	private PasswordType type;	
	private ApplicationUser applicationUser;
	
	public PasswordHistory(){
		
	}
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "PASSWORD_HISTORY_SEQ")
	@SequenceGenerator(name = "PASSWORD_HISTORY_SEQ", sequenceName = "PASSWORD_HISTORY_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "PASSWORD", length = 200, nullable = false)
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "CHANGE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}
	
	@Column(name = "PASSWORD_TYPE", nullable = false, length = 10)
	public PasswordType getType() {
		return type;
	}
	public void setType(PasswordType type) {
		this.type = type;
	}
	

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	public ApplicationUser getApplicationUser() {
		return applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

}
