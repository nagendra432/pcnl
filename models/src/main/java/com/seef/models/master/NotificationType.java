package com.seef.models.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name = "NOTIFICAITON_TYPES")
public class NotificationType extends AbstractDomainObject{

	private static final long serialVersionUID = -8327588601056882018L;
	
	private Long id;	
	private String type;	
	private String name;
	
	public NotificationType(){
		
	}
	
	public NotificationType(Long id, String name){
		this.id = id;
		this.name = name;
	}
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "NOTIFICAITON_TYPES_SEQ")
	@SequenceGenerator(name = "NOTIFICAITON_TYPES_SEQ", sequenceName = "NOTIFICAITON_TYPES_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "TYPE")
	public String getType() {
		return type;
	}
	@Column(name = "NAME")
	public String getName() {
		return name;
	}
	
	public void setType(String type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
