package com.seef.models.menu.dto;

import java.io.Serializable;

public class BaseMenu implements Serializable {
	
	private static final long serialVersionUID = 7225559856231846651L;
	
	private String key;
	private String url;
	private String name;
	private String icon;
	private int id;

	public String getKey() {
		return key;
	}

	public String getUrl() {
		return url;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
