package com.seef.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.UserStatus;

public interface BaseDao<T extends AbstractDomainObject,ID extends Serializable> {

	 public T save( final T entity );
	 
	 public T update( final T entity );
	 
	 public T find( final ID id );
	 
	 public T delete(final ID id);
	 
	 public void delete(T t);
	 
	 public int count(String queryName,Map<String,Object> params);
	 
	 public boolean isExist(String queryName,Map<String,Object> params);
	 
	 public boolean isExist(String queryName,Object... values);
	 
	 public List<T> executeNamedQuery(String queryName,Map<String,Object> params);
	 
	 public T findOne(String queryName,Map<String,Object> params);
	 
	 public <K extends AbstractDomainObject> List<K> executeNamedQuery(String queryName,Class<K> clazz);
	 
	 public <K extends AbstractDomainObject> K find(Class<K> clazz, final ID id );
	 
	 public <K extends AbstractDomainObject> K findOneByQuery(String queryName, Class<K> clazz, Map<String, Object> params);
	 
	 public <K extends AbstractDomainObject> List<K> findAll(String queryName, Class<K> clazz, Map<String, Object> params);
	 
	 public <K extends AbstractDomainObject> List<K> findAll(Class<K> clazz);
	 	 
	 public T findOneNativeQuery(String query,Map<String,Object> params);
	 
	 public int executeNamedUpdate(String queryName,Map<String,Object> params);
	 
	 public int executeNativeUpdate(String query,Map<String,Object> params);
	 
	 public T findTop(String queryName,Map<String,Object> params);
	 
	 public void detach(T t);
	 
	 public void flush();
	 
	 public T getReference(ID id);
	 
	 public Class<T> getPersistentClass();
	 
	 public <K extends AbstractDomainObject> K  getOneByCriteria(Class<K> clazz, Map<String, Object> criterias);
	 
	 public <K extends AbstractDomainObject> List<K> findAllByCriteria( Class<K> clazz, Map<String, Object> criterias);
	 
	 public <K extends AbstractDomainObject> List<K> findAllByCriteria( Class<K> clazz, Map<String, Object> criterias, String sortField, boolean desc) ;
	 
	 public T getByQuery(String jpaQuery);
	 
	 public ID getEntityID(T t);
	 
	 public <K extends AbstractDomainObject> K findOne(ID identifier,Class<K> clazz);
	 
	 public <K extends AbstractDomainObject> boolean isUnique(Class<K> clazz, K entity, Map<String, Object> properties);
	 
	 public String executeQuerySingleString(String queryName, Map<String, Object> params);
	 
	 public <K extends AbstractDomainObject> boolean isUnique(Class<K> clazz, Map<String, Object> properties);

	 public <K extends AbstractDomainObject> List<K> findByNamedQuery(String namedQuery, Map<String,Object> map);

	 public <K extends AbstractDomainObject> K getById(Class<K> clazz, AbstractDomainObject entity);
	 
	 public <K extends AbstractDomainObject> List<K> searchByQuery(Class<K> clazz, int noOfRecord, String query, String countQuery);
	 
	 public <T extends AbstractDomainObject> List<T> executeSimpleQuery(String query);
	 public <K extends AbstractDomainObject> List<K> executeNativeQuery(String query,String resultSetMapping);
	 
	 public UserStatus executeQueryStatusString(String queryName, Map<String, Object> params);
	 
}



