<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>



<div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">
               <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i><spring:message code="header.doctor"/></div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>
                  </div>
                  
              <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model">
	                       <form:hidden path="id"/>
	                       <div style="color:red">                      		
		                    <form:errors path="*" cssClass="errorcollection"/>
		                  </div> 
                  <div class="form-body">
                           <div class="alert alert-danger display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formerrormsg"/>
                           </div>
                           <div class="alert alert-success display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formsuccessmsg"/>
                           </div>
                   </div> 
                   
                   
                           
                
                          			<div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.name"/><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="name"  class="form-control"  />
                                       </div>
                                       </div>
                                   
           
               
                          			<div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.doctor.qualification"/><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="qualification"  class="form-control"  />
                                       </div>
                                       </div>
                                
               
             
                          			<div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.doctor.designation"/><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="designation"  class="form-control"  />
                                       </div>
                                       </div>
                 
                          			<div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.doctor.specilization"/><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="specilization"  class="form-control" />
                                       </div>
                                       </div>
                                    
                                    
               
                        	 <div class="form-group">
                            <label for="doctortype" class="col-md-3 control-label"><spring:message code="label.doctor.doctortype"/><span class="required">*</span></label>
                            <div class="col-md-4">
								<form:select path="doctorType" id="doctorType" class="input-sm form-control required" >
			          				<form:option value="">-<spring:message code="label.select"/>-</form:option>
			          				<c:forEach items="${doctorType}" var="doctorType">
				            			<form:option value="${doctorType.type}"><spring:message code="${doctorType.type}"/></form:option>
				            		</c:forEach>
			      				</form:select>
							</div>
                       	 </div>
                      
               
                <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green"><spring:message code="button.save"/></button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default"><spring:message code="button.cancel"/></button>                              
                           </div>
                        </div>
               
               
               
               </form:form>
               </div>
               </div>
               </div>
               </div>