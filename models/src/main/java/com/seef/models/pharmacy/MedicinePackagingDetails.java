package com.seef.models.pharmacy;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.seef.models.ApplicationUser;
import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name="MEDICINE_PACKAGING_DETAILS")
public class MedicinePackagingDetails extends AbstractDomainObject {

	private static final long serialVersionUID = -306150852731659433L;

	private Long id;
	
	private Medicine medicine;
	
	private Date received;
	
	private Date expiryDate;
	
	private String batchNo;
	 
	private Long units;
	
	private Double cost;
	
	private Supplier supplier;
	
	private String tax;
	
	private ApplicationUser expiredBy;
	
	private Date expiredOn ;
	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "MEDICINE_PACKAGING_DETAILS_SEQ")
	@SequenceGenerator(name = "MEDICINE_PACKAGING_DETAILS_SEQ", sequenceName = "MEDICINE_PACKAGING_DETAILS_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MEDICINE_ID")
	public Medicine getMedicine() {
		return medicine;
	}

	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}

	@Column(name="RECEIVED_ON")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getReceived() {
		return received;
	}

	public void setReceived(Date received) {
		this.received = received;
	}

	@Column(name="EXPIRY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
    
	@Column(name="BATCHNO")
	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	
	@Column(name="UNITS")
	public Long getUnits() {
		return units;
	}

	public void setUnits(Long units) {
		this.units = units;
	}

	@Column(name="COST")
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUPPLIER")
	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EXPIRED_BY")
	public ApplicationUser getExpiredBy() {
		return expiredBy;
	}

	public void setExpiredBy(ApplicationUser expiredBy) {
		this.expiredBy = expiredBy;
	}

	@Column(name="TAX")
	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}
	
	@Column(name = "EXPIRED_ON")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getExpiredOn() {
		return expiredOn;
	}

	public void setExpiredOn(Date expiredOn) {
		this.expiredOn = expiredOn;
	}

}
