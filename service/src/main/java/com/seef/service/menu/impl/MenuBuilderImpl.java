package com.seef.service.menu.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.seef.models.menu.dto.MenuList;
import com.seef.service.menu.MenuBuilder;

@Component("menuBuilder")
public class MenuBuilderImpl implements MenuBuilder, ApplicationContextAware {
	
	private static final Logger _log = LoggerFactory.getLogger(MenuBuilderImpl.class);

	private MenuList adminMenuList;
	private ApplicationContext applicationContext;
	private String menuConfigFile;
	

	@Override
	public MenuList buildAdminMenu(Collection<String> permission) {
		_log.info(">>> Loading  Menu permission size:{"+permission.size()+"}");
		try {
			adminMenuList=loadFromXMLStream(applicationContext.getResource("classpath:" + getMenuConfigFile()).getInputStream());
		} catch (IOException e) {
			_log.error(">>> message:{Error while building the menu"+e.getMessage()+"}");
			e.printStackTrace();
		}
		adminMenuList.filterMenus(permission);
		adminMenuList.buildMenuMap();
		return adminMenuList;
	}

	private MenuList loadFromXMLStream(InputStream xmlStream) {
		MenuList menuList = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MenuList.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			menuList = (MenuList) jaxbUnmarshaller.unmarshal(xmlStream);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return menuList;
	}

	@Override
	public MenuList buildAgencyMenu(Collection<String> permission) {
		return null;
	}

	/*@Override
	public void afterPropertiesSet() throws Exception {
		adminMenuList=loadFromXMLStream(applicationContext.getResource("classpath:" + getMenuConfigFile()).getInputStream());
	}*/

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext=applicationContext;
	}

	public String getMenuConfigFile() {
		return menuConfigFile;
	}

	public void setMenuConfigFile(String menuConfigFile) {
		this.menuConfigFile = menuConfigFile;
	}
	
	

}
