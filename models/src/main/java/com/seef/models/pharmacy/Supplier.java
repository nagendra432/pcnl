package com.seef.models.pharmacy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;



@Entity
@Table(name="SUPPLIER")
public class Supplier extends AbstractDomainObject {
	
	private static final long serialVersionUID = -2622936329389835499L;
	
	private Long id;
	
	private String name;

	private String company;	

	private String phoneNo;
	
	private String mailId;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "SUPPLIER_SEQ")
	@SequenceGenerator(name = "SUPPLIER_SEQ", sequenceName = "SUPPLIER_SEQ", allocationSize = 1)	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Column(name="SUPPLIER_NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="COMPANY_NAME")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	
	@Column(name="PHONENO")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Column(name="SUPPLIER_MAILID")
	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	

}