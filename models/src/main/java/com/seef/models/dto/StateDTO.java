package com.seef.models.dto;

import java.io.Serializable;

import com.seef.models.master.State;

public class StateDTO implements Serializable{
	
	private static final long serialVersionUID = 1629532072773765876L;
	
	private Long id;
	private String code;
	private String name;
	
	public StateDTO(){
		
	}
	
	public StateDTO(State state){
		this.id = state.getId();
		this.code = state.getCode();
		this.name = state.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
