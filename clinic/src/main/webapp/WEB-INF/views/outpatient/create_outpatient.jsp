<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.outpatient.firstname" var="lblfirstname"/>
<spring:message code="label.outpatient.middlename" var="lblmiddlename"/>
<spring:message code="label.outpatient.lastname" var="lbllastname"/>
<spring:message code="label.outpatient.phno" var="lblphno"/>
<spring:message code="label.address.line1" var="lbladdress1"/>
<spring:message code="label.address.line2" var="lbladdress2"/>
<spring:message code="label.address.zipcode" var="lblzipcode"/>
<spring:message code="label.address.city" var="lblcity"/>
<spring:message code="label.address.country" var="lblcountry"/>
<spring:message code="label.address.state" var="lblstate"/>
<spring:message code="label.address.fulladdress" var="lblfulladdress"/>
<spring:message code="label.outpatient.dob" var="lbldob"/>
<spring:message code="label.outpatient" var="lbloutpatient"/>
<spring:message code="label.outpatient.time" var="lbltime"/>
<spring:message code="label.appointmentdate" var="lblappointmentdate"/>
<spring:message code="label.consultationtype" var="lblconsultationtype"/>
<spring:message code="label.doctors" var="lbldoctors"/>
<spring:message code="label.fee" var="lblfee"/>
<spring:message code="label.save" var="lblsave"/>
<spring:message code="label.cancel" var="lblcancel"/>


<script type="text/javascript">
 $(document).ready(function(){
  $(".form_date").datetimepicker({         
   isRTL: App.isRTL(),
            format: "yyyy-mm-dd",
            weekStart: 1,
            todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 4,
      minView: 2,
      forceParse: 0,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
       });
  
  $(".form_datetime").datetimepicker({         
	   isRTL: App.isRTL(),
	            format: "yyyy-mm-dd hh:ii",
	            weekStart: 1,
	            todayBtn:  1,
	      autoclose: 1,
	      todayHighlight: 1,
	      startView: 2,
	      minView: 0,
	      forceParse: 0,
	            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
	       });
 });
</script>



   
   <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>Patient</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                     </div>
                  </div>
                  <div class="portlet-body">
                  <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model" >
                     <div class="tabbable-custom ">
                        <ul class="nav nav-tabs ">
                           <li class="active"><a href="#tab_5_1" data-toggle="tab">Personal Details</a></li>
                           <li><a href="#tab_5_2" data-toggle="tab">Appointment Details</a></li>
                        </ul>
                        <div class="tab-content">
                           <div class="tab-pane active" id="tab_5_1">
                              <div class="row">
                                  <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblmrno}<span class="required">*</span></label>
                                       <div class="col-md-7">
                                          <form:input path="mrno"  class="form-control" required="required" />
                                       </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                          			 <div class="form-group">
                                       <label class="control-label col-md-3">${lblfirstname}<span class="required">*</span></label>
                                       <div class="col-md-7">
                                        <form:input path="firstName"  class="form-control"   
					                                required="required" maxlength="20" minlength="3" alphabets="true"/>
                                       </div>
                                       </div>
                                       </div>
                                    </div>
                                    <div class="row">
                             <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblmiddlename}</label>
                                       <div class="col-md-7">
                                          <form:input path="middleName" class="form-control"  data-validate-length-range="3,3"/>
                                       </div>
                                       </div>
                                    </div>
                                    
                                     <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbllastname}</label>
                                       <div class="col-md-7">
                                           <form:input path="lastName" class="form-control"  data-validate-length-range="3,3"/>
                                       </div>
                                       </div>
                                    </div>
                            
                            </div>
                           
                           <div class="row">
                             <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbldob}<span class="required">*</span></label>
                                       <div class="col-md-7">
                                          <div class="input-group date form_date">
									   <form:input class="form-control"  path="dob"  required="required" date="true"/> 
									  
									   <span class="input-group-btn">
										<button class="btn default date-set" type="button">
											<i class="icon-calendar"></i>
										</button>
									</span>
								        </div>
								        
								  </div>
                                       </div>
                                    </div>
                                <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblphno}<span class="required">*</span></label>
                                       <div class="col-md-7">
                                          <form:input path="phoneNo" class="form-control"  required="required" minlength="6" maxlength="15" number="true"/>
                                       </div>
                                       </div>
                                    </div>    
                                    </div>
                                    
                                     <div class="row">
                             <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbladdress1}</label>
                                       <div class="col-md-7">
                                          <form:input path="address.line1" class="form-control"  data-validate-length-range="3,3"/>
								  </div>
                                       </div>
                                    </div>
                                <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbladdress2}</label>
                                       <div class="col-md-7">
                                         <form:input path="address.line2" class="form-control"  data-validate-length-range="3,3"/>
                                       </div>
                                       </div>
                                    </div>    
                                    </div>
                           
                           
                           
                           
                           <div class="row">
                                    
                                    <div class="col-md-6">
                                       <div class="form-group">
                                    	<label class="control-label col-md-3">${lblcountry}</label>
 									      <div class="col-md-7">
 									        <form:select path="address.country" class="form-control" id="country">
 									          <form:option value="">--select--</form:option>
 									           <c:forEach items="${countries}" var="country">
 									            <form:option value="${country.name}"><spring:message code="${country.name}"/></form:option>
 									           </c:forEach>
 									         </form:select>
 									      </div>
 									   </div>
 									</div>                                   
                                <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblstate}</label>
                                       <div class="col-md-7">
                                         <form:select path="address.state" class="form-control" id="state">
	                        				 <form:option value="">--select--</form:option>
     								       	<c:forEach items="${states}" var="state">
				                    	        <form:option value="${state.name}"><spring:message code="${state.name}"/></form:option>
				                    	    </c:forEach>
										  </form:select>
                             
                                       </div>
                                       </div>
                                    </div>    
                                    </div>
                            <div class="row">
                            <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblcity}</label>
                                       <div class="col-md-7">
                                          <form:input path="address.city" class="form-control"  data-validate-length-range="3,3"/>
								  </div>
                                       </div>
                                    </div>
                                <div class="col-md-6">
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblzipcode}</label>
                                       <div class="col-md-7">
                                          <form:input path="address.zipCode" class="form-control"  data-validate-length-range="3,3"/>
                                       </div>
                                       </div>
                                    </div>    
                                    </div>
                           
                            
                           </div>
                           <div class="tab-pane" id="tab_5_2">
                               <div class="form-group">
							<label class="control-label col-md-3">${lblappointmentdate}<span class="required">*</span></label>
							<div class="col-md-4">
								<div class="input-group date form_datetime">
									   <form:input  path="appointment.appointmentDate" required="required" class="form-control" id="scheduledate" /> 
									   <span class="input-group-btn">
										<button class="btn default date-set" type="button">
											<i class="icon-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
					<div class="form-group">
						<label class="control-label col-md-3">${lbldoctors}<span class="required">*</span></label>
						<div class="col-md-4">
								 <form:select path="appointment.doctor" class="form-control" id="doctors" required="required">
								 <form:option value="">--select--</form:option>
							      <c:forEach items="${doctors}" var="doctor">
							       <option value="${doctor.id}">${doctor.name}</option>
							       </c:forEach>
							     </form:select>
							</div>
						
						</div>
						
						<div class="form-group">
                              <label class="control-label col-md-3">${lbltime}<span class="required">*</span></label>
                              <div class="col-md-4">
                              <div class="input-group bootstrap-timepicker">
                                 <form:input path="appointment.time" class="form-control timepicker-default" required="required" />
                                  <span class="input-group-btn">
                                          <button class="btn default" type="button"><i class="icon-time"></i></button>
                                          </span>
                              </div>
                           </div>
                           </div>
			                        
			                          <div class="form-group">
							<label class="control-label col-md-3">${lblconsultationtype}<span class="required">*</span></label>
							<div class="col-md-4">
							  <form:select path="appointment.counsultationType" class="form-control" id="types" required="required" >
     						 		     <!--   <option value="consultation">CONSULTATION</option>
     						 		       <option value="followup">FOLLOW UP</option> -->
     						 		        <form:option value="">--select--</form:option>
     						 		         	<c:forEach items="${counsultationType}" var="consultationtype">
				                    	        <form:option value="${consultationtype.type}">${consultationtype.type}</form:option>
				                    	    </c:forEach>
								  </form:select>
							</div>
						</div> 
                           
                           <div class="form-group" id="other">
			                           <label class="control-label col-md-3">${lblfee}</label>
			                            <div class="col-md-4">
											<form:input path="appointment.fee"  class="form-control"/>
										</div>
			                       	</div>
                           
                             <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green">${lblsave}</button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default">${lblcancel}</button>                              
                           </div>
                        </div> 
                           
                           </div>
                        
                        </div>
                     </div>
                     
                     </form:form>
                  </div>
               </div>
               
 <!--   <script type="text/javascript">
      jQuery(document).ready(function() { 
    	  FormComponents.init(); 
         FormValidation.init();
         $("#scheduledate").change(function(){
        	 var str = $(this).val();
             $.get("getdoctors.htm?date="+ str,function(data) {
                $('select#doctors').html('<option value="">-- Select --</option>');
              $.each(data,function(index) {
               $('select#doctors').append('<option value="'+data[index].id+'">'+ data[index].name+ '</option>');
              });
                          
              });
        	  
        	});
         
         
             });
      </script>
 -->      



<script type="text/javascript">  
$(document).ready(function() {  
$('#types').change(function(){  
if($('#types').val() === 'CONSULTATION')  
   {  
   $('#other').show();   
   }  
else 
   {  
   $('#other').hide();     
   }  
});  
});  
</script>


