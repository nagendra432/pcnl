<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.doctorschedule.doctor" var="lbldoctor"/>
<spring:message code="label.doctorschedule.date" var="lblscheduledDate"/>
<spring:message code="label.doctorschedule.intime" var="lblintime"/>
<spring:message code="label.doctorschedule.outtime" var="lblouttime"/>
<spring:message code="label.doctorschedule.noofappointments" var="lblnoofappointments"/>


<script type="text/javascript">
 $(document).ready(function(){
  $(".form_datetime").datetimepicker({         
   isRTL: App.isRTL(),
            format: "yyyy-mm-dd",
            weekStart: 1,
            todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
       });
 });
</script>



<script type="text/javascript">

var FormValidation = function () {

    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);
            var submitButton = $('button[type="submit"]',form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                	doctor: {
                        required: true,
                    },
            scheduledDate:{
            required:true	
            },
            inTime:{
            	required:true
            },
            outTime:{
            	required:true
            },
            noOfAppointments:{
            	required:true
            }
           
                },

                  invalidHandler: function (event, validator) { //display error alert on form submit              
                 success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                 submitHandler: function (form) {
                  submitButton.prop("disabled",true);
                 form.submit();
                }  
          });
    }
       var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    } 

    return {
        //main function to initiate the module
        init: function () {

            handleWysihtml5();
            handleValidation1();
            handleValidation2();
        }
    };
}();
</script>

<div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">
               <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i><spring:message code="header.doctorschedule"/></div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>
                  </div>
                  
              <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model">
	                       <form:hidden path="id"/>
	                       <div style="color:red">                      		
		                    <form:errors path="*" cssClass="errorcollection"/>
		                  </div> 
                  <div class="form-body">
                           <div class="alert alert-danger display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formerrormsg"/>
                           </div>
                           <div class="alert alert-success display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formsuccessmsg"/>
                           </div>
                   </div> 
                   
                   
                           
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lbldoctor}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                         <form:select path="doctor" class="form-control" id="doctor" required="required">
	                        				 <form:option value="">--select--</form:option>
     								       	<c:forEach items="${doctors}" var="doctor">
				                    	        <form:option value="${doctor.id}"><spring:message code="${doctor.name}"/></form:option>
				                    	    </c:forEach>
										  </form:select>
                             
                                       </div>
                                       </div>
           
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblscheduledDate}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <div class="input-group date form_datetime">
									   <form:input class="form-control"  path="scheduledDate"  required="required" /> <span class="input-group-btn">
										<button class="btn default date-set" type="button">
											<i class="icon-calendar"></i>
										</button>
									</span>
								        </div>
								  </div>
                                       </div>
                          			
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblintime}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                       <div class="input-group bootstrap-timepicker">
                                          <form:input path="inTime"  class="form-control timepicker-24" time="true" />
                                          <span class="input-group-btn">
                                          <button class="btn default" type="button"><i class="icon-time"></i></button>
                                          </span>
                                       </div>
                                       </div>
                                       </div>
                 
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblouttime}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                       <div class="input-group bootstrap-timepicker">
                                          <form:input path="outTime"  class="form-control timepicker-24" time="true" />
                                          <span class="input-group-btn">
                                          <button class="btn default" type="button"><i class="icon-time"></i></button>
                                          </span>
                                       </div>
                                       </div>
                                       </div>
                                    
                                    <div class="form-group">
                                       <label class="control-label col-md-3">${lblnoofappointments}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="noOfAppointments"  class="form-control" number="true" />
                                       </div>
                                       </div>
               
                      
               
                <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green"><spring:message code="button.save"/></button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default"><spring:message code="button.cancel"/></button>                              
                           </div>
                        </div>
               
               
               
               </form:form>
               </div>
               </div>
               </div>
               </div>