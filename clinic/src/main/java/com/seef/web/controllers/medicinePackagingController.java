package com.seef.web.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.log.Log;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.seef.models.pharmacy.Medicine;
import com.seef.models.pharmacy.MedicinePackagingDetails;
import com.seef.models.pharmacy.Supplier;
import com.seef.service.BaseService;
import com.seef.service.MedicinePackagingService;
import com.seef.service.MedicineService;
import com.seef.service.SupplierService;
import com.seef.web.editors.AbstractModelEditor;

@Controller
@RequestMapping("/medicinepackaging")
public class medicinePackagingController extends CRUDController<MedicinePackagingDetails> {

	private static final Logger _log = LoggerFactory.getLogger(MedicineController.class);

	@Autowired
	MedicinePackagingService medicinePackagingService;

	@Autowired
	MedicineService medicineService;
   
	@Autowired
	SupplierService supplierService;

	@Override
	@RequestMapping(value="/create.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Long id = Long.valueOf(request.getParameter("id"));
		_log.info(">>>>> id is:"+id);
		Medicine medicine=genericDAO.getById(Medicine.class, id);
		_log.info(">>>>> medicine is:"+medicine.getMedicineName());
		MedicinePackagingDetails medicinePackagingDetails=new MedicinePackagingDetails();
		medicinePackagingDetails.setMedicine(medicine);
		setupCreate(modelMap, request);
		modelMap.addAttribute("model", medicinePackagingDetails);
		return "tileDefinition.create-"+getNameSpace();
	}



	@Override
	public void setupCreate(ModelMap modelMap,HttpServletRequest request)
	{
		modelMap.addAttribute("supplier",genericDAO.findAll(Supplier.class));
	}


	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
	   modelMap.addAttribute("medicineid",request.getParameter("id"));
	   _log.info("id++++++++++++++++++"+request.getParameter("id"));
		modelMap.addAttribute("modelList", medicinePackagingService.medicinePackaging_list(Long.valueOf(request.getParameter("id"))));
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);

		return "tileDefinition.manage-"+getNameSpace();
	}


	@RequestMapping(value="/save.htm",method=RequestMethod.POST)
	public String save(@ModelAttribute("model") MedicinePackagingDetails model,
			HttpServletRequest request, HttpServletResponse response,
			BindingResult bindingResult, ModelMap modelMap  ) {

	


		_log.info(">>>>> medicine is:"+model.getMedicine().getId());
		if(model.getId()==null)
		{
			model.setReceived(new Date());
			medicinePackagingService.saveMedicinePackaging(model, getLoggedInUser());
				return getRedirectURL("/manage.htm?id="+model.getMedicine().getId());


		}
		else
		{
			medicinePackagingService.saveOrUpdateMedicinePackaging(model, getLoggedInUser());
			 return getRedirectURL("/manage.htm");
		}


	}



	@Override
	public boolean validateUpdate(MedicinePackagingDetails model,
			BindingResult result) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateCreate(MedicinePackagingDetails entity,
			BindingResult bindingResult) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BaseService<MedicinePackagingDetails, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/getsupplier", method=RequestMethod.GET)
	public void getsupplier(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		PrintWriter out=response.getWriter();
		Supplier supplier=new Supplier();
		supplier.setName(request.getParameter("sname"));
		supplier.setCompany(request.getParameter("scomp"));
		supplier.setPhoneNo(request.getParameter("sphno"));
		supplier.setMailId(request.getParameter("smail"));
		supplierService.save(supplier, getLoggedInUser());
        List<Supplier> supplierslist=supplierService.supplier_list();
        
        List suppliers = new ArrayList();
		 Map supplierMap = null;
		for(Supplier section : supplierslist){
			supplierMap = new HashMap();
			supplierMap.put("id", section.getId());
			supplierMap.put("name", section.getName());
			suppliers.add(supplierMap);
			
		}
		      JSONArray jsonArray = new JSONArray(suppliers);
		      
		      out.print(jsonArray);
				
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	 binder.registerCustomEditor(Supplier.class, new AbstractModelEditor(Supplier.class));
	 super.initBinder(binder);
	}


	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "medicinepackaging";
	}

}
