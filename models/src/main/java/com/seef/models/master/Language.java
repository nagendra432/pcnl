package com.seef.models.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name = "LANGUAGE")
public class Language extends AbstractDomainObject {

	private static final long serialVersionUID = 1761260662464097191L;

	private Long id;
	
	private String code;
	
	private String name;
	
	private String orientation;
	
	private String base;
	
	public Language(){
		
	}
	
	public Language(Long id, String name){
		this.id = id;
		this.name = name;
	}
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "LANGUAGE_SEQ")
	@SequenceGenerator(name = "LANGUAGE_SEQ", sequenceName = "LANGUAGE_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "CODE")
	public String getCode() {
		return code;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "ORIENTATION")
	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	@Column(name = "BASE")
	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

}
