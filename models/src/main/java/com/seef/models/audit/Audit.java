package com.seef.models.audit;

import java.util.List;

import com.seef.models.base.BaseDomainModel;

public interface Audit extends BaseDomainModel {
	/**
	 * Returns the list of fields that should be audited.
	 * @return
	 */
	public List<String> getAuditableFields();

	/**
	 * Returns customized audit log message. When empty, audit logging
	 * uses standard audit message.
	 * @return
	 */
	public String getAuditMessage();

}
