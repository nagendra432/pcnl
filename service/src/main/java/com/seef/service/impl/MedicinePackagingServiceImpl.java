package com.seef.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.base.Status;
import com.seef.models.pharmacy.Medicine;
import com.seef.models.pharmacy.MedicinePackagingDetails;
import com.seef.models.pharmacy.Prescription;
import com.seef.service.MedicinePackagingService;

@Service
public class MedicinePackagingServiceImpl implements MedicinePackagingService{
	
	private static final Logger _log = LoggerFactory.getLogger(OutPatientServiceImpl.class);
	@Autowired
	GenericDAO genericDAO;

	@Override
	public List<MedicinePackagingDetails> medicinePackaging_list(Long id) {
		// TODO Auto-generated method stub
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("status", Status.ACTIVE);
		params.put("medicine",genericDAO.getById(Medicine.class, id));
		_log.info(">>>>>>> id"+id);
		return genericDAO.findAllByCriteria(MedicinePackagingDetails.class, params);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveMedicinePackaging(
			MedicinePackagingDetails medicinepackaging,
			ApplicationUser applicationUser) {
		// TODO Auto-generated method stub
		medicinepackaging.setStatus(Status.ACTIVE);
		medicinepackaging.setCreatedBy(applicationUser);
		medicinepackaging.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(medicinepackaging);
		_log.info(">>>>>>> Save Successfully");
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveOrUpdateMedicinePackaging(
			MedicinePackagingDetails medicinepackaging,
			ApplicationUser applicationUser) {
		// TODO Auto-generated method stub
		medicinepackaging.setModifiedBy(applicationUser);
		medicinepackaging.setModifiedOn(Calendar.getInstance().getTime());
		genericDAO.saveOrUpdate(medicinepackaging);
		_log.info(">>>>>>> Updated Successfully");
	}

	@Override
	public List<MedicinePackagingDetails> medicinePackaging_stocklist(Long id) {
		// TODO Auto-generated method stub
		String query = "SELECT p.id,p.created_by,p.created_on,p.modified_by,p.expired_by,p.modified_on,p.expired_on,p.status,p.received_on,p.cost,p.batchno,p.expiry_date,p.medicine_id,p.supplier,(p.UNITS- COALESCE(SUM(pr.UNITS), 0)) AS UNITS FROM medicine_packaging_details p LEFT JOIN prescription pr ON pr.MEDICINE_PACKAGING_DETAILS_ID=p.ID WHERE p.status='ACTIVE' AND p.medicine_id="+id+" GROUP BY p.BATCHNO";
		  List<MedicinePackagingDetails>  medicinePackagingDetails = genericDAO.executeNativeQuery(query, Prescription.FIND_MEDICINE_PACKAGE);
		System.out.print(medicinePackagingDetails.size());  
		return medicinePackagingDetails;
	}

}
