package com.seef.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.Disease;
import com.seef.models.base.Status;
import com.seef.service.DiseaseService;

@Service
public class DiseaseServiceImpl implements DiseaseService {
	
	private static final Logger _log = LoggerFactory.getLogger(DiseaseServiceImpl.class);

	@Autowired
	GenericDAO genericDAO;
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void saveDisease(Disease disease, ApplicationUser applicationUser) {
		if(disease.getId()==null){
		disease.setStatus(Status.ACTIVE);
		disease.setCreatedBy(applicationUser);
		disease.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(disease);
		_log.info(">>>>>Save Successfully");
		
		}else{
			disease.setModifiedBy(applicationUser);
			disease.setModifiedOn(Calendar.getInstance().getTime());
			genericDAO.saveOrUpdate(disease);
		}

	}

	@Override
	public Boolean validateUnique(Disease disease,Map<String, Object> map) {
		return genericDAO.isUnique(Disease.class, map);
	}
	
	public List<Disease> getAllDiseases(){
		return genericDAO.findAll(Disease.class);
	}

}
