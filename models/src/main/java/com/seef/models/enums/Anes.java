package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;

public enum Anes {
	
	GA("GA"),LA("LA"),SA("SA"),RA("RA");
	
	private String type;
	
	private Anes(String type) {
		this.type = type;
	}

	public static List<Anes> list() 
	{
		return Arrays.asList(values());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
