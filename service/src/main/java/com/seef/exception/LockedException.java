package com.seef.exception;

import org.apache.shiro.authc.AuthenticationException;

public class LockedException extends AuthenticationException{

	private static final long serialVersionUID = 3615086585755292198L;
	
	public LockedException(String message) {
		super(message);
	}

}
