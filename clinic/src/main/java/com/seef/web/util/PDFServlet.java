package com.seef.web.util;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class PDFServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response){
		try{
			String fileLocation = request.getParameter("pdfLocation");
			System.out.println("################ PDF Retrieving: " + fileLocation);
			File file = new File(fileLocation);
			BufferedInputStream fis = new BufferedInputStream(new FileInputStream(file));
			OutputStream ostream = response.getOutputStream();
			response.setContentType("application/pdf");
			response.addHeader("Content-Disposition", "attachment; filename=" + file.getName());
			byte[] pdfBytes= new byte[1024];
			while(fis.read(pdfBytes)!=-1)
				ostream.write(pdfBytes);
			
			fis.close();
			
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

}
