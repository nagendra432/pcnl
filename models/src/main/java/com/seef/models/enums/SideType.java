package com.seef.models.enums;

import java.util.ArrayList;
import java.util.List;

public enum SideType {
	Right("Right"), Left("Left"),Bilateral("Bilateral");
	private String type;

	private SideType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public List<SideType> getSides(){
		List<SideType> side=new ArrayList<SideType>();
		side.add(SideType.Left);
		side.add(SideType.Right);
		return side;
	}

}
