package com.seef.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.SideType;

@Entity
@Table(name = "STONES")
public class Stones extends AbstractDomainObject {

	private static final long serialVersionUID = -6446082971825981709L;
	private Long id;
	private SideType sideType;
	private String number;
	private Double size;
	private OutPatient patient;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "SIDETYPE")
	public SideType getSideType() {
		return sideType;
	}

	public void setSideType(SideType sideType) {
		this.sideType = sideType;
	}

	@Column(name="NUMBER")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name="SIZE")
	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	@ManyToOne
	@JoinColumn(name="PATIENT")
	public OutPatient getPatient() {
		return patient;
	}

	public void setPatient(OutPatient patient) {
		this.patient = patient;
	}

}
