package com.seef.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.dao.util.CollectionUtil;
import com.seef.models.ApplicationUser;
import com.seef.models.SystemUser;
import com.seef.models.enums.UserStatus;
import com.seef.models.enums.UserType;
import com.seef.service.SystemUserService;

@Service
public class SystemUserServiceImpl implements SystemUserService {
	
	@Autowired
	GenericDAO genericDAO;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public SystemUser saveOrUpdate(SystemUser applicationUser){
		return (SystemUser) genericDAO.saveOrUpdate(applicationUser);
	}

	@Override
	public ApplicationUser findByUserName(String username) {
		Map<String, Object> criterias = new HashMap<String, Object>();
		criterias.put("username", username);
		return genericDAO.getByCriteria(ApplicationUser.class, criterias);
	}
	
	@Override
	public SystemUser getSystemUserByUID(String uid) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"uid"}, new Object[]{uid});
		return genericDAO.getByCriteria(SystemUser.class, criterias);
	}

	@Override
	public SystemUser getSystemUserById(Long id) {
		return genericDAO.getById(SystemUser.class, id);
	}

	@Override
	public List<SystemUser> findSystemUserByCreator(Long createdById) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"createdBy.id"}, new Object[]{createdById});
		return genericDAO.findByCriteria(SystemUser.class, criterias);
	}
	public List<SystemUser> findSystemUserStatus(Long createdById) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"createdBy.id"}, new Object[]{createdById});
		return genericDAO.findByCriteria(SystemUser.class, criterias);
	}

	@Override
	public String findUserStatus(Long userId){
		Map<String,Object> criteria=new HashMap<String,Object>();
		criteria.put("userId", userId);
		UserStatus ststus=genericDAO.executeQueryStatusString("userStstus", criteria);
	    return ststus.name();	
	}

	@Override
	public List<SystemUser> findByUserType(UserType userType) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"userType"}, new Object[]{userType});
		return genericDAO.findByCriteria(SystemUser.class, criterias);
	}
}
