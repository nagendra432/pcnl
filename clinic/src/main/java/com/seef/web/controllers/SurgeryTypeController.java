package com.seef.web.controllers;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.models.ApplicationUser;
import com.seef.models.SurgeryType;
import com.seef.models.dto.UserSession;
import com.seef.service.SurgeryTypeService;

@Controller
@RequestMapping("/surgeryType")
public class SurgeryTypeController {
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	SurgeryTypeService surgeryTypeService;
	
	
	@RequestMapping("/create.htm")
	public String create(ModelMap modelMap, HttpServletRequest request){
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masterdata.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.surgery.type", null, userLocale));
		modelMap.addAttribute("model", new SurgeryType());	
		return "tileDefinition.create-surgeryType";
		
	}
	
	@RequestMapping(value="save.htm",method = RequestMethod.POST)
	public String save(@ModelAttribute("model") SurgeryType model,HttpServletRequest request, HttpServletResponse response,BindingResult bindingResult, ModelMap modelMap) {

		 if(model.getId() == null){
			 
			 if(!validateCreate(model, bindingResult))
			 {
				 return "tileDefinition.create-surgeryType" ;
			 }
			 else
			 {
				 surgeryTypeService.saveSurgeryType(model, getLoggedInUser());
				 return "redirect:/surgeryType/manage.htm";
			 }
		 }
		return "tileDefinition.create-surgeryType";
	}
	
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
			modelMap.addAttribute("modelList", surgeryTypeService.getAllSurgeryTypes());
			Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
			request.getSession().setAttribute("nav", messageSource.getMessage("masterdata.menu.name", null, userLocale));
			request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.surgery.type", null, userLocale));
			return "tileDefinition.manage-surgeryType";
	}
	
	public ApplicationUser getLoggedInUser() {
		UserSession userSession = (UserSession) SecurityUtils.getSubject() .getPrincipal();
		if(userSession != null)
			return userSession.getApplicationUser();
		else
			return null;
	}
	
	public boolean validateCreate(SurgeryType entity, BindingResult bindingResult) {
		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		criteria.put("name", entity.getName());
		if(!surgeryTypeService.validateUnique(entity, criteria))
		{
			 FieldError error=new FieldError("modelObject","name",entity.getName(), false, null, null, messageSource.getMessage("label.doctor.error.codeMessage",null,userLocale));
			   bindingResult.addError(error);
			   validationResult = false;
			
		}
		return validationResult;
	}

}
