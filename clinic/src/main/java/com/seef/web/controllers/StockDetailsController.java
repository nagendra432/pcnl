package com.seef.web.controllers;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.models.base.Status;
import com.seef.models.pharmacy.Medicine;
import com.seef.models.pharmacy.MedicinePackagingDetails;
import com.seef.service.BaseService;
import com.seef.service.MedicinePackagingService;
import com.seef.service.StockDetailsService;

@Controller
@RequestMapping("/stockdetails")
public class StockDetailsController extends CRUDController<Medicine> {
	
	private static final Logger _log = LoggerFactory.getLogger(StockDetailsController.class);
	
	@Autowired
	StockDetailsService stockDetailsService;
	
	@Autowired
	MedicinePackagingService medicinePackagingService;
	@Autowired
	MessageSource messageSource;

	@Override
	@RequestMapping(value="/view.htm",method=RequestMethod.GET)
	public String view(ModelMap modelMap, HttpServletRequest request) {
		_log.info(" >>> Stock Size list" + stockDetailsService.stock_list().size());
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("medicine.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("stock.medicine.menu.name", null, userLocale));
		modelMap.addAttribute("modelList",stockDetailsService.stock_list());
		return "tileDefinition.view-"+getNameSpace();
	}
	
	@Override
	@RequestMapping(value="/manage.htm",method=RequestMethod.GET)
	public String manage(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("medicine.menu.name", null, userLocale));
	    request.getSession().setAttribute("innerNav", messageSource.getMessage("stock.adjust.menu.name", null, userLocale));
		
		modelMap.addAttribute("modelList",stockDetailsService.expiry_list());
		return "tileDefinition.manage-"+getNameSpace();
	}
	
	@Override
	public boolean validateUpdate(Medicine model, BindingResult result) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@RequestMapping(value="/close.htm", method=RequestMethod.GET)
	public String close(ModelMap modelMap, HttpServletRequest request) {
		Long packageid = Long.valueOf(request.getParameter("id"));
		MedicinePackagingDetails packaging=stockDetailsService.getById(packageid);
		
		packaging.setStatus(Status.INACTIVE);
		medicinePackagingService.saveOrUpdateMedicinePackaging(packaging, getLoggedInUser());
		
		return getRedirectURL("/manage.htm");
	};
	

	@Override
	public boolean validateCreate(Medicine entity, BindingResult bindingResult) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BaseService<Medicine, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "stockdetails";
	}

}
