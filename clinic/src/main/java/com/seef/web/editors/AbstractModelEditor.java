package com.seef.web.editors;

import java.beans.PropertyEditorSupport;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.seef.models.base.AbstractDomainObject;

@Component
public class AbstractModelEditor extends PropertyEditorSupport {
	


	private Class<?> clazz;
	
	public AbstractModelEditor(Class<?> clazz) {
		this.clazz = clazz;
	}
	

	public AbstractModelEditor() {
	}
	
	private Long getPkFromString(String text) {
		return Long.parseLong(text);
	}

	public void setAsText(String text) throws IllegalArgumentException {
		AbstractDomainObject model = null;
		if (!StringUtils.isEmpty(text)) {
			model = getEntityInstance(); 
			model.setId(getPkFromString(text));			
		}
		else{
			model = null;
		}
		setValue(model);
	}
	
	
    protected AbstractDomainObject getEntityInstance()
    {
        try {
            return (AbstractDomainObject)clazz.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


}