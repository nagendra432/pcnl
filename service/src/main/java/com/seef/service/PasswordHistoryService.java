package com.seef.service;

import java.util.Date;

import com.seef.models.ApplicationUser;
import com.seef.models.PasswordHistory;
import com.seef.models.enums.PasswordType;


public interface PasswordHistoryService extends BaseService<PasswordHistory, Long> {


   
	public boolean isInLastThreeUsedPasswords(Long sysUserId, String newPass,PasswordType type);

	//public void savePasswordHistory(ApplicationUser sysUser, String password,PasswordType type);
	public boolean isValidatePasswordInPwdHistory(String password,ApplicationUser user);
	public Date getLastChangedPasswordByUserId(Long sysUserId);

	public void savePasswordHistory(ApplicationUser userFromDB, String password,PasswordType app);
}
