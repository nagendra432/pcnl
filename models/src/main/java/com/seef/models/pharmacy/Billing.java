package com.seef.models.pharmacy;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name="BILLING")
@NamedQueries({
@NamedQuery(name="findmaxbillno",query="select  max(billing.billNo) from Billing billing")})
public class Billing extends AbstractDomainObject {

	private static final long serialVersionUID = 1695459057788840457L;

	private Long id;
	private Long billNo;
	private String name;
	private List<Prescription> prescriptions;
	private Double totalCost;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "BILLING_SEQ")
	@SequenceGenerator(name = "BILLING_SEQ", sequenceName = "BILLING_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "BILL_NO")
	public Long getBillNo() {
		return billNo;
	}
	public void setBillNo(Long billNo) {
		this.billNo = billNo;
	}
	
	@Column(name = "NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="BILL_NO")
	public List<Prescription> getPrescriptions() {
		return prescriptions;
	}
	public void setPrescriptions(List<Prescription> prescriptions) {
		this.prescriptions = prescriptions;
	}
	
	@Column(name = "TOTAL_COST")
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	
	

}
