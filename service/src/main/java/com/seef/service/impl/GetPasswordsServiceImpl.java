package com.seef.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seef.dao.GenericDAO;
import com.seef.dao.util.CollectionUtil;
import com.seef.models.ApplicationUser;
import com.seef.models.PasswordHistory;
import com.seef.models.enums.PasswordType;
import com.seef.service.GetPasswordsService;

@Service
public class GetPasswordsServiceImpl implements GetPasswordsService {
	
	private static final Logger _log = LoggerFactory.getLogger(GetPasswordsServiceImpl.class);
	
	@Autowired
	private GenericDAO genericDAO;

	@Override
	public List<PasswordHistory> getApplicationUserLastPasswords(Long sysUserId,PasswordType passwordType) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"userId", "type"}, new Object[]{sysUserId, passwordType});
		_log.info("Returns Last User Passwords For System User");
		return genericDAO.findByNamedQuery("PasswordHistory.getApplicationUserLastThree",criterias);
	}
	
	@Override
	public List<PasswordHistory> getMobileUserLastPasswords(Long mobileUserId,PasswordType passwordType) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"userId", "type"}, new Object[]{mobileUserId, passwordType});
		_log.info("Returns Last User Passwords For Mobile User");
		return genericDAO.findByNamedQuery("PasswordHistory.getApplicationUserLastThree",criterias);
	}

	@Override
	public List<PasswordHistory> getPasswords(Long sysUserId,PasswordType passwordType, Long size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteOldPassword(PasswordHistory passwordHistory) {
		genericDAO.delete(passwordHistory);
		
	}

	@Override
	public void savePassword(PasswordHistory passwordHistory) {
		genericDAO.save(passwordHistory);
		
	}

	@Override
	public List<PasswordHistory> getLastMPINS(ApplicationUser mobileUser,PasswordType passwordType, Long size) {
		Map<String, Object> criterias=new HashMap<String, Object>();
		criterias.put("mobileUser",mobileUser);
		criterias.put("type", passwordType);
		return genericDAO.findByNamedQuery("PasswordHistory.LastThree",criterias);
	}

	@Override
	public List<PasswordHistory> getLastTPINS(ApplicationUser mobileUser,PasswordType passwordType, Long size) {
		Map<String, Object> criterias=new HashMap<String, Object>();
		criterias.put("mobileUser",mobileUser);
		criterias.put("type", passwordType);
		return genericDAO.findByNamedQuery("PasswordHistory.LastThree",criterias);
	}

}
