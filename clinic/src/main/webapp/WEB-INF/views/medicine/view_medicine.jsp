<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.medicinename" var="lblmedicinename"/>
<spring:message code="label.company" var="lblcompany"/>
<spring:message code="label.medicinetype" var="lblmedinetype"/>
<spring:message code="label.description" var="lbldescription"/>


<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-wide">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:history.go(-1);">&times;</button>
        <h4 class="modal-title" align="center"><spring:message code="label.medicine"/></h4>
     </div>
    <div class="modal-body">
    
            <div class="scroller" style="height:400px" data-always-visible="1" data-rail-visible1="1">
            <div class="row">
                        
                            <div class="wizard_body clearfix">
                            	<form:form   class="mws-form wzd-validate form-horizontal" commandName="model">
                                <fieldset class="wizard-step mws-form-inline">
								
								 <div class="col-md-6">		
								 <div class="form-group">
								<label class="control-label col-md-5" for="inputInfo">${lblmedicinename}</label>
								<div class="col-md-7">
									<span class="">
			                    	<form:input path="medicineName"  disabled="true" class="form-control" id="inputWarning"/>
								</span>		
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-5" for="inputInfo">${lblcompany}</label>
								<div class="col-md-7">
									<span class="">
			                    	<form:input path="company"  disabled="true" class="form-control" id="inputWarning"/>
								</span>		
								</div>
							</div>
						
                           <div class="form-group">
								<label class="control-label col-md-5" for="country">${lblmedinetype}</label>
								<div class="col-md-7">
									<span class="">
									<form:select path="medicineType" class="form-control" id="medicinetype" required="required" disabled="true">
									       <form:option value="">--select--</form:option>
		     						 		       	<c:forEach items="${medicineType}" var="medicinetype">
		     						 		       	<c:choose>
			                                          <c:when test="${model.medicineType eq medicinetype}">
			                                             <form:option value="${medicinetype.type}" selected="selected" >${medicinetype.type}</form:option>
			                                          </c:when>
		                                         <c:otherwise>
		                                            <form:option value="${medicinetype.type}">${medicinetype.type}"</form:option>
		                                        </c:otherwise>
		                                         </c:choose> 
		     						 		      	    </c:forEach>
						                    	    
										 </form:select>
     							  </span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-5" for="inputInfo">${lbldescription}</label>
								<div class="col-md-7">
									<span class="">
			                    	<form:textarea path="description"  disabled="true" class="form-control" id="inputWarning"/>
								</span>		
								</div>
							</div>
							
                           
                           </div>
						</fieldset>
                    </form:form>
                 </div>
                <hr />
              </div>
              </div>
              </div>
              
              </div><!--/widget-main-->
			</div><!--/widget-body-->
			
		</div>
		
<script>
    $("#myModal").modal({                   
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                   
    });
</script>