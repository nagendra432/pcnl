package com.seef.models.enums;

public enum Injurey {
	Right("Right"),Left("Left"),Both("Both");

	private String type;
	
	private Injurey(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
