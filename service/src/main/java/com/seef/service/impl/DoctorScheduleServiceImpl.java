package com.seef.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.base.Status;
import com.seef.models.doctor.DoctorSchedule;
import com.seef.service.DoctorScheduleService;

@Service
public class DoctorScheduleServiceImpl implements DoctorScheduleService{
	
	private static final Logger _log = LoggerFactory.getLogger(DoctorScheduleServiceImpl.class);
	
	@Autowired
	GenericDAO genericDAO;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void save(DoctorSchedule doctorSchedule, ApplicationUser applicationUser) {
		if(doctorSchedule.getId()==null){
			doctorSchedule.setStatus(Status.ACTIVE);
			doctorSchedule.setCreatedBy(applicationUser);
			doctorSchedule.setCreatedOn(Calendar.getInstance().getTime());
			genericDAO.save(doctorSchedule);
			_log.info(">>>>>>> Save Successfully");
		}
		else{
			doctorSchedule.setModifiedBy(applicationUser);
			doctorSchedule.setModifiedOn(Calendar.getInstance().getTime());
			genericDAO.saveOrUpdate(doctorSchedule);
			_log.info(">>>>>>>Updates Successfully");
		}
		
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveOrUpdate(DoctorSchedule doctorSchedule,ApplicationUser applicationUser) {
		doctorSchedule.setModifiedBy(applicationUser);
		doctorSchedule.setModifiedOn(Calendar.getInstance().getTime());
		genericDAO.saveOrUpdate(doctorSchedule);
		
	}


	@Override
	public List<DoctorSchedule> doctorschedule_list() {
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("status", Status.ACTIVE);
		return genericDAO.findAllByCriteria(DoctorSchedule.class, params);
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public DoctorSchedule getById(Long id) {
		return genericDAO.getById(DoctorSchedule.class, id);
	}


	@Override
	public List<DoctorSchedule> schedule_byDate(Map<String, Object> criteria) {
		// TODO Auto-generated method stub
		return genericDAO.findByNamedQuery("listbyscheduledate", criteria);
	}
	
	
}
