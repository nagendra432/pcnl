<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.outpatient.firstname" var="lblfirstname"/>
<spring:message code="label.outpatient.middlename" var="lblmiddlename"/>
<spring:message code="label.outpatient.lastname" var="lbllastname"/>
<spring:message code="label.outpatient.phno" var="lblphno"/>
<spring:message code="label.address.line1" var="lbladdress1"/>
<spring:message code="label.address.line2" var="lbladdress2"/>
<spring:message code="label.address.zipcode" var="lblzipcode"/>
<spring:message code="label.address.city" var="lblcity"/>
<spring:message code="label.address.country" var="lblcountry"/>
<spring:message code="label.address.state" var="lblstate"/>
<spring:message code="label.address.fulladdress" var="lblfulladdress"/>
<spring:message code="label.outpatient.dob" var="lbldob"/>
<spring:message code="label.outpatient.time" var="lbltime"/>
<spring:message code="label.appointmentdate" var="lblappointmentdate"/>
<spring:message code="label.consultationtype" var="lblconsultationtype"/>
<spring:message code="label.doctors" var="lbldoctors"/>
<spring:message code="label.fee" var="lblfee"/>



<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-wide">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:history.go(-1);">&times;</button>
        <h4 class="modal-title" align="center"><spring:message code="label.outpatient" /></h4>
     </div>
    <div class="modal-body">
    
            <div class="scroller" style="height:400px" data-always-visible="1" data-rail-visible1="1">
            <div class="row">
                        
                            <div class="wizard_body clearfix">
                            	<form:form action="save.htm" method="POST" class="mws-form wzd-validate form-horizontal" commandName="model">
                                <fieldset class="wizard-step mws-form-inline">
								<h4>
								<legend class="wizard-label"><i class="icol-user-thief-baldie"></i>${lblheaderoutpatient }</legend></h4>
								
								 <div class="col-md-6">		
								 <div class="form-group">
								<label class="control-label col-md-5" for="inputInfo">${lblmrno}</label>
								<div class="col-md-7">
									<span class="">
			                    	<form:input path="mrno"  disabled="true" class="form-control" id="inputWarning"/>
								</span>		
								</div>
							</div>
								<div class="form-group">
								<label class="control-label col-md-5" for="inputInfo">${lblfirstname}</label>
								<div class="col-md-7">
									<span class="">
			                    	<form:input path="firstName"  disabled="true" class="form-control" id="inputWarning"/>
								</span>		
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="control-label col-md-5" for="inputInfo">${lblmiddlename}</label>
								<div class="col-md-7">
									<span class="">
			                    	<form:input path="middleName"  disabled="true" class="form-control" id="inputWarning"/>
								</span>		
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-5" for="inputInfo">${lbllastname}</label>
								<div class="col-md-7">
									<span class="">
			                    	<form:input path="lastName"  disabled="true" class="form-control" id="inputWarning"/>
								</span>		
								</div>
							</div>
							
							
							 <div class="form-group">
                                   <label class="control-label col-md-5">${lbldob}</label>
                                   <div class="col-md-7">
                                   <form:input type="text" path="dob" size="16"  class="required form-control" disabled="true" id="inputWarning"/>
                                </div>
                             </div>	
                             
							 <div class="form-group">
                                   <label class="control-label col-md-5">${lblphno}</label>
                                   <div class="col-md-7">
                                   <form:input type="text" path="phoneNo"  class="required form-control" disabled="true" id="inputWarning"/>
                                </div>
                             </div>
							
							 <div class="form-group">
                              <label class="control-label col-md-5">${lbladdress1}</label>
                              <div class="col-md-7">
                                 <form:input path="address.line1" class="form-control" disabled="true" id="inputWarning" />
                              </div>
                           </div>
                           
                           <div class="form-group">
                              <label class="control-label col-md-5">${lbladdress2}</label>
                              <div class="col-md-7">
                                 <form:input path="address.line2" class="form-control"  disabled="true" id="inputWarning"/>
                              </div>
                           </div>
							
							<div class="form-group">
                              <label class="control-label col-md-5">${lblcity}</label>
                              <div class="col-md-7">
                                 <form:input path="address.city" class="form-control"  disabled="true" id="inputWarning"/>
                              </div>
                           </div>
                           <div class="form-group">
								<label class="control-label col-md-5" for="country">${lblcountry}</label>
								<div class="col-md-7">
									<span class="">
									  <form:select  path="address.country" required="required" class="form-control" id="select2_sample4" disabled="${disabledStatus}">
					 						<form:option value="">--${lblselect}--</form:option>
		                                        <c:forEach items="${countries}" var="country" >		                                         
		                                          <c:choose>
			                                          <c:when test="${model.address.country eq country.name}">
			                                             <form:option value="${country.id}" selected="selected"  data-country-code="${country.code }"><spring:message code="${country.name}"/></form:option>
			                                          </c:when>
		                                         <c:otherwise>
		                                            <form:option value="${country.id}"  data-country-code="${country.code }"><spring:message code="${country.name}"/></form:option>
		                                        </c:otherwise>
		                                         </c:choose> 
		                                         </c:forEach>
										</form:select>
     							  </span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-5" for="state">${lblstate}</label>
								<div class="col-md-7">
									<span class="">
									<form:select path="address.state" class="form-control" id="state" required="required" disabled="${disabledStatus}">
	                        				 <form:option value="">--${lblselect}--</form:option>
     								       	<c:forEach items="${states}" var="state">
				                    	        <form:option value="${state.name}"><spring:message code="${state.name}"/></form:option>
				                    	    </c:forEach>
										  </form:select>
     							    </span>
								</div>
							</div>
                           <div class="form-group">
								<label class="control-label col-md-5" for="zipcode">${lblzipcode}</label>
								<div class="col-md-7">
									<span class="">
	                    	      <form:input path="address.zipCode" class="form-control" id="zipcode" disabled="true"/>
									</span>	
								</div>
							</div>
							
                           </div>
						</fieldset>
                    </form:form>
                 </div>
                <hr />
              </div>
              </div>
              </div>
              
              </div><!--/widget-main-->
			</div><!--/widget-body-->
			
		</div>
		
<script>
    $("#myModal").modal({                   
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                   
    });
</script>


   <script type="text/javascript">
      jQuery(document).ready(function() { 
    	  FormComponents.init(); 
         FormValidation.init();
         $("#scheduledate").change(function(){
        	 var str = $(this).val();
             $.get("getdoctors.htm?date="+ str,function(data) {
                $('select#doctors').html('<option value="">-- Select --</option>');
              $.each(data,function(index) {
               $('select#doctors').append('<option value="'+data[index].id+'">'+ data[index].name+ '</option>');
              });
                          
              });
        	  
        	});
         
         
             });
      </script>
      



<script type="text/javascript">  
$(document).ready(function() {  
$('#types').change(function(){  
if($('#types').val() === 'CONSULTATION')  
   {  
   $('#other').show();   
   }  
else 
   {  
   $('#other').hide();     
   }  
});  
});  
</script>
