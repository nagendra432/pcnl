package com.seef.models.dto;


import java.io.Serializable;

import com.seef.models.ApplicationUser;
import com.seef.models.Preferences;
import com.seef.models.master.Theme;
import com.seef.models.menu.dto.Menu;

public class UserSession implements Serializable {

	private static final long serialVersionUID = -8389470427063469401L;
	
	private ApplicationUser applicationUser;
	private Long institutionId;
	private Long primaryId;
	private Preferences preferences;
	private Theme theme;
	private Long parentId;
	private Menu subMenu;

	
	public Long getInstitutionId() {
		return institutionId;
	}


	public void setInstitutionId(Long institutionId) {
		this.institutionId = institutionId;
	}



	public Long getPrimaryId() {
		return primaryId;
	}

	public void setPrimaryId(Long primaryId) {
		this.primaryId = primaryId;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Preferences getPreferences() {
		return preferences;
	}

	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}

	public Menu getSubMenu() {
		return subMenu;
	}

	public void setSubMenu(Menu subMenu) {
		this.subMenu = subMenu;
	}

	public ApplicationUser getApplicationUser() {
		return applicationUser;
	}


	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}


	public Theme getTheme() {
		return theme;
	}


	public void setTheme(Theme theme) {
		this.theme = theme;
	}

}
