<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.save" var="lblsave"/>
<spring:message code="label.cancel" var="lblcancel"/>
<spring:message code="label.medicinename" var="lblmedicinename"/>
<spring:message code="label.company" var="lblcompany"/>
<spring:message code="label.medicinetype" var="lblmedinetype"/>
<spring:message code="label.description" var="lbldescription"/>
<spring:message code="label.medicineregister" var="lblmedicineregister"/>


 <div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>${lblmedicineregister}</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model">
	                       <form:hidden path="id"/>
	                       <div style="color:red">                      		
		                    <form:errors path="*" cssClass="errorcollection"/>
		                  </div>
                        <div class="form-body">
                           <div class="alert alert-danger display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formerrormsg"/>
                           </div>
                           <div class="alert alert-success display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formsuccessmsg"/>
                           </div>
                   </div> 
                           
                           
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblmedicinename}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="medicineName"  class="form-control"  required="required"/>
                                       </div>
                                       </div>
                                  
                                   
                          			 <div class="form-group">
                                       <label class="control-label col-md-3">${lblcompany}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                        <form:input path="company"  class="form-control"   
					                                required="required" />
                                       </div>
                                       </div>
                                      
                                      <div class="form-group">
                                       <label class="control-label col-md-3">${lblmedinetype}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                        <form:select path="medicineType" class="form-control" id="medicinetype" required="required">
									       <form:option value="">--select--</form:option>
		     						 		       	<c:forEach items="${medicineType}" var="medicinetype">
						                    	        <form:option value="${medicinetype.type}">${medicinetype.type}</form:option>
						                    	    </c:forEach>
						                    	    
										 </form:select>
                                       </div>
                                       </div>
                                    <div class="form-group">
                                       <label class="control-label col-md-3">${lbldescription}</label>
                                       <div class="col-md-4">
                                        <form:textarea path="description"  class="form-control" />
                                       </div>
                                       </div>
                           
                           
                           
                           
                           
                        <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green">${lblsave}</button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default">${lblcancel}</button>                              
                           </div>
                        </div>
                     </form:form>
                     <!-- END FORM-->
                  </div>
               </div>
               <!-- END VALIDATION STATES-->
            </div>
         </div>
  