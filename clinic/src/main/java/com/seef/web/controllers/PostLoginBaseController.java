package com.seef.web.controllers;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.seef.models.ApplicationUser;
import com.seef.models.dto.UserSession;

public class PostLoginBaseController {

	@Autowired
	private MessageSource messageSource;


	public Session getSession() {
		return SecurityUtils.getSubject().getSession();
	}

	public ApplicationUser getLoggedInUser() {
		UserSession userSession = (UserSession) SecurityUtils.getSubject() .getPrincipal();
		if(userSession != null)
			return userSession.getApplicationUser();
		else
			return null;
	}

	public String getMessage(String code, Object[] args) {

		return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());

	}

	public Long getIdFromSession(){
		UserSession userSession = (UserSession) SecurityUtils.getSubject() .getPrincipal();
		return userSession.getPrimaryId();
	}

	public void setIdInSession(Long primaryId){
		UserSession userSession = (UserSession) SecurityUtils.getSubject() .getPrincipal();
		userSession.setPrimaryId(primaryId);
	}

	public Long getParentIdFromSession(){
		UserSession userSession = (UserSession) SecurityUtils.getSubject() .getPrincipal();
		return userSession.getParentId();
	}

	public void setParentIdInSession(Long parentId){
		UserSession userSession = (UserSession) SecurityUtils.getSubject() .getPrincipal();
		userSession.setParentId(parentId);
	}

}
