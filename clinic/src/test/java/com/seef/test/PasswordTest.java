package com.seef.test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.velocity.runtime.directive.Foreach;
import org.junit.Test;

public class PasswordTest {

	@Test
	public void test() {
		Long sysUserId = 12L; //Mobile User Id from Mpin & Tpin , SysUserId for Password
		String password = "123";
		Object salt = prepareSalt(String.valueOf(sysUserId));
		String hashedPasswordBase64 = new Sha256Hash(password, salt, 1024).toBase64();
		System.out.println("Hashed Password for User: " + sysUserId + ", " + hashedPasswordBase64);

	}

	public String prepareSalt(String suffix){
		return "SeefTech"+suffix;
	}
	
	@Test
	public void testSplit(){
		String message = "ABC;XYZ;123||ABC1;XYZ1;1231||ABC2;XYZ2;1232";
//		String message = "ABC;XYZ;123";
		String[] splittedMessage = message.split("\\|\\|");
		System.out.println(">> Lenght: " + splittedMessage.length);
		System.out.println(">>> Message: " + splittedMessage[0]);
	}
	
}
