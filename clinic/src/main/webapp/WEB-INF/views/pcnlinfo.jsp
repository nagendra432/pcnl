<%@ taglib prefix="form"  uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row">
<form action="${pageContext.request.contextPath}/outpatient/surgery.htm" id="form_sample_1" class="form-horizontal" method="post">
	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-4 control-label label-align">Patient MR No : </label>
			<div class="col-md-3" id="searchNo">
				<input type="text" name="searchMrno" id="searchMrno" required="required" class="form-control mrnumber" value="${mrNum}">
			</div>
			<button id="search" type="submit" class="btn default">Search</button>
			<button type="button" onclick="location.href='${pageContext.request.contextPath}/outpatient/create.htm';" class="btn default">Create New</button>
		</div>
	</div>
</form>
</div>

<c:if test="${model.id ne null}">
	<form:form action="updatePatient.htm"  id="form_sample_1" class="form-horizontal" commandName="model" >
	<form:hidden path="id"/>
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption"><i class="icon-cogs"></i>Patient Details</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">FirstName</label>
						<div class="col-md-6">
							<form:input path="firstName" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">LastName</label>
						<div class="col-md-6">
							 <form:input path="lastName" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Birth Date</label>
						<div class="col-md-6">
						   <form:input path="dob" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Gender</label>
						<div class="col-md-6">
						    <form:input path="gender" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Contact Number</label>
						<div class="col-md-6">
						   <form:input path="phoneNo" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">MR No</label>
						<div class="col-md-6">
							<form:input path="mrno" class="form-control" disabled="true"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="portlet box blue">
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label">Surgery Type</label>
						<div class="col-md-6">
						   	<select name="surgeryType" class="form-control select2me" id="surgeryTypeID">
						      	<option value="">-- select --</option>
							    <c:forEach items="${surgeryType}" var="surgerytype">
							      <option value="${surgerytype.id}">${surgerytype.name}</option>
							    </c:forEach>
					       </select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
 					<div class="form-group">
					<label class="control-label col-md-5">Surgery</label>
						<div class="col-md-6">
 							<form:select path="surgery" class="form-control select2me" id="surgeryID">
 							 <c:if test="${!empty model.surgery[0]}">
 							   <form:option value="${model.surgery[0].id}">${model.surgery[0].name}</form:option>
 							   </c:if>
							</form:select>
     					</div>
  					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label">Anes</label>
						<div class="col-md-6">
					       <form:select path="anes" class="form-control select2me">
					          <form:option value="">-- select --</form:option>
							  <form:options items="${anes}" itemLabel="type" itemValue="type" />
					       </form:select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
 					<div class="form-group">
					<label class="control-label col-md-5">ADTNL Procedure</label>
						<div class="col-md-6">
							<form:select path="adnlProcedure" class="form-control select2me">
							  <form:option value="">-- select --</form:option>
							  <form:options items="${adnlProcedures}" itemLabel="name" itemValue="id"/>
							</form:select>
     					</div>
  					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align"></label>
						<div class="col-md-6">
						   <input type="checkbox" id="right" checked="checked"> <label for="right">Stones Left Side </label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align"></label>
						<div class="col-md-6">
							<input type="checkbox" id="left"> <label for="left">Stones Right Side</label>
						</div>
					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Number</label>
						<div class="col-md-6">
							<form:input path="stone[0].number"  class="form-control" />
						</div>
					</div>
					
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Number</label>
						<div class="col-md-6">
							<form:input path="stone[1].number"  class="form-control" />
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Size(cm) 1</label>
						<div class="col-md-6">
							<form:input path="stone[0].size"  class="form-control"  />
						</div>
					</div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label label-align">Size(cm) 2</label>
					<div class="col-md-6">
						<form:input path="stone[1].size"  class="form-control" />
					</div>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Energy</label>
						<div class="col-md-6">
							<form:input path="energy" class="form-control"/>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align"></label>
						<div class="col-md-6">
						   <input type="checkbox" id="right" checked="checked"> <label for="right">DJ Stent Left Side </label>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align"></label>
						<div class="col-md-6">
							<input type="checkbox" id="left" > <label for="left">DJ Stent Right Side</label>
						</div>
					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Size</label>
						<div class="col-md-6">
						   <form:input path="dJStent[0].size"  class="form-control"  />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Size</label>
						<div class="col-md-6">
						   <form:input path="dJStent[1].size"  class="form-control"  />
						</div>
					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label label-align">Dangler</label>
						<div class="col-md-6">
							<form:input path="dJStent[0].dangler" class="form-control"/>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-5 control-label">Surgeon</label>
						<div class="col-md-6">
							<form:select path="surgeon"  class="form-control select2me">
							  <form:option value="">-- select --</form:option>
							  <form:options items="${surgeons}" itemLabel="firstName" itemValue="id"/>
							</form:select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
 					<div class="form-group">
					<label class="control-label col-md-5">Asst Surgeon</label>
						<div class="col-md-6">
							<form:select path="asstSurgeon"  class="form-control select2me">
							  <form:option value="">-- select --</form:option>
							  <form:options items="${asstSurgeons}" itemLabel="firstName" itemValue="id"/>
							</form:select>
     					</div>
  					</div>
				</div>
			 </div>
			 
			 <div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-3 control-label label-align">Remarks</label>
						<div class="col-md-8">
							<form:textarea path="remarks" rows="3" class="form-control"/>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-3 control-label label-align">Complications</label>
						<div class="col-md-8">
							<form:textarea path="complication" rows="3" class="form-control"/>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="form-actions fluid">
	    <div class="col-md-offset-5 col-md-7">
	       <button type="submit" class="btn green">Update</button>
	       <button type="button" onclick="javascript:history.go(-1);" class="btn default">Cancel</button>                              
	    </div>
    </div> 
	</form:form>
</c:if>
<script type="text/javascript">
$(document).ready(function() {
  $("#surgeryTypeID").change(function() {
   $.ajax({
    type : "GET",
    url : "${pageContext.request.contextPath}/outpatient/surType.htm",
    dataType : 'json',
    data : {
     "surgeryTypeID" : $(this).val()
    },
    success : function(json) {
     var surgeryDiv = $('#surgeryID');
     if($('#surgeryID').empty()) {
      surgeryDiv.append('<option value="">---Select Surgerys ---</option>');
     }
     $.each(json,function(i, value) {     
      surgeryDiv.append('<option value="' + value.id + '">'+ value.name+ '</option>');
     });
    }
   });
  });
}); 
</script>