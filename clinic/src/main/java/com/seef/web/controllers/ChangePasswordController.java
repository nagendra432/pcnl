package com.seef.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.SystemUser;
import com.seef.models.enums.PasswordType;
import com.seef.service.BaseService;
import com.seef.service.ForcePasswordService;
import com.seef.service.PasswordHistoryService;
import com.seef.service.PasswordManager;

@RequestMapping("changepassword")
@Controller
public class ChangePasswordController extends CRUDController<SystemUser> {
	private static final Logger _log = LoggerFactory.getLogger(ChangePasswordController.class);
	
	@Autowired
	ForcePasswordService forcePasswordService;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	private PasswordManager passwordManager;
	
	@Autowired
	PasswordHistoryService passwordHistoryService;	

	@Autowired
	GenericDAO genericDAO;

	
	
	
	
	
	@Override
	@RequestMapping(value="updatepassword.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request) 
	{
		return "tileDefinition.create-"+getNameSpace();
	};
	

	@RequestMapping(value="updatenewpassword",method=RequestMethod.POST)
   public String update(@RequestParam String oldPassword, @RequestParam String confirmPassword,ModelMap modelMap,HttpServletRequest request) 
	{
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		ApplicationUser loggedInUser=getLoggedInUser();
		if(!forcePasswordService.validateOldPassword(loggedInUser, oldPassword))
		{
			modelMap.addAttribute("error", this.messageSource.getMessage("error.label.password.old", null, userLocale));
			_log.info(">>>message:"+this.messageSource.getMessage("error.label.password.old", null, userLocale));
			return "tileDefinition.create-" + getNameSpace();
			
		}
		List<String> list=validateConfirmPassword(confirmPassword, modelMap, request);
		if(list.isEmpty()==false){
			modelMap.addAttribute("errorheader",this.messageSource.getMessage("error.label.forcepassword.header",null,userLocale));
			_log.info(">>>message:"+this.messageSource.getMessage("error.label.forcepassword.header", null, userLocale));
			modelMap.addAttribute("error", list);
			return "tileDefinition.create-" + getNameSpace();
		}
		if(!passwordHistoryService.isValidatePasswordInPwdHistory(confirmPassword, loggedInUser))
		{
			modelMap.addAttribute("error", this.messageSource.getMessage("error.user.validatePassword", null, userLocale));
			_log.info(">>>message:"+this.messageSource.getMessage("error.user.validatePassword", null, userLocale));
			return "tileDefinition.create-" + getNameSpace();
		}
		else{
			passwordHistoryService.savePasswordHistory(loggedInUser, confirmPassword, PasswordType.APP);
			forcePasswordService.update(getLoggedInUser(), confirmPassword);
			_log.info(">>>message:Password SuccessFully Updated ");
			return "tileDefinition.manage-" + getNameSpace();
		}
		
		
	};
	
	
public List<String> validateConfirmPassword(String confirmPassword, ModelMap modelMap,HttpServletRequest request) {
		
		Locale userLocale =new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		String error=null;
		List<String> list=null;
		
		if(null != getLoggedInUser()){
			 	error=forcePasswordService.validateConformPassword(confirmPassword);
			 	list=new ArrayList<String>();
				
				  if(!error.isEmpty()){
					list.add(this.messageSource.getMessage(error, null, userLocale));
				 }
		}
		return list;
	}


	@Override
	public boolean validateUpdate(SystemUser model, BindingResult result) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateCreate(SystemUser entity, BindingResult bindingResult) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BaseService<SystemUser, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "changepassword";
	}

}
