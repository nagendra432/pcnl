package com.seef.service;

import java.util.List;
import java.util.Map;




import com.seef.models.ApplicationUser;
import com.seef.models.SurgeryType;


public interface SurgeryTypeService {
	
	public void saveSurgeryType(SurgeryType surgeryType,ApplicationUser applicationUser);
	public Boolean validateUnique(SurgeryType surgeryType,Map<String, Object> map);
	public List<SurgeryType> getAllSurgeryTypes();
	public SurgeryType findById(Long id);

}
