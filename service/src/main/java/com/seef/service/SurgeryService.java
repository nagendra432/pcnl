package com.seef.service;

import java.util.List;
import java.util.Map;


import com.seef.models.ApplicationUser;
import com.seef.models.Surgery;


public interface SurgeryService {
	
	public void saveSurgery(Surgery surgery,ApplicationUser applicationUser);
	public Boolean validateUnique(Surgery surgery,Map<String, Object> map);
	public List<Surgery> getAllsurgeries();
	public List<Surgery> findBySurgeryType(Long typeID);
	public Surgery findById(Long id);

}
