package com.seef.exception;

import org.apache.shiro.authc.AuthenticationException;

public class BlockedException extends AuthenticationException{

	private static final long serialVersionUID = 3615086585755292198L;
	
	public BlockedException(String message) {
		super(message);
	}

}
