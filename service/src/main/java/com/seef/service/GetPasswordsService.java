package com.seef.service;

import java.util.List;

import com.seef.models.ApplicationUser;
import com.seef.models.PasswordHistory;
import com.seef.models.enums.PasswordType;

public interface GetPasswordsService {
	
	//APPLICATIONUSER PASSWORD
	public List<PasswordHistory> getApplicationUserLastPasswords(Long sysUserId, PasswordType passwordType);
	public List<PasswordHistory> getMobileUserLastPasswords(Long mobileUserId, PasswordType passwordType);
	
	public List<PasswordHistory> getPasswords(Long sysUserId,PasswordType passwordType,Long size);	
	public  void deleteOldPassword(PasswordHistory passwordHistory);
	public  void savePassword(PasswordHistory passwordHistory);
	
	//MPIN&TPIN
	public List<PasswordHistory> getLastMPINS(ApplicationUser mobileUser,PasswordType passwordType, Long size);
	public List<PasswordHistory> getLastTPINS(ApplicationUser mobileUser,PasswordType passwordType,Long size);

}
