package com.seef.dao.util;

import java.lang.reflect.Method;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seef.models.audit.Audit;



/**
 * @author allanctan
 *
 */
public class CrudUtil {

	private static Logger _log=LoggerFactory.getLogger(CrudUtil.class);

	private static final String SQL_PARAM = ":([^\\s]+)";
	private static final Pattern SQL_PARAM_PATTERN = Pattern.compile(
			SQL_PARAM, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    /**
     * Creates the logging message for new audit logs
     * @param obj
     * @return
     */
    public static String buildCreateMessage(Audit obj) {
    	StringBuffer message = new StringBuffer("Ajouté ");
    	message.append(obj.getClass().getSimpleName())  // class name
    		.append(" ")
    		.append("id")
    		.append(":");
    	Object value = retrieveObjectValue(obj, "id");
    	if (value!=null)
    		message.append(value.toString())
    		.append(" - ");
    	// loop through the fields list
		List auditFields = obj.getAuditableFields();
		int count = 0;
		for (Object property:auditFields) {
			Object ret = retrieveObjectValue(obj, property.toString());
			if (ret!=null && ret.toString().trim().length()>0) {
				if (count > 0)
					message.append(",");
				message.append(property)
					.append("=")
					.append(ret.toString());
				count++;
			}
		}

    	return message.toString();
    }
    
    public static String buildChangedMessage(Audit obj) {
    	StringBuffer message = new StringBuffer("Changé ");
    	message.append(obj.getClass().getSimpleName())  // class name
    		.append(" ")
    		.append("id")
    		.append(":");
    	Object value = retrieveObjectValue(obj, "id");
    	if (value!=null)
    		message.append(value.toString())
    		.append(" - ");
    	// loop through the fields list
		List auditFields = obj.getAuditableFields();
		int count = 0;
		for (Object property:auditFields) {
			Object ret = retrieveObjectValue(obj, property.toString());
			if (ret!=null && ret.toString().trim().length()>0) {
				if (count > 0)
					message.append(",");
				message.append(property)
					.append("=")
					.append(ret.toString());
				count++;
			}
		}

    	return message.toString();
    }

    /**
     * Creates the logging message for update audit logs
     * @param obj
     * @return
     */
    public static String buildUpdateMessage(Audit oldObject, Audit newObject) {

    	StringBuffer message = new StringBuffer("Changé ");
    	message.append(oldObject.getClass().getSimpleName())  // class name
    		.append(" ")
    		.append("id")
    		.append(":");
    	Object value = retrieveObjectValue(oldObject, "id");
    	if (value!=null)
    		message.append(value.toString())
    		.append(" - ");
    	// loop through the fields list
		List auditFields = newObject.getAuditableFields();
		_log.info("<<<<	auditFields Size"+auditFields.size());
		int count = 0;
		for (Object property:auditFields) {
			Object oldValue = retrieveObjectValue(oldObject, property.toString());
			Object newValue = retrieveObjectValue(newObject, property.toString());
			if (oldValue == null) oldValue = new String("");
			if (newValue == null) newValue = new String("");
			if (!oldValue.equals(newValue)) {
				if (count > 0)
					message.append(",");
				message.append(property)
					.append(" De ‘")
					.append(oldValue.toString())
					.append("‘ A ‘")
					.append(newValue.toString())
					.append("‘");
				count++;
			}
		}
		_log.info("<<<<	Audit message"+message.toString());

    	return message.toString();
    }

    /**
     * Creates the logging message for new audit logs
     * @param obj
     * @return
     */
    public static String buildDeleteMessage(Audit obj) {
    	StringBuffer message = new StringBuffer("Deleted ");
    	message.append(obj.getClass().getSimpleName())  // class name
    		.append(" ")
    		.append("id")
    		.append(":");
    	Object value = retrieveObjectValue(obj, "id");
    	if (value!=null)
    		message.append(value.toString());
    	return message.toString();
    }

    /**
     * Retrieves the property name for a method name.
     * (e.g. getName will return name)
     * @param methodName
     * @return
     */
    public static String getPropertyName(String methodName) {
    	if (StringUtils.isEmpty(methodName) || methodName.length()<=3)
    		return null;
    	if (methodName.startsWith("get") || methodName.startsWith("set")) {
    		String prop = methodName.substring(4);
    		char c = Character.toLowerCase(methodName.charAt(3));
    		return c+prop;
    	} else
    		return null;
    }
    /**
     * Retrieves the getter method name for a given property.
     * (e.g. name will return getName)
     * @param propertyName
     * @return
     */
    public static String getGetterMethodName(String propertyName) {
    	if (StringUtils.isEmpty(propertyName) || propertyName.length()<=0)
    		return null;
    	char c = Character.toUpperCase(propertyName.charAt(0));
    	return "get"+c+propertyName.substring(1);
    }

	/**
	 * This method retrieves the object value that corresponds to the property specified.
	 * This method can recurse inner classes until specified property is reached.
	 *
	 * For example:
	 * obj.firstName
	 * obj.address.Zipcode
	 *
	 * @param obj
	 * @param property
	 * @return
	 */
	public static Object retrieveObjectValue(Object obj, String property) {
		if (property.contains(".")) {
			// we need to recurse down to final object
			String props[] = property.split("\\.");
			try {
				Method method = obj.getClass().getMethod(getGetterMethodName(props[0]));
				Object ivalue = method.invoke(obj);
				if (ivalue==null)
					return null;
				return retrieveObjectValue(ivalue,property.substring(props[0].length()+1));
			} catch (Exception e) {
				_log.error("Failed to retrieve value for "+property);
				throw new RuntimeException("Retrieve Error");
			}
		} else {
			// let’s get the object value directly
			try {
				Method method = obj.getClass().getMethod(getGetterMethodName(property));
				return method.invoke(obj);
			} catch (Exception e) {
				_log.error("Failed to retrieve value for "+property);
				throw new RuntimeException("Retrieve Error");
			}
		}
	}

	/**
	 * This method retrieves the object type that corresponds to the property specified.
	 * This method can recurse inner classes until specified property is reached.
	 *
	 * For example:
	 * obj.firstName
	 * obj.address.Zipcode
	 *
	 * @param obj
	 * @param property
	 * @return
	 */
	public static Class retrieveObjectType(Object obj, String property) {
		if (property.contains(".")) {
			// we need to recurse down to final object
			String props[] = property.split("\\.");
			try {
				Method method = obj.getClass().getMethod(getGetterMethodName(props[0]));
				Object ivalue = method.invoke(obj);
				return retrieveObjectType(ivalue,property.substring(props[0].length()+1));
			} catch (Exception e) {
				_log.error("Failed to retrieve value for "+property);
				throw new RuntimeException("Retrieve Error");
			}
		} else {
			// let’s get the object value directly
			try {
				Method method = obj.getClass().getMethod(getGetterMethodName(property));
				return method.getReturnType();
			} catch (Exception e) {
				_log.error("Failed to retrieve value for "+property);
				throw new RuntimeException("Retrieve Error");
			}
		}
	}
}