package com.seef.service;

import com.seef.models.ApplicationUser;
import com.seef.models.SystemUser;

public interface ForcePasswordService {
	public void update(ApplicationUser sysuser, String newPassword);
	public String validateConformPassword(String conformPassword);
	public boolean validateOldPassword(ApplicationUser sysUser, String oldPassword);
	public String   getEncryptedPwd(SystemUser sysUser,String cnfpwd);

}
