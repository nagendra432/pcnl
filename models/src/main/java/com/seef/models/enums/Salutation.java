package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;


public enum Salutation
{
	SalutationMr("SalutationMr"), SalutationMrs("SalutationMrs"), SalutationMiss("SalutationMiss"), SalutationDoctor("SalutationDoctor");

	private String type;
	
	private Salutation(String type) {
		this.type = type;
	}

	public static List<Salutation> list() 
	{
		return Arrays.asList(values());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	
}
