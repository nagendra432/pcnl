package com.seef.models.pharmacy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name="PRESCRIPTION")
@SqlResultSetMapping(name="medicinepackage",
        entities={@EntityResult(entityClass=MedicinePackagingDetails.class,fields={
        	@FieldResult(name="units",column="UNITS"),
        	@FieldResult(name="id",column="ID"),
        	@FieldResult(name="createdBy",column="CREATED_BY"),
        	@FieldResult(name="modifiedBy",column="MODIFIED_BY"),
        	@FieldResult(name="expiredBy",column="EXPIRED_BY"),
        	@FieldResult(name="createdOn",column="CREATED_ON"),
        	@FieldResult(name="modifiedOn",column="MODIFIED_ON"),
        	@FieldResult(name="expiredOn",column="EXPIRED_ON"),
        	@FieldResult(name="status",column="STATUS"),
        	@FieldResult(name="batchNo",column="BATCHNO"),
        	@FieldResult(name="cost",column="COST"),
        	@FieldResult(name="expiryDate",column="EXPIRY_DATE"),
        	@FieldResult(name="received",column="RECEIVED_ON"),
        	@FieldResult(name="medicine",column="MEDICINE_ID"),
        	@FieldResult(name="supplier",column="SUPPLIER"),
        	@FieldResult(name="tax",column="TAX")
        })}
		)
public class Prescription extends AbstractDomainObject {

	private static final long serialVersionUID = 1697689057788840457L;
	public static final String FIND_MEDICINE_PACKAGE = "medicinepackage";

	private Long id;
	private Medicine medicine;
	private MedicinePackagingDetails medicinePackagingDetails;
	private Long units;
	private Double cost;
	
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "PRESCRIPTION_SEQ")
	@SequenceGenerator(name = "PRESCRIPTION_SEQ", sequenceName = "PRESCRIPTION_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MEDICINE_ID")
	public Medicine getMedicine() {
		return medicine;
	}
	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MEDICINE_PACKAGING_DETAILS_ID")
	public MedicinePackagingDetails getMedicinePackagingDetails() {
		return medicinePackagingDetails;
	}
	public void setMedicinePackagingDetails(
			MedicinePackagingDetails medicinePackagingDetails) {
		this.medicinePackagingDetails = medicinePackagingDetails;
	}
	
	@Column(name="UNITS")
	public Long getUnits() {
		return units;
	}
	public void setUnits(Long units) {
		this.units = units;
	}
	
	@Column(name="COST")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}

}
