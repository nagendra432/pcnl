package com.seef.dao;

import java.util.List;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.master.Language;
import com.seef.models.master.NotificationType;
import com.seef.models.master.PermissionGroup;



public interface LookupDataDao extends BaseDao<AbstractDomainObject, Long> {

	public <K extends AbstractDomainObject> List<K> getAll(Class<K> clazz);

	

	public Language findLangaugeByCode(String localeCode);
	
	public List<PermissionGroup> findPermissionGroup();
	
	public PermissionGroup findPermissionGroupById(Long permissionGId);

	public List<NotificationType> findNotificationTypes();
	
	public NotificationType findNotificationTypeById(Long notificationTypeId);
	
	
	public boolean isExist(Object value,String queryName);
		
	public AbstractDomainObject save( AbstractDomainObject entity );
	
	public <K extends AbstractDomainObject> K getById(Class<K> clazz, Long id);
	
	
}
