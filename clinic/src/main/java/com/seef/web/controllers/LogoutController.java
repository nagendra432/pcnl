package com.seef.web.controllers;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.service.AuthenticationService;

@Controller
@RequestMapping(value = "/logout")
public class LogoutController extends PostLoginBaseController {
	
	private static final Logger _log = LoggerFactory.getLogger(LogoutController.class);
	
	@Autowired
	AuthenticationService  authenticationService;

	@Autowired
	GenericDAO genericDAO;


	@RequestMapping(value = "/logout",method=RequestMethod.GET)
	public String  onLogoutSuccess(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getSession().removeAttribute("nav");
		request.getSession().removeAttribute("innerNav");
		Timestamp currTime = new Timestamp(Calendar.getInstance().getTimeInMillis());	

		//TODO: This is incorrect approach as it violates JPA Version concept , but as we don't have Shiro concurrent session management implementation doing it temporarily
		if(getLoggedInUser() != null){
			_log.info(">>> LoggedInUserId:  " +getLoggedInUser().getId());
			ApplicationUser sysUser = genericDAO.getById(ApplicationUser.class, getLoggedInUser().getId());
			authenticationService.updateUserLastLogoutTime(sysUser, currTime);
		}
		SecurityUtils.getSubject().logout();
		return "tileDefinition.loggedout";
	}


}
