<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="datatables"
	uri="http://github.com/dandelion/datatables"%>


<spring:message code="label.billno" var="lblbillno"/>
<spring:message code="label.name" var="lblpatientname"/>
<spring:message code="label.medicinename" var="lblmedicinename"/>
<spring:message code="label.batchno" var="lblbatchno"/>
<spring:message code="label.units" var="lblunit"/>
<spring:message code="label.availableunits" var="lblavailableunit"/>
<spring:message code="label.save" var="lblsave"/>
<spring:message code="label.cancel" var="lblcancel"/>

<script type="text/javascript" src="jquery.print.js"></script>

 <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model">
 <div id="my_div">
 <div id="printdiv">
 
 <table >
	 <tr>
		 <td ><label >${lblbillno}</label></td>
		 <td> <form:input path="billNo"  class="form-control required"  readonly="true" /></td>
	</tr>
	<tr><td style="height:20px;"></td></tr>
	<tr>
	    <td>  <label >${lblpatientname}</label></td>
		 <td> <form:input path="name" readonly="true"  class="form-control required"  />   </td>
	 </tr>
	 
	  
	
 </table>
 		
				<table class="table-bordered table-striped table-responsive col-md-8" style="margin-top:3%;">
				<thead>
                    <tr>
                        <th style="text-align:center;"><spring:message code="label.medicinename"/></th>
                        <th style="text-align:center;"><spring:message code="label.stock.batchno"/></th>
                        <th style="text-align:center;"><spring:message code="label.units"/></th>
                        <th style="text-align:center;"><spring:message code="label.unitcosts"/></th>
                         <th style="width:120px;text-align:center;"><spring:message code="label.cost"/></th>
                    </tr>
                </thead>
                <tfoot style="padding:20px !important;">
                <tr >
                 <td colspan="4" align="right"  >  <label><spring:message code="label.totalcost"/></label></td>
                <td align="right">${model.totalCost}</td>
                </tr>
                <tr>
				<td colspan="5" style="text-align: center;">
			   <a class="btn blue hidden-print" onclick="javascript:window.print();">Print <i class="icon-print"></i></a></td>
				</tr>
                </tfoot>
                <tbody>
				<c:forEach items="${model.prescriptions}" var="prescriptions">
				 <tr>
				  <td style="text-align:center;">${prescriptions.medicine.medicineName}</td>
				 <td>${prescriptions.medicinePackagingDetails.batchNo}</td>
				 <td>${prescriptions.units}</td>
				 <td align="right">${prescriptions.medicinePackagingDetails.cost}</td>
				 <td align="right">${prescriptions.cost}</td>
				 </tr>
				</c:forEach>
				
				</tbody>
				</table>
			</div>	
			</div>
 </form:form>
 

 
 