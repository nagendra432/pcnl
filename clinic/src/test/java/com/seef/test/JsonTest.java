package com.seef.test;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.json.simple.JSONObject;
import org.junit.Test;

import com.seef.models.Surgery;

public class JsonTest {

	@SuppressWarnings("unchecked")
	@Test
	public void jsonTest() {
    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
   Surgery surgery=new Surgery();
   surgery.setId(1L);
		  JSONObject jsonObject=new JSONObject();
		 jsonObject.put("surgeryInfo",surgery);
		    String json = null;
			try {
				json = ow.writeValueAsString(jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		    System.out.println(">>>>>>>. jsonObject :"+json);
	}
}
