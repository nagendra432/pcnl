package com.seef.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.Gender;
import com.seef.models.enums.NationalIdType;
import com.seef.models.enums.Salutation;

@Entity
@Table(name="SYSTEM_USER_DETAILS")
public class SystemUserDetails extends AbstractDomainObject {

	private static final long serialVersionUID = 382784494173688516L;
	
	private Long id;
	private String nationality;             
	private String nationalityId;
	private NationalIdType nationalIdType;
	private Date dateOfBirth;               
	private String occupation;                
	private Gender gender;  
	private String applicantPhoto;
	private String nationalIdPhoto;
	private String signaturePhoto;
	private Salutation salutation;
	private SystemUser systemUser;
	
	public SystemUserDetails(){
		
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "SYSTEM_USER_DETAILS_SEQ")
	@SequenceGenerator(name = "SYSTEM_USER_DETAILS_SEQ", sequenceName = "SYSTEM_USER_DETAILS_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="NATIONALITY")
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	@Column(name="NATIONALITY_ID")
	public String getNationalityId() {
		return nationalityId;
	}
	public void setNationalityId(String nationalityId) {
		this.nationalityId = nationalityId;
	}
	
	@Column(name="NATIONAL_ID_TYPE")
	@Enumerated(EnumType.STRING)
	public NationalIdType getNationalIdType() {
		return nationalIdType;
	}

	public void setNationalIdType(NationalIdType nationalIdType) {
		this.nationalIdType = nationalIdType;
	}
	
	@Column(name="DATE_OF_BIRTH")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	@Column(name="OCCUPATION")
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
	@Column(name="GENDER")
	@Enumerated(EnumType.STRING)
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	
	@Column(name="APPLICANT_PHOTO")
	public String getApplicantPhoto() {
		return applicantPhoto;
	}

	public void setApplicantPhoto(String applicantPhoto) {
		this.applicantPhoto = applicantPhoto;
	}

	@Column(name="NATIONALID_PHOTO")
	public String getNationalIdPhoto() {
		return nationalIdPhoto;
	}

	public void setNationalIdPhoto(String nationalIdPhoto) {
		this.nationalIdPhoto = nationalIdPhoto;
	}

	@Column(name="SIGNATURE_PHOTO")
	public String getSignaturePhoto() {
		return signaturePhoto;
	}

	public void setSignaturePhoto(String signaturePhoto) {
		this.signaturePhoto = signaturePhoto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Column(name="SALUTATION")
	@Enumerated(EnumType.STRING)
	public Salutation getSalutation() {
		return salutation;
	}

	public void setSalutation(Salutation salutation) {
		this.salutation = salutation;
	}

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SYSTEM_USER_ID")
	public SystemUser getSystemUser() {
		return systemUser;
	}

	public void setSystemUser(SystemUser systemUser) {
		this.systemUser = systemUser;
	}
	
}
