package com.seef.exception;

import org.apache.shiro.authc.AuthenticationException;

public class InactiveException extends AuthenticationException{

	private static final long serialVersionUID = 3615086585755292198L;
	
	public InactiveException(String message) {
		super(message);
	}

}
