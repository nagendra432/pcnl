package com.seef.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seef.dao.GenericDAO;
import com.seef.dao.util.CollectionUtil;
import com.seef.models.ApplicationUser;
import com.seef.service.MobileUserService;

@Service
public class MobileUserServiceImpl implements MobileUserService {
	
	@Autowired
	GenericDAO genericDAO;

	@Override
	public ApplicationUser getMobileUserByUsername(String username) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"username"}, new Object[]{username});
		return genericDAO.getByCriteria(ApplicationUser.class, criterias);
	}

	@Override
	public ApplicationUser getMobileUserByMobile(String mobileNumber, Long institutionId) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"mobileNumber", "institution.id"}, new Object[]{mobileNumber, institutionId});
		return genericDAO.getByCriteria(ApplicationUser.class, criterias);
	}

	@Override
	public ApplicationUser getMobileUserByUID(String uid) {
		Map<String, Object> criterias = CollectionUtil.toMap(new String[]{"uid"}, new Object[]{uid});
		return genericDAO.getByCriteria(ApplicationUser.class, criterias);
	}

	@Override
	public ApplicationUser getMobileUserById(Long id) {
		return genericDAO.getById(ApplicationUser.class, id);
	}

}
