package com.seef.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seef.dao.GenericDAO;
import com.seef.models.pharmacy.MedicinePackagingDetails;
import com.seef.models.pharmacy.Prescription;
import com.seef.service.StockDetailsService;

@Service
public class StockDetailsServiceImpl implements StockDetailsService {
	
	
	@Autowired
	GenericDAO genericDAO;

	@Override
	public List<MedicinePackagingDetails> stock_list() {
		
		String query = "SELECT p.id,p.created_by,p.created_on,p.modified_by,p.expired_by,p.modified_on,p.expired_on,p.status,p.received_on,p.cost,p.batchno,p.expiry_date,p.medicine_id,p.supplier,p.tax,(p.UNITS- COALESCE(SUM(pr.UNITS), 0)) AS UNITS FROM medicine_packaging_details p LEFT JOIN prescription pr ON pr.MEDICINE_PACKAGING_DETAILS_ID=p.ID WHERE p.status='ACTIVE' GROUP BY p.BATCHNO";
		  List<MedicinePackagingDetails>  medicinePackagingDetails = genericDAO.executeNativeQuery(query, Prescription.FIND_MEDICINE_PACKAGE);
		System.out.print(medicinePackagingDetails.size());  
		return medicinePackagingDetails;
	}

	@Override
	public List<MedicinePackagingDetails> expiry_list() {
		// TODO Auto-generated method stub
		String query = "SELECT p.id,p.created_by,p.created_on,p.modified_by,p.expired_by,p.modified_on,p.expired_on,p.status,p.received_on,p.cost,p.batchno,p.expiry_date,p.medicine_id,p.supplier,p.tax,(p.UNITS- COALESCE(SUM(pr.UNITS), 0)) AS UNITS FROM medicine_packaging_details p LEFT JOIN prescription pr ON pr.MEDICINE_PACKAGING_DETAILS_ID=p.ID WHERE p.status='ACTIVE' AND (month(p.expiry_date)=month(CURRENT_DATE) and year(p.expiry_date)=year(CURRENT_DATE)) GROUP BY p.BATCHNO";
		  List<MedicinePackagingDetails>  medicinePackagingDetails = genericDAO.executeNativeQuery(query, Prescription.FIND_MEDICINE_PACKAGE);
		return medicinePackagingDetails;
	}

	@Override
	public MedicinePackagingDetails getById(Long id) {
		// TODO Auto-generated method stub
		return genericDAO.getById(MedicinePackagingDetails.class, id);
	}
}
