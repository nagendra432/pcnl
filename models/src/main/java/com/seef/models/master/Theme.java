package com.seef.models.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name = "THEME")
public class Theme extends AbstractDomainObject{

	private static final long serialVersionUID = -6333985898320724299L;
	
	private Long id;	
	private String name;	
	private String code;	
	private String location;

	public Theme(){
	}

	public Theme(Long id, String name){
		this.id = id;
		this.name = name;
	}
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "THEME_SEQ")
	@SequenceGenerator(name = "THEME_SEQ", sequenceName = "THEME_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "NAME", length = 40, nullable = false)
	public String getName() {
		return name;
	}

	@Column(name = "CODE", length = 20)
	public String getCode() {
		return code;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "LOCATION", length = 255)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
