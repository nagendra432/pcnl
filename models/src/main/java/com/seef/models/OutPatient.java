package com.seef.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.Anes;
import com.seef.models.enums.Gender;

@Entity
@Table(name = "OUTPATIENT_REGISTRATION")
@NamedQueries({ 
	@NamedQuery(name = "findmaxmrno", query = "select  max(outpatient.mrno) from OutPatient outpatient"),
	@NamedQuery(name="finduserInfoByMrno" ,query="select patient from OutPatient patient WHERE patient.mrno=:mrNo")
})
@SqlResultSetMapping(name="outpatientpackage",
entities={@EntityResult(entityClass=OutPatient.class,fields={
	@FieldResult(name="id",column="ID"),
	@FieldResult(name="createdBy",column="CREATED_BY"),
	@FieldResult(name="modifiedBy",column="MODIFIED_BY"),
	@FieldResult(name="expiredBy",column="EXPIRED_BY"),
	@FieldResult(name="createdOn",column="CREATED_ON"),
	@FieldResult(name="modifiedOn",column="MODIFIED_ON"),
	@FieldResult(name="mrno",column="MRNO"),
	@FieldResult(name="status",column="STATUS"),
	@FieldResult(name="firstName",column="FIRST_NAME"),
	@FieldResult(name="lastName",column="LAST_NAME"),
	@FieldResult(name="phoneNo",column="PHONE_NUMBER"),
	@FieldResult(name="dob",column="DATE_OF_BIRTH"),
	@FieldResult(name="address",column="ADDRESS_ID"),
	@FieldResult(name="appointment",column="APPOINTMENT_ID"),
	@FieldResult(name="remarks",column="REMARKS"),
	@FieldResult(name="avlink",column="AVLINK"),
	@FieldResult(name="middleName",column="MIDDLE_NAME"),
	@FieldResult(name="adnlProcedure",column="ADNLPROCEDURE_ID"),
	@FieldResult(name="anes",column="ANES"),
	@FieldResult(name="energy",column="ENERGY"),
	@FieldResult(name="surgeon",column="SURGEON"),
	@FieldResult(name="asstSurgeon",column="ASST_SURGEON"),
	@FieldResult(name="complication",column="COMPLICATION"),
	@FieldResult(name="gender",column="GENDER")
})}
)
public class OutPatient extends AbstractDomainObject {

	private static final long serialVersionUID = 1695876757788840457L;
	public static final String FIND_PATIENT_PACKAGE = "outpatientpackage";

	private Long id;
	private String mrno;
	private String firstName;
	private String lastName;
	private String middleName;
	private String phoneNo;
	private Address address;
	private Date dob;
	private String fullName;
	private Appointment appointment;
	private List<Stones> stone;
	private List<DJStent> dJStent;
	private String remarks;
	private String avlink;
	private List<Surgery> surgery;
	private Gender gender; 
	private ADNLProcedure adnlProcedure;
	private Anes anes;
	private String energy;
	private SystemUser surgeon;
	private SystemUser asstSurgeon;
	private String complication;

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "OUTPATIENT_REGISTRATION_SEQ")
	@SequenceGenerator(name = "OUTPATIENT_REGISTRATION_SEQ", sequenceName = "OUTPATIENT_REGISTRATION_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "APPOINTMENT_ID")
	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	@Column(name = "MRNO")
	public String getMrno() {
		return mrno;
	}

	public void setMrno(String mrno) {
		this.mrno = mrno;
	}

	@Column(name = "FIRST_NAME")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "LAST_NAME")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "MIDDLE_NAME")
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Column(name = "PHONE_NUMBER")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Column(name = "DATE_OF_BIRTH")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "ADDRESS_ID")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@OneToMany(fetch=FetchType.LAZY,targetEntity=Stones.class,mappedBy="patient")
	public List<Stones> getStone() {
		return stone;
	}

	public void setStone(List<Stones> stone) {
		this.stone = stone;
	}

	@OneToMany(fetch=FetchType.LAZY,targetEntity=DJStent.class,mappedBy="patient")
	public List<DJStent> getdJStent() {
		return dJStent;
	}

	public void setdJStent(List<DJStent> dJStent) {
		this.dJStent = dJStent;
	}

	@Column(name="REMARKS")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name="AVLINK")
	public String getAvlink() {
		return avlink;
	}

	public void setAvlink(String avlink) {
		this.avlink = avlink;
	}

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(joinColumns=@JoinColumn(name="PATIENT_ID",referencedColumnName="ID"),inverseJoinColumns=@JoinColumn(name="SURGERY_ID",referencedColumnName="ID"))
	public List<Surgery> getSurgery() {
		return surgery;
	}

	public void setSurgery(List<Surgery> surgery) {
		this.surgery = surgery;
	}

	@Column(name="GENDER")
	@Enumerated(EnumType.STRING)
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ADNLPROCEDURE_ID")
	public ADNLProcedure getAdnlProcedure() {
		return adnlProcedure;
	}

	public void setAdnlProcedure(ADNLProcedure adnlProcedure) {
		this.adnlProcedure = adnlProcedure;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ANES")
	public Anes getAnes() {
		return anes;
	}

	public void setAnes(Anes anes) {
		this.anes = anes;
	}
	
	@Column(name="ENERGY")
	public String getEnergy() {
		return energy;
	}

	public void setEnergy(String energy) {
		this.energy = energy;
	}

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="SURGEON")
	public ApplicationUser getSurgeon() {
		return surgeon;
	}

	public void setSurgeon(SystemUser surgeon) {
		this.surgeon = surgeon;
	}

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ASST_SURGEON")
	public ApplicationUser getAsstSurgeon() {
		return asstSurgeon;
	}

	public void setAsstSurgeon(SystemUser asstSurgeon) {
		this.asstSurgeon = asstSurgeon;
	}

    @Column(name="COMPLICATION")
	public String getComplication() {
		return complication;
	}

	public void setComplication(String complication) {
		this.complication = complication;
	}

	@Transient
	public String getFullName() {
		this.fullName = this.firstName + "  " + this.middleName + " " + this.lastName;
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
