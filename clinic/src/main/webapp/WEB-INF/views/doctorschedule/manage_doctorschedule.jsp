<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="datatables"
	uri="http://github.com/dandelion/datatables"%>


<spring:message code="label.view" var="lblView"/>
<spring:message code="label.edit" var="lblEdit"/>
<spring:message code="label.action" var="lblaction"/>
<spring:message code="label.doctor" var="lbldoctor"/>
<spring:message code="label.doctorschedule.date" var="lblscheduleddate"/>
<spring:message code="label.doctorschedule.intime" var="lblintime"/>
<spring:message code="label.doctorschedule.outtime" var="lblouttime"/>
<spring:message code="label.viewtakeappointment" var="lblviewappointment"/>
<spring:message code="label.noofappointments" var="lblnoofappointments"/>
<spring:message code="label.doctorschedule.date" var="lblscheduledDate"/>
<!-- BEGIN PAGE HEADER-->


<script type="text/javascript">
 $(document).ready(function(){
  $(".form_datetime").datetimepicker({         
   isRTL: App.isRTL(),
            format: "yyyy-mm-dd",
            weekStart: 1,
            todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
       });
 });
 
 function rowCallback( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	    if (aData[4] =="ACTIVE" ) {
	    	$(nRow).addClass("deactivateRow");
	    }
	}
 
 
</script>


<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<spring:message code="header.doctorschedule"/>
			
		</h3>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
 <form:form action="manage.htm"  id="form_sample_1" class="form-horizontal" method="get">
 <div class="form-group">
							<label class="control-label col-md-3">${lblscheduledDate}<span class="required">*</span></label>
							<div class="col-md-4">
								<div class="input-group date form_datetime">
								<input id="scheduledDate" name="scheduledDate" required="required" class="form-control">
									    <span class="input-group-btn">
										<button class="btn default date-set" type="button">
											<i class="icon-calendar"></i>
										</button>
									</span>
									 
								</div>
								 <button type="submit" class="btn green">Search</button>
							</div>
						</div>
						
</form:form>
</div>


<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
	<div class="col-md-12">

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-edit"></i>
					<spring:message code="label.resultsfordoctorschedule" />
					
					
					
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					 <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="col-lg-2 pull-right">
					<div class="table-toolbar pull-right">
						

					</div>
				</div>
				<datatables:table id="sample_editable_1" data="${modelList}" cdn="false" row="doctorschedule" cssClass="table table-striped table-bordered table-hover">
					<datatables:column title="${lbldoctor}" property="doctor.name" />
					<datatables:column title="${lblscheduleddate}" property="scheduledDate"
						cssClass="hidden-480" cssCellClass="hidden-480" />	
				   <datatables:column title="${lblintime}" property="inTime" />
				      <datatables:column title="${lblouttime}" property="outTime" />
				       <datatables:column title="${lblnoofappointments}" property="noOfAppointments" />
					<datatables:column title="${lblaction}" sortable="false">
						<div class='hidden-phone visible-desktop btn-group'>
							<a href="view.htm?id=${doctorschedule.id}" data-rel="tooltip"
								title="${lblView}"><i
								class="btn btn-default btn-sm my-table_btn icon-eye-open"></i></a>
								 <a href="edit.htm?id=${doctorschedule.id}" data-rel="tooltip" title="${lblEdit}"><i class="btn btn-default btn-sm my-table_btn icon-edit"></i></a>
								
						</div>

					</datatables:column>
				</datatables:table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->