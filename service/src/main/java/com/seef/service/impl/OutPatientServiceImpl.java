package com.seef.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ADNLProcedure;
import com.seef.models.ApplicationUser;
import com.seef.models.DJStent;
import com.seef.models.OutPatient;
import com.seef.models.Stones;
import com.seef.models.SystemUser;
import com.seef.models.base.Status;
import com.seef.service.OutPatientService;

@Service
public class OutPatientServiceImpl implements OutPatientService  {

	private static final Logger _log = LoggerFactory.getLogger(OutPatientServiceImpl.class);
	
	@Autowired
	GenericDAO genericDAO;
		
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveOutPatient(OutPatient outPatient, ApplicationUser applicationUser) {
		outPatient.getAppointment().setOutPatient(outPatient);
		outPatient.getAppointment().setStatus(Status.ACTIVE);
		outPatient.setStatus(Status.ACTIVE);
		outPatient.setCreatedBy(applicationUser);
		outPatient.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(outPatient);
		_log.info(">>>>>>> Save Successfully");
		
	}
	
	
	

	@Override
	public Long getMaxMRNO() {
		Map<String,Object> criteria=new HashMap<String,Object>();
		/*Long codeInt;
		Long code=1000L;
		 
		List<OutPatient> patients=genericDAO.findAll(OutPatient.class);
		if(patients.size()==0){
			codeInt=code;
		}
		else{
			codeInt=patients.size()+code;
		}*/
		Long code=genericDAO.executeQuerySinglequery("findmaxmrno", criteria);
		if(code==null){
			 code=1000L;
		}
		else{
			code=Long.valueOf(code).longValue()+1;
			
		}
		
		
		return code; 
		
	}

	@Override
	public List<OutPatient> outPatiensByParams(int age,String gender,Long surgeryId,Long surgeryTypeID,String remarks,String surgeon){
		
		StringBuilder builder=new StringBuilder();
		builder.append("select patient.ADNLPROCEDURE_ID,patient.ANES, patient.ENERGY,patient.ASST_SURGEON,patient.SURGEON,patient.COMPLICATION,patient.CREATED_ON,patient.ADDRESS_ID,patient.AVLINK,patient.MIDDLE_NAME,patient.APPOINTMENT_ID,patient.MODIFIED_ON,patient.STATUS,patient.LAST_NAME,patient.CREATED_BY,patient.MODIFIED_BY,patient.ID,patient.DATE_OF_BIRTH,patient.FIRST_NAME,patient.STATUS,patient.GENDER,patient.MRNO,patient.REMARKS,patient.PHONE_NUMBER,patient.LAST_NAME FROM outpatient_registration patient ");
		
		if(null!=surgeryId || null!=surgeryTypeID)
			builder.append(" left join  outpatient_registration_surgery patientsurgery on patient.ID=patientsurgery.PATIENT_ID left join surgery surgery on surgery.ID=patientsurgery.SURGERY_ID ");
		
		if(age>0 || null!=gender || null!=surgeryId || null!=surgeryTypeID || null!=remarks){
			
			builder.append(" where ");
			if(age>=0)
				builder.append(" TIMESTAMPDIFF(YEAR,patient.DATE_OF_BIRTH,CURDATE())= " +age  +" OR ");
			if(null!=gender && !gender.isEmpty() && !gender.equals("") && gender!="" )
				builder.append(" patient.GENDER LIKE ' " +gender  +"' OR ");
			if(null!=remarks && !remarks.isEmpty() && !remarks.equals("") && remarks!="" )
				builder.append(" patient.REMARKS LIKE  '% " +remarks  +"%' OR ");
			if(null!=surgeryId && surgeryId>0)
				builder.append(" surgery.ID = " +surgeryId  +" OR ");
			if(null!=surgeryTypeID && surgeryTypeID>0)
				builder.append(" surgery.SURGERYTYPE_ID = " +surgeryTypeID  +" OR ");
			if(null!=surgeon && !surgeon.isEmpty() && !surgeon.equals("") && surgeon!="" )
				builder.append(" patient.SURGEON = " +surgeon  +" OR ");
			builder.append(" patient.STATUS like 'ACTIVE'");
		}
		
		List<OutPatient>  outPatientsDetails = genericDAO.executeNativeQuery(builder.toString(), OutPatient.FIND_PATIENT_PACKAGE);
		System.out.print(outPatientsDetails.size());  
		return outPatientsDetails;
	}



	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveOrUpdateOutPatient(OutPatient outPatient,
			ApplicationUser applicationUser) {
		
		outPatient.setModifiedBy(applicationUser);
		outPatient.setModifiedOn(Calendar.getInstance().getTime());
		
		if(null!=outPatient.getStone()){
			for (Stones stone : outPatient.getStone()) {
				stone.setPatient(outPatient);
				stone.setNumber(String.valueOf(outPatient.getStone().size()));
				genericDAO.save(stone);
			}
		}
		if(null!=outPatient.getdJStent()){
			for (DJStent djStent :outPatient.getdJStent()) {
				djStent.setPatient(outPatient);
				genericDAO.save(djStent);
			}
		}
		if(null!=outPatient.getAdnlProcedure())
		outPatient.setAdnlProcedure(genericDAO.find(ADNLProcedure.class, outPatient.getAdnlProcedure().getId()));
		if(null!=outPatient.getSurgeon().getId())
			outPatient.setSurgeon(genericDAO.find(SystemUser.class, outPatient.getSurgeon().getId()));
		if(null!=outPatient.getAsstSurgeon().getId())
			outPatient.setAsstSurgeon(genericDAO.find(SystemUser.class, outPatient.getAsstSurgeon().getId()));
		genericDAO.saveOrUpdate(outPatient);
		_log.info(">>>>>>> Updated Successfully");
		
	}




	@Override
	public List<OutPatient> outPatient_list() {
		// TODO Auto-generated method stub
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("status", Status.ACTIVE);
		return genericDAO.findAllByCriteria(OutPatient.class, params);
		
	}




	@Override
	public OutPatient getById(Long id) {
		// TODO Auto-generated method stub
		return genericDAO.getById(OutPatient.class, id);
		
	}




	@Override
	public Boolean validateUnique(OutPatient outPatient, Map<String, Object> map) {
		// TODO Auto-generated method stub
		return genericDAO.isUnique(OutPatient.class,outPatient,map);
	}




	@Override
	public OutPatient getById(OutPatient outPatient) {
		// TODO Auto-generated method stub
		//genericDAO.findByCriteria(Appointment.class, CollectionUtil.toMap(new String[]{"outPatient.id"}, new Object[]{id}));
		return genericDAO.getById(OutPatient.class, outPatient);
	}

	@Override
	public OutPatient findByMrNo(String mrNo){
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("mrNo", mrNo);
		try{
		    return genericDAO.executeNamedQuery("finduserInfoByMrno", OutPatient.class, params).get(0);
		}catch(java.lang.IndexOutOfBoundsException e){
			return new OutPatient();
		}
		
	}


	@Override
	public OutPatient getOutpatient(Map<String, Object> criteria) {
		// TODO Auto-generated method stub
		return genericDAO.getByCriteria(OutPatient.class, criteria);
	}
	
}
