package com.seef.service;

import java.sql.Timestamp;

import com.seef.models.ApplicationUser;

public interface AuthenticationService{



	public  void updateUserLastLoginTime(ApplicationUser user, Timestamp currTime);
	public  void updateUserLastFailedLogin(ApplicationUser user, Timestamp currTime);
	public  void updateUserLastLogoutTime(ApplicationUser user, Timestamp currTime);

}
