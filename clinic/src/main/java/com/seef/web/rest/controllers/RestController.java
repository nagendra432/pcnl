package com.seef.web.rest.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seef.models.Surgery;
import com.seef.service.SurgeryService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping(value="/rest")
public class RestController {
	@Autowired
	private SurgeryService surgeryService;
	
	@RequestMapping(value="/surType.htm",method=RequestMethod.GET)
	@ResponseBody
	public List<Surgery> surgeryFinder(ModelMap modelMap, HttpServletRequest request,@RequestParam("surgeryTypeID") Long surgeryTypeID){
	   System.out.println(">>>>>surgeryTypeID"+surgeryTypeID);
		return surgeryService.findBySurgeryType(surgeryTypeID);
	}
}
