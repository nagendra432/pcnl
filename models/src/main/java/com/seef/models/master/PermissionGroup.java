package com.seef.models.master;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name="PERMISSION_GROUP")
@NamedQueries({@NamedQuery(name="permissiongroup.fetchPermissionGroup",query="Select permissiongrop from  PermissionGroup permissiongrop where permissiongrop.id = :permissionGroupId"),
    @NamedQuery(name="permissiongroup.fetchNonPermissionGroup",query="Select permissiongrop from  PermissionGroup permissiongrop where permissiongrop.id!=1 and permissiongrop.id!=2"),
   @NamedQuery(name="permissiongroup.findAll",query="Select permissiongrop from  PermissionGroup permissiongrop")
})
public class PermissionGroup extends AbstractDomainObject {

	private static final long serialVersionUID = 4712854668281772671L;
	
	private Long id;	
	private String groupName;	
	private List<Permission> permissions;
	private String external;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "PERMISSION_GROUP_SEQ")
	@SequenceGenerator(name = "PERMISSION_GROUP_SEQ", sequenceName = "PERMISSION_GROUP_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="GROUPNAME")
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="permissionGroup")
	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
	
	@Column(name="EXTERNEL")
	public String getExternal() {
		return external;
	}

	public void setExternal(String external) {
		this.external = external;
	}
	
}
