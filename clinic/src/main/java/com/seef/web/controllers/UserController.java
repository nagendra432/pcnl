package com.seef.web.controllers;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.Preferences;
import com.seef.models.Role;
import com.seef.models.SystemUser;
import com.seef.models.base.Status;
import com.seef.models.enums.Gender;
import com.seef.models.enums.Salutation;
import com.seef.models.enums.UserStatus;
import com.seef.models.enums.UserType;
import com.seef.service.BaseService;
import com.seef.service.SystemUserService;
import com.seef.web.editors.AbstractModelEditor;

@Controller
@RequestMapping(value="/user")
public class UserController extends CRUDController<SystemUser> {
	
	@Autowired
	private GenericDAO genericDAO;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private SystemUserService systemUserService;
	
	@RequestMapping(value = "/create.htm", method = RequestMethod.GET)
	public String create(HttpServletRequest request,ModelMap modelMap) {
		
			modelMap.addAttribute("model", new SystemUser());
			modelMap.addAttribute("salutation", Salutation.list());
			modelMap.addAttribute("roles", genericDAO.findAll(Role.class));
			modelMap.addAttribute("designation", UserType.getDesgnation());
			modelMap.addAttribute("genders", Gender.values());
			
			Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
			request.getSession().setAttribute("nav", messageSource.getMessage("user.menu.name", null, userLocale));
			request.getSession().setAttribute("innerNav", messageSource.getMessage("create.user.menu.name", null, userLocale));
		    return "tileDefinition.create-"+getNameSpace();
	}


	@RequestMapping(value = "/save.htm", method = RequestMethod.POST)
	public String signUp(HttpServletRequest request,ModelMap modelMap,@ModelAttribute("model")SystemUser systemUser ) {
			  Locale locale = request.getLocale();
			  String localeCode=locale.getLanguage();
			  Locale locale1=new Locale(localeCode);
		   
		     systemUser.setUserStatus(UserStatus.ACTIVE);
		     systemUser.setStatus(Status.ACTIVE);
		     Preferences preferences=(Preferences) genericDAO.find(Preferences.class,1L);
		     systemUser.setPreferences(preferences);
		     systemUserService.saveOrUpdate(systemUser);
		     ApplicationUser applicationUser=systemUserService.findByUserName(systemUser.getUsername());
		     Object salt = prepareSalt(String.valueOf(applicationUser.getId()));
		     systemUser.setPassword(new Sha256Hash(systemUser.getPassword(), salt, 1024).toBase64()); 
		     systemUser.getSystemUserDetails().setSystemUser(systemUser);
		     systemUserService.saveOrUpdate(systemUser);
		     
		     modelMap.addAttribute("login",messageSource.getMessage("label.login",null, locale));
		     modelMap.addAttribute("ForgetPassword",messageSource.getMessage("label.ForgetPassword",null, locale));
		     modelMap.addAttribute("toresetyourpassword",messageSource.getMessage("label.toresetyourpassword",null, locale1));
		     modelMap.addAttribute("username",messageSource.getMessage("label.admin.username",null, locale1));
		     modelMap.addAttribute("password",messageSource.getMessage("label.password",null, locale1));
		    return "redirect:/user/manage.htm";
	}
	
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap,HttpServletRequest request )
	{
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("user.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.user.menu.name", null, userLocale));
		modelMap.addAttribute("modelList", genericDAO.findAll(SystemUser.class));
		return "tileDefinition.manage-"+getNameSpace();
	}
	
	@Override
	@RequestMapping(value = "/edit.htm", method = RequestMethod.GET)
	public String edit(ModelMap modelMap, HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		SystemUser systemUser=genericDAO.find(SystemUser.class, id);
		modelMap.addAttribute("roles", genericDAO.findAll(Role.class));
		setupCreate(modelMap, request);
		modelMap.addAttribute("model", systemUser);
		return "tileDefinition.create-"+getNameSpace();
	}
	
	@Override
	@RequestMapping(value = "/view.htm", method = RequestMethod.GET)
	public String view(ModelMap modelMap, HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		SystemUser systemUser=genericDAO.find(SystemUser.class, id);
		modelMap.addAttribute("roles", genericDAO.findAll(Role.class));
		setupCreate(modelMap, request);
		modelMap.addAttribute("model", systemUser);
		return "tileDefinition.view-"+getNameSpace();
	}
	
	@Override
	public boolean validateUpdate(SystemUser model, BindingResult result) {
		return false;
	}

	@Override
	public boolean validateCreate(SystemUser entity, BindingResult bindingResult) {
		return false;
	}

	@Override
	public BaseService<SystemUser, Long> getService() {
		return null;
	}

	@Override
	public String getNameSpace() {
		return "user";
	}

	private String prepareSalt(String suffix){
		return "SeefTech"+suffix;
	}
			
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.registerCustomEditor(Role.class, new AbstractModelEditor(Role.class));
	}
}
