package com.seef.service.impl.shiro;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seef.dao.GenericDAO;
import com.seef.exception.BlockedException;
import com.seef.exception.InactiveException;
import com.seef.exception.LockedException;
import com.seef.models.ApplicationUser;
import com.seef.models.dto.UserSession;
import com.seef.models.enums.UserStatus;
import com.seef.models.master.Permission;
import com.seef.service.SystemUserService;

@Service("systemUserAuthRealm")
public class SystemUserAuthRealm extends AuthorizingRealm{


	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	GenericDAO genericDAO;

	private static Logger logger=LoggerFactory.getLogger(SystemUserAuthRealm.class);   

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		if (principals == null) {
			throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
		}
		logger.debug("doGetAuthorizationInfo() invoked");
		UserSession userSession = (UserSession) getAvailablePrincipal(principals);
		List<Permission> permissions = userSession.getApplicationUser().getRole().getPermissions();
		Set<String> perms=new HashSet<String>();
		for(Permission perm:permissions){
			perms.add(perm.getName());
		}
		SimpleAuthorizationInfo simpleAuthorizationInfo=new SimpleAuthorizationInfo(perms);
		Set<String> userPermissions = getUserPermissions(permissions);
		simpleAuthorizationInfo.setStringPermissions(userPermissions);
		logger.debug("doGetAuthorizationInfo() Exit");
		return simpleAuthorizationInfo;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;

		if(null == usernamePasswordToken.getUsername()){
			throw new AccountException("Null usernames are not allowed by this realm.");
		}

		logger.debug("doGetAuthenticationInfo()");
		ApplicationUser applicationUser = systemUserService.findByUserName(usernamePasswordToken.getUsername());

		if(null == applicationUser){
			throw new AuthenticationException("No account found for user [" + usernamePasswordToken.getUsername() + "]");
		}
		else if(applicationUser.getUserStatus().equals(UserStatus.BLOCKED)){
			throw new BlockedException("error.msg.blocked");
		}
		else if(applicationUser.getUserStatus().equals(UserStatus.LOCKED)){
			throw new LockedException("error.msg.locked");
		}
		else if(applicationUser.getUserStatus().equals(UserStatus.INACTIVE)){
			throw new InactiveException("error.msg.inactive");
		}		

		UserSession userSession=new UserSession();
		userSession.setApplicationUser(applicationUser);
		//userSession.setInstitution(applicationUser.getInstitution());
		userSession.setPreferences(applicationUser.getPreferences());
		userSession.setTheme(applicationUser.getPreferences().getTheme());
		SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(userSession, applicationUser.getPassword().toCharArray(), getName());
		simpleAuthenticationInfo.setCredentialsSalt(ByteSource.Util.bytes("SeefTech" + applicationUser.getId()));
		logger.debug("doGetAuthenticationInfo() Exit");
		return simpleAuthenticationInfo;
	}

	private Set<String> getUserPermissions(List<Permission> permissions){
		Set<String> perms = new HashSet<String>();
		for(Permission perm:permissions){
			perms.add(perm.getName());
		}
		logger.debug("Permissions :  ", permissions);
		return perms;

	}

	public SystemUserService getSystemUserService() {
		return systemUserService;
	}

	public void setSystemUserService(SystemUserService systemUserService) {
		this.systemUserService = systemUserService;
	}

}
