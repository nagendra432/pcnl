package com.seef.web.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.JsonObject;
import com.seef.dao.util.CollectionUtil;
import com.seef.models.Appointment;
import com.seef.models.OutPatient;
import com.seef.models.base.Status;
import com.seef.models.doctor.Doctor;
import com.seef.models.doctor.DoctorSchedule;
import com.seef.models.enums.CounsultationType;
import com.seef.service.AppointmentService;
import com.seef.service.BaseService;
import com.seef.service.OutPatientService;
import com.seef.web.editors.AbstractModelEditor;

@Controller
@RequestMapping("/takeappointment")
public class takeAppointmentController extends CRUDController<Appointment> {
	private static final Logger _log = LoggerFactory.getLogger(OutPatientRegistrationController.class);
	
	@Autowired 
	OutPatientService outPatientService;
	
	@Autowired 
	AppointmentService appointmentService;
	
	
	@Autowired
	MessageSource messageSource;
	
	
	@Override
	@RequestMapping(value="/create.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request) {
		
		String optionval = request.getParameter("optionval");
		
		String txtmrnophno = request.getParameter("txtmrno");
		Map<String, Object> criteria=new HashMap<String, Object>();
		
		if(optionval.equals("1"))
		{
			criteria.put("mrno", Long.valueOf(txtmrnophno));
		}
		else 
		{
			criteria.put("phoneNo", txtmrnophno);
		}
		
		OutPatient outpatient=outPatientService.getOutpatient(criteria);
		criteria.clear();
		criteria.put("outpatient", outpatient);
		modelMap.addAttribute("modelList", genericDAO.findByNamedQuery("lasttworecords", criteria));
		if(outpatient!=null){
			_log.info(">>>>>>>>>>>>>>>>"+outpatient.getFirstName());
			
			//claculation for age
			
			Calendar cal1 = new GregorianCalendar();
			cal1.setTime(outpatient.getDob());
			Date dob=outpatient.getDob();
			int year1 = Calendar.getInstance().get(Calendar.YEAR);
			int year2 =cal1.get(Calendar.YEAR);
			int age = year1 - year2;
			
			//
			Appointment appointment=new Appointment();
			appointment.setOutPatient(outpatient);
			appointment.setAge(age);
			
			setupCreate(modelMap,request);
			request.getSession().setAttribute("outptid", outpatient.getId());
			modelMap.addAttribute("model",appointment);
						
			return "tileDefinition.create-"+getNameSpace();
			
						
		}
		else{
			_log.info(">>>>>>>>>>>>>>>>outpatient not registered");
			modelMap.addAttribute("errorheader","outpatient not registered");
			modelMap.addAttribute("txtmrno",txtmrnophno);
			
			return "tileDefinition.search-"+getNameSpace();
		}
		
		
	}
	
	@Override
	@RequestMapping(value="/save.htm",method=RequestMethod.POST)
	public String save(@Valid @ModelAttribute("model") Appointment model, HttpServletRequest request, HttpServletResponse response, BindingResult bindingResult, ModelMap modelMap)
	{ 
		if(model.getId() == null){	
			Long tmpid= Long.valueOf( request.getSession().getAttribute("outptid").toString());
			
			if(!validateCreate(model, bindingResult,tmpid))
			 {
				OutPatient outpatient=outPatientService.getById(tmpid);
				model.setOutPatient(outpatient);
				modelMap.addAttribute("model",model);
				return "tileDefinition.create-"+getNameSpace();
			 }
			else
			{
				model.setOutPatient(outPatientService.getById(tmpid));			
				appointmentService.saveAppointment(model, getLoggedInUser());
				return getRedirectURL("/manage.htm");
			}
		 }	
     else
        {	
    	appointmentService.saveOrUpdateAppointment(model, getLoggedInUser());
    	return getRedirectURL("/manage.htm");
        }
		
	}
	
	@Override
	public void setupCreate(ModelMap modelMap,HttpServletRequest request)
	{
	    modelMap.addAttribute("counsultationType", CounsultationType.list());
	    modelMap.addAttribute("doctors", genericDAO.findAll(Doctor.class));
	}
	
	@Override
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.appointment.menu.name", null, userLocale));
		try
		{
		String searchDate1=request.getParameter("appointmentDate");
		Date searchDate= null;
		
		if(searchDate1==null)
		{	
			
		Calendar cal1 = new GregorianCalendar();
	    searchDate=new Date();
		}
		else
		{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			searchDate =df.parse(searchDate1);
			
		}
		
		modelMap.addAttribute("modelList", appointmentService.Appointment_byDate(CollectionUtil.toMap(new String[]{"date"}, new Object[]{searchDate})));
		}
		catch (Exception ex ){
	          System.out.println(ex);
	       }
		return "tileDefinition.manage-"+getNameSpace();
	}
	
	/*@Override
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String managelastrecords(ModelMap modelMap, HttpServletRequest request) {
	}*/
	
	@Override
	@RequestMapping(value="/search.htm", method=RequestMethod.GET)
	public String search(ModelMap modelMap, HttpServletRequest request) {
		// TODO Auto-generated method stub
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("create.appointment.menu.name", null, userLocale));
		return "tileDefinition.search-"+getNameSpace();
	}

	@Override
	public boolean validateUpdate(Appointment model, BindingResult result) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean validateCreate(Appointment entity,
			BindingResult bindingResult,Long outptid) {
		// TODO Auto-generated method stub

		Boolean validationResult=true;
		Locale userLocale=new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		Map<String,Object> criteria=new HashMap<String,Object>();
		
		criteria.put("date", entity.getAppointmentDate());
		criteria.put("outpatient", outptid);
		if(appointmentService.sameDayAppointmentCheck(criteria) >0)
		{
			 FieldError error=new FieldError("modelObject","appointmentDate",entity.getAppointmentDate(), false, null, null, messageSource.getMessage("label.appointmentdate.error.codeMessage",null,userLocale));
			   bindingResult.addError(error);
			   validationResult = false;
		}
		return validationResult;
	
		}

	@Override
	public BaseService<Appointment, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "takeappointment";
	}

	@Override
	public boolean validateCreate(Appointment entity,
			BindingResult bindingResult) {
		// TODO Auto-generated method stub
		return false;
	}
	@RequestMapping(value="/close.htm", method=RequestMethod.GET)
	public String close(ModelMap modelMap, HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		Appointment appointment=appointmentService.getById(id);
		appointment.setStatus(Status.COMPLETED);
		appointmentService.saveOrUpdateAppointment(appointment, getLoggedInUser());
		return getRedirectURL("/manage.htm");
	};
	
	@RequestMapping(value="/rejected.htm", method=RequestMethod.GET)
	public String rejected(ModelMap modelMap, HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		Appointment appointment=appointmentService.getById(id);
		appointment.setStatus(Status.CANCELLED);
		appointmentService.saveOrUpdateAppointment(appointment, getLoggedInUser());
		return getRedirectURL("/manage.htm");
	};
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/getdoctors", method=RequestMethod.GET)
	public void getChapters(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		PrintWriter out=response.getWriter();
		String date= request.getParameter("date");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	    Map<String,Object> criteria=new HashMap<String,Object>();
		
		try {
			_log.info(">>Date:"+dateFormatter.parse(date));
			criteria.put("scheduledDate",dateFormatter.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<DoctorSchedule> doctorslist = appointmentService.Doctors_listbyDate(criteria);
		_log.info(">>doctor size:"+doctorslist.size());
		criteria.clear();
		List doctors = new ArrayList();
		 Map medicineMap = null;
		for(DoctorSchedule section : doctorslist){
			criteria.put("doctor",section.getDoctor());
			try {
				criteria.put("appointmentDate",dateFormatter.parse(date));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<Appointment> appointments=genericDAO.findByCriteria(Appointment.class, criteria);
			
			if((Integer.parseInt(section.getNoOfAppointments())-appointments.size())!=0){
				medicineMap = new HashMap(); 
				String name=section.getDoctor().getName()+"( "+section.getInTime()+" - "+section.getOutTime()+" )"+"--"+section.getDoctor().getSpecilization();
				medicineMap.put("id", section.getDoctor().getId());
				medicineMap.put("name", name);
				doctors.add(medicineMap);
			}
		}
		      JSONArray jsonArray = new JSONArray(doctors);
		      
		      out.print(jsonArray);
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/getdoctorschedule", method=RequestMethod.GET)
	public void getdoctorschedule(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		PrintWriter out=response.getWriter();
		String date= request.getParameter("date");
		Long doctorid=Long.valueOf(request.getParameter("doctorid"));
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	    Map<String,Object> criteria=new HashMap<String,Object>();
		
		try {
			_log.info(">>Date:"+dateFormatter.parse(date));
			criteria.put("appointmentDate",dateFormatter.parse(date));
			criteria.put("doctorid", doctorid);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	List<Appointment> appointmentslist = appointmentService.DoctorsSchedule_listbyDate(criteria);
		_log.info(">>.........appointments size:"+appointmentslist.size());
		
		criteria.clear();
		try {
			criteria.put("scheduledDate",dateFormatter.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		criteria.put("doctor.id", doctorid);
	    String noOfAppts=genericDAO.getByCriteria(DoctorSchedule.class, criteria).getNoOfAppointments();
	    _log.info(">>......... no of appointments"+noOfAppts);
		List appointments = new ArrayList();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("totalappointments", appointmentslist.size());
		jsonObject.put("appdoctor", noOfAppts);
		org.json.simple.JSONArray jsonArray = new org.json.simple.JSONArray();
		for(Appointment section : appointmentslist){
			JSONObject jsonObject2 = new JSONObject();
			jsonObject2.put("id", section.getAppointmentDate().toString().substring(10));
			jsonArray.add(jsonObject2);
		  }
		  jsonObject.put("dates", jsonArray);
		  out.print(jsonObject);
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		binder.registerCustomEditor(Doctor.class, new AbstractModelEditor(Doctor.class));
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}


}
