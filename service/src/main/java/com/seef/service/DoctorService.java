package com.seef.service;

import java.util.List;
import java.util.Map;

import com.seef.models.ApplicationUser;
import com.seef.models.doctor.Doctor;

public interface DoctorService {
	public void saveDoctor(Doctor doctor,ApplicationUser applicationUser);
	public List<Doctor> doctor_list();
	 public Doctor getById(Doctor entity);
	 public Doctor editDoctor(Long id);
	 public Boolean validateUnique(Doctor doctor, Map<String, Object> map);
	public void saveOrUpdateDoctor(Doctor doctor, ApplicationUser applicationUser);
	public Doctor getById(Long id);
}
