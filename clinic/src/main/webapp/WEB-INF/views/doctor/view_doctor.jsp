<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>





<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-wide">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:history.go(-1);">&times;</button>
        <h4 class="modal-title" align="center"><spring:message code="label.header.doctor"/></h4>
     </div>
    <div class="modal-body">
    
            <div class="scroller" style="height:400px" data-always-visible="1" data-rail-visible1="1">
            <div class="row">
                        
                            <div class="wizard_body clearfix">
                            	<form:form   class="mws-form wzd-validate form-horizontal" commandName="model">
                                <fieldset class="wizard-step mws-form-inline">
								
								 <div class="col-md-6">		
								<div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.name"/></label>
                                       <div class="col-md-6">
                                          <form:input path="name"  class="form-control" disabled="true" />
                                       </div>
                                       </div>
                                   
           
               
                          			<div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.doctor.qualification"/></label>
                                       <div class="col-md-6">
                                          <form:input path="qualification"  class="form-control" disabled="true" />
                                       </div>
                                       </div>
                                
               
             
                          			<div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.doctor.designation"/></label>
                                       <div class="col-md-6">
                                          <form:input path="designation"  class="form-control"  disabled="true"/>
                                       </div>
                                       </div>
                 
                          			<div class="form-group">
                                       <label class="control-label col-md-3"><spring:message code="label.doctor.specilization"/></label>
                                       <div class="col-md-6">
                                          <form:input path="specilization"  class="form-control" disabled="true" />
                                       </div>
                                       </div>
                                    
                                    
               
                        	 <div class="form-group">
                            <label for="doctortype" class="col-md-3 control-label"><spring:message code="label.doctor.doctortype"/></label>
                            <div class="col-md-6">
								<form:select path="doctorType" id="doctorType" class="input-sm form-control required" disabled="true" >
			          				<form:option value="">-<spring:message code="label.select"/>-</form:option>
			          				<c:forEach items="${doctorType}" var="doctorType">
				            			<form:option value="${doctorType.type}"><spring:message code="${doctorType.type}"/></form:option>
				            		</c:forEach>
			      				</form:select>
							</div>
                       	 </div>
                      
               
              
                           
                           </div>
						</fieldset>
                    </form:form>
                 </div>
                <hr />
              </div>
              </div>
              </div>
              
              </div><!--/widget-main-->
			</div><!--/widget-body-->
			
		</div>
		
<script>
    $("#myModal").modal({                   
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                   
    });
</script>
















































