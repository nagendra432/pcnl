package com.seef.service;

import java.util.List;

import com.seef.models.ApplicationUser;
import com.seef.models.pharmacy.Medicine;
import com.seef.models.pharmacy.Supplier;

public interface SupplierService {
	
	public void save(Supplier supplier,ApplicationUser applicationuser);
	public void saveorupdate(Supplier supplier,ApplicationUser applicationuser);
	public Supplier getById(Long id);
	public List<Supplier> supplier_list();
	
}
