package com.seef.models.base;
import java.io.Serializable;

public interface BaseDomainModel extends Serializable{
	Long getId();
	void setId(Long id);

}
