<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.outpatient.firstname" var="lblfirstname"/>
<spring:message code="label.outpatient.middlename" var="lblmiddlename"/>
<spring:message code="label.outpatient.lastname" var="lbllastname"/>
<spring:message code="label.outpatient.phno" var="lblphno"/>
<spring:message code="label.address.line1" var="lbladdress1"/>
<spring:message code="label.address.line2" var="lbladdress2"/>
<spring:message code="label.address.zipcode" var="lblzipcode"/>
<spring:message code="label.address.city" var="lblcity"/>
<spring:message code="label.address.country" var="lblcountry"/>
<spring:message code="label.address.state" var="lblstate"/>
<spring:message code="label.address.fulladdress" var="lblfulladdress"/>
<spring:message code="label.outpatient.dob" var="lbldob"/>
<spring:message code="label.outpatient" var="lbloutpatient"/>
<spring:message code="label.outpatient.time" var="lbltime"/>
<spring:message code="label.appointmentdate" var="lblappointmentdate"/>
<spring:message code="label.consultationtype" var="lblconsultationtype"/>
<spring:message code="label.doctors" var="lbldoctors"/>
<spring:message code="label.fee" var="lblfee"/>
<spring:message code="label.save" var="lblsave"/>
<spring:message code="label.cancel" var="lblcancel"/>


<script type="text/javascript">
 $(document).ready(function(){
  $(".form_date").datetimepicker({         
   isRTL: App.isRTL(),
            format: "yyyy-mm-dd",
            weekStart: 1,
            todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 4,
      minView: 2,
      forceParse: 0,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
       });
  
  $(".form_datetime").datetimepicker({         
	   isRTL: App.isRTL(),
	            format: "yyyy-mm-dd hh:ii",
	            weekStart: 1,
	            todayBtn:  1,
	      autoclose: 1,
	      todayHighlight: 1,
	      startView: 2,
	      minView: 0,
	      forceParse: 0,
	            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
	       });
 });
</script>



   
   <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>User</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                     </div>
                  </div>
                  <div class="portlet-body">
                  <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model" >
                     <div class="tabbable-custom ">
                        <div class="tab-content">
                           <div class="tab-pane active" id="tab_5_1">
                              <div class="row">
                                    <div class="col-md-6">
	                          			 <div class="form-group">
		                                       <label class="control-label col-md-3">Salutation<span class="required">*</span></label>
		                                       <div class="col-md-7">
		                                         <form:select path="systemUserDetails.salutation" cssClass="form-control">
				                                     <form:options items="${salutation}" itemValue="type"/>
				                                 </form:select>
		                                       </div>
	                                      </div>
                                      </div>
                                      <div class="col-md-6">
                                       <div class="form-group">
		                                       <label class="control-label col-md-3">First Name<span class="required">*</span></label>
		                                       <div class="col-md-7">
		                                          <form:input path="firstName"  class="form-control required"  maxlength="20" minlength="3" alphabets="true"/>
		                                       </div>
	                                      </div>
                                      </div>
                               </div>
		                       <div class="row">
		                            <div class="col-md-6">
		                        			<div class="form-group">
		                                     <label class="control-label col-md-3">Mobile<span class="required">*</span></label>
		                                     <div class="col-md-7">
		                                        <form:input path="mobileNumber" class="form-control required" maxlength="11" minlength="5" data-validate-length-range="3,3"/>
		                                     </div>
		                                   </div>
		                              </div>
	                                  <div class="col-md-6">
	                        			<div class="form-group">
	                                     <label class="control-label col-md-3">Last Name<span class="required">*</span></label>
	                                     <div class="col-md-7">
	                                         <form:input path="lastName" class="form-control required"  data-validate-length-range="3,3"/>
	                                     </div>
	                                   </div>
	                                 </div>
		                      </div>
		                      
		                       <div class="row">
                                    <div class="col-md-6">
	                          			 <div class="form-group">
		                                       <label class="control-label col-md-3">Gender<span class="required">*</span></label>
		                                       <div class="col-md-7">
			                                        <form:select path="systemUserDetails.gender">
			                                           <form:options items="${genders}" itemLabel="type" itemValue="type" class="form-control" />
			                                        </form:select>
		                                       </div>
	                                      </div>
                                      </div>
                                      <div class="col-md-6">
                                       <div class="form-group">
		                                       <label class="control-label col-md-3">Username<span class="required">*</span></label>
		                                       <div class="col-md-7">
		                                          <form:input path="Username"  class="form-control"  maxlength="20" minlength="3"/>
		                                       </div>
	                                      </div>
                                      </div>
                               </div>
                               <c:if test="${null eq model.id}">
                                <div class="row">
                                    <div class="col-md-6">
	                          			 <div class="form-group">
		                                       <label class="control-label col-md-3">Password<span class="required">*</span></label>
		                                       <div class="col-md-7">
			                                        <form:password path="password"   class="form-control required"  maxlength="20" minlength="3" />
		                                       </div>
	                                      </div>
                                      </div>
                                      <div class="col-md-6">
                                       <div class="form-group">
		                                       <label class="control-label col-md-3">Conform Password<span class="required">*</span></label>
		                                       <div class="col-md-7">
		                                          <input type="password" name="conformPassword " id="conpass" class="form-control required" />
		                                       </div>
	                                      </div>
                                      </div>
                               </div>
                               </c:if>
                               
                              <div class="row">
                                      <div class="col-md-6">
                                       <div class="form-group">
		                                       <label class="control-label col-md-3">Staff Id<span class="required">*</span></label>
		                                       <div class="col-md-7">
		                                          <form:input path="systemUserDetails.signaturePhoto"  class="form-control required"  maxlength="20" minlength="3"/>
		                                       </div>
	                                      </div>
                                      </div>
                               </div>
                               <div class="row">
                                   <div class="col-md-6">
                          			 <div class="form-group">
	                                       <label class="control-label col-md-3">Designation<span class="required">*</span></label>
	                                       <div class="col-md-7">
		                                        <form:select path="userType" class="form-control required">
		                                           <form:options items="${designation}"   />
		                                        </form:select>
	                                       </div>
                                      </div>
                                    </div>
                                     <div class="col-md-6">
                                       <div class="form-group">
		                                       <label class="control-label col-md-3">Role<span class="required">*</span></label>
		                                       <div class="col-md-7">
			                                        <form:select path="role" class="form-control required" >
			                                           <form:options items="${roles}" itemLabel="name" itemValue="id" />
			                                        </form:select>
		                                        </div>
	                                      </div>
                                      </div>
                              </div>
                             <div class="form-actions fluid">
	                           <div class="col-md-offset-3 col-md-9">
	                              <button type="submit" class="btn green">${lblsave}</button>
	                              <button type="button" onclick="javascript:history.go(-1);" class="btn default">${lblcancel}</button>                              
	                           </div>
                            </div> 
                         </div>
                        </div>
                   </div>
                     </form:form>
                  </div>
               </div>
               
   <script type="text/javascript">
      jQuery(document).ready(function() { 
    	  FormComponents.init(); 
         FormValidation.init();
         $("#scheduledate").change(function(){
        	 var str = $(this).val();
             $.get("getdoctors.htm?date="+ str,function(data) {
                $('select#doctors').html('<option value="">-- Select --</option>');
              $.each(data,function(index) {
               $('select#doctors').append('<option value="'+data[index].id+'">'+ data[index].name+ '</option>');
              });
                          
              });
        	  
        	});
         
         
             });
      </script>
      



<script type="text/javascript">  
$(document).ready(function() {  
$('#types').change(function(){  
if($('#types').val() === 'CONSULTATION')  
   {  
   $('#other').show();   
   }  
else 
   {  
   $('#other').hide();     
   }  
});  
});  
</script>


