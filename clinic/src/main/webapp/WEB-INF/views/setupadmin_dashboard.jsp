<%-- <%@ page import="java.util.List,java.util.Date,java.text.SimpleDateFormat,com.seef.models.dto.UserSession,org.apache.shiro.SecurityUtils" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head>
<!-- For  Graph  -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/graph/transaction.css" />
<script src="${pageContext.request.contextPath}/assets/scripts/graph/amcharts.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/scripts/graph/serial.js" type="text/javascript"></script>


<!-- For Total Graph Downloading -->
<script src="${pageContext.request.contextPath}/assets/scripts/graph/amexport.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/scripts/graph/rgbcolor.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/scripts/graph/canvg.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/scripts/graph/jspdf.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/scripts/graph/filesaver.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/scripts/graph/jspdf.plugin.addimage.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery.searchabledropdown-1.0.8.min.js"></script>
<script>
			$(document).ready(function() {
			    $("select").searchable();
			});
</script>
</head>
<!-- <meta  http-equiv="refresh" content="5" /> -->
<c:if test="${!empty passwordMessage}">
		     <script type="text/javascript" class="select">
		        BootstrapDialog.show({
		         title: '<spring:message code='label.password.alert'/>',
		         type: BootstrapDialog.TYPE_WARNING, 
		         message: "${passwordMessage}",
		   	    });
		     </script>
 </c:if>
 
 <div id="id">Select  Institution:
	   	<label>
			<select class="select2">
				<option>--Please Select--</option>
				<%String data1=(String)request.getAttribute("data");
				  String splitdata1[]=data1.split(",");
				    for(int n3=0;n3<=splitdata1.length-1;n3++){
				%> 
				<option onClick="selectDataset('<%=splitdata1[n3]%>',<%=n3%>)"><%=splitdata1[n3]%></option>
				<%}%>
			</select>
	   	</label>
</div>
 <script type="text/javascript">
 function selectDataset(name,d) {
     chart.dataProvider = chartData[d];
     chart.titles=[{"text": name+" Transactions", "size": 14, "color": "#008000", "alpha": 1, "bold": true}];
     chart.validateData();
     chart.animateAgain();
 }
 var chart;
<%!String[] splitdata=null;%>
 var chartData = [
                  <%
                  List totaltransaction=(List)request.getAttribute("total");
                  for(int n1=0;n1<totaltransaction.size();n1++){
                	 splitdata=((String)totaltransaction.get(n1)).split(",");
                  %>
     		[
      		<%
      			 for(int n2=1;n2<splitdata.length;n2+=4){
      		%>
			{
			    "Service": "<%= splitdata[n2] %>",
			    "Used": <%=splitdata[n2+1]%>,
			    "Amount": <%=splitdata[n2+2]%>,
			    "Fee":<%=splitdata[n2+3]%>,
			},
			<%
      		}
			%>
     		],
     
     	<%}%>
 			]
		 AmCharts.ready(function () {
		     // SERIAL CHART
		     chart = new AmCharts.AmSerialChart();
		     chart.dataProvider = chartData;
		     chart.categoryField = "Service";
		     chart.plotAreaBorderAlpha = 0.2;
		
		   
             chart.fontSize=10;
             chart.borderAlpha=1;
             chart.borderColor="#FF4500";
		    
             // AXES
             var categoryAxis = chart.categoryAxis;
             categoryAxis.tickLength=5;
             categoryAxis.axisThickness=1;
             categoryAxis.minHorizontalGap=1;
             categoryAxis.gridAlpha=0.18;
             //categoryAxis.gridColor="#008000";
             categoryAxis.labelRotation = 90;
             categoryAxis.gridPosition = "start";
		
		     // value
		     var valueAxis = new AmCharts.ValueAxis();
		     valueAxis.stackType = "regular";
		     valueAxis.gridAlpha = 0.18;
		     valueAxis.axisAlpha = 0;
		     chart.addValueAxis(valueAxis);
		
		     // CURSOR
             var chartCursor = new AmCharts.ChartCursor();
             chartCursor.cursorAlpha = 0;
             //chartCursor.bulletsEnabled=true;
             chartCursor.zoomable = false;
             chartCursor.categoryBalloonEnabled = true;
             chart.addChartCursor(chartCursor);
             chart.creditsPosition = "top-right";
				
             chart.exportConfig={
                 menuTop: "21px",
                 menuBottom: "auto",
                 menuLeft: "21px",
                 backgroundColor: "#1E90FF",

                 menuItemStyle	: {
                 backgroundColor			: '#1E90FF',
                 rollOverBackgroundColor	: '#DDDDDD',
                 },
                 menuItems: [{
                     textAlign: 'center',
                     icon: '${pageContext.request.contextPath}/assets/img/export.png',
                     onclick:function(){},
                     items: [{
                         title: 'JPG',
                         format: 'jpg',
                     }, {
                         title: 'PNG',
                         format: 'png',
                     }, {
                         title: 'SVG',
                         format: 'svg',
                     }, {
                         title: 'PDF',
                         format: 'pdf',
                     }]
                 }]
             };
		     
		     // GRAPHS
		     // first graph    
		     var graph = new AmCharts.AmGraph();
		     graph.title = "Used";
		    // graph.labelText = "[[value]]";
		     graph.valueField = "Used";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#FF4500";
		     graph.balloonText = "<span style='color:#555555;'>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
		     chart.addGraph(graph);
		
		     // second graph              
		     graph = new AmCharts.AmGraph();
		     graph.title = "Amount";
		    // graph.labelText = "[[value]]";
		     graph.valueField = "Amount";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#228B22";
		     graph.balloonText = "<span style='color:#555555;'>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
		     chart.addGraph(graph);                
		
		     // third graph                              
		     graph = new AmCharts.AmGraph();
		     graph.title = "Fee";
		   //  graph.labelText = "[[value]]";
		     graph.valueField = "Fee";
		     graph.type = "column";
		     graph.lineAlpha = 0;
		     graph.fillAlphas = 1;
		     graph.lineColor = "#8B4513";
		     graph.balloonText = "<span style='color:#555555;'>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
		     chart.addGraph(graph);
		
		     // LEGEND                  
		     var legend = new AmCharts.AmLegend();
		     //legend.spacing=10;
		     legend.autoMargins=false;
		     legend.position = "absolute";
             legend.markerType = "square";
		     legend.borderAlpha = 0.0;
		     legend.horizontalGap = 10;
		    
             
             chart.addLegend(legend, "legenddiv");
		     
		     
		     // WRITEs
		     chart.write("chartdiv");
		 });
</script>


<div id="chartdiv" style="width: 100%; height: 370px;"></div>
<div id="legenddiv"></div>
 --%>