package com.seef.models;

import java.text.SimpleDateFormat;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("SYSTEM_USER")
public class SystemUser extends ApplicationUser {

	private static final long serialVersionUID = -1476603470738009321L;


	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	SystemUserDetails systemUserDetails;

	public SystemUser(){

	}
	
	public SystemUser(Long id){
		this.id=id;
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@OneToOne(mappedBy="systemUser",cascade={CascadeType.ALL})
	public SystemUserDetails getSystemUserDetails() {
		return systemUserDetails;
	}

	public void setSystemUserDetails(SystemUserDetails systemUserDetails) {
		this.systemUserDetails = systemUserDetails;
	}




}