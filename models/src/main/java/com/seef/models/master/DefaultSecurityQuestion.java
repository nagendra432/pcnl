package com.seef.models.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name = "DEFAULT_SECURITY_QUESTION")
public class DefaultSecurityQuestion extends AbstractDomainObject {


	private static final long serialVersionUID = -6733216870338030388L;

	private Long id;

	private String question;

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "DEFAULT_SECURITY_QUESTION_SEQ")
	@SequenceGenerator(name = "DEFAULT_SECURITY_QUESTION_SEQ", sequenceName = "DEFAULT_SECURITY_QUESTION_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "QUESTION")
	public String getQuestion() {
		return question;
	}
	
	public void setQuestion(String question) {
		this.question = question;
	}

}
