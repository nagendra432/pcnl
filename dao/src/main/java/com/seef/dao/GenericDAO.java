package com.seef.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.UserStatus;

/*****************************************************
 * Generic Class for common DAO Operations
 * Added by: SRK 
 * Date: 11th July, 2013
 *****************************************************/
public interface GenericDAO extends BaseDao<AbstractDomainObject, Long> {

	public EntityManager getEntityManager(); //For AuditReader 

	public <K extends AbstractDomainObject> K getById(Class<K> clazz, Long id);	

	public <K extends AbstractDomainObject> K  getByCriteria(Class<K> clazz, Map<String, Object> criterias);

	public <K extends AbstractDomainObject> List<K> findByCriteria( Class<K> clazz, Map<String, Object> criterias);

	public <K extends AbstractDomainObject> List<K> findAll(Class<K> clazz);

	public AbstractDomainObject save( AbstractDomainObject entity );

	public AbstractDomainObject saveOrUpdate(AbstractDomainObject entity);

	public <T extends AbstractDomainObject> boolean isUnique(Class<T> clazz, T entity, Map<String, Object> properties);

	public String executeQuerySingleString(String queryName, Map<String, Object> params);

	public <K extends AbstractDomainObject> List<K> findByNamedQuery(String namedQuery, Map<String,Object> map);

	public <T extends AbstractDomainObject> T getById(Class<T> clazz, AbstractDomainObject entity);

	public <K extends AbstractDomainObject> List<K> executeNamedQuery(String queryName,Class<K> clazz,Map<String, Object> params);

	public <K extends AbstractDomainObject> List<K> searchByQuery(Class<K> clazz, int noOfRecord, String query, String countQuery) ;

	public <T extends AbstractDomainObject> List<T> executeSimpleQuery(String query);

	public <K extends AbstractDomainObject> List<K> findByCriteria( Class<K> clazz, Map<String, Object> criterias, String sortField, boolean desc);
	
	//Added by akilesh
	
	public Long executeQuerySinglequery(String queryName, Map<String, Object> params);
	
	public UserStatus executeQueryStatusString(String queryName, Map<String, Object> params);
	
	public <K extends AbstractDomainObject> K findOneNativeQuery(String query,Map<String,Object> params,Class<K> clazz);
	//public <K extends AbstractDomainObject> List<K> findAllByInstitution(Class<K> clazz, Institution institution);
	
}
