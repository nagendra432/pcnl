package com.seef.models.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name="PERMISSIONS")
public class Permission extends AbstractDomainObject implements Comparable<Permission>{

	private static final long serialVersionUID = 3523363867262803727L;
	
	private Long id;	
	private String name;	
	private String description;	
	private int order;	
    private PermissionGroup permissionGroup;
	private String key;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "PERMISSIONS_SEQ")
	@SequenceGenerator(name = "PERMISSIONS_SEQ", sequenceName = "PERMISSIONS_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@ManyToOne
	@JoinColumn(name="PERMISSION_GROUP_ID")
	@JsonBackReference
	public PermissionGroup getPermissionGroup() {
		return permissionGroup;
	}

	public void setPermissionGroup(PermissionGroup permissionGroup) {
		this.permissionGroup = permissionGroup;
	}
	
	@Column(name="PERMISSION_ORDER")
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	
	@Column(name="NAME_KEY")
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	
	@Override
	public int compareTo(Permission permission) {
		int order = permission.getOrder();
		return this.order - order; //asceding order
		//return order - this.order //descending order
	}

}
