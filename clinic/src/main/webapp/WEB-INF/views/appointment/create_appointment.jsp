<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.outpatient.mrno" var="lblmrno"/>
<spring:message code="label.appointment" var="lblappointment"/>
<spring:message code="label.name" var="lblname"/>
<spring:message code="label.outpatient.lastname" var="lbllastname"/>
<spring:message code="label.outpatient.phno" var="lblphno"/>
<spring:message code="label.address.fulladdress" var="lblfulladdress"/>
<spring:message code="label.age" var="lblage"/>
<spring:message code="label.outpatient" var="lbloutpatient"/>
<spring:message code="label.save" var="lblsave"/>
<spring:message code="label.cancel" var="lblcancel"/>
<spring:message code="label.appointmentdate" var="lblappointmentdate"/>
<spring:message code="label.consultationtype" var="lblconsultationtype"/>
<spring:message code="label.doctors" var="lbldoctors"/>
<spring:message code="label.outpatient.time" var="lbltime"/>
<spring:message code="label.fee" var="lblfee"/>


<script type="text/javascript">
 $(document).ready(function(){
	 var todate = new Date();
	 todate.setDate(todate.getDate() + 15);
	
  $(".form_datetime").datetimepicker({         
   isRTL: App.isRTL(),
            format: "yyyy-mm-dd hh:ii",
            todayBtn:  1,
		      autoclose: 1,
		      todayHighlight: 0,
		      startView: 2,
		      minView: 0,
		      startDate:new Date(),
		      endDate:todate,
		      forceParse: 0,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
       });
 });
</script>


 <div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>${lblappointment}</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model">
	                       <form:hidden path="id"/>
	                       <form:hidden path="outPatient.id"/>
	                        <div style="color:red">                      		
		                    <form:errors path="*" cssClass="errorcollection"/>
		                  </div>
                        <div class="form-body">
                           <div class="alert alert-danger display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formerrormsg"/>
                           </div>
                           <div class="alert alert-success display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formsuccessmsg"/>
                           </div>
                   
                           
                          
                        </div> <div class="form-group">
                              <label class="control-label col-md-3">${lblmrno}</label>
                              <div class="col-md-4">
                                 <form:input path="outPatient.mrno"  class="form-control" readonly="true"/>
                              </div>
                           </div>
                           
                           
                           <div class="form-group">
                              <label class="control-label col-md-3">${lblname}</label>
                              <div class="col-md-4">
                                 <form:input path="outPatient.fullName"  class="form-control"   
					                              readonly="true"  />
                              </div>
                           </div>
                                                    
                           	<div class="form-group">
							<label class="control-label col-md-3">${lblage}</label>
							<div class="col-md-4">
								<form:input path="age" class="form-control" readonly="true"/>
							</div>
						</div>
                         
                            <div class="form-group">
                              <label class="control-label col-md-3">${lblphno}</label>
                              <div class="col-md-4">
                                 <form:input path="outPatient.phoneNo" class="form-control"  readonly="true"/>
                              </div>
                           </div>
                           
                              <div class="form-group">
                              <label class="control-label col-md-3">${lblfulladdress}</label>
                              <div class="col-md-4">
                                 <form:textarea path="outPatient.address.fullAddress" class="form-control" readonly="true" />
                              </div>
                           </div>
                           
                           <div class="form-group">
							<label class="control-label col-md-3">${lblappointmentdate}<span class="required">*</span></label>
							<div class="col-md-4">
								<div class="input-group date form_datetime">
									   <form:input  path="appointmentDate" required="required" class="form-control" id="scheduledate"/>
									    <span class="input-group-btn">
										<button class="btn default date-set" type="button">
											<i class="icon-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						
							<div class="form-group">
						<label class="control-label col-md-3">${lbldoctors}<span class="required">*</span></label>
						<div class="col-md-4">
								 <form:select path="doctor" class="form-control" id="doctors" required="required">
								 <form:option value="">--select--</form:option>
							      <c:forEach items="${doctors}" var="doctor">
							       <option value="${doctor.id}">${doctor.name}</option>
							       </c:forEach>
							     </form:select>
							</div>
						
						</div>
						
						<div class="form-group">
                              <label class="control-label col-md-3">${lbltime}<span class="required">*</span></label>
                              <div class="col-md-4">
                              <div class="input-group bootstrap-timepicker">
                                 <form:input path="time" class="form-control timepicker-default" required="required" />
                                  <span class="input-group-btn">
                                          <button class="btn default" type="button"><i class="icon-time"></i></button>
                                          </span>
                              </div>
                           </div>
                           </div>
						
                            <div class="form-group">
							<label class="control-label col-md-3">${lblconsultationtype}<span class="required">*</span></label>
							<div class="col-md-4">
							  <form:select path="counsultationType" class="form-control" id="types" required="required">
							   <form:option value="">--select--</form:option>
     						 		         	<c:forEach items="${counsultationType}" var="consultationtype">
				                    	        <form:option value="${consultationtype.type}">${consultationtype.type}</form:option>
				                    	    </c:forEach>
								  </form:select>
							</div>
						</div> 
					 <div class="form-group" id="other">
			                           <label class="control-label col-md-3">${lblfee}</label>
			                            <div class="col-md-4">
											<form:input path="fee"  class="form-control"/>
										</div>
			                       	</div> 
						
                        <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green">${lblsave}</button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default">${lblcancel}</button>                              
                           </div>
                        </div> 
                     </form:form>
                     <!-- END FORM-->
                  </div>
               </div>
               <!-- END VALIDATION STATES-->
            </div>
         </div>
         
            <script type="text/javascript">
      jQuery(document).ready(function() { 
    	  FormComponents.init(); 
         FormValidation.init();
         $("#scheduledate").change(function(){
        	 var str = $(this).val();
             $.get("getdoctors.htm?date="+ str,function(data) {
                $('select#doctors').html('<option value="">-- Select --</option>');
              $.each(data,function(index) {
               $('select#doctors').append('<option value="'+data[index].id+'">'+ data[index].name+ '</option>');
              });
                          
              });
        	  
        	});
         
         
             });
      </script>
      
      
            <script type="text/javascript">  
$(document).ready(function() {  
$('#types').change(function(){  
if($('#types').val() === 'CONSULTATION')  
   {  
   $('#other').show();   
   }  
else 
   {  
   $('#other').hide();     
   }  
});  
});  
</script>
  
   


