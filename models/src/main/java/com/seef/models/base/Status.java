package com.seef.models.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Status {
	NEW("NEW"),INACTIVE("INACTIVE"), ACTIVE("ACTIVE"), PENDING("PENDING"), ASSIGNED("ASSIGNED"), APPROVED("APPROVED"), REJECTED("REJECTED"), SENTBACK("SENTBACK"),COMPLETED("COMPLETED"),CANCELLED("CANCELLED");

	private String val;

	private Status(String val) {
		this.val = val;
	}

	public static List<Status> list() 
	{
		return Arrays.asList(values());
	}
	
	public static List<Status> getGeneralStatuslist() 
	{
		List<Status> statuses = new ArrayList<Status>();
		statuses.add(ACTIVE);
		statuses.add(INACTIVE);
		return statuses;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

}
