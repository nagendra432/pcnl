package com.seef.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;

@Entity
@Table(name = "SECURITY_QUESTION")
public class SecurityQuestion extends AbstractDomainObject {

	private static final long serialVersionUID = -8834526433238124894L;
	
	private Long id;	
	private String question;	
	private String answer;
	
	public SecurityQuestion(){
	}
	
	public SecurityQuestion(SecurityQuestion securityQuestion){
		this.id=securityQuestion.getId();
		this.question=securityQuestion.getQuestion();
		this.answer=securityQuestion.getAnswer();
	}
	
   public SecurityQuestion(Long id,String question,String answer){
 		this.id=id;
 		this.question=question;
 		this.answer=answer;
	}
   
    @Id
	@Column(name = "ID")
	@GeneratedValue(generator = "SECURITY_QUESTION_SEQ")
	@SequenceGenerator(name = "SECURITY_QUESTION_SEQ", sequenceName = "SECURITY_QUESTION_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	
	@Column(name = "QUESTION")
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}

	@Column(name = "ANSWER")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}

}