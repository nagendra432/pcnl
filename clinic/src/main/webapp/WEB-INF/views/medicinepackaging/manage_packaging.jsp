<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="datatables"
	uri="http://github.com/dandelion/datatables"%>


<spring:message code="label.view" var="lblView"/>
<spring:message code="label.edit" var="lblEdit"/>
<spring:message code="label.action" var="lblaction"/>
	<spring:message code="label.medicinename" var="lblmedicinename"/>
	<spring:message code="label.stock.batchno" var="lblbatchno"/>
	<spring:message code="label.stock.receiveddate" var="lblreceiveddate"/>
	<spring:message code="label.stock.expirydate" var="lblexpirydate"/>
	<spring:message code="label.stock.units" var="lblunits"/>
	<spring:message code="label.stock.supplier" var="lblsupplier"/>


<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<spring:message code="header.medicinepackaging.manage" />
			
		</h3>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
	<div class="col-md-12">

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-edit"></i>
					<spring:message code="label.resultsfor" />
					
					<spring:message code="header.medicinepackaging.manage" />
					
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					 <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="col-lg-2 pull-right">
					
				<div class="table-toolbar pull-right">
	              <div class="btn-group">
	                <a href="create.htm?id=${medicineid}"><button id="sample_editable_1_new" class="btn green"><spring:message code="button.addnew"/><i class="icon-plus"></i> </button></a>
	              </div>
	           	</div>
				</div>
				<datatables:table id="sample_editable_1" data="${modelList}" cdn="false" row="medicinepackaging" cssClass="table table-striped table-bordered table-hover">
					<datatables:column title="${lblmedicinename}" property="medicine.medicineName" />
					<datatables:column title="${lblbatchno}" property="batchNo"></datatables:column>
					<datatables:column title="${lblreceiveddate}" property="received"></datatables:column>
					<datatables:column title="${lblexpirydate}" property="expiryDate"></datatables:column>
					<datatables:column title="${lblunits}" property="units"></datatables:column>
					<datatables:column title="${lblsupplier}" property="supplier.name"></datatables:column>
                    
					
					
				</datatables:table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->


