package com.seef.service.impl;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.stereotype.Component;

import com.seef.models.ApplicationUser;
import com.seef.models.SystemUser;
import com.seef.service.PasswordManager;

@Component("passwordManager")
public class PasswordManagerImpl implements PasswordManager {

	public void encryptPassword(SystemUser systemUser) {
		Object salt = prepareSalt(String.valueOf(systemUser.getId()));
		String hashedPasswordBase64 = new Sha256Hash(systemUser.getPassword(), salt, 1024).toBase64();
		systemUser.setPassword(hashedPasswordBase64);
	}

	public void encryptPassword(ApplicationUser mobileUser) {
		Object salt = prepareSalt(String.valueOf(mobileUser.getId()));
		String hashedPasswordBase64 = new Sha256Hash(mobileUser.getPassword(), salt, 1024).toBase64();
		mobileUser.setPassword(hashedPasswordBase64);
	}

	public boolean verifyPassword(String password, SystemUser systemUser) {
		String hashedPasswordBase64 = new Sha256Hash(password, prepareSalt(systemUser.getId()), 1024).toBase64();
		return hashedPasswordBase64.equals(systemUser.getPassword());
	}

	public boolean verifyPassword(String password, ApplicationUser mobileUser) {
		String hashedPasswordBase64 = new Sha256Hash(password, prepareSalt(mobileUser.getId()), 1024).toBase64();
		return hashedPasswordBase64.equals(mobileUser.getPassword());
	}




	private String getEncryptedPin(String pin,Object salt) {
		return new Sha256Hash(pin,salt, 1024).toBase64();
	}

	

	@Override
	public boolean verify(String actual, String salt, String provided) {
		String hashedPasswordBase64 = new Sha256Hash(provided, salt, 1024).toBase64();
		return hashedPasswordBase64.equals(actual);
	}

	@Override
	public String encrypt(String plain, String salt) {
		return getEncryptedPin(plain, salt);
	}


	public String prepareSalt(String suffix){
		//FIXME : need to see how to manage this efficiently
		return "SeefTech"+suffix;
	}

	public String prepareSalt(Long salt) {
		return prepareSalt(String.valueOf(salt));
	}

	@Override
	public String getEncryptedPassword(Long id, String plainTextPassword) {
		Object salt = prepareSalt(String.valueOf(id));
		String hashedPasswordBase64 = new Sha256Hash(plainTextPassword, salt, 1024).toBase64();
		System.out.println("Hashed Password for User: " +  hashedPasswordBase64);
		return hashedPasswordBase64;
	}

	


}
