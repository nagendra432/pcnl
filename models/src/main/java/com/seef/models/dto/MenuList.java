package com.seef.models.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.seef.models.menu.dto.Menu;

@XmlRootElement(name="menuList")
public class MenuList implements Serializable {

	private static final long serialVersionUID = 2613655366616243206L;

	private List<Menu> menus;

	private Map<Integer, Menu> menuMap;

	public MenuList(){
		this.menuMap=new HashMap<Integer, Menu>();
	}

	@XmlElement(name="menu")
	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public void filterMenus(Collection<String> permissions) {
		if(null != this.menus){
			for(Menu menu : this.menus){
				menu.filter(permissions);
			}
		}

	}

	public Map<Integer, Menu> getMenuMap() {
		return menuMap;
	}

	public void setMenuMap(Map<Integer, Menu> menuMap) {
		this.menuMap = menuMap;
	}

	public void addMenuMap(int menuId,Menu menu){
		this.menuMap.put(menuId, menu);
	}

	public Menu getByMenuId(int menuId){
		return menuMap.get(menuId);
	}

	public void buildMenuMap() {
		for(Menu menu : this.menus){
			buildMenuMap(menu);
		}
	}

	private void buildMenuMap(Menu menu) {
		menuMap.put(menu.getId(), menu);
		if(null != menu.getMenus() && menu.getMenus().size()>0){
			for(Menu subMenu : menu.getMenus()){
				buildMenuMap(subMenu);
			}
		}
	}


}
