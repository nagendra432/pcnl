package com.seef.dao.util;

import java.util.HashMap;
import java.util.Map;

public class CollectionUtil {
	
	public static Map<String, Object> toMap(String[] keys,Object[] values){
		Map<String, Object> params=new HashMap<String, Object>(keys.length);
		for(int pos=0;pos<keys.length;pos++){
			params.put(keys[pos], values[pos]);
		}
		return params;
	}

}
