package com.seef.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.SideType;

@Entity
@Table(name="DJSTENT")
public class DJStent extends AbstractDomainObject {

	private static final long serialVersionUID = 3167252402130802153L;
	private Long id;
	private SideType sideType;
	private String size;
	private OutPatient patient;
	private String dangler;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "SIDETYPE")
	public SideType getSideType() {
		return sideType;
	}

	public void setSideType(SideType sideType) {
		this.sideType = sideType;
	}

	@Column(name="SIZE")
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	@ManyToOne
	@JoinColumn(name="PATIENT")
	 public OutPatient getPatient() {
		return patient;
	}

	public void setPatient(OutPatient patient) {
		this.patient = patient;
	}
	
	@Column(name="DANGLER")
	public String getDangler() {
		return dangler;
	}

	public void setDangler(String dangler) {
		this.dangler = dangler;
	}

}
