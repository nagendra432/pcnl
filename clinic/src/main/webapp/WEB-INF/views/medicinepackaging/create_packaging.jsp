<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.save" var="lblsave"/>
<spring:message code="label.cancel" var="lblcancel"/>
<spring:message code="label.medicinename" var="lblmedicinename"/>
<spring:message code="header.medicinepackaging.manage" var="lblmedicinepackaging"/>
<spring:message code="label.receiveddate" var="lblreceiveddate"/>
<spring:message code="label.expirydate" var="lblexpirydate"/>
<spring:message code="label.units" var="lblunits"/>
<spring:message code="label.cost" var="lblcost"/>
<spring:message code="label.stock.supplier" var="lblsupplier"/>
<spring:message code="label.batchno" var="lblbatchno"/>
<spring:message code="label.addsupplier" var="lbladd"/>
<spring:message code="label.tax" var="lbltax"/>


 <div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>${lblmedicinepackaging}</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model">
                 <!-- <input type="hidden" name="supname" id="supname"/>
                   <input type="hidden" name="supcompany" id="supcompany"/>
                    <input type="hidden" name="supphoneNo" id="supphoneNo"/>
                     <input type="hidden" name="supmailId" id="supmailId"/> -->
	                       <form:hidden path="id"/>
	                       <form:hidden  path="medicine.id"/> 
	                       <div style="color:red">                      		
		                    <form:errors path="*" cssClass="errorcollection"/>
		                  </div>
                        <div class="form-body">
                           <div class="alert alert-danger display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formerrormsg"/>
                           </div>
                           <div class="alert alert-success display-hide">
                              <button class="close" data-dismiss="alert"></button>
                              <spring:message code="label.formsuccessmsg"/>
                           </div>
                   </div> 
                           
                          
                                  
                          			<div class="form-group">
                                       <label class="control-label col-md-3">${lblmedicinename}</label>
                                       <div class="col-md-4">
                                          <form:input path="medicine.medicineName"  class="form-control" required="required" readonly="true" />
                                       </div>
                                       </div>
                                    
                                    <div class="form-group">
                                       <label class="control-label col-md-3">${lblbatchno}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <form:input path="batchNo"  class="form-control" required="required"  />
                                       </div>
                                       </div>
                                   
                                    
                          			 <div class="form-group">
                                       <label class="control-label col-md-3">${lblexpirydate}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                        <div class="input-group date form_datepicker_expdate"  >
	                                      <form:input path="expiryDate"  class="form-control" id="expdate" name="expdate"   required="required" />
	                                        <span class="input-group-btn">
												<button class="btn default date-set" type="button"><i class="icon-calendar"></i>
												</button>
											</span>
	                                        </div>
                                        
                                       </div>
                                       </div>
                                    
                                       
                                     
                          			   <div class="form-group">
                                       <label class="control-label col-md-3">${lblunits}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                        <form:input path="units"  class="form-control"    required="required" />
                                       </div>
                                       </div>
                                    
                                     
                                   
                          			   <div class="form-group">
                                       <label class="control-label col-md-3">${lblcost}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                        <form:input path="cost"  class="form-control"    required="required" />
                                       </div>
                                       </div>
                                    
                                     <div class="form-group">
                                       <label class="control-label col-md-3">${lbltax}<span class="required">*</span></label>
                                       <div class="col-md-4">
                                        <form:input path="tax"  class="form-control"    required="required" />
                                       </div>
                                       </div>
                                     
                                       <div class="form-group">
                                    	<label class="control-label col-md-3">${lblsupplier}<span class="required">*</span></label>
 									      <div class="col-md-4">
 									        <form:select path="supplier" class="form-control" id="supplier" required="required">
 									          <form:option value="">--select--</form:option>
 									           <c:forEach items="${supplier}" var="supplier">
 									            <form:option value="${supplier.id}"><spring:message code="${supplier.name}"/></form:option>
 									           </c:forEach>
 									         </form:select>
 									      </div>
 									       <a href="${pageContext.request.contextPath}/supplier/createsupplier.htm" data-rel="tooltip" title="${lblmedicinepackaging}"><font color="blue"></font></a>
 									       	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create Supplier</button>
 									   </div>
 								 
                                   
                           
                        <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="submit" class="btn green" >${lblsave}</button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default">${lblcancel}</button>                              
                           </div>
                        </div>
                     </form:form>
                     
                     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title" id="myModalLabel">SUPPLIER</h4>
				      </div>
				      
				      
				      <div class="modal-body">
				          <form action="save.htm"  id="form_pop_1" class="form-horizontal" >
				            <div class="scroller" style="height:400px" data-always-visible="1" data-rail-visible1="1">
				            <div class="row">
                        
                            <div class="wizard_body clearfix">
                                <fieldset class="wizard-step mws-form-inline">
                             
                                <div class="col-md-6">	
                                <div class="form-group">
                                       <label class="control-label col-md-4"><spring:message code="label.name"/><span class="required">*</span></label>
                                       <div class="col-md-8">
                                          <input name="psupname" id="psupname" class="form-control"   />
                                       </div>
                                       </div>
                                       
                    <div class="form-group">
                                       <label class="control-label col-md-4"><spring:message code="label.company"/><span class="required">*</span></label>
                                       <div class="col-md-8">
                                          <input name="psupcompany" id="psupcompany" class="form-control"  />
                                       </div>
                                       </div> 
                   
                      <div class="form-group">
                                       <label class="control-label col-md-4"><spring:message code="label.phoneno"/><span class="required">*</span></label>
                                       <div class="col-md-8">
                                          <input name="psupphoneNo" id="psupphoneNo" class="form-control"  maxlength="10"/>
                                       </div>
                                       </div> 
                   
                      <div class="form-group">
                                       <label class="control-label col-md-4"><spring:message code="label.mailId"/><span class="required">*</span></label>
                                       <div class="col-md-8">
                                          <input name="psupmailId" id="psupmailId" class="form-control" onblur="emailcheck()" />
                                       </div>
                                       </div>  
                               <div class="form-actions fluid">
                           <div class="col-md-offset-3 col-md-9">
                              <button type="button" id="btn"   class="btn default">${lblsave}</button>                              
                           </div>
                        </div> 
                                
                                
                                
                                
                            </div>
						</fieldset>
                 </div>
                <hr />
              </div>
              </div>
              </form>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div> 
                     <!-- END FORM-->
                  </div>
               </div>
               <!-- END VALIDATION STATES-->
            </div>
         </div>
  
  

<script>
 $(document).ready(function(){
	         
	        	    $('#btn').click(function() {
	        	    	var msg="";
	        	    	if($('#psupname').val()==""){
	        	    		msg="Please enter name";
	        	    	}
	        	    	 if($('#psupcompany').val()==""){
	        	    		msg=msg+" Please enter company";
	        	    	} 
	        	    	 if($('#psupphoneNo').val()==""){
	        	    			msg=msg+" Please enter phone no";
	        	    		} 
	        	    	 if($('#psupmailId').val()==""){
	        	    			msg=msg+" Please enter email";
	        	    		} 
	        	    		
	        	    	 if(msg=="")
	        	    		 {
	        	    		   var supname=$('#psupname').val();
	        	    			var supcompany=$('#psupcompany').val();
	        	    			var supphoneNo=$('#psupphoneNo').val();
	        	    			var supmailId=$('#psupmailId').val();
	        	    		 		 $.get("getsupplier.htm?sname="+ supname+"&scomp="+supcompany+"&sphno="+supphoneNo+"&smail="+supmailId,function(data) {
	        	    				 $('select#supplier').html('<option value="">-- Select --</option>');
	        	    		         $.each(data,function(index) {
	        	    		          $('select#supplier').append('<option value="'+data[index].id+'">'+ data[index].name+ '</option>');
	        	    		         });
	        	    		                   
	        	    		       });
	        	    		 		 $("#psupname").val('');
	        	    				 $("#psupcompany").val('');
	        	    				 $("#psupphoneNo").val('');
	        	    				 $("#psupmailId").val('');	 
	        	    		 $('#myModal').modal('hide');
	        	    		 }
	        	    		 
	        	    	 else{
	        	    		 alert(msg);
	        	    	 }
	        	     
	        	    });
	          
	$(".form_datepicker_expdate").datetimepicker({         
	    isRTL: App.isRTL(),
	             format: "yyyy-mm-dd",
	             weekStart: 1,
	             todayBtn:  1,
	       autoclose: 1,
	       todayHighlight: 1,
	       startView: 4,
	       minView: 2,
	       startDate:new Date(),
	       forceParse: 0,
	       pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")          
	        });
	  });

</script>



