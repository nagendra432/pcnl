package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;


public enum DeliveryStatus
{
	PENDING("PENDING"), PREPARED("PREPARED"), DELIVERED("DELIVERED"), FAILED("FAILED");

	private String type;
	
	private DeliveryStatus(String type) {
		this.type = type;
	}

	public static List<DeliveryStatus> list() 
	{
		return Arrays.asList(values());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	
}
