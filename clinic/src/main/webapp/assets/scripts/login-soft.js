var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	            	userName: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	            	userName: {
	                    required: "Username is required1."
	                },
	                password: {
	                    required: "Password is required2."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-error', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	            	userName: {
	                    required: true
	                }
	            },

	            messages: {
	            	userName: {
	                    required: "Username is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	               /* form.submit();*/
	            	
	            	var postVars  = $(form).serialize();
					$.post('forgotpassword/securityquestion.htm', postVars, function(data) { 
						console.log(data);
						if(data.succes){
							 $('.forget-form').hide();
							 $('.security-form').show();
						///$(location).attr('href', data.msg);
						}else{
							var $forgetform = $('.forget-form'),
							    userName = $forgetform.find('input[name="userName"]'),
							    $formgroup = $forgetform.find('div.form-group');
							
								userName.val('');
								$formgroup.addClass('has-error');
								$formgroup.append('<span for="userName" class="help-block">'+data.error+'</span>');
							  
						}
					});
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}
	
	
	var handleSecurityQuestions = function () {
		$('.security-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	            	securityQuestions0: {
	                    required: true
	                },
	                securityQuestions1: {
	                    required: true
	                }
	            },

	            messages: {
	            	securityQuestions0: {
	                    required: "Security Question1 is required."
	                },
	                securityQuestions1: {
	                    required: "Security Question2 is required."
	                }
	            	
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	               /* form.submit();*/
	            	
	            	var postVars  = $(form).serialize();
					$.post('forgotpassword/sendpassword.htm', postVars, function(data) { 
						//console.log(data);
						if(data.msg){
						$(location).attr('href', data.msg);
						}else{
							var $securityform = $('.security-form'),
							securityQuestions0 = $securityform.find('input[name="securityQuestions0"]'),
							securityQuestions1 = $securityform.find('input[name="securityQuestions1"]'),
						    $formgroup = $securityform.find('div.form-group');
						    
						    securityQuestions0.val('');
						    securityQuestions1.val('');
						    
						    $formgroup.addClass('has-error');
						    $formgroup.append('<span class="help-block">'+data.error+'</span>');
						  
							  
						}
					});
	            }
	        });

	        $('.security-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.security-form').validate().form()) {
	                    $('.security-form').submit();
	                }
	                return false;
	            }
	        });

	        /*jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });*/

	        jQuery('#back-btn1').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.security-form').hide();
	        });

	}

	
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleForgetPassword();
            handleSecurityQuestions();
          
	       	$.backstretch([
		        "./assets/img/bg/robotic-surgery.png",
		        "./assets/img/bg/banner.png",
		        "./assets/img/bg/Lobby.jpg",
		        "./assets/img/bg/ot.jpg",
		        "./assets/img/bg/1.jpg",
		        "./assets/img/bg/6.jpg",
		        "./assets/img/bg/7.jpg"
		        ], {
		          fade: 1000,
		          duration: 8000
		    });
        }

    };

}();