package com.seef.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.base.Status;
import com.seef.models.pharmacy.Billing;
import com.seef.models.pharmacy.Prescription;
import com.seef.service.BillingService;

@Service
public class BillingServiceImpl implements BillingService {
	
	private static final Logger _log = LoggerFactory.getLogger(BillingServiceImpl.class);

	@Autowired
	GenericDAO genericDAO;
	
	@Override
	@Transactional
	public void saveBill(Billing billing, ApplicationUser applicationUser) {
		// TODO Auto-generated method stub
		billing.setStatus(Status.ACTIVE);
		billing.setCreatedBy(applicationUser);
		billing.setCreatedOn(Calendar.getInstance().getTime());
	   
	    List<Prescription> prescriptions= billing.getPrescriptions();
	    List<Prescription> prescriptions1=new ArrayList<Prescription>();
	    for(Prescription prescription:prescriptions){
	    	System.out.println("medicine id.............."+prescription.getMedicine());
	    	System.out.println("medicine id.............."+prescription.getMedicine().getId());
	    	prescription.setStatus(Status.ACTIVE);
	    	prescription.setCreatedBy(applicationUser);
	    	prescription.setCreatedOn(Calendar.getInstance().getTime());
	    	prescriptions1.add(prescription);
	    }
	    billing.setPrescriptions(prescriptions1);
		genericDAO.save(billing);
		_log.info(">>Save Successfully>>");
	}

	@Override
	public long getMaxBillno() {
		// TODO Auto-generated method stub
		Map<String,Object> criteria=new HashMap<String,Object>();
		
		Long billno=genericDAO.executeQuerySinglequery("findmaxbillno", criteria);
		if(billno==null){
			billno=10000L;
		}
		else{
			billno=Long.valueOf(billno).longValue()+1;
			
		}
		
		
		return billno; 
	}

	@Override
	public Billing getBill(Long billno) {
		// TODO Auto-generated method stub
		Map<String, Object> criteria=new HashMap<String, Object>();
		criteria.put("billNo", billno);
		return genericDAO.getByCriteria(Billing.class, criteria);
	}
	

}
