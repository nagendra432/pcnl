package com.seef.service;

import java.util.List;
import java.util.Map;

import com.seef.models.ApplicationUser;
import com.seef.models.doctor.DoctorSchedule;

public interface DoctorScheduleService {
	public void save(DoctorSchedule doctorSchedule,ApplicationUser applicationUser);
	public void saveOrUpdate(DoctorSchedule doctorSchedule,ApplicationUser applicationUser);
	public List<DoctorSchedule> doctorschedule_list();
	public List<DoctorSchedule> schedule_byDate(Map<String,Object> criteria);
	public DoctorSchedule getById(Long id);

}
