package com.seef.models.core.dto;

import java.io.Serializable;

public class SurgeryDto implements Serializable{

	private static final long serialVersionUID = 8851983869168095701L;
	private Long id;
	private String name;
	private String discription;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

}
