package com.seef.service;

import java.util.List;

import com.seef.models.ApplicationUser;
import com.seef.models.SystemUser;
import com.seef.models.enums.UserType;

public interface SystemUserService {

	ApplicationUser findByUserName(String username);
	public SystemUser getSystemUserByUID(String uid);
	public SystemUser getSystemUserById(Long id);
	public List<SystemUser> findSystemUserByCreator(Long createdById);
	public SystemUser saveOrUpdate(SystemUser applicationUser);
	public String findUserStatus(Long userId);
	public List<SystemUser> findByUserType(UserType surgeon);
}
