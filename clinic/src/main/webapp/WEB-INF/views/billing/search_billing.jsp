<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			<spring:message code="header.bill.view" />
			
		</h3>
	</div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
            <div class="col-md-12">
               <!-- BEGIN VALIDATION STATES-->
               <div class="portlet box blue">
</div>
 <div class="portlet-body form">
                     <!-- BEGIN FORM-->

<form:form action="view.htm" name="appointmentForm" class="form-horizontal"  method="get" id="insert">
			<div style="color: #ff0000;">
				<span  class="errorMessage">${errorheader}</span>
				
			</div>
			  <div class="form-body">
			  </div>
			              <div class="form-group">
                            <label class="control-label col-md-3"><spring:message code="label.searchBy.billno"/><span class="required">*</span></label>	
                             <div class="col-md-4" class="form-control" >
                             <input type="text" name="txtbillno"  required="required">
                               <button type="submit" class="btn green"><spring:message code="button.search"/></button>      
                             </div>
                             </div>
                           
                           </form:form>
                           </div>
                           </div>
                 
                           </div>
                 