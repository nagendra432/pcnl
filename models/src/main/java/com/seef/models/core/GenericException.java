package com.seef.models.core;

import java.util.ArrayList;
import java.util.List;

import com.seef.models.core.dto.Error;


public class GenericException extends RuntimeException {

	private static final long serialVersionUID = -3978405394478986567L;
	
	public List<Error> errors;
	private String message;
	
	public GenericException(){
		
	}
	
	public GenericException(String errorCode){
		Error error=new Error();
		error.setCode(errorCode);
		addError(error);
		this.message = errorCode;
	}
	
	public GenericException(List<Error> errors,Throwable throwable){
		super(throwable);
		this.errors=errors;
	}
	
	public GenericException(Error error,Throwable throwable){
		super(throwable);
		addError(error);
	}



	private void addError(Error error) {
		if(null == this.errors){
			this.errors=new ArrayList<Error>();
		}
		this.errors.add(error);
	}

	public List<Error> getErrors() {
		return errors;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getFirstErrorCode(){
		String errorCode="";
		if(null != this.errors  && this.errors.size()>0){
			errorCode=this.errors.get(0).getCode();
		}
		return errorCode;
	}

}
