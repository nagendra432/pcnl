package com.seef.web.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seef.dao.util.CollectionUtil;
import com.seef.models.Appointment;
import com.seef.models.OutPatient;
import com.seef.models.base.Status;
import com.seef.models.doctor.Doctor;
import com.seef.models.doctor.DoctorSchedule;
import com.seef.models.enums.CounsultationType;
import com.seef.service.AppointmentService;
import com.seef.service.BaseService;
import com.seef.service.OutPatientService;
import com.seef.web.editors.AbstractModelEditor;




@Controller
@RequestMapping("/appointment")
public class AppointmentController extends CRUDController<Appointment> {
	
	private static final Logger _log = LoggerFactory.getLogger(AppointmentController.class);

	@Autowired 
	AppointmentService appointmentService;
	
		
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	OutPatientService outPatientService;
	
	
	@Override
	@RequestMapping(value="/create.htm",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request)
	{
		Long id = Long.valueOf(request.getParameter("id"));
		OutPatient outpatient=outPatientService.getById(id);
		Calendar cal1 = new GregorianCalendar();
		cal1.setTime(outpatient.getDob());
		Date dob=outpatient.getDob();
		int year1 = Calendar.getInstance().get(Calendar.YEAR);
		int year2 =cal1.get(Calendar.YEAR);
		int age = year1 - year2;
		Appointment appointment=new Appointment();
		appointment.setOutPatient(outpatient);
		appointment.setAge(age);
		
		setupCreate(modelMap,request);
		
		modelMap.addAttribute("model",appointment);
		return "tileDefinition.create-"+getNameSpace();
		
		
	};
	
	@Override
	@RequestMapping(value="/save.htm",method=RequestMethod.POST)
	public String save(@ModelAttribute("model") Appointment model, HttpServletRequest request, HttpServletResponse response, BindingResult bindingResult, ModelMap modelMap)
	{ 
		if(model.getId() == null){	
			appointmentService.saveAppointment(model, getLoggedInUser());
		 }	
    else{	
    	appointmentService.saveOrUpdateAppointment(model, getLoggedInUser());
    }
		 return getRedirectURL("/manage.htm?id="+request.getSession().getAttribute("outptid"));
	};
	
	@Override
	@RequestMapping(value="/manage.htm", method=RequestMethod.GET)
	public String manage(ModelMap modelMap, HttpServletRequest request) {
		Locale userLocale = new Locale(getLoggedInUser().getPreferences().getLanguage().getCode().split("_")[0]);
		request.getSession().setAttribute("nav", messageSource.getMessage("masters.menu.name", null, userLocale));
		request.getSession().setAttribute("innerNav", messageSource.getMessage("manage.outpatient.menu.name", null, userLocale));
		Long outptid = Long.valueOf(request.getParameter("id"));
		request.getSession().setAttribute("outptid", outptid);
		modelMap.addAttribute("modelList", appointmentService.Appointment_list(CollectionUtil.toMap(new String[]{"outpatient"}, new Object[]{outptid})));
		return "tileDefinition.manage-"+getNameSpace();
	};
	
	
	@RequestMapping(value="/close.htm", method=RequestMethod.GET)
	public String close(ModelMap modelMap, HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		Appointment appointment=appointmentService.getById(id);
		appointment.setStatus(Status.ACTIVE);
		appointmentService.saveOrUpdateAppointment(appointment, getLoggedInUser());
		
		return getRedirectURL("/manage.htm?id="+request.getSession().getAttribute("outptid"));
	};
	
	@RequestMapping(value="/rejected.htm", method=RequestMethod.GET)
	public String rejected(ModelMap modelMap, HttpServletRequest request) {
		Long id = Long.valueOf(request.getParameter("id"));
		Appointment appointment=appointmentService.getById(id);
		appointment.setStatus(Status.INACTIVE);
		appointmentService.saveOrUpdateAppointment(appointment, getLoggedInUser());
		System.out.print("......reached close");
		return getRedirectURL("/manage.htm?id="+request.getSession().getAttribute("outptid"));
	};
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/getdoctors", method=RequestMethod.GET)
	public void getChapters(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		PrintWriter out=response.getWriter();
		String date= request.getParameter("date");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	    Map<String,Object> criteria=new HashMap<String,Object>();
		
		try {
			_log.info(">>Date:"+dateFormatter.parse(date));
			criteria.put("scheduledDate",dateFormatter.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<DoctorSchedule> doctorslist = appointmentService.Doctors_listbyDate(criteria);
		_log.info(">>doctor size:"+doctorslist.size());
		criteria.clear();
		List doctors = new ArrayList();
		 Map medicineMap = null;
		for(DoctorSchedule section : doctorslist){
			criteria.put("doctor",section.getDoctor());
			try {
				criteria.put("appointmentDate",dateFormatter.parse(date));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<Appointment> appointments=genericDAO.findByCriteria(Appointment.class, criteria);
			
			if((Integer.parseInt(section.getNoOfAppointments())-appointments.size())!=0){
				medicineMap = new HashMap(); 
				String name=section.getDoctor().getName()+"-"+section.getDoctor().getSpecilization()+"( "+section.getInTime()+" - "+section.getOutTime()+" )";
				medicineMap.put("id", section.getDoctor().getId());
				medicineMap.put("name", name);
				doctors.add(medicineMap);
			}
		}
		      JSONArray jsonArray = new JSONArray(doctors);
		      
		      out.print(jsonArray);
	}
	
	
	
	@Override
	public void setupCreate(ModelMap modelMap,HttpServletRequest request)
	{
		
	    modelMap.addAttribute("counsultationType", CounsultationType.list());
	    modelMap.addAttribute("doctors", genericDAO.findAll(Doctor.class));
	    
	}
	
	@Override
	public boolean validateUpdate(Appointment model, BindingResult result) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateCreate(Appointment entity,
			BindingResult bindingResult) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BaseService<Appointment, Long> getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "appointment";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Doctor.class, new AbstractModelEditor(Doctor.class));
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	

}
