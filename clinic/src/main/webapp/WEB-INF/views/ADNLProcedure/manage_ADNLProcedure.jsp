<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="datatables"
	uri="http://github.com/dandelion/datatables"%>


<spring:message code="label.view" var="lblView"/>
<spring:message code="label.edit" var="lblEdit"/>
<spring:message code="label.action" var="lblaction"/>
<spring:message code="label.discription" var="lbldis"/>
<spring:message code="label.name" var="lblname"/>
<spring:message code="label.phneno" var="lblphneno"/>
<spring:message code="label.viewtakeappointment" var="lblviewappointment"/>

<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			
		</h3>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->

<div class="row">
	<div class="col-md-12">

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-edit"></i>
					<spring:message code="label.resultsfor" />
					
					<spring:message code="label.ADNLProcedure" />
				
					
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					 <a href="javascript:;"
						class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="col-lg-2 pull-right">
					<div class="table-toolbar pull-right">
						 <div class="btn-group">
		                <a href="create.htm"><button id="sample_editable_1_new" class="btn green"><spring:message code="button.addnew"/><i class="icon-plus"></i> </button></a>
		              </div>

					</div>
				</div>
				<datatables:table id="sample_editable_1" data="${modelList}" cdn="false" row="outpatient" cssClass="table table-striped table-bordered table-hover">
					<datatables:column title="${lblname}" property="name" />
					<datatables:column title="${lbldis}" property="discription"
						cssClass="hidden-480" cssCellClass="hidden-480" />	
				  <%--  <datatables:column title="${lblphneno}" property="phoneNo" /> --%>
					<%-- <datatables:column title="${lblaction}" sortable="false">
						<div class='hidden-phone visible-desktop btn-group'>
							<a href="view.htm?id=${outpatient.id }" data-rel="tooltip"
								title="${lblView}"><i
								class="btn btn-default btn-sm my-table_btn icon-eye-open"></i></a> <a
								href="edit.htm?id=${outpatient.id}" data-rel="tooltip"
								title="${lblEdit}"><i
								class="btn btn-default btn-sm my-table_btn icon-edit"></i></a>
								<a href="${pageContext.request.contextPath}/appointment/manage.htm?id=${outpatient.id}" data-rel="tooltip"  style="color:red" title="${lblviewappointment}">
								<i class="btn btn-default btn-sm my-table_btn icon-book"></i></a> 
						</div>

					</datatables:column> --%>
				</datatables:table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT -->