package com.seef.models.audit;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.seef.models.ApplicationUser;
import com.seef.models.base.AbstractDomainObject;


@Entity
@Table(name = "AUDIT_LOG")
@NamedQueries({
	@NamedQuery(name = "AuditLog.findlogs", query = "Select audit from AuditLog audit where audit.institutionId=:institutionId and CONVERT(VARCHAR(11),audit.createdAt,106) =CONVERT(VARCHAR(11),:createdAt,106)"),
	@NamedQuery(name = "AuditLog.findlogsDate", query = "Select audit from AuditLog audit  where audit.institutionId = :institutionId and CONVERT(VARCHAR(11),audit.createdAt,101)  between :fromDate and :toDate"),
	@NamedQuery(name = "AuditLog.findlogsentityclass", query = "Select audit from AuditLog audit where audit.institutionId=:institutionId and CONVERT(VARCHAR(11),audit.createdAt,101)  between :fromDate and :toDate and audit.entityClass LIKE :entityClass"),
	@NamedQuery(name = "AuditLogSetupLevel.findlogs", query = "Select audit from AuditLog audit where audit.institutionId is NULL and CONVERT(VARCHAR(11),audit.createdAt,106) =CONVERT(VARCHAR(11),:createdAt,106)"),	
	@NamedQuery(name = "AuditLogSetupLevel.findlogsDate", query = "Select audit from AuditLog audit where audit.institutionId is NULL and CONVERT(VARCHAR(11),audit.createdAt,101)  between :fromDate and :toDate")
})
public class AuditLog extends AbstractDomainObject {

	private static final long serialVersionUID = 269168041517643087L;
	
	private Long id;
	
	private Long entityId;
	
	private String entityClass;
	
	private String reference;
	
	private Date createdAt;
	
	private Long institutionId;
	
	private String message;
	
	private String userId;
	
	private transient Object object;
	
	private transient ApplicationUser applicationUser;

	public AuditLog() {
		
	}
	
	public AuditLog(String message, Long entityId, String entityClass,String userId) {
		this.message = message;
		this.entityId = entityId;
		this.entityClass = entityClass;
		this.userId = userId;
		this.setCreatedAt(new Date());
	}
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "AUDIT_LOG_SEQ")
	@SequenceGenerator(name = "AUDIT_LOG_SEQ", sequenceName = "AUDIT_LOG_SEQ", allocationSize = 1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id=id;
	}

	@Column(name = "ENTITY_CLASS")
	public String getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(String entityClass) {
		this.entityClass = entityClass;
	}

	@Lob
	@Column(name = "MESSAGE", nullable = false, updatable = false)
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Column(name = "USER_ID", updatable = false)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Transient
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	@Column(name = "REFERENCE")
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name="INSTITUTION_ID")
	public Long getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Long institutionId) {
		this.institutionId = institutionId;
	}

	@Transient
	public ApplicationUser getApplicationUser() {
		return applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	@Column(name = "ENTITY_ID")
	public Long getEntityId() {
		return entityId;
	}
	
	public final void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
}