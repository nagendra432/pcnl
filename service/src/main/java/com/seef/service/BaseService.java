package com.seef.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.seef.models.base.AbstractDomainObject;

public interface BaseService<T extends AbstractDomainObject, ID extends Serializable> {

	/** 
	 * Saves a given entity. Use the returned instance for further operations as
	 * the save operation might have changed the entity instance completely.
	 * 
	 * @param entity
	 * @return the saved entity
	 */
	T save(T entity);

	/**
	 * Retrives an entity by its primary key.
	 * 
	 * @param id
	 * @return the entity with the given primary key or {@code null} if none
	 *         found
	 * @throws IllegalArgumentException
	 *             if primaryKey is {@code null}
	 */
	T update(T entity);


	void delete(ID id);

	List<T> findAll();

	T find(ID id);

	T getReference(ID id);

	void flush(); 


	public T getByCriteria(Class<T> clazz, Map<String, Object> criterias);

	public List<T> findByCriteria(Class<T> clazz, Map<String, Object> criterias, String sortField, boolean desc);

	public List<T> findByCriteria(Class<T> clazz, Map<String, Object> criterias);
	
	public T getByQuery(String jpaQuery);
	
	public <K extends AbstractDomainObject> List<K> findAll(String queryName, Class<K> clazz, Map<String, Object> params);
	
	public Object find(@SuppressWarnings("rawtypes") Class clazz, final ID id ); 
}
