<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<spring:message code="label.billno" var="lblbillno"/>
<spring:message code="label.name" var="lblpatientname"/>
<spring:message code="label.medicinename" var="lblmedicinename"/>
<spring:message code="label.batchno" var="lblbatchno"/>
<spring:message code="label.units" var="lblunit"/>
<spring:message code="label.availableunits" var="lblavailableunit"/>
<spring:message code="label.save.invoice" var="lblsubmit"/>
<spring:message code="label.cancel" var="lblcancel"/>


<script type="text/javascript">

var requiredMessage="${minMaxRequired}";
var numberMessage="${notValidNumber}";
var alphabetMessage="${gradeval}";

$(document).ready(function() {
	var currentRow=$("#currentRowCount").val();
	
	
	$('#form_sample_1').validate();


	$('#btnAdd').click(function() {
		
		$('#form_sample_1').validate();
	});
});

function removeRow(rowId){
	
	var cost=$("#prescriptionList"+rowId+"cost").val();
	$('#total').val(parseFloat($('#total').val())-parseFloat(cost)); 
	$('#gradRow'+rowId).remove();
}

function addRow(){
	var currentRow=$("#currentRowCount").val();
	$("#gradTable > tbody").append(buildRow(currentRow));
	//addRule(currentRow);
	$("#currentRowCount").val(++currentRow);
}

function checkRowsCount(){
	var currentRow=$("#currentRowCount").val();

	if($('#total').val() =='' || parseFloat($('#total').val())== 0)
		{
		alert("enter atleast one medicine details");
		return false;
		}
	
	 return true;
}


/* function addRule(rowCount){
	
	$( "#prescriptionList"+rowCount+"name" ).rules( "add", {
		number: true,
		required :true,
		negative:true,
		messages:{
			required : requiredMessage,
	        number : numberMessage
		}
});
	
	$( "#prescriptionList"+rowCount+"outbound" ).rules( "add", {
		number: true,
		required :true,
		greater:true,
		negative2:true,
		messages:{
			required : requiredMessage,
	        number : numberMessage
		}
	});
	
	
	$( "#prescriptionList"+rowCount+"grade" ).rules( "add", {
		alphabet :true,
		required :true,
		max :1,
		messages:{
			required : requiredMessage,
			alphabet : alphabetMessage
		}
	});
	
	$( "#prescriptionList"+rowCount+"gradePoint" ).rules( "add", {
		number: true,
		required :true,
		negative3:true,
		messages:{
			required : requiredMessage,
	        number : numberMessage
		}
	});
	
	
	$.validator.addMethod("negative", function(value, element) {
		var name= $("#prescriptionList"+rowCount+"name").val();
        return name>0 ;
	},"<spring:message code='label.error.negativeValue'/>");
	
	$.validator.addMethod("negative2", function(value, element) {
		var outbound= $("#prescriptionList"+rowCount+"outbound").val();
        return outbound>0 ;
	},"<spring:message code='label.error.negativeValue'/>");
	
	$.validator.addMethod("negative3", function(value, element) {
		var gradePoint= $("#prescriptionList"+rowCount+"gradePoint").val();
        return gradePoint>0 ;
	},"<spring:message code='label.error.negativeValue'/>");
	
	$.validator.addMethod("greater", function(value, element) {
		var name=parseInt($("#prescriptionList"+rowCount+"name").val());
		var outbound=parseInt($("#prescriptionList"+rowCount+"outbound").val());
        return name>outbound ;
	},"<spring:message code='label.error.greater'/>");

}
 */

function buildRow(rowCount){
	<spring:message code='button.remove' var="removeButton"/>
	var medicinename=$("#medicine option:selected").val();
	var batchno=$("#medicinepkg option:selected").text();
	var units=$('#units').val();
	var unitcostid=$("#medicinepkg option:selected").val();
	var arr = unitcostid.split('-');
	var pkgid=arr[0];
	var unitcost=arr[1];
	var cost=units*unitcost;
	if(medicinename != '' && batchno != '' && units != '')
		{
		if($('#total').val() =='')
			$('#total').val(cost);
		else
				$('#total').val(parseFloat($('#total').val())+cost); 
		$('#medicine').val('');
		$('#units').val('');
		$("#availableunits").val('');
		$("#medicinepkg").val('');
	       return '<tr id="gradRow'+rowCount+'"><td><input readonly="true"  type="text" name="prescriptions['+rowCount+'].medicine.id" class="form-control" id="grades'+rowCount+'name" value='+medicinename+' /></td><td><input type="text" readonly="true" class="form-control" name="prescriptions['+rowCount+'].medicinePackagingDetails.batchno" id="grades'+rowCount+'outbound" value='+batchno+' /></td><td><input readonly="true" type="text" class="form-control" name="prescriptions['+rowCount+'].units" id="grades'+rowCount+'grade" value='+units+' /></td><td><input readonly="true" type="text" class="form-control" name="prescriptions['+rowCount+'].unitcost" id="grades'+rowCount+'grade" value='+unitcost+' /></td><td><input readonly="true" type="text" class="form-control" name="prescriptions['+rowCount+'].cost" id="prescriptionList'+rowCount+'cost" value='+cost+' /></td><td class="icon-column"><input type="hidden" name="prescriptions['+rowCount+'].medicinePackagingDetails.id" value='+pkgid+' /><td class="icon-column"><input type="hidden" name="prescriptionList['+rowCount+'].remove" value="0" /><button class="btn default" type="button" onclick="javaScript:removeRow('+rowCount+')">${removeButton}</button></td></tr>';
        }
	   else{
		 alert("fill all the details");
	 }
}



</script>

 <form:form action="save.htm"  id="form_sample_1" class="form-horizontal" commandName="model" onsubmit="return checkRowsCount()">
 <table>
	 <tr>
	 <td colspan="8">
	 <table><tr>
		 <td><label >${lblbillno}</label></td>
		 <td > <form:input path="billNo"  class="form-control required"  readonly="true" /></td> 
		<td style="width:50px;"></td>
	    <td style="margin-left:5%;">  <label style="text-align:center;">${lblpatientname}<span class="required"  style="color: red">*</span></label></td>
		 <td class="form-group" > <form:input path="name"   class="form-control"   required="required" />   </td>
		 </tr>
		 </table></td>
	 </tr>
	 <tr style="height:20px;"><td></td></tr>
	 <tr >
	       <td>
	         <label >${lblmedicinename}<span class="required"  style="color: red">*</span></label></td>
	         
		 <td>
	         <select name="medicine" id="medicine" style="width: 100px" class="form-control input-large select2me"  >
	         <option value="">--Select--</option>
	           <c:forEach items="${medicinelist}" var="grad">
	           <option value="${grad.id}">${grad.medicineName}</option>
	           </c:forEach>
	         </select>
	       </td>
	      
	       <td width="30px"></td>
	       <td>
	       <label >${lblbatchno}<span class="required" style="color: red">*</span></label></td>
		 <td>
	        <select  id="medicinepkg" name="medicinepkg" >
	          <c:forEach items="${batches}" var="batch">
	           <option value="${batch.cost}">${batch.name}</option>
	           </c:forEach>
	         </select>
	       </td>
	       <td style="width:30px;"></td>
	        <td>
	       <label >${lblavailableunit}<span class="required"  style="color: red">*</span></label></td>
	       
	       <td>
	         <input type="text" readonly="readonly"  id="availableunits" name="availableunits"/>
	       </td>
	       <td style="width:30px;"></td>
	       <td>
	       <label >${lblunit}<span class="required"  style="color: red">*</span></label></td>
	       
		 <td>
	       <input  type="text"  id="units" name="units"/>
	       </td>
	        <td style="width:20px;"></td>
	       <td>
	        <button class="btn green" type="button" id="btnAdd" onclick="javaScript:addRow()">Add</button> 
	       </td>
     </tr>
 </table>
 
    <table class="table" id="gradTable" style="margin-top:20px;">
                                            	<thead>
                                                    <tr>
                                                        <th><spring:message code="label.medicinename"/></th>
                                                        <th><spring:message code="label.stock.batchno"/></th>
                                                        <th><spring:message code="label.units"/></th>
                                                        <th><spring:message code="label.unitcosts"/></th>
                                                         <th><spring:message code="label.cost"/></th>
                                                    </tr>
                                                </thead>
												 <tfoot>
												  <tr>
												  <td colspan="3"></td>
												    <td><spring:message code="label.total"/></td>
												    <td> <input type="text" id="total" readonly="readonly" name="total"/></td>
												  </tr>
												</tfoot>
                                                <tbody>
                                                <c:set var="rowCount" value="0"></c:set>
                                        
                                                    <input type="hidden" id="currentRowCount" value="${rowCount}" />
                                                </tbody>
                                            </table>
             <div class="form-actions fluid">
                           <div class="col-md-offset-5 col-md-9">
                              <button type="submit"  class="btn green">${lblsubmit}</button>
                              <button type="button" onclick="javascript:history.go(-1);" class="btn default">${lblcancel}</button>                              
                           </div>
                        </div>     
                                                      
 
 </form:form>
 
 <script type="text/javascript">
      jQuery(document).ready(function() { 
    	  FormComponents.init(); 
         FormValidation.init();
         $("select#medicine").change(function() {
         var str = $(this).val();
         $.get("getpackaging.htm?medicineid="+ str,function(data) {
            $('select#medicinepkg').html('<option value="">-- Select --</option>');
          $.each(data,function(index) {
           $('select#medicinepkg').append('<option value="'+data[index].id+'-'+data[index].cost+'-'+data[index].units+'">'+ data[index].name+ '</option>');
          });
                      
          });
         });
         
         $("select#medicinepkg").change(function() {
        	 var avbunits=$("#medicinepkg option:selected").val();
        		var arr = avbunits.split('-');
        		var units=arr[2];
        	$("#availableunits").val(units);
         
         });
         $('#units').blur(function() {
        	 if(parseFloat($("#units").val()) > parseFloat($("#availableunits").val()))
        		 {
        		 alert("please enter valid units");
        	 $("#units").val("");
        		 }
        	});
         
             });
      </script>
 
 