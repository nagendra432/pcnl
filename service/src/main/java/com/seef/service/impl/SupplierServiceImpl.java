package com.seef.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.seef.dao.GenericDAO;
import com.seef.models.ApplicationUser;
import com.seef.models.base.Status;
import com.seef.models.pharmacy.Medicine;
import com.seef.models.pharmacy.Supplier;
import com.seef.service.SupplierService;



@Service
public class SupplierServiceImpl implements SupplierService{
	
	private static final Logger _log = LoggerFactory.getLogger(SupplierServiceImpl.class);
	

	@Autowired
	GenericDAO genericDAO;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void save(Supplier supplier, ApplicationUser applicationuser) {
		supplier.setStatus(Status.ACTIVE);
		supplier.setCreatedBy(applicationuser);
		supplier.setCreatedOn(Calendar.getInstance().getTime());
		genericDAO.save(supplier);
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveorupdate(Supplier supplier, ApplicationUser applicationuser) {
		supplier.setModifiedBy(applicationuser);
		supplier.setModifiedOn(Calendar.getInstance().getTime());
		genericDAO.saveOrUpdate(supplier);
	
		
	}

	@Override
	public Supplier getById(Long id) {
		return genericDAO.getById(Supplier.class,id);
	}

	@Override
	public List<Supplier> supplier_list() {
				Map<String,Object> params=new HashMap<String,Object>();
				params.put("status", Status.ACTIVE);
				return genericDAO.findAllByCriteria(Supplier.class, params);
	}

}
