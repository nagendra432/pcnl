package com.seef.web.controllers;

import java.lang.reflect.ParameterizedType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.seef.dao.GenericDAO;
import com.seef.models.base.AbstractDomainObject;
import com.seef.models.enums.UserStatus;
import com.seef.service.BaseService;

@SuppressWarnings("unchecked")
public abstract class CRUDController<T extends AbstractDomainObject> extends PostLoginBaseController {

	@Autowired
	GenericDAO genericDAO;

	private static final Logger _log = LoggerFactory.getLogger(CRUDController.class);

	
	@Autowired
	protected Validator validator;


	public void setupCreate(ModelMap modelMap, HttpServletRequest request) {
		//Default is no implementation
	}

	public void setupUpdate(T model, ModelMap modelMap , HttpServletRequest request) {
		setupCreate(modelMap, request);
	}

	public void setupList(ModelMap modelMap, HttpServletRequest request) {
		modelMap.addAttribute("UserStatus", UserStatus.list());
	}

	@RequestMapping(value="/create",method=RequestMethod.GET)
	public String create(ModelMap modelMap, HttpServletRequest request){
		setupCreate(modelMap, request);
		modelMap.addAttribute("disabledStatus", "false");
		return "tileDefinition.create-"+getNameSpace();
	}

	@RequestMapping(value="/view",method=RequestMethod.GET)
	public String view(ModelMap modelMap, HttpServletRequest request){		
		setupCreate(modelMap, request);
		modelMap.addAttribute("disabledStatus", "true");
		return "tileDefinition.view-"+getNameSpace();
	}

	@RequestMapping(value="/search",method=RequestMethod.GET)
	public String search(ModelMap modelMap, HttpServletRequest request){		
		setupCreate(modelMap, request);		
		return "tileDefinition.create-"+getNameSpace();
	}


	public void internalCreate(T model, ModelMap modelMap) {

	}


	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String save(@Valid @ModelAttribute("model") T model,HttpServletRequest request,HttpServletResponse response, BindingResult bindingResult, ModelMap modelMap){
		_log.info("Reached to save in section controller");

		if( !bindingResult.hasErrors() && validateCreate(model, bindingResult) ){

			genericDAO.save(model);
		}else{
			return "tileDefinition.create-"+getNameSpace();
		}

		return getRedirectURL("/manage.htm");
	}

	@RequestMapping(value="/update",method=RequestMethod.POST)
	public String update(@Valid @ModelAttribute("model") T model,HttpServletRequest request,HttpServletResponse reaponse,BindingResult result, ModelMap modelMap){

		return getRedirectURL("/manage.htm")+"?pageNumber=1";
	}

	public String getRedirectURL(String uri) {
		return "redirect:/"+getNameSpace()+uri;
	}

	@RequestMapping(value="/edit",method=RequestMethod.GET)
	public String edit(ModelMap modelMap, HttpServletRequest request){		
		setupCreate(modelMap, request);
		modelMap.addAttribute("disabledStatus", "true");
		return "tileDefinition.edit-"+getNameSpace();
	}


	@RequestMapping(value="/assign",method=RequestMethod.GET)
	public String assign(@RequestParam("id") Long id, HttpServletRequest request){	
		
		return getRedirectURL("/managepending.htm");
	}

	public abstract boolean validateUpdate(T model,BindingResult result);

	public  boolean validateAuthorization(T model,BindingResult result){
		return true;
	}


	@RequestMapping(value="/get",method=RequestMethod.GET)
	public String show(@RequestParam("id") Long id,ModelMap modelMap,@ModelAttribute("model") T t){
		modelMap.addAttribute("model",genericDAO.getById(getEntityClass(), id));
		return "tileDefinition.create-"+getNameSpace();
	}

	@RequestMapping(value="/manage",method=RequestMethod.GET)
	public String manage(ModelMap modelMap, HttpServletRequest request){
		setupList(modelMap, request);
		List<T> modelList = null;
/*//		if(getLoggedInUserInstitution() == null){
*/			modelList = genericDAO.findAll(getEntityClass());
	/*	}*/
		/*else{
			modelList = genericDAO.findAllByInstitution(getEntityClass(), getLoggedInUserInstitution());
			
		}*/
		modelMap.addAttribute("modelList", modelList);
		
		return "tileDefinition.manage-"+getNameSpace();
	}


	/**********************************************
	 * Maker-Checker/4EP Functions
	 * @param modelMap
	 * @param request
	 * @return
	 ***********************************************/

	@RequestMapping(value="/managepending",method=RequestMethod.GET)
	public String managePending(ModelMap modelMap, HttpServletRequest request){

		return "tileDefinition.pending-"+getNameSpace();
	}


	@RequestMapping(value="/managespecificpending",method=RequestMethod.GET)
	public String manageSpecificPending(ModelMap modelMap, HttpServletRequest request){

		return "tileDefinition.specificpending-"+getNameSpace();
	}

	@RequestMapping(value="/fetch",method=RequestMethod.GET)
	public String fetch(@RequestParam("id") Long id, ModelMap modelMap, HttpServletRequest request){

		return "tileDefinition.fetch-"+getNameSpace();
	}
	
	@RequestMapping(value="/process",method=RequestMethod.GET)
	public String process(@RequestParam("authQueueId") Long authQueueId, @RequestParam("checkerRemarks") String checkerRemarks, @RequestParam("cmd") String cmd, ModelMap modelMap, HttpServletRequest request){

		return getRedirectURL("/managespecificpending.htm");
	}


	public abstract boolean validateCreate(T entity,BindingResult bindingResult);

	public abstract BaseService<T, Long> getService();

	public abstract String getNameSpace();

	protected String getPrimaryKey()
	{
		return "id";
	}

	protected Class<T> getEntityClass()
	{
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}


	protected T getEntityInstance()
	{
		try {
			return (T) Class.forName(getEntityClass().getName()).newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	@ModelAttribute("model")
	public T setupModel(HttpServletRequest request) {
		String primaryKey = request.getParameter(getPrimaryKey());
		Long id = 0L;
		if(primaryKey != null && !StringUtils.isEmpty(primaryKey)) id = Long.valueOf(primaryKey);
		if(primaryKey == null ||  StringUtils.isEmpty(primaryKey)) {
			return getEntityInstance();
		} else {
			return (T)genericDAO.getById(getEntityClass(), id);
		}
	}   

	public Validator getValidator() {
		return validator;
	}

	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

}
