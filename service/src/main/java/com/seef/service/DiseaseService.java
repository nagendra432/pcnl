package com.seef.service;


import java.util.List;
import java.util.Map;

import com.seef.models.ApplicationUser;
import com.seef.models.Disease;

public interface DiseaseService {
	
	public void saveDisease(Disease disease,ApplicationUser applicationUser);
	public Boolean validateUnique(Disease disease,Map<String, Object> map);
	public List<Disease> getAllDiseases();

}
