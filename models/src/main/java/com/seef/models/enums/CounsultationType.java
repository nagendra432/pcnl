package com.seef.models.enums;

import java.util.Arrays;
import java.util.List;

public enum CounsultationType {
	
	CONSULTATION("CONSULTATION"),FOLLOWUP("FOLLOWUP");

	private String type;
	
	private CounsultationType(String type) {
		this.type = type;
	}

	public static List<CounsultationType> list() 
	{
		return Arrays.asList(values());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
